{
    "id": "d12e62c5-4c4f-4af0-8349-1ba8b4bd9035",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fon_8bit",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "CF Quebec Stamp",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "64c127f3-1458-47c6-a9a4-92d959a75e31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 35,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "83189fec-b2e7-415e-b384-78de8bb23a2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 35,
                "offset": 6,
                "shift": 17,
                "w": 5,
                "x": 138,
                "y": 76
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "ba8e45f3-f4ce-4877-820e-d76c45e150a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 35,
                "offset": 4,
                "shift": 17,
                "w": 10,
                "x": 126,
                "y": 76
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "d7cd2a09-fad3-4d7e-9e4b-a718ad223462",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 35,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 107,
                "y": 76
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "8f7a2ce1-f5a0-4786-a4ae-f746cb297d29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 92,
                "y": 76
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "764b693b-a66f-4162-9cd4-b1d83c18e58a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 35,
                "offset": 0,
                "shift": 17,
                "w": 18,
                "x": 72,
                "y": 76
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "51c684bf-40d9-4f90-b78d-11d6c561200d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 35,
                "offset": 0,
                "shift": 17,
                "w": 18,
                "x": 52,
                "y": 76
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "5aaefd4b-0c88-488d-8ddf-d4f04cc4ada1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 35,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 42,
                "y": 76
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "5da95012-40dc-4d6a-9bd6-d2b80b52c8fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 35,
                "offset": 4,
                "shift": 17,
                "w": 10,
                "x": 30,
                "y": 76
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "a293737c-560a-4c56-95b1-4f760322a050",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 35,
                "offset": 4,
                "shift": 17,
                "w": 10,
                "x": 18,
                "y": 76
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "2bddd007-2e01-4f25-a5a4-49d9eea3e39e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 145,
                "y": 76
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "a2e8c47f-0edd-493d-ad7b-28d238a6b1ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 2,
                "y": 76
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "2f1da869-7dad-43a5-b53c-ab424493b9a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 35,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 477,
                "y": 39
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "1e989ed5-44ee-40a5-aeaa-63136c0fe04a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 35,
                "offset": 1,
                "shift": 11,
                "w": 11,
                "x": 464,
                "y": 39
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "d8ff82ec-ee94-4c7b-9210-939f5e2141a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 35,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 455,
                "y": 39
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "a9e3c569-bdd9-4d79-8414-59e1df9b356d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 440,
                "y": 39
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "15fd7d1c-15b9-4faa-9ae9-bd2b1ecdb6d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 35,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 421,
                "y": 39
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "c44e5365-8dad-4934-a870-235e79e48c7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 404,
                "y": 39
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "08726661-ea2f-4982-9f7d-2646ad4d4edb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 387,
                "y": 39
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "7591868c-00e4-4b7a-b616-0883e67738c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 370,
                "y": 39
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "43b27066-c4e7-4094-8f8e-2458630f43c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 35,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 351,
                "y": 39
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "85600186-f3b1-4fa7-b85e-489bc338b5fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 487,
                "y": 39
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "b460e9cc-45b9-47b6-8ec2-1ac33bcaddbf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 35,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 162,
                "y": 76
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "a9794e17-5770-400c-8380-e6dff94347de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 181,
                "y": 76
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "3e38a879-f6dc-46b7-9542-d5674f504bed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 198,
                "y": 76
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "05e9105c-a1da-45d6-9932-24af9232d5dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 108,
                "y": 113
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "160b6165-7394-458d-86b8-d18963202be6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 35,
                "offset": 6,
                "shift": 17,
                "w": 5,
                "x": 101,
                "y": 113
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "e8b90637-238c-4504-9cff-6bddd67afe99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 35,
                "offset": 5,
                "shift": 17,
                "w": 6,
                "x": 93,
                "y": 113
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "a60db1b7-99e4-478c-a740-32612aef386b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 77,
                "y": 113
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "b22ba5e2-a91f-483e-abb1-760b8af49e6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 61,
                "y": 113
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "3dd5983b-eb69-4d25-8a1b-84b91726dfce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 45,
                "y": 113
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "f6d7e2b8-1a74-49b8-85f0-023ab5663db9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 30,
                "y": 113
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "8c48960b-010a-4b32-b2b8-912930aa1dd8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 35,
                "offset": 1,
                "shift": 26,
                "w": 26,
                "x": 2,
                "y": 113
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "d51d3994-c7cc-494d-8b0a-faea7d7abcb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 35,
                "offset": -1,
                "shift": 20,
                "w": 22,
                "x": 467,
                "y": 76
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "4b7ca19f-b217-44b6-8010-604bb38919ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 35,
                "offset": 1,
                "shift": 18,
                "w": 18,
                "x": 447,
                "y": 76
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "e8568d87-c43e-4b26-bd3f-5ce1414b5e0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 35,
                "offset": 1,
                "shift": 19,
                "w": 19,
                "x": 426,
                "y": 76
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "cc10046b-5c5f-4424-93ce-2366be9679b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 35,
                "offset": 1,
                "shift": 21,
                "w": 22,
                "x": 402,
                "y": 76
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "b45ac2c3-8978-4fbb-a84b-c54bde1f6db8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 384,
                "y": 76
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "958bcdee-a3b3-4e80-9dde-ee71b4045d06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 35,
                "offset": 1,
                "shift": 16,
                "w": 16,
                "x": 366,
                "y": 76
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "49e33d86-d368-44eb-a887-d5324a47fdc1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 35,
                "offset": 0,
                "shift": 21,
                "w": 23,
                "x": 341,
                "y": 76
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "74e56751-b37c-4998-80c6-e480e19919da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 35,
                "offset": 1,
                "shift": 23,
                "w": 22,
                "x": 317,
                "y": 76
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "ad431e2c-4769-429e-982f-0f4db2b99e4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 35,
                "offset": 1,
                "shift": 11,
                "w": 11,
                "x": 304,
                "y": 76
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "1a15ce59-021d-4764-9a9a-678ed58c6408",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 35,
                "offset": -2,
                "shift": 12,
                "w": 14,
                "x": 288,
                "y": 76
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "4733593e-cf83-4fd0-adf1-eab4068ab743",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 35,
                "offset": 1,
                "shift": 22,
                "w": 22,
                "x": 264,
                "y": 76
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "f25c44ce-bc1a-4294-aeb7-5e8a00c5cc4e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 35,
                "offset": 1,
                "shift": 16,
                "w": 16,
                "x": 246,
                "y": 76
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "2374f02d-7a1f-4330-9b04-cf117b216668",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 35,
                "offset": 0,
                "shift": 28,
                "w": 28,
                "x": 216,
                "y": 76
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "88d6bda0-9397-4a65-98f6-8242921bbcdb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 35,
                "offset": 1,
                "shift": 22,
                "w": 22,
                "x": 327,
                "y": 39
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "2f178374-e523-4de7-9b2f-a438d7f68cff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 35,
                "offset": 1,
                "shift": 21,
                "w": 22,
                "x": 303,
                "y": 39
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "b8e52dc4-a53e-4699-b512-d7cf0e36cf64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 35,
                "offset": 0,
                "shift": 17,
                "w": 18,
                "x": 283,
                "y": 39
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "95d10a3a-0b55-46a3-ab38-7a41d2f40eb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 35,
                "offset": 1,
                "shift": 21,
                "w": 22,
                "x": 402,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "0b23ba7c-e8d6-48d6-a50d-0e0e6141cf33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 35,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 368,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "0b46b37f-d48e-4735-a70b-e78aa58e8281",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 35,
                "offset": 1,
                "shift": 15,
                "w": 15,
                "x": 351,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "fe7e6bf1-bd4c-4e92-a3d9-6bf4e9911d74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 35,
                "offset": 1,
                "shift": 19,
                "w": 19,
                "x": 330,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "8ef3e8f5-738f-41db-8f7d-8b2bdda1e927",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 35,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 307,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "d4bdbd4c-c31d-4fd4-b5fd-43c331f3c168",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 35,
                "offset": 1,
                "shift": 22,
                "w": 22,
                "x": 283,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "6b2c5139-a06e-4c30-ad53-53c053c0740d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 35,
                "offset": 1,
                "shift": 29,
                "w": 29,
                "x": 252,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "8ce7def1-cdec-46ec-b0e9-420ffa7be154",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 35,
                "offset": 1,
                "shift": 21,
                "w": 21,
                "x": 229,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "a219cf7b-675a-498f-aaf8-b2b7736fc8d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 35,
                "offset": 0,
                "shift": 20,
                "w": 21,
                "x": 206,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "f36b958b-0f1a-44c1-8335-2abe77b08f88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 35,
                "offset": 0,
                "shift": 19,
                "w": 20,
                "x": 184,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "ebf78ca2-d089-440c-91d1-ed0d9e90f95f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 35,
                "offset": 5,
                "shift": 17,
                "w": 9,
                "x": 391,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "d97850c9-bc64-4200-8672-2ac5d6baf033",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 169,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "aa9a93c0-81be-43a9-9550-704120d48ad2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 35,
                "offset": 4,
                "shift": 17,
                "w": 8,
                "x": 143,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "303496ce-6906-4ec2-8cee-4a9688963858",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 126,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "4a3945f9-c923-48f6-ae3d-af85d01e1456",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 35,
                "offset": -1,
                "shift": 17,
                "w": 19,
                "x": 105,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "9e325fb6-e9f9-406d-aa76-ebe8b2a9e8dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 35,
                "offset": 5,
                "shift": 17,
                "w": 7,
                "x": 96,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "b317221c-8e01-4be0-8f93-209df184aaa4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 80,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "2fe5aea0-4d10-45dd-8e07-19b77384dff6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 64,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "ce94bca0-90aa-4fd3-b033-d610955dff2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 48,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "abc56d8e-fac8-4b5d-8750-142a2fbd11f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 31,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "0354e248-243e-409e-ba91-bfb1e4c5633a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 14,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "0ada398f-9b76-4f47-a932-ff2acfc635e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 153,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "41a0c360-e1a5-46b1-8f71-e018f164d6be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 426,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "1db9e15e-7209-4b52-8811-4599690d5085",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 92,
                "y": 39
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "4795acc0-ff67-4a6e-a11b-bedfc3e06627",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 443,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "d8dffece-08cc-4a19-be30-3abf393c58ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 12,
                "x": 254,
                "y": 39
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "1e4e4e76-7b39-4dd1-865d-2c5e710434a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 35,
                "offset": 3,
                "shift": 17,
                "w": 14,
                "x": 238,
                "y": 39
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "41c5b0f5-a31b-47e4-852a-ad680d7788eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 222,
                "y": 39
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "fe876b19-9f40-408f-9bce-a9a3351a9d16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 204,
                "y": 39
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "3c3044b4-7177-4166-abea-691bd1fa692b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 188,
                "y": 39
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "39602507-212b-44f9-8fa1-f60dc3a5645e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 171,
                "y": 39
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "e29fc4ee-5e21-492d-a0d3-adeeb372d556",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 155,
                "y": 39
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "c15090d6-cb97-4a53-befe-5454e54c70fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 138,
                "y": 39
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "874efa9c-32b5-4b8b-8164-d941d44e5122",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 35,
                "offset": 3,
                "shift": 17,
                "w": 12,
                "x": 124,
                "y": 39
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "c8f19431-6acb-4a1f-a11c-2cb9ed570c9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 268,
                "y": 39
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "ba8e1fd6-34a3-437b-8d81-e8b0c4983464",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 108,
                "y": 39
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "51e8552b-1edf-4750-a786-edf9c7d2a630",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 76,
                "y": 39
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "bcf38286-9942-482b-9a75-f29d4de47a0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 58,
                "y": 39
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "65ab24c7-ab25-4f42-8832-8fd8f1c3ae82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 35,
                "offset": -1,
                "shift": 17,
                "w": 19,
                "x": 37,
                "y": 39
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "836b80f8-464b-42d9-b3ac-7eb92c46fe2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 20,
                "y": 39
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "fc0b0256-4fc4-4fb6-8ade-69ae60cb0795",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 35,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 2,
                "y": 39
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "18e43ec2-293a-40f1-a4d7-0f5106bd6de1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 492,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "5573e799-9062-458d-9541-1e57f347294a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 35,
                "offset": 3,
                "shift": 17,
                "w": 12,
                "x": 478,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "0afab303-c58d-4715-a460-664b654b222a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 35,
                "offset": 7,
                "shift": 17,
                "w": 3,
                "x": 473,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "f5b6d599-e3a2-4314-9812-975e7267298f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 35,
                "offset": 3,
                "shift": 17,
                "w": 12,
                "x": 459,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "7667c7a5-2f0f-400c-a5e2-8d67deef6085",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 35,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 126,
                "y": 113
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "22d92c28-9535-4e89-b7d8-ac8d4046bdb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 35,
                "offset": 5,
                "shift": 28,
                "w": 18,
                "x": 142,
                "y": 113
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "c6c51410-0016-42bd-a1de-9bf84d824c5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 39,
            "second": 44
        },
        {
            "id": "ebde7a25-55c1-464a-90e8-9645f3ced525",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 39,
            "second": 45
        },
        {
            "id": "54de0a2b-b001-4351-8b9f-b6b446249733",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 39,
            "second": 46
        },
        {
            "id": "9ac4d722-f8cf-48b8-9b7c-77bcb944168d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 48
        },
        {
            "id": "016a7d91-6133-4552-9ac0-e7a24ffb30b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 49
        },
        {
            "id": "5b6a5d73-8447-4de9-93c1-476c4feabe8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 50
        },
        {
            "id": "d46f1d21-1fcc-4225-b606-fe1a2f53b353",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 51
        },
        {
            "id": "6b797584-6d09-481e-8785-468a391a735a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 39,
            "second": 52
        },
        {
            "id": "ca766aa2-7167-4e78-90d7-2769f1c3b3c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 53
        },
        {
            "id": "e8fcc23e-7d1e-4364-8384-4cf62964f057",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 54
        },
        {
            "id": "994d4ffd-384e-4ae8-aa10-523ccf4c0804",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 56
        },
        {
            "id": "1842235b-0729-4bd0-96f1-e66ff23a844c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 57
        },
        {
            "id": "f1c23124-4b27-47a6-b43c-a8d8ed175773",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 64
        },
        {
            "id": "84b59ba6-be17-44a9-a1e4-fed9a32789f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 39,
            "second": 65
        },
        {
            "id": "66f3a8c9-d55d-490b-a837-81793768b7d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 67
        },
        {
            "id": "41269a67-9983-4eca-8344-1de4be19fc12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 71
        },
        {
            "id": "a6a6f679-db70-4b99-99c9-492dedd66a4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 79
        },
        {
            "id": "7771573b-8429-44ee-9d8d-5d1cf7cd34ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 81
        },
        {
            "id": "bd698522-b692-4b7b-8747-3d23a71a99a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 83
        },
        {
            "id": "e860572b-028a-443a-91e3-2c8fc70eeacd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 90
        },
        {
            "id": "629a5d52-a4a0-4a71-a97a-bfa47a3349b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 44,
            "second": 39
        },
        {
            "id": "fcb66d3b-c40f-4492-bfea-1c93e46c4411",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 44,
            "second": 46
        },
        {
            "id": "0aa25cbb-33b1-4750-be43-a77f2083486d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 44,
            "second": 48
        },
        {
            "id": "def67a10-30a3-4e73-8fbb-4c095f06018b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 49
        },
        {
            "id": "3c2e46b6-a86c-40b2-8a67-9f6341d08225",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 44,
            "second": 51
        },
        {
            "id": "4c0f8c4d-f024-4b9e-b79e-b56ecf4ec51a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 44,
            "second": 52
        },
        {
            "id": "84151765-206a-4561-9bbe-b44449c86044",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 44,
            "second": 53
        },
        {
            "id": "8e64dcc5-5bcb-4284-8677-6324f6da7778",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 44,
            "second": 54
        },
        {
            "id": "ccb4db58-95b1-4cf9-a021-4734d7ecc104",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 55
        },
        {
            "id": "c0409d4c-554e-4c2d-9e7c-0c6f57e6bf45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 44,
            "second": 56
        },
        {
            "id": "9a293850-ecef-4858-a2bd-54c6fc42a33b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 57
        },
        {
            "id": "60e7c230-63ed-4141-b92e-0f5fd2e95a9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 44,
            "second": 64
        },
        {
            "id": "990ada96-d697-4e06-9f6b-75bdff282d1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 44,
            "second": 65
        },
        {
            "id": "d2af308a-335e-4c19-9c6b-bccbd2318e4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 69
        },
        {
            "id": "e559d24d-bfe4-4f84-a0d0-bb5d5c0d3814",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 44,
            "second": 74
        },
        {
            "id": "5107f7e0-bb8b-4b9d-b416-5dbd229f45b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 44,
            "second": 83
        },
        {
            "id": "7a3ef4c9-a3b0-4ba8-9ffd-afd22654aad1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 84
        },
        {
            "id": "d33537a4-dc82-4e73-b925-3ea3eea06f82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 85
        },
        {
            "id": "687197be-0934-458c-b0b4-3280c6762eb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 44,
            "second": 86
        },
        {
            "id": "c85e6729-ad08-4ec9-8b04-a7fb6dfcb9c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 87
        },
        {
            "id": "da612145-d502-4724-9a59-0b94bba62913",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 89
        },
        {
            "id": "80ad2423-85c1-4937-9bd0-3ed53de59318",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 44,
            "second": 90
        },
        {
            "id": "4f28e28e-bae3-41ee-b2bf-133e2a477f7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 44,
            "second": 8217
        },
        {
            "id": "2a79d36f-7f79-4a47-a351-0a2e9e26087a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 45,
            "second": 39
        },
        {
            "id": "e53d78f2-3d42-4992-bb2a-8dac92bf436b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 45,
            "second": 44
        },
        {
            "id": "d5317814-e7d3-403d-b929-5361a0123eb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 45,
            "second": 45
        },
        {
            "id": "719ad964-c3af-40f1-ba85-81a2b48dbeee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 46
        },
        {
            "id": "53694d41-4b39-4843-a9a0-5cb843d85c0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 45,
            "second": 48
        },
        {
            "id": "eec7f527-05b6-4c44-b79f-033d969b42ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 45,
            "second": 49
        },
        {
            "id": "5b616b50-74a5-4a60-941c-1afbf1371999",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 45,
            "second": 50
        },
        {
            "id": "e98c07d5-512d-4938-b9e3-70bcee443315",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 51
        },
        {
            "id": "30a6b321-bd62-41b8-afce-f4bb5eb3b555",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 45,
            "second": 52
        },
        {
            "id": "394e7571-0c7f-4a7c-a298-fa1622b65504",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 45,
            "second": 54
        },
        {
            "id": "d5beacce-d272-44b3-8d78-f1babaafa91c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 45,
            "second": 55
        },
        {
            "id": "7b0d2c1b-2339-42eb-9210-900d3688b072",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 45,
            "second": 56
        },
        {
            "id": "2775bc71-0917-48e4-a048-36b516eea460",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 45,
            "second": 57
        },
        {
            "id": "ce6f592d-b92a-4caa-b21d-ab6812691b56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 45,
            "second": 64
        },
        {
            "id": "8bc46689-3099-4b21-ab0d-1bd1300f202c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 65
        },
        {
            "id": "302a51c2-c18b-485b-ae42-999b917fec22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 45,
            "second": 67
        },
        {
            "id": "5aac4079-2e74-40b4-bf13-1f0847432eee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 68
        },
        {
            "id": "7f35d292-5745-4dc0-9eef-2d217e5faa45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 69
        },
        {
            "id": "2d7d9859-3141-40a6-ba95-d103f0293e33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 70
        },
        {
            "id": "2d6a496c-7b43-4cf8-bda0-9bb803f6dac5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 45,
            "second": 71
        },
        {
            "id": "bc87af4e-e439-4900-8a4f-a1dc5bb1f8b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 74
        },
        {
            "id": "3d9aea39-96ee-437f-98c2-93af02aa75fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 45,
            "second": 79
        },
        {
            "id": "b8fcfeb3-7f6e-44f0-851b-3c2f8a1090fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 80
        },
        {
            "id": "4317dea6-29cd-4e75-ae23-2b2262383207",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 45,
            "second": 81
        },
        {
            "id": "e698dfda-5d06-4669-ae58-dbe896315604",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 45,
            "second": 84
        },
        {
            "id": "23106c58-3fac-47c9-913b-dad86e15862c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 45,
            "second": 86
        },
        {
            "id": "456fa5a2-ced4-48ab-9e31-91ac82ada540",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 87
        },
        {
            "id": "728b1d38-fe46-42ff-922f-2bbfab5cfae0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 45,
            "second": 88
        },
        {
            "id": "b17c3c1a-86b3-4226-9d6b-dc4f22fe89ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 45,
            "second": 89
        },
        {
            "id": "ac87fdef-1bbc-4d3f-814b-d8bf2327dc4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 90
        },
        {
            "id": "ebb07f30-e51b-480f-b2e7-24a7410a226b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 45,
            "second": 8217
        },
        {
            "id": "60a2f6b2-242c-4f62-8145-cc423cbeefd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 46,
            "second": 39
        },
        {
            "id": "67513bff-772a-4650-89cb-df2d45cd7337",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 46,
            "second": 46
        },
        {
            "id": "eadd39d1-9c77-4273-9a13-a8530b44dfbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 49
        },
        {
            "id": "739f2fd6-d8ab-4e23-8ec6-5a07e0515757",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 46,
            "second": 51
        },
        {
            "id": "3cb48a8d-705b-4d57-8041-2d01fdf490e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 55
        },
        {
            "id": "706f7b5f-e743-48e7-b33d-6a26dac04e5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 46,
            "second": 56
        },
        {
            "id": "a20a4662-5dc0-46b4-93c1-1c5d0aba3bd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 57
        },
        {
            "id": "ec58313f-cbdb-4c50-a665-362e81f7359d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 64
        },
        {
            "id": "84fc0bfd-607c-4441-83bf-bd46ae83a788",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 46,
            "second": 65
        },
        {
            "id": "29fb2479-5bc9-490a-a6cc-02ba2095c37c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 67
        },
        {
            "id": "89efcf74-61e7-4ba0-a5ce-07236395525a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 69
        },
        {
            "id": "c10572bb-d08b-4d95-901d-e9f0761d547d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 71
        },
        {
            "id": "306128dc-db17-4071-bfb2-83793c84c9e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 74
        },
        {
            "id": "1d994311-bacd-486a-a14f-c9026b475919",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 79
        },
        {
            "id": "844d28a2-d7c5-438e-b08f-eae14ef642dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 84
        },
        {
            "id": "c52818e5-0bb7-4288-933a-4256375aa2ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 85
        },
        {
            "id": "a965a364-d830-4627-9970-7c9608c80090",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 46,
            "second": 86
        },
        {
            "id": "f11994fd-e24f-4c0b-861d-36cc7e3f1b5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 87
        },
        {
            "id": "834b0798-267e-4f23-8316-08ab5e9ce84e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 89
        },
        {
            "id": "177fe311-ec9a-4bb4-b08a-d24d3f82edc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 46,
            "second": 90
        },
        {
            "id": "b8321d76-4737-4d4c-aa27-66504bbbcb4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 46,
            "second": 8217
        },
        {
            "id": "700dbd37-a01c-4048-b883-c0eb838ec617",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 48,
            "second": 44
        },
        {
            "id": "3cc4b989-7861-41a6-b451-95bfec2699ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 46
        },
        {
            "id": "a9316075-3bfd-48a3-aecd-eb501f9299a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 64
        },
        {
            "id": "749b2d92-4342-4f2b-a89e-dbd6eedee277",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 65
        },
        {
            "id": "0321f7bd-8634-4e31-8bc6-75266873fb84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 48,
            "second": 66
        },
        {
            "id": "4114a367-3358-408e-8a60-a79e29bb8380",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 48,
            "second": 68
        },
        {
            "id": "0d596968-9463-4fca-bb3e-9ae9dde09060",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 48,
            "second": 69
        },
        {
            "id": "446cdccf-c5d0-4315-9308-ae0270ed0506",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 48,
            "second": 70
        },
        {
            "id": "6e297236-be22-4867-a828-27f465947823",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 48,
            "second": 72
        },
        {
            "id": "1513fece-3e92-416f-9424-9c30e2aeb51f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 48,
            "second": 73
        },
        {
            "id": "8e6f2a99-5709-44c4-948a-7ce9d216026b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 48,
            "second": 74
        },
        {
            "id": "7b081417-bbc6-410e-a76a-5507938e37ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 48,
            "second": 75
        },
        {
            "id": "0fd6cd2f-482b-4c91-af6a-d5596a86f4de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 48,
            "second": 76
        },
        {
            "id": "bf849349-9a16-41bb-a42d-ceaec9c11d9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 48,
            "second": 77
        },
        {
            "id": "d0acda3b-2c85-4ea0-afd5-6aa095cf77b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 48,
            "second": 78
        },
        {
            "id": "4c0303cb-6586-4dbc-8cfb-244708ecf095",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 79
        },
        {
            "id": "e589e637-8b94-40d6-8ffc-3013b2d04cfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 48,
            "second": 80
        },
        {
            "id": "6797e979-5c75-4159-be24-c850a2359a65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 48,
            "second": 82
        },
        {
            "id": "3b4a6ce8-6f85-49c8-8d33-40684c6bbc48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 84
        },
        {
            "id": "bfd0a245-edc0-4348-a30a-e09e86840514",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 48,
            "second": 85
        },
        {
            "id": "afd36cd4-fcd9-49ab-93b6-dc8b92211ea1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 48,
            "second": 86
        },
        {
            "id": "cfc54fbf-de9c-4c32-8535-5e85955b15e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 48,
            "second": 87
        },
        {
            "id": "c5f9d7a1-6d18-4903-8f74-5d8f9e7a0712",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 48,
            "second": 88
        },
        {
            "id": "f51b64f2-3cbd-4b37-8512-e1953dec407a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 48,
            "second": 89
        },
        {
            "id": "f03ae9a4-26be-413a-97ed-f7631eb2b18e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 48,
            "second": 90
        },
        {
            "id": "033ec9ec-7ac3-4afe-99ba-143ca140b5fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 49,
            "second": 39
        },
        {
            "id": "06e72ed4-a85d-4387-a042-59c1c426cbc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 44
        },
        {
            "id": "274589f9-9bea-4c28-92e9-39d23df330f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 49,
            "second": 45
        },
        {
            "id": "1ec7e345-cbe4-417a-8742-f3ddbc5b7c35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 49,
            "second": 46
        },
        {
            "id": "1c964357-476f-4225-9697-f68f08f106d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 49,
            "second": 64
        },
        {
            "id": "0e002ea1-9f01-4285-9859-4e6893cb3cb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 49,
            "second": 66
        },
        {
            "id": "1210295c-749e-4b8d-ba81-2ffe9452f255",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 49,
            "second": 67
        },
        {
            "id": "233f5300-d2ec-455e-822c-d9a0b3b1a952",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 49,
            "second": 68
        },
        {
            "id": "789ebf88-da1b-4f89-b701-1fa25be9d90a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 49,
            "second": 69
        },
        {
            "id": "04165d42-5e6a-4640-a1d3-95e233afd83a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 49,
            "second": 70
        },
        {
            "id": "0aec3694-c8f9-4ecb-a8fb-3328c8a0aed1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 49,
            "second": 71
        },
        {
            "id": "3c16d350-280b-4d08-a2ff-ed7641574521",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 49,
            "second": 72
        },
        {
            "id": "580f06ae-abbb-4a6b-bf4f-feb08b30511d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 73
        },
        {
            "id": "674f34f0-94d2-4d51-9a4a-3e89edaf848e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 49,
            "second": 74
        },
        {
            "id": "55d7f9f9-317e-44b9-8a8c-3fca07525e97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 75
        },
        {
            "id": "38ffed02-0aba-484d-b921-067ca7bc2201",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 76
        },
        {
            "id": "615a06c7-1db2-4665-822c-d58e73f84995",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 77
        },
        {
            "id": "3fa15ee2-9cb4-4a0a-9d8f-603c3ec659d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 49,
            "second": 78
        },
        {
            "id": "d5522e4b-e4c5-4c1d-bd2b-1d4241ac58bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 49,
            "second": 79
        },
        {
            "id": "94d21c24-dedc-4a57-bf80-acf697c5122b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 80
        },
        {
            "id": "40906238-245a-4e58-b8a1-0192cfcfc770",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 49,
            "second": 81
        },
        {
            "id": "7d04491c-3a02-49cc-86e9-8fc003468001",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 82
        },
        {
            "id": "d415e25d-fb35-4c42-a04b-25ededad626a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 49,
            "second": 83
        },
        {
            "id": "9b55461d-ca8e-486e-9a57-f9df31bf8230",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 49,
            "second": 84
        },
        {
            "id": "7f4a4338-882f-4b98-9756-137a604e4997",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 49,
            "second": 85
        },
        {
            "id": "3f588309-9dff-4c64-b69c-2a158ff7496d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 49,
            "second": 86
        },
        {
            "id": "cc973f58-aa54-459c-93ad-6f6faa4ceccd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 49,
            "second": 87
        },
        {
            "id": "d1886b65-2739-4769-9440-de131e18c960",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 49,
            "second": 88
        },
        {
            "id": "dbb43b60-c666-4271-9755-28b72657666a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 49,
            "second": 89
        },
        {
            "id": "fec96c8a-a476-4574-b5a1-724f8ccced23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 90
        },
        {
            "id": "97194e00-dbb4-4e92-976a-753683c31c3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 49,
            "second": 8217
        },
        {
            "id": "0653e15b-dff7-4a3f-9108-b532764e03c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 39
        },
        {
            "id": "c17e83b3-d158-4d70-8909-9c6f743c3d2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 44
        },
        {
            "id": "62d8eaae-be80-42f8-8e4b-7bf224d92eb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 50,
            "second": 45
        },
        {
            "id": "eca2479c-4a78-46f3-833d-24eb57a39dcf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 46
        },
        {
            "id": "08a94464-3558-4bef-a669-59f0f3c702ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 50,
            "second": 64
        },
        {
            "id": "0b64c29b-2e40-4515-a198-aa00d92dd6e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 50,
            "second": 66
        },
        {
            "id": "48e5487a-11c6-4159-8943-2d8541c3c8c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 50,
            "second": 67
        },
        {
            "id": "3488f257-89cb-4a0d-8361-36740c9e7144",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 50,
            "second": 68
        },
        {
            "id": "4b09a120-a67d-4ba0-abd9-bd5012f89d31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 50,
            "second": 69
        },
        {
            "id": "343f31f7-cf6f-4f7f-88e1-158281eabcbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 50,
            "second": 70
        },
        {
            "id": "098033e8-c84b-4800-bc1f-dae56f1e820b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 50,
            "second": 71
        },
        {
            "id": "c026b1ab-3284-4180-8777-c236ab2feb2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 72
        },
        {
            "id": "c2fe708b-36ee-403e-859a-cc2dac69a3dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 73
        },
        {
            "id": "ff91636c-c897-4d54-aad7-f85d63874b0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 50,
            "second": 74
        },
        {
            "id": "dd67e60d-54ee-4ec0-805f-7066f06c6537",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 75
        },
        {
            "id": "3be1530c-9eba-430a-96c3-a44460b040fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 76
        },
        {
            "id": "d4638599-4116-42f2-915b-0d06a092526a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 77
        },
        {
            "id": "06051207-1916-4610-adb6-4f41e71345c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 78
        },
        {
            "id": "c889bff8-d4cb-4285-808c-9329482135ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 50,
            "second": 79
        },
        {
            "id": "95b84bd2-40b9-4a10-ac5f-a5264321bf6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 80
        },
        {
            "id": "fd07a94c-3b8f-4b51-b783-9c7a271e0d13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 50,
            "second": 81
        },
        {
            "id": "0d3f58c4-908a-481c-9091-95c2261729fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 82
        },
        {
            "id": "120011b3-b8e1-4994-95bd-37dbcbd52cf2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 83
        },
        {
            "id": "6ac6c60b-f6e8-498d-ab53-af9075768b08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 50,
            "second": 84
        },
        {
            "id": "4249a693-d2e6-4442-baf6-6f4f8b81a7a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 50,
            "second": 85
        },
        {
            "id": "717ae72b-3a0d-4d9e-bc7e-16c22003fc2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 50,
            "second": 86
        },
        {
            "id": "423a4673-f104-4f61-8ec8-225b5326c622",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 50,
            "second": 87
        },
        {
            "id": "5a644d5e-f038-473b-a94e-945d391ee694",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 50,
            "second": 88
        },
        {
            "id": "0b2efc68-b3d1-4c33-90dd-c691bc2a83d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 50,
            "second": 89
        },
        {
            "id": "73befed8-20cf-470a-9c1b-da4690cb9c9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 90
        },
        {
            "id": "ab691d7b-4016-4009-abf3-fc8b28e21a20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 8217
        },
        {
            "id": "41a6f003-8ce8-4cba-a2a4-00e88a7d6f0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 51,
            "second": 39
        },
        {
            "id": "7e04158b-f5b6-4b21-a9dc-ea3ccc20d086",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 51,
            "second": 44
        },
        {
            "id": "11f20e49-9c4a-4e0f-9be6-af93ec337f9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 46
        },
        {
            "id": "e5576c96-75f6-4477-bbae-4d000debbcd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 51,
            "second": 64
        },
        {
            "id": "0dd586b4-f584-405a-9a3d-17b7f88ce6ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 51,
            "second": 65
        },
        {
            "id": "cf737da8-a991-43ea-85af-6b61369cfbe7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 51,
            "second": 66
        },
        {
            "id": "8059bf95-53fe-4ff6-9c88-ce65f3a72d6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 67
        },
        {
            "id": "4de33619-d460-44cd-87dc-37347f485e3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 51,
            "second": 68
        },
        {
            "id": "9ceb64a5-c764-4d64-970d-680b927b98f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 51,
            "second": 69
        },
        {
            "id": "d731f447-3dff-41dc-b99b-64240f1bedfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 51,
            "second": 70
        },
        {
            "id": "35ad2bf5-37ec-4212-b520-dbfb136e8329",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 71
        },
        {
            "id": "162e25fb-401d-48d4-b634-187669467779",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 51,
            "second": 72
        },
        {
            "id": "06eb6113-7329-41f4-92ac-e3c7fca5fac1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 51,
            "second": 73
        },
        {
            "id": "d86c8c94-30c3-420a-aef7-7ee6712e871c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 51,
            "second": 74
        },
        {
            "id": "5da2f562-bb3f-4ddc-a3ca-25dbc1ad574e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 51,
            "second": 75
        },
        {
            "id": "8e640e35-25a3-4be6-b3a7-cc2eebba0dcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 51,
            "second": 76
        },
        {
            "id": "189e9c18-5a21-4bc1-9528-a55775b61621",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 51,
            "second": 77
        },
        {
            "id": "fd45c25d-4468-4f1c-a3ea-694b77022a39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 51,
            "second": 78
        },
        {
            "id": "0fdf11ce-8521-4326-9a7c-e9b139a48b0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 79
        },
        {
            "id": "b68b9109-dcca-4487-a2b4-25d1f94be4a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 51,
            "second": 80
        },
        {
            "id": "f732d577-b391-4d20-b29b-b02a8529daf1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 81
        },
        {
            "id": "145a9729-e80d-46ee-987e-95f19def0505",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 51,
            "second": 82
        },
        {
            "id": "bc309c67-979e-4339-8da3-66d05ce2f70d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 51,
            "second": 83
        },
        {
            "id": "95894683-45d4-4b09-bf6b-3c7bef5959ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 51,
            "second": 84
        },
        {
            "id": "c91f8ce2-7897-4f48-b61c-835089b562fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 51,
            "second": 85
        },
        {
            "id": "5448e173-a66a-475b-bef9-a137550bbfed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 51,
            "second": 86
        },
        {
            "id": "fcf4cecc-73be-444b-9c66-94d413516a9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 51,
            "second": 87
        },
        {
            "id": "7a51ded6-ebf4-4e4c-9399-7cd0097a9f23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 51,
            "second": 88
        },
        {
            "id": "cc9098fb-df59-4d27-8232-83d7621eed1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 51,
            "second": 89
        },
        {
            "id": "bd627305-0fbf-4abe-a180-ed6046363097",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 51,
            "second": 90
        },
        {
            "id": "94dfd240-f124-42fe-b3cf-624e33a83d6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 51,
            "second": 8217
        },
        {
            "id": "a7af9d77-f26d-4a34-8938-9950a4a5c191",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 52,
            "second": 39
        },
        {
            "id": "89c32663-9f16-4593-9be5-f553506be9d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 52,
            "second": 64
        },
        {
            "id": "43567dcf-8fb3-4b8f-8c0b-269eafc34832",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 65
        },
        {
            "id": "e584e07b-00cb-4585-9b48-2b8aa72f1427",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 67
        },
        {
            "id": "e9e7ca1e-e763-404b-92c8-79fbcd2680a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 69
        },
        {
            "id": "c04b332d-5e90-4034-807f-4d97f1592dac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 71
        },
        {
            "id": "c6337bf7-a6a9-4a07-9697-99d5d31fa762",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 52,
            "second": 74
        },
        {
            "id": "1563c146-f870-44b8-9071-2f246c18b848",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 79
        },
        {
            "id": "6cb33616-2547-4b5f-af1e-c44ec78dcd0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 81
        },
        {
            "id": "a6a75222-f118-45c9-8f7c-90e7b845df0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 52,
            "second": 84
        },
        {
            "id": "7a151426-c1ba-4a44-a4b1-0ccf3320b354",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 52,
            "second": 85
        },
        {
            "id": "d0e39292-455d-43ca-b120-a57e37d1d925",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 52,
            "second": 86
        },
        {
            "id": "fcbd1966-e62b-4667-b149-1687d80cdd67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 52,
            "second": 87
        },
        {
            "id": "09d60b19-34bd-4b02-8758-c43c089b0f61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 88
        },
        {
            "id": "ccf25c5f-aa6f-49f1-8be7-eab410c7b2ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 52,
            "second": 89
        },
        {
            "id": "a1f208b2-bf71-4d50-a54b-b06115db601a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 52,
            "second": 8217
        },
        {
            "id": "945f0d37-4ddc-4633-ad08-90f5b7e6f132",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 53,
            "second": 39
        },
        {
            "id": "b948ef8f-2440-4cde-94a5-1f92e9635830",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 53,
            "second": 44
        },
        {
            "id": "db139a16-0228-4f12-ab4b-e1c2a8792390",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 45
        },
        {
            "id": "d4ed4f2b-aa81-45a7-806c-a3d3b8e2c4d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 53,
            "second": 46
        },
        {
            "id": "d25a86fd-9a83-4ed8-88ed-05567802a3a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 53,
            "second": 64
        },
        {
            "id": "efd5e034-e5e1-49cc-b8f8-79597e0a1805",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 53,
            "second": 65
        },
        {
            "id": "15d73965-596c-469d-bea1-59833ca5709b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 53,
            "second": 66
        },
        {
            "id": "d99d79c2-36c5-4ae4-a061-ab963ffbdb2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 53,
            "second": 67
        },
        {
            "id": "de338885-52a7-4e0a-b699-231b36c2f44f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 53,
            "second": 68
        },
        {
            "id": "1809e727-8b78-4f45-a5c7-b543d996dfa7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 53,
            "second": 69
        },
        {
            "id": "e0ea73fe-cad8-40df-988b-d0d3e8a388fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 53,
            "second": 70
        },
        {
            "id": "84377617-5b0a-4cda-a026-ca0d90bdd5b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 71
        },
        {
            "id": "51ce3887-65b3-439f-84ca-2ffe4074e185",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 53,
            "second": 72
        },
        {
            "id": "db987ef7-01e6-4625-a71c-81f9cec5d181",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 53,
            "second": 73
        },
        {
            "id": "3a5673cf-91dd-430e-bd8f-f7d3915403dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 53,
            "second": 74
        },
        {
            "id": "a39c35c4-0d80-4fa6-b6d1-852f6d51024a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 53,
            "second": 75
        },
        {
            "id": "8c0e9468-0e3e-46c1-89ca-9dff2d8c84be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 53,
            "second": 76
        },
        {
            "id": "edfd6793-601c-451d-ad00-fe0995995cf6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 53,
            "second": 77
        },
        {
            "id": "bd7b3505-96e6-4ff0-9786-ca7e68c094b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 53,
            "second": 78
        },
        {
            "id": "51ef71ee-7df4-4ed1-a9fa-5f97ec339c2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 53,
            "second": 79
        },
        {
            "id": "acd3cf61-3333-488d-95af-ee0539ee442b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 53,
            "second": 80
        },
        {
            "id": "da07a620-f662-404c-8633-5d501635a585",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 53,
            "second": 81
        },
        {
            "id": "e06a13d3-edad-4515-a69a-6f99abad54a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 53,
            "second": 82
        },
        {
            "id": "c1a3c159-a54b-4a01-b1d3-9b9b07422d65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 53,
            "second": 83
        },
        {
            "id": "e527a3dd-6cec-45ec-954f-e393f081e161",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 53,
            "second": 84
        },
        {
            "id": "466386b6-2fef-43ca-9af5-de510638409a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 53,
            "second": 85
        },
        {
            "id": "7baf1086-40e0-44cd-85ab-1710cd67195f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 53,
            "second": 86
        },
        {
            "id": "2483264f-1c1a-4c9d-847a-26bcff2ac334",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 53,
            "second": 87
        },
        {
            "id": "dd9323b2-3b4a-4c9e-bfed-79fd0756838b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 53,
            "second": 88
        },
        {
            "id": "abc4f769-375f-4638-af2f-b9f5f1794086",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 53,
            "second": 89
        },
        {
            "id": "b285bafd-5558-4542-b802-cbf8102b8ef2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 53,
            "second": 90
        },
        {
            "id": "924dffa6-8956-46cc-b535-954c20754053",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 53,
            "second": 8217
        },
        {
            "id": "55b9027d-7e75-440a-8d6d-1bc02e40dc45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 54,
            "second": 39
        },
        {
            "id": "47543ba1-8998-428a-9003-d8762f361227",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 44
        },
        {
            "id": "9c9e03ae-3e8a-4fff-842e-a0a5098d70ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 46
        },
        {
            "id": "adae550f-8189-4550-b643-a1214c193f23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 54,
            "second": 64
        },
        {
            "id": "db0cc9f5-7bd6-4a4a-b81c-3739881d1601",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 65
        },
        {
            "id": "dd67c8a5-91de-4a60-8f11-e578ef1d629d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 54,
            "second": 66
        },
        {
            "id": "e200283f-0bd3-4672-a564-756425b3531f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 67
        },
        {
            "id": "cba0d742-b189-48b0-896f-a87a1d2b6fa3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 54,
            "second": 68
        },
        {
            "id": "43dd7ef6-66b1-4691-8220-9ac783b13c6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 54,
            "second": 69
        },
        {
            "id": "ecc48417-d9b5-4130-a630-da7c849ab4ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 54,
            "second": 70
        },
        {
            "id": "8f30ae1e-60dc-4716-bef1-e84e49900f2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 71
        },
        {
            "id": "b4a24ef4-b92b-4a42-84aa-f13db2112064",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 54,
            "second": 72
        },
        {
            "id": "1d421003-e719-4c10-8548-0a774850614b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 54,
            "second": 73
        },
        {
            "id": "28640377-df7f-4ea7-abe1-657d2e97bdbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 54,
            "second": 74
        },
        {
            "id": "d2b71a1f-5b41-42c1-bfea-9864535d670a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 54,
            "second": 75
        },
        {
            "id": "148be70a-b92a-42fc-b3ac-f99dd9f2113b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 54,
            "second": 76
        },
        {
            "id": "492fd4f3-78f6-41db-9d6b-b3737f566868",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 54,
            "second": 77
        },
        {
            "id": "d2458c65-966b-443e-a029-d443340092cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 54,
            "second": 78
        },
        {
            "id": "4d884742-d148-4d43-9bdf-2f096d867f62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 79
        },
        {
            "id": "685aa1db-a6da-4e73-824c-14bdac35d15e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 54,
            "second": 80
        },
        {
            "id": "02f97033-5f01-443d-a39a-6f4275e828a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 81
        },
        {
            "id": "c503a3c9-697f-474b-a9a0-fa7560197aac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 54,
            "second": 82
        },
        {
            "id": "7e0f713c-9836-4a04-b4c9-d518231a9d54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 54,
            "second": 83
        },
        {
            "id": "c54d45e1-f106-4739-9a94-4e0477ab81c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 54,
            "second": 84
        },
        {
            "id": "4e7b59d7-ee0b-443b-bb92-2fb6d7d1b94a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 54,
            "second": 85
        },
        {
            "id": "2606c01c-48ed-4c25-98e4-b5f6341a2a76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 54,
            "second": 86
        },
        {
            "id": "0b89608b-2895-47f0-b07f-cabceb8acc12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 54,
            "second": 87
        },
        {
            "id": "cf775b15-7214-448a-9a91-0b23c8b527a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 54,
            "second": 88
        },
        {
            "id": "2fa11d44-303a-4ee7-ba50-c1bb96d6b464",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 54,
            "second": 89
        },
        {
            "id": "431c6846-f426-4545-bfd8-2b9c0cb56fbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 54,
            "second": 90
        },
        {
            "id": "97070176-5f5c-4369-8a8c-e8bb43e7e564",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 54,
            "second": 8217
        },
        {
            "id": "bd453311-70e4-4fcd-9b83-89452ec6492a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 39
        },
        {
            "id": "114e1fb8-8840-4236-9460-6b3fbef701f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 55,
            "second": 44
        },
        {
            "id": "cda7e8b9-f19f-45a0-9776-d9c8f1f2897d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 55,
            "second": 45
        },
        {
            "id": "db89c13e-210d-4225-a7c7-e1513795fbfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 55,
            "second": 46
        },
        {
            "id": "5c900256-7a22-4ff4-8d43-500be79f8862",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 55,
            "second": 64
        },
        {
            "id": "31612be2-b185-4382-afaa-0bddee9a4c4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 55,
            "second": 65
        },
        {
            "id": "6da54074-36f4-4006-9cac-586788053a84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 66
        },
        {
            "id": "d8370b73-43cf-41b9-a5fc-0aedf423efa4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 55,
            "second": 67
        },
        {
            "id": "2eec8d04-a5b2-41a7-99ef-816f43095d1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 68
        },
        {
            "id": "56a201f8-461b-4766-9110-90b26a5a3b9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 69
        },
        {
            "id": "cbf3d2c1-c970-4d1d-88ab-17bc69598178",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 70
        },
        {
            "id": "11cbb0d8-a0da-4a1b-b1b9-96485045887f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 55,
            "second": 71
        },
        {
            "id": "a7816347-00d5-48c6-b418-57301dfd641f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 72
        },
        {
            "id": "4140def8-bd3b-4a6c-a78e-a3ab3f0257d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 73
        },
        {
            "id": "c02a6706-8697-410b-baed-74f8256597d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 74
        },
        {
            "id": "72e030c5-2279-4531-9ab1-8f2af872d914",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 75
        },
        {
            "id": "8d933144-941c-4cbf-b65c-3ea6f6f44c7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 76
        },
        {
            "id": "8b2290bc-3eb5-4b6f-89b5-6dbe48d4a5c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 77
        },
        {
            "id": "9397f2e8-225e-42b4-92dd-35ce56b3e341",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 78
        },
        {
            "id": "539fe4a2-7e60-481a-a236-68bc8bdcffd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 55,
            "second": 79
        },
        {
            "id": "ac9660bd-7e70-4a57-a5c3-065b6dfe5905",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 80
        },
        {
            "id": "fad5da6b-fc1b-431d-87c3-92b32594b0da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 55,
            "second": 81
        },
        {
            "id": "6e93f798-7938-4c2c-adfc-056e43588265",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 82
        },
        {
            "id": "1d17600c-cf94-4c65-9010-cad4427ef45e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 83
        },
        {
            "id": "4b511d17-bfe0-4434-a283-2bfe7bbcb684",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 84
        },
        {
            "id": "84f0afda-d3c8-4f7f-af3c-14f51f5ea168",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 85
        },
        {
            "id": "90413cac-2de9-4d5a-8e14-78a06f943410",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 86
        },
        {
            "id": "60fd6fe2-72e1-4cea-ab92-92f940ef8f41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 87
        },
        {
            "id": "12e547c2-4501-4617-9bff-91690c4a7919",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 88
        },
        {
            "id": "43f37709-637d-4c70-9885-524b1aa7ffd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 89
        },
        {
            "id": "ff43be4c-be11-46f4-8a5d-cfb730d6ef99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 55,
            "second": 90
        },
        {
            "id": "3d6bae9c-bedc-420d-9424-50295bc28e5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 8217
        },
        {
            "id": "0f7857e5-28af-43cd-804a-4cca227ea478",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 56,
            "second": 39
        },
        {
            "id": "d7733449-d7a5-4198-be91-fb55dd1be0e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 44
        },
        {
            "id": "f8a7e6d9-75f8-4e65-8e12-eba1a41f3caa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 46
        },
        {
            "id": "a7c8f313-7797-4545-9ec5-e85c7eb382cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 56,
            "second": 64
        },
        {
            "id": "966c5cc2-b06d-4397-b5de-9261f1898337",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 65
        },
        {
            "id": "386b5a0b-f272-4deb-98a6-1d96a68bbc1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 56,
            "second": 66
        },
        {
            "id": "f67285dc-8718-4bbc-b979-df17084bf22f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 67
        },
        {
            "id": "0133884b-1511-4497-aeb7-ca99a2e66af2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 56,
            "second": 68
        },
        {
            "id": "4e2f6754-049f-4e1d-b6b7-c6ef169c487f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 56,
            "second": 69
        },
        {
            "id": "2ade1b06-bd37-48a7-87b9-a78798748413",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 56,
            "second": 70
        },
        {
            "id": "a4fc680b-07a0-4cdc-9b11-58a310e7bcf1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 71
        },
        {
            "id": "5a1c7c1b-ebf2-4137-ac20-c6d9b1a35385",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 56,
            "second": 72
        },
        {
            "id": "feb77223-d65d-4264-915a-60917d5c2a68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 56,
            "second": 73
        },
        {
            "id": "ee21a008-ba2c-4230-92dd-5888d7971af2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 56,
            "second": 74
        },
        {
            "id": "54c99ffd-8d4c-43ae-8201-325e219a1c37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 56,
            "second": 75
        },
        {
            "id": "165dfd17-194d-4f8a-81c4-8ce86197d8c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 56,
            "second": 76
        },
        {
            "id": "d6ae8cad-002c-4a79-b5fc-3bef6880e9d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 56,
            "second": 77
        },
        {
            "id": "7c1c1e78-0281-4348-94fd-f110c431cfd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 56,
            "second": 78
        },
        {
            "id": "d5b7d777-0ff9-4d13-8432-57c7dc2a40c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 79
        },
        {
            "id": "35cf7657-d0e3-4d5a-a819-52f91b8d769c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 56,
            "second": 80
        },
        {
            "id": "5e8ab0ac-8c1c-4d62-aa0a-a5d199c2912d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 81
        },
        {
            "id": "02bfb0f9-800a-46ad-9308-091d7567c032",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 56,
            "second": 82
        },
        {
            "id": "669bdcb1-db10-44e2-9209-5dd816a9c907",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 83
        },
        {
            "id": "18c834b9-8889-4c7b-ba10-45df1c251245",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 56,
            "second": 84
        },
        {
            "id": "7a1769b2-da07-4a99-8b71-0e3c5ccb6026",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 56,
            "second": 85
        },
        {
            "id": "c8c5a4e3-4c83-4ff7-8149-4a8bf9e6297a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 56,
            "second": 86
        },
        {
            "id": "aa1f9c33-667c-45ab-b0d8-2539a8a96bde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 56,
            "second": 87
        },
        {
            "id": "100ce67c-dc6b-4007-881d-4a87db7dfdd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 56,
            "second": 88
        },
        {
            "id": "4a4b46bb-38e7-4a3b-8d46-6d5896f50dde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 56,
            "second": 89
        },
        {
            "id": "ab15e960-812a-4c69-a300-9f7ad1c8b5c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 90
        },
        {
            "id": "bde09c4d-493d-4e0a-9bdf-1158b33bcad1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 56,
            "second": 8217
        },
        {
            "id": "489f2c51-6700-48e3-8377-357243d4fbf1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 57,
            "second": 44
        },
        {
            "id": "62948d1f-5b51-40da-8871-349ce6e380fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 45
        },
        {
            "id": "8b08c7f3-25d4-4ff0-80b5-4909db9f6c1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 57,
            "second": 46
        },
        {
            "id": "4f6b293b-d4c2-42ab-8c58-c83f40753044",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 64
        },
        {
            "id": "efcc0e04-798a-4961-9fb0-93c2996495e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 57,
            "second": 65
        },
        {
            "id": "a75a6b7d-88e3-4368-97b7-bbca3a2395bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 57,
            "second": 66
        },
        {
            "id": "1985e145-69b7-45e3-9e9f-5250cfd4c5ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 67
        },
        {
            "id": "d2860996-0bcc-4415-a5df-0a97934f4124",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 57,
            "second": 68
        },
        {
            "id": "9ffe4ed4-8785-4e4c-8c7d-d8b9562457f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 57,
            "second": 69
        },
        {
            "id": "0f325f81-1ab3-44a5-bd42-6abb87e62dc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 57,
            "second": 70
        },
        {
            "id": "66fa482e-266e-4e11-a763-2cdd297f65fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 57,
            "second": 72
        },
        {
            "id": "f1340198-acf7-44bd-b4cd-233d730ba899",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 57,
            "second": 73
        },
        {
            "id": "6d5782c7-b528-4e63-b565-7667e71b4ea8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 57,
            "second": 74
        },
        {
            "id": "ed0dae11-f441-4ce7-babb-bd407db065e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 57,
            "second": 75
        },
        {
            "id": "18b893ad-294b-4957-b015-cab916225cc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 57,
            "second": 76
        },
        {
            "id": "61064dca-1c71-4c45-8d5b-fbaad38d0fb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 57,
            "second": 77
        },
        {
            "id": "10a69ffc-3914-41ed-9674-355bb9f57f55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 57,
            "second": 78
        },
        {
            "id": "f9b5288a-66c4-4857-a9f2-1ef4a217f21c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 79
        },
        {
            "id": "c567f827-0e15-4de9-81da-001b32b5b1ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 57,
            "second": 80
        },
        {
            "id": "20728dcf-3cc4-44ad-9432-3fac209b73cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 81
        },
        {
            "id": "cc183414-4a10-49c3-beb1-e2c95ac1082f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 57,
            "second": 82
        },
        {
            "id": "398ae7eb-0126-4348-8082-0edb5695f55f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 83
        },
        {
            "id": "0a00504b-cb37-4084-a6a1-59eeaca70aa4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 57,
            "second": 84
        },
        {
            "id": "b3f59b58-896a-490a-983d-c60a6da3faf4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 57,
            "second": 85
        },
        {
            "id": "e0ca2c11-d3ee-48bf-bd0d-a74a1ffc5b8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 57,
            "second": 86
        },
        {
            "id": "3333c5ab-0715-40ff-8bff-ca81087442a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 57,
            "second": 87
        },
        {
            "id": "ce382657-728e-4767-b931-6040380dfd7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 57,
            "second": 88
        },
        {
            "id": "0e7b1dc9-e01e-43e5-b140-5fa60e76e4a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 57,
            "second": 89
        },
        {
            "id": "e1aa7ccb-6678-40f1-8516-b4b5a11201cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 57,
            "second": 90
        },
        {
            "id": "55f75161-71d8-429a-aa42-8d1442a5f8d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 64,
            "second": 39
        },
        {
            "id": "3a54f5b1-aba5-4842-8a5d-483a0550ef5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 64,
            "second": 44
        },
        {
            "id": "8ea852a5-c80b-48e3-8842-636f018113ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 64,
            "second": 45
        },
        {
            "id": "6c26cd87-fd98-428d-9b63-d3b7bcfa27fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 64,
            "second": 46
        },
        {
            "id": "bd63a861-46fe-4ef9-ac82-cfa024f98bb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 64,
            "second": 48
        },
        {
            "id": "ddad2fbc-bf6a-46e5-b7f7-27e83ced9c90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 49
        },
        {
            "id": "133e2ffa-d0b2-4d83-bf38-a383a8c4fbc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 64,
            "second": 51
        },
        {
            "id": "8a81c3bc-35d8-4d3b-9849-e7b5b6fd278c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 64,
            "second": 52
        },
        {
            "id": "99276c01-019f-4d1a-b768-5fc4edec1d75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 64,
            "second": 54
        },
        {
            "id": "decc2ae1-d773-4d6e-bf4c-f9da5740ab66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 64,
            "second": 56
        },
        {
            "id": "3ae9e196-f57a-4412-afbc-5e71cc6d0531",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 64,
            "second": 57
        },
        {
            "id": "3863d3f0-fa3b-465e-a0ef-d47b6bd3da59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 65
        },
        {
            "id": "6f15a3eb-6dbc-4558-9d72-bb981278f692",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 64,
            "second": 67
        },
        {
            "id": "7ed9ac2b-aa80-44bc-82f2-080ffba361d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 68
        },
        {
            "id": "aea9d838-d29b-4743-8e3f-5a944c92d698",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 70
        },
        {
            "id": "8427ffd8-5c14-4eae-9ee1-3323c54a884f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 64,
            "second": 71
        },
        {
            "id": "6f3f5ae9-8712-4efa-8294-084e725c93da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 74
        },
        {
            "id": "62f7177e-bd5a-4e7e-9d54-53eb67c115a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 64,
            "second": 79
        },
        {
            "id": "ab66c122-adac-4f08-abe1-cfbaa41e3732",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 64,
            "second": 81
        },
        {
            "id": "5fc1808a-fe35-4336-9033-03d50cef0f74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 64,
            "second": 83
        },
        {
            "id": "b018faff-47f7-49ce-b780-9ebae5d067ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 64,
            "second": 84
        },
        {
            "id": "e7486fb4-9d06-462e-914e-8f19d3d24f85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 64,
            "second": 86
        },
        {
            "id": "55b9f2e8-d564-41f0-a807-739ef2ed97d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 87
        },
        {
            "id": "969be526-80d9-4eaf-9bf3-c2f7913ffcd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 64,
            "second": 88
        },
        {
            "id": "86cb9b7e-82b6-479f-bff6-581a1d5d99f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 64,
            "second": 89
        },
        {
            "id": "7ede8a09-46ee-4767-bc6d-74298fb22523",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 64,
            "second": 90
        },
        {
            "id": "71ad759d-411b-405a-afaf-724672876f4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 64,
            "second": 8217
        },
        {
            "id": "a5ef9d36-1be3-4992-aaa9-f6a15796243e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 39
        },
        {
            "id": "f2c72af0-9582-48ac-9595-662f4672ae43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 44
        },
        {
            "id": "34a9a95a-b265-4af7-bbff-7108efd56101",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 45
        },
        {
            "id": "84ddd480-53c7-4d30-8fbf-3160307fbc62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 48
        },
        {
            "id": "ec621c90-4c78-420f-a39f-db81f6a76adc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 49
        },
        {
            "id": "2640c268-4dc6-499a-968c-a7108a76edb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 51
        },
        {
            "id": "a880354b-035e-4340-a7df-d1b6e2107369",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 52
        },
        {
            "id": "2ba9aeef-cc29-41cd-8647-e454a67df837",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 53
        },
        {
            "id": "4e042d7c-9cd5-459b-9956-4991bb0858b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 54
        },
        {
            "id": "347703d1-8c8e-4345-b27d-851902617f50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 55
        },
        {
            "id": "6925a303-a727-4425-8f15-609dbcd7a99f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 57
        },
        {
            "id": "76420c55-04e2-468c-8547-b0fda5ebd84d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 64
        },
        {
            "id": "569527c7-2d04-46dd-87e9-e00bc155e201",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 65
        },
        {
            "id": "9b95b2f6-4011-4641-9fb2-7400126332a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 67
        },
        {
            "id": "71c14061-cbe0-4735-a766-b5a9b103653d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 71
        },
        {
            "id": "324ffd50-a461-4f1f-beea-8201ddd5cd80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 74
        },
        {
            "id": "2d7e1df2-e91f-447a-9571-1265208ebff3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 79
        },
        {
            "id": "df45144a-1e33-4560-be81-b708294693f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 81
        },
        {
            "id": "bff9cc81-a549-494a-8da7-0f839f5a0c5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 84
        },
        {
            "id": "26f41573-9d27-460f-976c-40ee1823f85f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 85
        },
        {
            "id": "c122d5b6-9a00-42e5-841d-8b0ef3802520",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 65,
            "second": 86
        },
        {
            "id": "7b517b9d-3a8c-47ec-8a74-4863852cb174",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 87
        },
        {
            "id": "8e857b2e-d709-4eaa-87f7-d26cc320e8ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 89
        },
        {
            "id": "ed63b9b9-9a04-4fdb-b844-2152006f75c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 8217
        },
        {
            "id": "f27fc529-00e0-43a2-9804-14019636acf5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 66,
            "second": 45
        },
        {
            "id": "ad95d3c3-4d84-4be2-b94b-c1dab05d33c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 46
        },
        {
            "id": "d3864d04-2122-4127-99f8-ff21d4e1a2df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 48
        },
        {
            "id": "7431c501-763b-4b6d-9d7e-6e2e441959d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 51
        },
        {
            "id": "e337951a-e4c2-4e4d-9991-daa4dd9b7dda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 52
        },
        {
            "id": "3be38f32-c2b2-4c85-a4fc-cdee7a16c3dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 53
        },
        {
            "id": "7273e568-5ea9-446a-97e0-24001241f34f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 54
        },
        {
            "id": "6b519343-7a50-4ccf-90db-7d13c3b58d2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 66,
            "second": 56
        },
        {
            "id": "b1dc9c90-597d-46a9-b011-36788db9b096",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 67
        },
        {
            "id": "f133f0fc-b4a7-49f8-ac7e-a5ffe8bc8363",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 71
        },
        {
            "id": "e41af4aa-64b0-4999-bd5e-de558a168bb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 74
        },
        {
            "id": "ad645b58-d9a3-4beb-bae4-2f71d5e0f400",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 79
        },
        {
            "id": "616e3351-fe11-454c-8f1f-c94fb843c226",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 81
        },
        {
            "id": "70b9abbb-44cf-4085-8a1c-624494b0eeec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 67,
            "second": 45
        },
        {
            "id": "9b4f7c85-4e2f-4b6d-9e37-9063bba98f89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 49
        },
        {
            "id": "7520be49-a565-457d-b1c5-0d3948843aef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 51
        },
        {
            "id": "7293e4d3-4fbb-421b-881e-73c5df8d21ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 67,
            "second": 52
        },
        {
            "id": "dd9131f1-8cc6-4553-93f4-8723ad5670d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 53
        },
        {
            "id": "b1e44904-1a54-4982-8780-770ee8454aee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 67,
            "second": 64
        },
        {
            "id": "d42eba1a-9201-4c7d-a645-cccfeabe830b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 67
        },
        {
            "id": "b3ee9d3a-57de-4954-a960-d41038832d22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 71
        },
        {
            "id": "05294567-ebd1-41f4-a191-51b149bfd513",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 79
        },
        {
            "id": "3bcd1f69-f38f-493b-b8a0-c622a673f610",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 39
        },
        {
            "id": "a6e6fb61-0d88-4588-a1ff-5c17c3c7063e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 45
        },
        {
            "id": "df91d1e7-73d3-4fb8-a0fc-20fd6af5b293",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 48
        },
        {
            "id": "9117f396-2d54-4da4-8b2e-9a0b9ba31bc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 50
        },
        {
            "id": "8a3a82bf-df3a-43fb-9450-82320d521e20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 52
        },
        {
            "id": "53d9f2ed-2aef-46fb-99ae-1084107ab9b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 54
        },
        {
            "id": "0ca4eaba-cdb7-4a60-b6b1-430481ff2067",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 55
        },
        {
            "id": "9f8b979f-bc26-431c-831b-16aa1e1eb135",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 56
        },
        {
            "id": "a5bee331-5bed-4edc-b344-5c100ca0f9da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 57
        },
        {
            "id": "cf33ed71-2d33-495b-b756-5230950613e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 64
        },
        {
            "id": "e31aad6f-b4c1-458a-aba1-f31f05896455",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 67
        },
        {
            "id": "c56ff372-22f1-466e-ab1b-5df714a8c197",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 71
        },
        {
            "id": "849cc640-07fb-4014-abfe-34ec875c5088",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 79
        },
        {
            "id": "f6908f39-1c20-4923-bfaa-8cd3600ff57c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 81
        },
        {
            "id": "dfcf3fc7-4468-4abc-8ee3-281be7a41de3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 83
        },
        {
            "id": "74890c2c-a406-460d-97d3-4ef6e3d901ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 84
        },
        {
            "id": "835570b3-5336-4727-9453-b8a78d1a3374",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 86
        },
        {
            "id": "6bdd9648-cced-45cc-af31-ca083c75409d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 89
        },
        {
            "id": "55682a8e-a4ac-4590-bda4-b9e3dbea004f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 8217
        },
        {
            "id": "0b4dc5e7-59cb-4ae1-92d9-acaab4b66cf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 69,
            "second": 44
        },
        {
            "id": "d052e0df-7e88-4b46-9738-bab67d0b1d00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 45
        },
        {
            "id": "fefde3db-7825-4cf1-b4dc-c7028c7d2ccc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 69,
            "second": 46
        },
        {
            "id": "2f49dc4a-70f8-432d-865d-94e162fe7596",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 49
        },
        {
            "id": "b9526557-5494-4adc-be57-eff6a35424f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 57
        },
        {
            "id": "ac5b73f6-7e3f-459f-be4d-304f3db81099",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 69,
            "second": 64
        },
        {
            "id": "7a81a96d-677c-4baa-8dd1-c94de88317d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 69,
            "second": 65
        },
        {
            "id": "9834ff66-2e67-4b21-bcfe-41f501d08c39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 67
        },
        {
            "id": "da4c8547-6d70-448d-8cc1-ccc739f9045e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 74
        },
        {
            "id": "bc73d2d2-3221-4acf-b048-5d3a74b61832",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 69,
            "second": 90
        },
        {
            "id": "d1ca17c1-703c-44c4-9519-306e4a786a92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 44
        },
        {
            "id": "f5f9f6c1-2014-480c-a5d2-76f7889d19ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 45
        },
        {
            "id": "f091da88-3b6c-42a6-b6fb-db0b17230050",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 70,
            "second": 46
        },
        {
            "id": "9f81eff4-97cc-4ce6-83fd-877f4e78a0c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 49
        },
        {
            "id": "f4ab97fc-608e-4a65-a0d1-3a026a9c49e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 50
        },
        {
            "id": "6045f501-f9fa-452d-a940-0d0043075898",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 51
        },
        {
            "id": "2d870e2d-9e62-462c-8071-8ee9cebf9610",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 52
        },
        {
            "id": "ba15d8a4-ab28-43a3-900e-9b166a0fd893",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 53
        },
        {
            "id": "4805da4d-9a87-46f5-9403-2076845c44ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 54
        },
        {
            "id": "91870397-450f-4db2-9ea3-64d667746388",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 64
        },
        {
            "id": "a15c03a0-80e2-4afb-8981-05b77e56e237",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 65
        },
        {
            "id": "b88cc7b5-a4a1-4d63-b832-bcc7b49c6bff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 67
        },
        {
            "id": "a787a072-a2f8-4417-aad7-b79f5392e425",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 71
        },
        {
            "id": "4b537129-91b7-424d-8bf7-a457900fb578",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 79
        },
        {
            "id": "e5dcd975-8ac8-424f-bec0-1fae79a571e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 81
        },
        {
            "id": "5a1f6fbb-ed85-4cba-8bca-0adfcac7f51c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 82
        },
        {
            "id": "ed821b85-7481-48f0-b243-6cd697178246",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 44
        },
        {
            "id": "d1c3d657-7933-40c3-b3e5-9e24a5a0b69e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 45
        },
        {
            "id": "8d8cc63f-63e2-491c-afd1-1daab7382a5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 46
        },
        {
            "id": "d9ef5bcb-815f-4b71-80d2-82b93579483e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 48
        },
        {
            "id": "d5fa117d-32ad-4bbf-9ead-9322a820ef05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 49
        },
        {
            "id": "229cc66b-7944-44b9-af2d-207456828595",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 71,
            "second": 52
        },
        {
            "id": "0cb48d5a-a58e-4fe8-908a-8873b2ce668b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 71,
            "second": 54
        },
        {
            "id": "30fd4170-51e2-47f1-ae89-614eed8de9da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 56
        },
        {
            "id": "4edb57f4-911d-4b8b-888b-3a920c6ce453",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 65
        },
        {
            "id": "3eebe44d-ddf4-4c89-b06e-04441ac208d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 67
        },
        {
            "id": "2dd4559b-5098-4041-9ff2-4f4c02cbd63b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 71
        },
        {
            "id": "f7b44a2d-802c-4b88-ad74-127e0eaf46b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 74
        },
        {
            "id": "5878afb5-d60a-4f8e-9b36-15d30b71c28f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 79
        },
        {
            "id": "f2ffaa3e-8267-445b-a815-611db39a84fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 81
        },
        {
            "id": "0cf502f7-12a7-4f06-bc35-46cb231ca961",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 45
        },
        {
            "id": "ec198a5a-8e76-4ec0-b05c-151ea9d92980",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 48
        },
        {
            "id": "944c8814-fc98-4579-bc34-022068c075c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 49
        },
        {
            "id": "19ddf9c2-b799-4028-8655-e99b51cc9f15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 51
        },
        {
            "id": "5a8c4736-0395-4a39-b0d3-9adaceea60e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 52
        },
        {
            "id": "f61f61de-4c36-4feb-87dc-494ff5ba748c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 53
        },
        {
            "id": "f03f9940-6772-4b81-8bc6-df801d4e0d26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 54
        },
        {
            "id": "644fc55e-6d1c-41cb-8160-b207d044b995",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 57
        },
        {
            "id": "ddae8eb0-3ee8-4057-b872-fb76d89db351",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 64
        },
        {
            "id": "10dd0f68-9033-4be6-9b42-dfec46730263",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 72,
            "second": 65
        },
        {
            "id": "90cb4dc5-66c3-4ff5-828f-a89254309d1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 67
        },
        {
            "id": "78679c15-9454-45ef-9e18-0f7fe70ab091",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 71
        },
        {
            "id": "b187f92e-9d19-4a8f-9145-33f7fe1b8ca2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 79
        },
        {
            "id": "40fc108e-175d-4f4f-8429-d087bbe09d61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 81
        },
        {
            "id": "29c980a0-32d4-4603-84d9-8474dc78bdc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 73,
            "second": 45
        },
        {
            "id": "17f9fe53-f1f1-4f9b-999a-8e7f8e9cb782",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 48
        },
        {
            "id": "93a4edeb-d084-4fbd-b933-ad316e0ff5f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 73,
            "second": 49
        },
        {
            "id": "7cf17a76-da55-4edc-909c-21d2e659d5eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 51
        },
        {
            "id": "4c622da2-d9f7-4c42-ad79-65d4bd5fe0e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 52
        },
        {
            "id": "9633c5fb-853b-4de5-b67a-945f26905986",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 53
        },
        {
            "id": "95698e21-2775-45cc-96fb-e543a6150ddf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 54
        },
        {
            "id": "fb86bf68-b15e-4443-8491-73fb7be39777",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 57
        },
        {
            "id": "11f3a0c8-5768-454e-b96b-fefe90a82d7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 73,
            "second": 64
        },
        {
            "id": "ead799c9-1fcb-4d99-9942-4d16b75e30cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 65
        },
        {
            "id": "b142d6cf-e727-497f-92b1-393537a731ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 73,
            "second": 67
        },
        {
            "id": "703207f7-5c74-4cac-87ee-1edf7255194d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 71
        },
        {
            "id": "93132760-ae1c-42b9-badf-584f7f357669",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 73,
            "second": 79
        },
        {
            "id": "fb716f2a-0b80-4fd8-bf63-5946ef38b16e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 73,
            "second": 81
        },
        {
            "id": "cbc24aac-8aeb-4ea3-af3d-cf19230340a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 90
        },
        {
            "id": "b3bfbb3b-036a-423e-8be6-f4cea45ca590",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 44
        },
        {
            "id": "85475bd3-f7c3-4e2a-93b4-f8407780a3c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 45
        },
        {
            "id": "64c6b2cf-9d12-4a3a-b206-0e7cd28528a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 46
        },
        {
            "id": "ed790ee3-9e5d-4bed-8e0c-f519b64f8c02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 48
        },
        {
            "id": "a85e4f65-a022-4614-8fc7-d80f7c7f169d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 49
        },
        {
            "id": "04430b1f-ef79-4a3b-93aa-1aaf31a201d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 50
        },
        {
            "id": "9669c339-db4b-4072-b8d2-e24801675493",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 51
        },
        {
            "id": "a844bfe1-940d-4fda-8c99-87158454fa77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 52
        },
        {
            "id": "982310e6-43da-4991-bc2f-930d9a13d5db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 53
        },
        {
            "id": "224a111a-d6dd-452d-b031-2ce851204220",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 54
        },
        {
            "id": "6a0345d1-43dd-4db8-8815-432541da059d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 56
        },
        {
            "id": "a28e9c60-7b91-4e9f-991d-995f6352339d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 57
        },
        {
            "id": "afb98ae7-657f-43b2-ae2d-adfe33cd8581",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 64
        },
        {
            "id": "3fa14e48-a89c-48f6-b7ac-e3de93301db5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 67
        },
        {
            "id": "d27762db-08d7-497c-b01e-315643a7ba92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 71
        },
        {
            "id": "1ab0f59d-3b56-43c2-931f-5a48b8567826",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 79
        },
        {
            "id": "bbd2dc7a-8475-4af9-9412-f35a22524cfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 81
        },
        {
            "id": "62cac62d-b6d9-40d7-8ae3-fe7d155df56e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 83
        },
        {
            "id": "cc444c63-59ec-4805-b82e-9acea8a2a112",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 90
        },
        {
            "id": "a683c937-d1d7-4e6b-bd1b-edcd577c6fd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 39
        },
        {
            "id": "57cee23f-e673-407c-915e-b745f94d9df9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 44
        },
        {
            "id": "8b90fa85-3c40-4592-89a2-2910a0cb0798",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 45
        },
        {
            "id": "a030b34a-0eae-491f-8d5f-aac65bdf2d49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 48
        },
        {
            "id": "2cc1ca86-47d0-4e56-b99c-549e3f1fd448",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 49
        },
        {
            "id": "61260659-b897-40f1-beac-67378001c17f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 51
        },
        {
            "id": "1fed9f04-a618-4468-895b-ccdad3df803d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 52
        },
        {
            "id": "22990b65-5495-4ea2-8056-d507df2c79bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 53
        },
        {
            "id": "6081f32f-47a7-47f7-9371-9c803b5bcf9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 54
        },
        {
            "id": "345f6b0b-9240-40c5-ace8-a27eb18ccae5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 55
        },
        {
            "id": "c698405d-0ac0-4bfa-98c8-77283f983007",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 57
        },
        {
            "id": "a1cc9751-f23b-4737-9e23-5311763620c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 75,
            "second": 64
        },
        {
            "id": "04e8e589-2db4-4b9d-bdb3-0328ea2de56f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 65
        },
        {
            "id": "c1d237bf-1f7b-4a1a-86fa-e49650ed2717",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 67
        },
        {
            "id": "daa9024a-5cac-4bd8-ae29-47084b6369f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 71
        },
        {
            "id": "cc4d4df6-fa65-4512-aa9d-c5f89dfc5ee8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 74
        },
        {
            "id": "c7f92661-f914-463d-8954-4462014c0f6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 79
        },
        {
            "id": "b1b94bc4-9683-4897-b7b7-b9417045f53b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 75,
            "second": 81
        },
        {
            "id": "4764e407-28ca-4e57-ba45-86d0be7cf5ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 84
        },
        {
            "id": "24a6f052-4047-4590-bce8-d69435a13c0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 85
        },
        {
            "id": "b8b74a0f-98cf-4088-9be2-52166c5d0080",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 86
        },
        {
            "id": "b4b7b954-8c6c-47ba-8719-8df857cb489b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 87
        },
        {
            "id": "636dd294-17c5-40ed-8ed5-87ba1d44c068",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 89
        },
        {
            "id": "f0a148ce-c865-4fc9-9791-12521e761923",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 8217
        },
        {
            "id": "f18794be-ec2e-4267-81b9-74c2cf6298b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 39
        },
        {
            "id": "21b036ac-9a62-4200-b8f5-d1766c78e68b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 76,
            "second": 44
        },
        {
            "id": "1d2ee291-59b6-4ada-abbb-77115c799b05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 76,
            "second": 46
        },
        {
            "id": "c3b29f7c-e577-483e-9f3a-d40e5ce49ffb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 49
        },
        {
            "id": "245f7931-b268-47da-b6f6-8f9d3c668062",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 55
        },
        {
            "id": "a51ec9d9-e8ab-4375-a2b9-5bf09d3b3075",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 56
        },
        {
            "id": "6b241e59-66ef-4e46-bf9b-d1b0c13e64cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 57
        },
        {
            "id": "1910bade-4088-4cdb-81c6-c8c550675f54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 76,
            "second": 64
        },
        {
            "id": "c3605277-927b-4adc-bd39-1b096c350680",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 65
        },
        {
            "id": "e9b0d47e-e51a-471e-82bb-32e7598a4454",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 69
        },
        {
            "id": "6523ca4e-d5bf-4119-9f04-830a067e0177",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 74
        },
        {
            "id": "ceda29d1-3d4a-47e5-b9bc-e029dda604b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 84
        },
        {
            "id": "4b767e96-5401-41c7-a6f6-10081c102c74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 85
        },
        {
            "id": "6b17d436-6fcb-41c9-90be-66f27ab935f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 86
        },
        {
            "id": "ba87251d-7f2d-4cc5-8812-cfae0d708960",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 87
        },
        {
            "id": "25d7d67a-7d5a-48ec-880d-d421b24098d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 89
        },
        {
            "id": "eb10098f-c0c7-4e29-b558-737e0bbd5489",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 90
        },
        {
            "id": "1e5da3cc-5960-4e32-83b5-603f416e99e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 8217
        },
        {
            "id": "6aa843a7-15e7-4cd8-ba23-a866ef13938b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 45
        },
        {
            "id": "e0a0cfa3-a234-474e-b800-4c9d885e69dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 48
        },
        {
            "id": "8f0ee3a6-611c-4e02-bba3-e458ce41174d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 77,
            "second": 49
        },
        {
            "id": "895896ee-55c7-4998-8926-1d8d794ebdb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 51
        },
        {
            "id": "a4f30279-76cc-4b32-a71e-a136081850ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 52
        },
        {
            "id": "4cd66b14-c46f-4537-83ce-29b6c6ce1dd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 53
        },
        {
            "id": "c032d565-d0e8-4750-8384-6f76eea91c14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 54
        },
        {
            "id": "23aebea5-87bc-46e9-9a29-0b4a6fe517b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 56
        },
        {
            "id": "1298ca17-e041-4bab-aaea-578c10885fa5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 57
        },
        {
            "id": "84a7122e-2aae-4655-afa1-7c2a03706513",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 77,
            "second": 64
        },
        {
            "id": "8db372d6-99d9-4f5d-83f0-77d879915c54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 65
        },
        {
            "id": "f01a2d37-ab1e-44da-bc51-b3857e39561c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 77,
            "second": 67
        },
        {
            "id": "79b38260-db4d-4c74-b1f3-09563f1fb50f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 71
        },
        {
            "id": "4812ca3e-a632-4af5-8997-61664aad2546",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 77,
            "second": 79
        },
        {
            "id": "fcc072d3-84a5-44b6-a756-ec217d648742",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 81
        },
        {
            "id": "5f2ef5f9-b3d6-4126-b728-3d47c80e1a68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 44
        },
        {
            "id": "6c085528-2cd3-4f81-ae1a-8da4ca741a6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 78,
            "second": 45
        },
        {
            "id": "ff3fccbd-2997-4538-a929-d5081d1a639b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 46
        },
        {
            "id": "736c0db7-4200-4c0a-9176-9b7a694ba32c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 48
        },
        {
            "id": "a7bb1585-294a-448a-855a-3737a1725830",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 78,
            "second": 49
        },
        {
            "id": "80805afd-6c8b-4719-ab3f-548e511573e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 78,
            "second": 50
        },
        {
            "id": "fb5f12a8-799f-4ce7-8691-a693acde4bad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 51
        },
        {
            "id": "13674ca1-e923-4de9-9fb2-541e63f600f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 52
        },
        {
            "id": "6884db76-ea68-4aa7-be58-71a54c0fca17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 53
        },
        {
            "id": "3d0bb411-6c69-4002-8312-0cb22be7af88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 54
        },
        {
            "id": "40e1f8e9-349c-492c-9868-38458749581e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 56
        },
        {
            "id": "eeed99e4-5450-43d1-8eb7-6ae24cb1353f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 57
        },
        {
            "id": "2ad85fc5-3119-4d3a-9f5a-0442196cb3cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 78,
            "second": 64
        },
        {
            "id": "c91027d4-17d5-4946-a1fe-67443f720b11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 67
        },
        {
            "id": "0760f772-47df-4ba4-824f-338def93584f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 71
        },
        {
            "id": "5040b1f3-cfba-412e-a8c9-ba94a6177687",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 78,
            "second": 79
        },
        {
            "id": "b854860a-f6f4-42d6-aab2-7b8694b99004",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 81
        },
        {
            "id": "778d13be-98ff-4ced-864c-6188c41779ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 83
        },
        {
            "id": "9a22290f-59b5-41a1-83ae-b4609a8d37e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 90
        },
        {
            "id": "c6cf6db0-6f57-4a12-ac4d-fc49c65a03a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 39
        },
        {
            "id": "83780407-4643-4dba-9a53-3c04ba85153a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 45
        },
        {
            "id": "405eb857-7252-449f-9538-6758512c5be1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 48
        },
        {
            "id": "0a6ce6f3-2c0d-4fad-bb22-d4bded7aac42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 50
        },
        {
            "id": "c5d0c675-7f14-4807-848a-309c089170db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 51
        },
        {
            "id": "781edf95-af79-4ffd-90ec-26b235d6bd50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 52
        },
        {
            "id": "7c5f7377-0aa8-4757-9de1-352f1098668a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 54
        },
        {
            "id": "dc6f8f5c-55f7-4686-a4e8-5ea523acfa57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 55
        },
        {
            "id": "f64d50be-8c75-4f5d-ae5f-1a1466998ffe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 56
        },
        {
            "id": "d7619951-d9e8-4e23-b1b0-8ff98e384cc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 57
        },
        {
            "id": "69a22ecd-f25f-4e0d-9746-18e36afcea08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 64
        },
        {
            "id": "d90c9b74-b3e3-46c6-8b2f-8286e68b60d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 67
        },
        {
            "id": "7530c20b-5808-4fc2-aa33-9164e8a37503",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 71
        },
        {
            "id": "6afaef83-f188-433d-a230-cb4baf4a5260",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 79
        },
        {
            "id": "aee73127-d7ad-4e4f-b800-7f19549c3336",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 81
        },
        {
            "id": "11cbdafb-69f3-43c8-9476-4d5d5c6c6801",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 83
        },
        {
            "id": "6b3efade-27f2-4344-bc9d-3d940f72c6d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 86
        },
        {
            "id": "a4946d70-93ba-4c8c-8cce-b8748f007708",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 89
        },
        {
            "id": "3ff416a2-f778-471d-b59e-085ef42f48be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 8217
        },
        {
            "id": "71edcb93-9492-4a24-aa14-5b222eee8071",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 80,
            "second": 39
        },
        {
            "id": "36ff314f-f150-4522-9e91-b51e6eb3c7ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 44
        },
        {
            "id": "4da6e82a-5098-4dcb-8399-1b1840154c0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 45
        },
        {
            "id": "e2cee5e5-d67d-45f3-b0fa-8288706b2205",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 80,
            "second": 46
        },
        {
            "id": "ce554a05-d835-4710-a9d0-0754b77aeb95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 48
        },
        {
            "id": "177211e4-b52a-40ec-ad86-5f8c7465071c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 52
        },
        {
            "id": "9490947a-7de2-467a-9964-be488f344d07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 53
        },
        {
            "id": "e26d7dca-abeb-42c7-9e07-183fc771cef0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 56
        },
        {
            "id": "cbf08eaf-ca3f-4204-8e06-8b42ecbe6834",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 57
        },
        {
            "id": "f4591263-1a77-4b82-bf37-099816aeb946",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "0d32db2e-2c04-45fd-9ef1-1fdcdbd1d644",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 69
        },
        {
            "id": "47bf2b39-770c-4621-afa7-e736340c012a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 70
        },
        {
            "id": "561f4a29-dde6-44c9-8b7f-bc1d1364baa7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 73
        },
        {
            "id": "bbcda040-caad-433a-8412-b7e32068d3c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 74
        },
        {
            "id": "d2a60be4-972c-40ca-a18d-7baf225bb908",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 77
        },
        {
            "id": "9401273a-b1d1-49bf-afb8-b514e5d58b05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 78
        },
        {
            "id": "00d06d16-9d83-436f-ab87-1949e3a207ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 83
        },
        {
            "id": "ba2b3d59-79bf-45f2-a68d-486db2ebf7d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 86
        },
        {
            "id": "2d1e4d7b-22f1-41ee-a95f-67d4726c76bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 87
        },
        {
            "id": "773e255c-a3af-4478-b451-e831f43583f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 89
        },
        {
            "id": "d600f198-e730-4855-a9e6-a7c091a8a378",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 80,
            "second": 8217
        },
        {
            "id": "4e2b6a14-958f-4084-9680-a4394440d3fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 81,
            "second": 39
        },
        {
            "id": "5a4af436-304b-46ae-ba1d-d42aa484faa5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 81,
            "second": 44
        },
        {
            "id": "8dd4da37-5e00-476b-acc9-c188224e7a66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 81,
            "second": 45
        },
        {
            "id": "30ab80b7-694d-4616-b9d8-bfe7c7ba2ec6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 81,
            "second": 48
        },
        {
            "id": "e52d136f-8aa7-4fba-8ae5-3d006d344c13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 81,
            "second": 50
        },
        {
            "id": "666f8a5e-aeb2-4c9f-8765-c4ae8546c1b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 81,
            "second": 52
        },
        {
            "id": "89268ea2-5f87-4ac3-9b81-8ba369df64b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 81,
            "second": 54
        },
        {
            "id": "a4ebab03-e8fa-48ce-8fb0-8e8e5eba0009",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 81,
            "second": 55
        },
        {
            "id": "17f47734-e875-4d1a-b8b1-d8f746b920bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 81,
            "second": 56
        },
        {
            "id": "55ad1c9d-99e6-4e41-b850-501df3b88190",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 81,
            "second": 57
        },
        {
            "id": "997c177c-9da6-4f0d-bf42-5002e755a1ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 81,
            "second": 64
        },
        {
            "id": "4fce6ee8-4715-48e8-8367-574556bc7b76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 65
        },
        {
            "id": "d0015646-729e-46d0-98fa-df5c2d9b4411",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 81,
            "second": 67
        },
        {
            "id": "50703633-d6e9-4989-8e6f-56232e1628f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 70
        },
        {
            "id": "9e5190e6-0d97-4d5b-981f-092ff56d89e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 81,
            "second": 71
        },
        {
            "id": "a3f69d40-9e18-41b6-9a0e-4729eac241d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 81,
            "second": 74
        },
        {
            "id": "f58e9c1a-40b9-4d30-9636-62ac0894085d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 81,
            "second": 79
        },
        {
            "id": "936dd2cd-7eef-4532-91f1-50192b390bb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 81,
            "second": 81
        },
        {
            "id": "bb9e5bd4-c747-42f2-88c2-da6ceaaa35c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 81,
            "second": 83
        },
        {
            "id": "2f98e0e8-1b9b-49c8-885f-041fdeaf2648",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 86
        },
        {
            "id": "804badfb-d90b-461e-9aa5-5069de9a6215",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 87
        },
        {
            "id": "a26e7cfe-07c5-4150-b718-fc09005a07aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 88
        },
        {
            "id": "1efc1939-aa21-4053-997b-34e71c9f26aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 89
        },
        {
            "id": "d5b361c5-2393-4772-bbed-a330b997ea72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 81,
            "second": 8217
        },
        {
            "id": "21628942-d1ea-49cc-9376-d4a027623b7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 39
        },
        {
            "id": "d54be64b-ca82-491a-9076-c29ba54eabc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 82,
            "second": 45
        },
        {
            "id": "c2f571b0-76f3-478e-bca6-8913b290a850",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 48
        },
        {
            "id": "7e2fe7a9-ef6f-4a05-9fad-d78f6afcb726",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 49
        },
        {
            "id": "b6371920-003d-4751-a7ab-591f18257098",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 51
        },
        {
            "id": "115ac3fd-4281-4677-b2b0-77862effd27e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 82,
            "second": 52
        },
        {
            "id": "779ca98d-e84f-4f38-a14b-385b82c9ef20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 53
        },
        {
            "id": "2c5899e6-e88f-444b-8955-be24989b2229",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 54
        },
        {
            "id": "f019dc66-0c2a-4af6-abfc-b9383cfcfbdc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 55
        },
        {
            "id": "82a2c855-438f-42a8-9d1a-12347034d376",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 56
        },
        {
            "id": "25e1054a-05b2-47de-b244-45a0e0083c9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 57
        },
        {
            "id": "a627dc92-5c8c-4c90-b7e0-30004edc0e47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 82,
            "second": 64
        },
        {
            "id": "740fb6ec-e816-4a74-aa44-8138a8251274",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 65
        },
        {
            "id": "6184fcd3-8635-4c34-9661-fc407bbe6ae9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 67
        },
        {
            "id": "66916ce1-8bf1-45e7-a9c8-d037e25047c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 69
        },
        {
            "id": "11aa8770-bdd4-4fff-8ab3-135039d6af52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 71
        },
        {
            "id": "8bbb47ea-701b-4b4a-b2cb-f43a6ea3dfe0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 82,
            "second": 74
        },
        {
            "id": "243d19bd-c393-4f6d-90cb-6b3b159746d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 79
        },
        {
            "id": "740c8972-ed31-4b7d-9ae3-c3ccae1abb53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 81
        },
        {
            "id": "6610e5ca-ab16-4732-ae56-3c6691cdb0a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 84
        },
        {
            "id": "24cb1a9a-4955-4183-a9e6-226e1b4ef76d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 85
        },
        {
            "id": "b2a4a4bb-2b3e-4d5f-adb6-1833261b6cf6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 82,
            "second": 86
        },
        {
            "id": "0db9357f-41f7-4d05-b925-295103d9d261",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 82,
            "second": 87
        },
        {
            "id": "0e53fb45-3c3b-48a8-8045-26d398e22b9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 82,
            "second": 89
        },
        {
            "id": "f626a617-c94d-4fb1-99f3-9f132d97b122",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 8217
        },
        {
            "id": "1f19121f-69ff-4d9c-bd62-deb681281b8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 83,
            "second": 45
        },
        {
            "id": "ad3ce192-4c81-48e2-bc70-8f2e62e98c39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 46
        },
        {
            "id": "5276c308-c419-45c1-b1c6-c39ef400ef59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 48
        },
        {
            "id": "76f35532-1cc7-4bb5-8608-bbfc22117986",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 49
        },
        {
            "id": "8c31808e-925b-4f48-9688-b2e9961a5ff9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 52
        },
        {
            "id": "033391ff-b85c-473d-b63d-816934058319",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 54
        },
        {
            "id": "e4397a62-b923-4d41-a68b-c0bdf03aa2b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 56
        },
        {
            "id": "d2a407c8-8c48-4d37-897e-2058b69a22af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 67
        },
        {
            "id": "37c27f05-47fc-4a31-8af1-6d2887a9583d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 71
        },
        {
            "id": "70635a87-8a9e-4b81-90be-18ef21da7212",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 79
        },
        {
            "id": "f9e4e3c6-995c-48b3-a3a6-497edb6e731c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 81
        },
        {
            "id": "ab93b663-b2f3-42b5-95b8-7c742636d769",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 44
        },
        {
            "id": "6de5f266-3679-4ae1-a993-6dcd7a940a32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 45
        },
        {
            "id": "72e1e7d4-5a74-4ca2-b755-1ecd92f6380c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 46
        },
        {
            "id": "16c12e2a-8615-40f1-bb4d-c488cbe979fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 49
        },
        {
            "id": "9caa3d21-459c-473e-95b2-ec79142f63fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 50
        },
        {
            "id": "a2c2135c-474f-4947-925c-ce3ad55aed4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 51
        },
        {
            "id": "eace6b60-3217-43a4-99da-83fde98956c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 52
        },
        {
            "id": "122c7c33-0564-4d2c-8655-096cbb2b7cda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 53
        },
        {
            "id": "3dade35c-05f8-4411-a1e5-e24e621df96b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 54
        },
        {
            "id": "c1cd94a5-3e63-418b-855e-3c257d20c10b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 64
        },
        {
            "id": "a9641d84-6c99-4c85-b4ee-17b79dfa7bdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 65
        },
        {
            "id": "9cc86e7f-0c9f-48c0-89f4-919b20492f0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 67
        },
        {
            "id": "ab09f3bb-951b-4bbd-b960-301586feebd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 71
        },
        {
            "id": "4a4bd61d-9e1c-40d6-8792-d684a9d1d0db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 44
        },
        {
            "id": "72d8862a-b56e-46ef-8e0a-59cd57dd579b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 45
        },
        {
            "id": "fb0d42dc-a5c6-4167-86cf-7d6ed1ff9b9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 46
        },
        {
            "id": "389bca1e-c6e2-43d0-8286-bfcc48557d6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 48
        },
        {
            "id": "b3fd8cf6-2226-49c7-87ed-d51089d16d7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 49
        },
        {
            "id": "b9c47baf-2662-4d30-a8d8-437ae10b4674",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 50
        },
        {
            "id": "efebd093-f2cd-4151-8c4b-e8f0e7ad07c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 51
        },
        {
            "id": "5b7d5858-05af-45c5-be0f-101a0a06242b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 52
        },
        {
            "id": "4a43b1d3-1cfb-4673-82e0-90465da5870d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 53
        },
        {
            "id": "1bff4509-005a-4b43-a222-e0c202933e25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 54
        },
        {
            "id": "2d0d5e7c-3dc6-430f-a857-3b3f9d50ef21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 56
        },
        {
            "id": "4519ea8c-e26e-4172-a01d-4fb6c12b3064",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 57
        },
        {
            "id": "3af235cc-865b-4b9c-91ff-ac18955e08f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 64
        },
        {
            "id": "62b26608-0eca-4314-801e-488d5cfd4227",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 65
        },
        {
            "id": "3ab671f0-2f2d-4ae3-9bca-c24563daacfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 67
        },
        {
            "id": "6db57e7e-bc72-45b3-a305-23dc91a80b3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 71
        },
        {
            "id": "d0826b78-5ac0-4ad5-8d2f-9d69b8d1ce21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 79
        },
        {
            "id": "feb57b1c-06d6-4ef8-983a-905bd24d69d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 81
        },
        {
            "id": "c98d8178-cb32-43e4-807b-6b05cdac58b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 83
        },
        {
            "id": "b719d448-1084-4006-a7d3-7cb9ca7d508e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 90
        },
        {
            "id": "5a0e4063-77fa-4e94-8455-35c24183f1b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 86,
            "second": 44
        },
        {
            "id": "60ce81a9-04d6-4a28-9682-ea1e56877374",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 86,
            "second": 45
        },
        {
            "id": "a337ed2a-ad83-424e-8e2a-0556f50778ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 46
        },
        {
            "id": "eab624e3-11ff-4a6d-be22-1a62b60668bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 48
        },
        {
            "id": "29e74718-deca-41ae-bb56-472564e905ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 49
        },
        {
            "id": "15b1ee09-df7f-4186-85a8-2cbf644651c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 50
        },
        {
            "id": "fe618698-14f9-43aa-8862-68d8fece09c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 51
        },
        {
            "id": "8956af91-55c3-4994-a897-3beb0b00df71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 52
        },
        {
            "id": "646bd7e9-6ebe-4df0-babf-4bf568be0f2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 53
        },
        {
            "id": "7bb37b57-7c32-4356-ab79-f9adc00871af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 54
        },
        {
            "id": "40143fbb-5c5b-47de-95ce-af576cbd7df5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 56
        },
        {
            "id": "dd4899b7-c3a7-410d-95f2-6d12255610f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 57
        },
        {
            "id": "a3df31b7-d034-4985-8475-f15740e34050",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 64
        },
        {
            "id": "560d5371-d440-4f83-87b6-dfbd83c4cd8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 86,
            "second": 65
        },
        {
            "id": "1767e281-cfa1-490d-9762-73f4e5c42358",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 67
        },
        {
            "id": "71f174a2-8fa2-4a09-85ef-cf885222a809",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 71
        },
        {
            "id": "72e7ae2a-dd48-462f-a1bd-c54576ae2a79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 79
        },
        {
            "id": "d08f5b7f-a641-4a6c-9615-4c24e00b3ffd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 81
        },
        {
            "id": "3461e4c4-c6d9-4240-9e50-907cbacea744",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 83
        },
        {
            "id": "5324bde0-47d6-4f38-874a-1a355cb0c7f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 90
        },
        {
            "id": "6b641ac5-e1fd-4efb-be5e-3d5b3cfadbc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 87,
            "second": 44
        },
        {
            "id": "1b6ea24f-9c21-40cb-b416-6c4fa4c07900",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 45
        },
        {
            "id": "ca7e596f-b151-4e44-a480-a31f48328cf6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 46
        },
        {
            "id": "e6cc7c2b-1210-4bfb-8f31-337220106fbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 48
        },
        {
            "id": "962140a8-5416-4a53-a6ef-ef82235c35dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 49
        },
        {
            "id": "9b9e4611-c53e-49fb-ba77-0b8855af3c08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 50
        },
        {
            "id": "9afff3c4-48d8-41cd-bd4a-4396378466d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 51
        },
        {
            "id": "a06cdc57-9116-4620-8568-dc53c26e7908",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 52
        },
        {
            "id": "859b74d7-7354-436d-92cf-798afe82216a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 53
        },
        {
            "id": "f9f8ab4f-e89b-4c0e-8e70-ab4349b9a881",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 54
        },
        {
            "id": "0cb8e88f-9bc6-4fca-ada4-80df4855626c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 56
        },
        {
            "id": "abeddba3-3f0d-47c6-b490-0183ef983f34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 57
        },
        {
            "id": "0ed78add-124a-4ef2-9697-2de4f9b18b61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 64
        },
        {
            "id": "abaec884-28e6-476f-a303-3f7a615b35d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 65
        },
        {
            "id": "f226fae3-7971-4f5a-a0d4-42009f7ecb68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 67
        },
        {
            "id": "17dcd27f-d7da-45f2-8e1e-44786ae2198f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 71
        },
        {
            "id": "193fd961-76f0-4bfa-94b5-dee5145f16b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 79
        },
        {
            "id": "21ad2bd8-0a43-4b8f-9ed6-965a6db12251",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 81
        },
        {
            "id": "1b176a01-d022-4213-9d78-0cce02a43165",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 83
        },
        {
            "id": "43eeffa0-590a-4ddc-8a7d-0d1218eb87f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 90
        },
        {
            "id": "1507817c-192d-4a8e-80a2-cbb5d0f29533",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 44
        },
        {
            "id": "b709a192-3c72-4b03-9892-d8b79c4c69e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 45
        },
        {
            "id": "fdcccbb2-2389-4378-afb5-08082d6ebe6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 48
        },
        {
            "id": "4d50e885-98f7-472b-b8ff-fbbfd6d3cd3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 49
        },
        {
            "id": "e2fa3424-a91e-488f-90ec-254732789b6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 51
        },
        {
            "id": "4fcdb7fe-37f0-4b44-b712-4e04e4110237",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 52
        },
        {
            "id": "1aa74145-0dad-49f6-9cb2-e7c80435402c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 53
        },
        {
            "id": "20f7acb4-f1f3-4d97-b961-6029dd5c2fbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 54
        },
        {
            "id": "9dddb22f-0644-4883-9140-d7db86a270f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 57
        },
        {
            "id": "eaf2f7d0-207f-4ef1-90ca-dd7afefc4ead",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 88,
            "second": 64
        },
        {
            "id": "42979484-00a0-4379-a2c2-1e7b3a047d6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 65
        },
        {
            "id": "f3013375-2ba1-4094-8c17-23c37573d5ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 67
        },
        {
            "id": "55a03c00-54cd-4c52-8e22-0b6b36ff3d02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 71
        },
        {
            "id": "3f6e5e3b-f5e3-4849-862d-0de2c45428d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 79
        },
        {
            "id": "5d09a4bb-1001-483f-a8f8-a966a0cc9485",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 81
        },
        {
            "id": "5ff5d257-de44-47a1-9f73-45f474c263bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 44
        },
        {
            "id": "c7ba8c3b-33ef-496c-940a-7e3458e9d017",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 45
        },
        {
            "id": "1e72f12d-a2ca-48ab-9540-7f2b0b6aa961",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 46
        },
        {
            "id": "36116e26-d90b-4ec5-987d-7901ac2d8911",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 48
        },
        {
            "id": "47744abb-02f5-4c43-813c-399b58ecb861",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 49
        },
        {
            "id": "fdbb2f0f-5c27-415e-807e-d68a77e6a7df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 50
        },
        {
            "id": "cbf6a793-ed79-4725-b465-5e2d6e12c1b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 51
        },
        {
            "id": "72be0847-f284-4a8f-bf44-f9c1bc291eda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 52
        },
        {
            "id": "ac1d118c-2d9e-4df8-9f1d-3cfe39fef714",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 53
        },
        {
            "id": "3083aeaf-dbcb-479f-897a-383aade3633b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 54
        },
        {
            "id": "a5594e6c-95a7-43ac-af35-ed90f386203e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 56
        },
        {
            "id": "3254a79a-001e-4725-89b1-4cb49710f14b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 57
        },
        {
            "id": "260eaee2-b00f-4186-8b7c-b159c665d0bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 64
        },
        {
            "id": "fe514754-dee0-4a9c-ac92-f01ccc37f326",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 65
        },
        {
            "id": "ac1df938-7ffe-42e1-8508-1b4e0f26c199",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 67
        },
        {
            "id": "4c016556-43ba-4d88-b472-00e1b3ec372c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 71
        },
        {
            "id": "d6cab06b-2955-4f40-bfbb-f019edce6287",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 79
        },
        {
            "id": "365b45e4-c017-4424-8deb-40b0932ffef3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 81
        },
        {
            "id": "d7b6871e-5ea3-46c0-8440-4046ccf01467",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 83
        },
        {
            "id": "98735dd5-db5b-41be-8604-5915419c031d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 90
        },
        {
            "id": "ee4a57fb-ba68-4be1-a7c7-c9227ecb4058",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 45
        },
        {
            "id": "997a1f6e-d6f3-4bf5-9aa1-f1d28a280bbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 90,
            "second": 49
        },
        {
            "id": "939cad1e-e957-4ed4-84fb-a0a98a04b0c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 57
        },
        {
            "id": "b45517af-2b69-43ea-a63b-a3f01e366838",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 90,
            "second": 64
        },
        {
            "id": "509db15b-ea18-42a1-b04d-b5d576a066de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 65
        },
        {
            "id": "3e256bd2-ee8d-4ae6-b1f2-5511f66ae79e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 67
        },
        {
            "id": "ebe713fe-67dd-484e-bff5-bad54286a8fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 71
        },
        {
            "id": "f4ea6a20-95a6-4acc-a656-fae20151a2e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 79
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 22,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}