{
    "id": "d12e62c5-4c4f-4af0-8349-1ba8b4bd9035",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fon_8bit",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "CF Quebec Stamp",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "6719b613-7da9-41b6-82f7-18ec781dddea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 48,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "df28309a-ab7f-4fac-8017-661755be228b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 48,
                "offset": 9,
                "shift": 24,
                "w": 6,
                "x": 2,
                "y": 152
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "5d2aecb7-1ef6-4757-8b91-d32638e4d3e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 48,
                "offset": 5,
                "shift": 24,
                "w": 14,
                "x": 488,
                "y": 102
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "aa95185d-6fa9-4521-84b0-8bb26609fc71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 48,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 462,
                "y": 102
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "086a3fc5-9228-41cd-8a60-e18a0f23fe92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 48,
                "offset": 3,
                "shift": 24,
                "w": 18,
                "x": 442,
                "y": 102
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "4ca2b3d2-bae8-4c17-ab4c-91a336bda74b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 48,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 416,
                "y": 102
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "32cc8428-db56-48d6-83e5-c6eaeab6458c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 48,
                "offset": 1,
                "shift": 24,
                "w": 23,
                "x": 391,
                "y": 102
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "3bd2360c-1f2b-4d9c-8598-92fb5aadb215",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 48,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 379,
                "y": 102
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "201911b0-e7a5-4508-9de0-ba3e69c465cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 48,
                "offset": 5,
                "shift": 24,
                "w": 14,
                "x": 363,
                "y": 102
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "185aa2ac-ad12-41e8-947d-c406413d1b68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 48,
                "offset": 5,
                "shift": 24,
                "w": 14,
                "x": 347,
                "y": 102
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "f804f085-2b51-45ed-96ef-1f3e14db5fec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 10,
                "y": 152
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "72567db9-ced0-456f-be43-98047b2a1799",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 19,
                "x": 326,
                "y": 102
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "092cf3c6-7a6b-4ae0-845d-7613fe027363",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 48,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 292,
                "y": 102
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "c05e612d-5e71-4b5c-9841-c4d7ad4658d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 48,
                "offset": 1,
                "shift": 15,
                "w": 15,
                "x": 275,
                "y": 102
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "3381b627-9640-4838-80ec-9e1b14cea629",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 48,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 264,
                "y": 102
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "5e7e5923-4672-4262-aea6-729922f622de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 48,
                "offset": 4,
                "shift": 24,
                "w": 16,
                "x": 246,
                "y": 102
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "555dc920-1ef9-4cbd-b318-da6ec10366c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 48,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 222,
                "y": 102
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "0e7a70a0-bb06-4f96-8cca-a584e0eea307",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 200,
                "y": 102
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "3862cefe-4720-446a-ab18-d2b00839c725",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 48,
                "offset": 1,
                "shift": 24,
                "w": 21,
                "x": 177,
                "y": 102
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "80166f0d-c330-417e-b2cd-e1740183d1f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 48,
                "offset": 1,
                "shift": 24,
                "w": 21,
                "x": 154,
                "y": 102
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "330569fa-a584-47c1-920d-e2bed8f944f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 48,
                "offset": 0,
                "shift": 24,
                "w": 23,
                "x": 129,
                "y": 102
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "330416f4-eb44-4d09-afff-ebbba6a5436a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 304,
                "y": 102
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "695f0480-379c-4b01-a1d2-5cfde9b55704",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 48,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 32,
                "y": 152
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "b6b7bdb0-13fa-4d8b-9245-43a2b387e63b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 56,
                "y": 152
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "8ca0747b-bca1-4cbd-988e-a361e2a6408d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 48,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 78,
                "y": 152
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "ead9e4a3-3b62-4131-a6f9-dfcc91995281",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 48,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 102,
                "y": 202
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "36b2f2b6-fa98-4d3b-a809-c60b6a570868",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 48,
                "offset": 9,
                "shift": 24,
                "w": 6,
                "x": 94,
                "y": 202
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "e81c8689-88a0-444d-a79f-03f96798fa6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 48,
                "offset": 8,
                "shift": 24,
                "w": 7,
                "x": 85,
                "y": 202
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "541a1842-0cd0-4b36-8acd-a0f4c0f96dea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 19,
                "x": 64,
                "y": 202
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "00f872bb-2335-48dd-a473-fd69ad4d8804",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 19,
                "x": 43,
                "y": 202
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "43bfacc5-da4e-4162-8bae-1b15949a7b6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 19,
                "x": 22,
                "y": 202
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "a7a74115-096d-4547-bf74-55c30f37af36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 48,
                "offset": 3,
                "shift": 24,
                "w": 18,
                "x": 2,
                "y": 202
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "75de1e55-6932-4f98-9311-e8904f37715a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 48,
                "offset": 2,
                "shift": 36,
                "w": 34,
                "x": 469,
                "y": 152
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "7582dde6-b6b6-45ea-be31-7e521639c0e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 48,
                "offset": -1,
                "shift": 27,
                "w": 29,
                "x": 438,
                "y": 152
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "b89e52d8-63c3-409a-9f46-716fb7c4d9a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 48,
                "offset": 1,
                "shift": 24,
                "w": 25,
                "x": 411,
                "y": 152
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "cb296d6b-2b63-4360-a883-ae7ad913c194",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 48,
                "offset": 1,
                "shift": 26,
                "w": 27,
                "x": 382,
                "y": 152
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "649fab03-254e-403d-96c2-fc8d911db1b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 48,
                "offset": 1,
                "shift": 29,
                "w": 31,
                "x": 349,
                "y": 152
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "2709105d-78d4-4499-832f-31cd865dd6e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 48,
                "offset": 1,
                "shift": 23,
                "w": 23,
                "x": 324,
                "y": 152
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "693f399c-b896-4dcf-ac0f-5b7a1133b1d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 48,
                "offset": 2,
                "shift": 22,
                "w": 21,
                "x": 301,
                "y": 152
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "3f00c1ff-06f9-4d90-9a64-2c3fb14bef9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 48,
                "offset": 1,
                "shift": 29,
                "w": 30,
                "x": 269,
                "y": 152
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "1d3fd1ef-d793-4966-926a-c6a1178099ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 48,
                "offset": 1,
                "shift": 31,
                "w": 30,
                "x": 237,
                "y": 152
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "aafc21f8-3a85-4b4b-a630-09ac1fa40a1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 48,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 220,
                "y": 152
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "87b77d96-89a1-4fdc-9f14-9ca73b670cb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 48,
                "offset": -3,
                "shift": 16,
                "w": 19,
                "x": 199,
                "y": 152
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "398f84df-f614-48b4-a9cd-bcf119813136",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 48,
                "offset": 1,
                "shift": 31,
                "w": 31,
                "x": 166,
                "y": 152
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "2bafe33e-c55b-465c-9bcf-45ecff625290",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 48,
                "offset": 1,
                "shift": 22,
                "w": 23,
                "x": 141,
                "y": 152
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "efb2cccd-9977-4764-968b-e995d6799bea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 48,
                "offset": 1,
                "shift": 38,
                "w": 37,
                "x": 102,
                "y": 152
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "3469666a-f2ab-42c9-a6b8-6f9d355da876",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 48,
                "offset": 1,
                "shift": 31,
                "w": 30,
                "x": 97,
                "y": 102
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "06b6f686-5b14-4758-abd0-66603ea1a442",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 48,
                "offset": 1,
                "shift": 28,
                "w": 30,
                "x": 65,
                "y": 102
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "e2243b1c-244f-4b5e-aa28-31be189f7871",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 48,
                "offset": 1,
                "shift": 24,
                "w": 24,
                "x": 39,
                "y": 102
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "f2521bbc-7bca-4664-a27d-8bd4bfb613ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 48,
                "offset": 1,
                "shift": 28,
                "w": 30,
                "x": 45,
                "y": 52
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "5648a0b2-a239-450a-9fd4-9a99c6afa91f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 48,
                "offset": 1,
                "shift": 28,
                "w": 28,
                "x": 2,
                "y": 52
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "ca866196-af4c-4926-b80b-82dd875d966c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 48,
                "offset": 1,
                "shift": 21,
                "w": 21,
                "x": 459,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "3e1a3775-0b7f-4f84-8206-ff7b1107a11f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 48,
                "offset": 1,
                "shift": 26,
                "w": 26,
                "x": 431,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "e402264f-cea9-428c-83bb-c0c4a3f41188",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 48,
                "offset": 1,
                "shift": 30,
                "w": 30,
                "x": 399,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "8acc49e0-54fd-4521-922d-fc6b1fb38f26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 48,
                "offset": 1,
                "shift": 31,
                "w": 31,
                "x": 366,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "90ce4cd4-da2b-4796-9fbe-4684031d56f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 48,
                "offset": 1,
                "shift": 40,
                "w": 40,
                "x": 324,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "303f5358-2cf7-4349-99a7-b2eee2abda99",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 48,
                "offset": 1,
                "shift": 29,
                "w": 29,
                "x": 293,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "d5d26719-b2c7-4b48-b015-ec238359a0fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 48,
                "offset": 1,
                "shift": 28,
                "w": 28,
                "x": 263,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "d2b83eaf-f833-4fa3-be49-24533f027c11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 48,
                "offset": 0,
                "shift": 26,
                "w": 27,
                "x": 234,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "f850a887-00f9-4e6e-a5f6-e8c428836369",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 48,
                "offset": 8,
                "shift": 24,
                "w": 11,
                "x": 32,
                "y": 52
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "2577633a-040b-4339-92d7-2bb5d0573cf2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 48,
                "offset": 4,
                "shift": 24,
                "w": 16,
                "x": 216,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "ec418244-e790-4e51-b967-c51f1ed543fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 48,
                "offset": 5,
                "shift": 24,
                "w": 11,
                "x": 182,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "1e9e604d-da6c-4a24-a35a-3ecdbb9c4893",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 160,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "a0325c05-21d1-44c9-84e9-407098ec86f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 48,
                "offset": -1,
                "shift": 24,
                "w": 26,
                "x": 132,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "3de5644e-9caa-45d5-b6d2-b2ac324cd0a4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 48,
                "offset": 8,
                "shift": 24,
                "w": 8,
                "x": 122,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "b9338e83-5f78-4075-8423-0f3da1df00bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 19,
                "x": 101,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "4776af2e-cbbb-4842-9f2f-7ac608847b8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 48,
                "offset": 3,
                "shift": 24,
                "w": 19,
                "x": 80,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "4cce6667-2c0e-4ecd-a05d-8d1340ddd130",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 48,
                "offset": 3,
                "shift": 24,
                "w": 18,
                "x": 60,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "e83db5cb-952f-495e-8e56-3150035437bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 19,
                "x": 39,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "01a53f66-19dc-4cba-8a85-82420b8c39b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 17,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "92011d24-7e00-4864-8347-bf539ae68238",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 48,
                "offset": 3,
                "shift": 24,
                "w": 19,
                "x": 195,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "0059bce7-36b1-440d-9edb-a2be3f53926e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 77,
                "y": 52
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "d8c45ebb-8b35-4804-add0-85d814ffaa7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 48,
                "offset": 3,
                "shift": 24,
                "w": 18,
                "x": 301,
                "y": 52
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "79452c48-99ec-4653-bfc7-117fc363c708",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 48,
                "offset": 3,
                "shift": 24,
                "w": 18,
                "x": 99,
                "y": 52
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "faf17297-85c1-4a28-956d-1f319f7641b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 15,
                "x": 2,
                "y": 102
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "5f3d72a2-7d4d-46ce-bd22-97e3e12e1163",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 48,
                "offset": 4,
                "shift": 24,
                "w": 19,
                "x": 488,
                "y": 52
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "b5d89fba-fceb-40ae-8ac4-615216752ea3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 48,
                "offset": 3,
                "shift": 24,
                "w": 18,
                "x": 468,
                "y": 52
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "9f96f8bc-90ae-4fdb-b07e-5ed7e42eec47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 48,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 444,
                "y": 52
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "31f9f2ff-e845-4eb1-b63b-aa246b145af8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 48,
                "offset": 3,
                "shift": 24,
                "w": 18,
                "x": 424,
                "y": 52
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "d1b49552-a725-4ce8-a648-829cf1d92794",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 402,
                "y": 52
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "03e78314-0f66-49fe-b3b1-7b748c88f05f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 48,
                "offset": 3,
                "shift": 24,
                "w": 19,
                "x": 381,
                "y": 52
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "416bf290-cbf4-47a7-885e-d10d6b098919",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 19,
                "x": 360,
                "y": 52
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "789b0af8-bd75-4cbd-a6c2-1209b3025557",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 48,
                "offset": 5,
                "shift": 24,
                "w": 16,
                "x": 342,
                "y": 52
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "795bc5a1-6100-4484-9468-6fbc97922d98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 48,
                "offset": 3,
                "shift": 24,
                "w": 18,
                "x": 19,
                "y": 102
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "1db34eb3-05c3-4f95-8a3e-f82d501dd9f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 19,
                "x": 321,
                "y": 52
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "ba262cae-1538-4ad5-8e67-7be40a3b4bcd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 48,
                "offset": 3,
                "shift": 24,
                "w": 18,
                "x": 281,
                "y": 52
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "b472a703-6f96-43ae-8cdd-d6581b64540c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 48,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 257,
                "y": 52
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "421038a4-354d-44ea-b6ef-938efd932013",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 48,
                "offset": -1,
                "shift": 24,
                "w": 26,
                "x": 229,
                "y": 52
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "bc4085f3-256f-4775-921f-af0c2ae98f8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 48,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 205,
                "y": 52
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "bb3b9ae1-479a-4efe-9044-432787bb5cb5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 48,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 181,
                "y": 52
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "0b1779c4-2926-4354-9171-f09d17c8350a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 48,
                "offset": 3,
                "shift": 24,
                "w": 18,
                "x": 161,
                "y": 52
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "dcdfee59-e535-40ad-a94e-ad57a6056a86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 48,
                "offset": 4,
                "shift": 24,
                "w": 16,
                "x": 143,
                "y": 52
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "b6eb69c0-b9df-4ee3-9a37-8b9176f3e83c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 48,
                "offset": 10,
                "shift": 24,
                "w": 4,
                "x": 137,
                "y": 52
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "06b8a3ed-6f05-4857-9e33-bdf44cbf6227",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 48,
                "offset": 4,
                "shift": 24,
                "w": 16,
                "x": 119,
                "y": 52
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "dae7ffd9-f542-436c-80ae-1705d5a7db19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 48,
                "offset": 2,
                "shift": 24,
                "w": 19,
                "x": 126,
                "y": 202
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "4dd0f455-11be-4b64-9732-dadfbbf549ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 48,
                "offset": 8,
                "shift": 39,
                "w": 23,
                "x": 147,
                "y": 202
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "04356dc0-6a64-4ccd-ad1c-32f8f1fda16e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 39,
            "second": 44
        },
        {
            "id": "fb9e0215-c201-4b56-b4e4-68c8923d1614",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 39,
            "second": 45
        },
        {
            "id": "065782d3-c2e8-495b-ac6c-3e1da137af1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 39,
            "second": 46
        },
        {
            "id": "2652be52-6c01-421f-86f3-014ca9e7de0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 48
        },
        {
            "id": "15f67101-e141-428a-94b8-b420dccfdfff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 49
        },
        {
            "id": "8f010881-b5f3-4344-a5e5-c0952007af8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 50
        },
        {
            "id": "1a0aa7ce-6f90-422d-b9b2-adcd68d3cf6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 51
        },
        {
            "id": "6fc04b2a-05a0-408d-bad0-479ac59f701c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 39,
            "second": 52
        },
        {
            "id": "973aaf5d-477b-4eb4-b009-095c2f2eaded",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 53
        },
        {
            "id": "cfe9f005-b993-4d08-8f49-c320ad7f28c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 54
        },
        {
            "id": "0adff997-41e7-48a2-9b75-98db2be57cdd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 56
        },
        {
            "id": "085f124b-abdc-4aa9-9129-72d6c2ed6ec8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 57
        },
        {
            "id": "c5e646e5-5f92-4de0-a47c-7641bd3fde14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 39,
            "second": 64
        },
        {
            "id": "bf3754f4-bce0-47fa-a617-4332f7f05a21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 39,
            "second": 65
        },
        {
            "id": "8cbce9ac-473c-4696-9ceb-405f83648276",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 67
        },
        {
            "id": "1c7e4b1d-dc2f-47eb-84b3-5ddb0a784c8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 71
        },
        {
            "id": "b525cdac-7b16-49b2-b17a-dec9b449d411",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 79
        },
        {
            "id": "ee6f0ce7-271b-48cd-a4f4-0f9ccb7b9470",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 81
        },
        {
            "id": "1659dc40-1a8e-4dd9-94bd-833f5bc2be90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 83
        },
        {
            "id": "3ee08df0-d06e-4c8d-bee9-7499863f058d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 90
        },
        {
            "id": "73dbc223-2245-4ca5-92c7-aa1ddf6eb999",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 44,
            "second": 39
        },
        {
            "id": "5067bb76-047b-4aad-9349-facf30102737",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 44,
            "second": 46
        },
        {
            "id": "a82f8495-9dbd-4c6a-b66f-97154689313c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 44,
            "second": 48
        },
        {
            "id": "53d3a843-ab5a-4411-8cee-afc433ef855c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 49
        },
        {
            "id": "899c8067-02e7-4a1b-b6e7-e1e7716f8f74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 44,
            "second": 51
        },
        {
            "id": "fc8701ac-c966-4f4f-9bf1-29e624cd724a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 44,
            "second": 52
        },
        {
            "id": "79f42a9e-5e03-4a37-9cef-2962a642b82d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 44,
            "second": 53
        },
        {
            "id": "5dfd935b-241e-4c03-ae45-7ac02432294e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 44,
            "second": 54
        },
        {
            "id": "db4818a5-35e8-4c7f-b028-002557dbd908",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 55
        },
        {
            "id": "6093c643-3245-42fa-9987-31fde9cbcefd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 44,
            "second": 56
        },
        {
            "id": "6eb932d6-9021-49c5-bde4-fc051a55d412",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 57
        },
        {
            "id": "d45ec736-f986-41c0-824f-dd1460dfff07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 44,
            "second": 64
        },
        {
            "id": "bb5a0213-00e0-4659-92b6-34cd32821302",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 44,
            "second": 65
        },
        {
            "id": "0f5fee88-62b4-4985-a6a4-7dfbd4e34e4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 44,
            "second": 69
        },
        {
            "id": "aa04fd88-3898-4855-86ff-945c2132837d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 44,
            "second": 74
        },
        {
            "id": "55dec8c8-2d9c-4d27-8f72-7ce752d7ddbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 44,
            "second": 83
        },
        {
            "id": "354d9b06-9794-4be1-a7eb-c07e71aaa468",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 44,
            "second": 84
        },
        {
            "id": "aeb11623-d520-4d8f-a2bc-d799e33a76e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 44,
            "second": 85
        },
        {
            "id": "30b42cd4-7c2f-4cc1-af9c-61408456211d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 44,
            "second": 86
        },
        {
            "id": "8eab7636-e1f0-4d9a-beb1-86273dfa1f5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 87
        },
        {
            "id": "02ddf4b7-36ce-4ffa-9007-6780cd81853b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 44,
            "second": 89
        },
        {
            "id": "77d50471-4505-4851-ac49-bbc32ded449b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 44,
            "second": 90
        },
        {
            "id": "b66228c2-55c0-4a02-9e1d-a987f1fb4e65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 44,
            "second": 8217
        },
        {
            "id": "d1a6fe0a-6acb-4796-be78-a310734ea5ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 45,
            "second": 39
        },
        {
            "id": "4c6450d0-a5ca-494b-8d94-8be524604226",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 45,
            "second": 44
        },
        {
            "id": "a1a63e25-a886-4dd0-95e0-a7ae6574283f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 45,
            "second": 45
        },
        {
            "id": "e86e2f1b-89a4-42ad-a1c6-6b1490a73154",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 46
        },
        {
            "id": "e1073b3a-da85-4d67-ae6c-d9c20277c662",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 45,
            "second": 48
        },
        {
            "id": "a46ebe7b-b3f1-4126-9ab3-a9d8b41f25c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 45,
            "second": 49
        },
        {
            "id": "daae020f-5389-428a-9c37-dd059f0d2f6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 45,
            "second": 50
        },
        {
            "id": "55fb8162-724a-44cc-955f-96238a7ceb81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 51
        },
        {
            "id": "b389a67a-89ae-4c67-8d13-37a0ad86be0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 45,
            "second": 52
        },
        {
            "id": "6cb2351e-ba37-4599-833d-0d3e088ccb6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 45,
            "second": 54
        },
        {
            "id": "0494c494-1249-4209-b3cb-c5fca2f4173b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 45,
            "second": 55
        },
        {
            "id": "0d20ddd3-ba8e-419f-84ed-f027d8a2df01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 45,
            "second": 56
        },
        {
            "id": "a89bfbef-ee62-45e5-bc71-4d76b726daf2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 45,
            "second": 57
        },
        {
            "id": "c7cbd82a-7f71-4316-9f97-b979e298f1c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 45,
            "second": 64
        },
        {
            "id": "5cd4e319-d482-4881-aae1-38a450eeaca3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 65
        },
        {
            "id": "4a3c3088-1ea9-43f5-9c91-77356584e027",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 45,
            "second": 67
        },
        {
            "id": "3a9954f3-705d-4cdd-bb6b-59d33fe11fb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 68
        },
        {
            "id": "f331da5d-a119-4df2-969b-f0c97364a180",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 69
        },
        {
            "id": "f41de8dc-e776-40d1-ad57-701fa8136b64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 70
        },
        {
            "id": "fdd4d4c0-4cef-43f8-bdc6-fe27f0e6e88e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 45,
            "second": 71
        },
        {
            "id": "6329290c-ce6d-4282-9538-e8da519fc850",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 74
        },
        {
            "id": "bae66bae-f6bc-4aab-8dcc-55db79a78c8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 45,
            "second": 79
        },
        {
            "id": "58aefb9c-5e33-4b44-bf22-111e7119d74b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 80
        },
        {
            "id": "280cc517-c0da-420c-beeb-951da75362e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 45,
            "second": 81
        },
        {
            "id": "37fecbf0-46e6-45ef-87dc-03e43a4719a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 45,
            "second": 84
        },
        {
            "id": "5a7cbdd3-ee23-470c-a306-8440f9ad1b42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 45,
            "second": 86
        },
        {
            "id": "9688abf8-c64a-4d6b-9dde-f60fb12da112",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 87
        },
        {
            "id": "5af3571c-ba1b-4d43-872b-4c9efd1cd454",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 45,
            "second": 88
        },
        {
            "id": "d355d974-966a-4ede-b07e-59e7bec1f240",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 45,
            "second": 89
        },
        {
            "id": "2685a7d7-a2ac-48fe-8813-04aa59ba595f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 90
        },
        {
            "id": "80c5768b-144d-49da-a6ba-968cee9fbe89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 45,
            "second": 8217
        },
        {
            "id": "7834e858-b6b1-47a1-a9dd-8010b186f96e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 46,
            "second": 39
        },
        {
            "id": "8e2d7e9f-b8cd-4529-82de-c4c3247463bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 46,
            "second": 46
        },
        {
            "id": "0284cae8-7bc4-4fbc-b99d-9cce76fd5424",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 49
        },
        {
            "id": "0fb5c4eb-f639-43d9-929f-0357c7141755",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 46,
            "second": 51
        },
        {
            "id": "9ef10a9f-23b1-4d15-ab52-9538432d4f3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 55
        },
        {
            "id": "b3a41c9c-6a7e-4499-9604-f1b2389abf6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 46,
            "second": 56
        },
        {
            "id": "7be9e3e3-3b25-4475-9912-785727f641e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 57
        },
        {
            "id": "a89e24cd-685c-4d3c-a9ae-746c07db26bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 46,
            "second": 64
        },
        {
            "id": "3edeb88f-6d8d-4422-856f-e32227aa5383",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 46,
            "second": 65
        },
        {
            "id": "b1c090a3-036b-4ad8-ad74-4b67ea9e96f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 67
        },
        {
            "id": "800006fc-fa4d-450f-ac74-f12c28bdc8d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 69
        },
        {
            "id": "11462a93-e925-4407-93fd-4f1e65d072d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 71
        },
        {
            "id": "4f525abc-4a49-4dbf-bedc-fc2c691a62e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 46,
            "second": 74
        },
        {
            "id": "c1cb6d88-53dd-4382-b607-2950af509cf0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 79
        },
        {
            "id": "90b6736b-4313-4f13-b3c6-fa7591e051d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 46,
            "second": 84
        },
        {
            "id": "7546c87d-c21b-4a71-a42d-382199cd78ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 46,
            "second": 85
        },
        {
            "id": "e574a78f-3d4f-416c-8f82-59a2dc6e0ab6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 46,
            "second": 86
        },
        {
            "id": "8c180665-6651-4e5c-a27e-2bb919221e2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 46,
            "second": 87
        },
        {
            "id": "0edc3afb-acbf-47af-8635-35cde9cf735e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 46,
            "second": 89
        },
        {
            "id": "faeb7b94-9aee-44a9-889b-bf497621f66f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 46,
            "second": 90
        },
        {
            "id": "0ba14e31-03ad-41bc-9ccd-a4a2b9b6e807",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 46,
            "second": 8217
        },
        {
            "id": "7bb7f1d5-f24b-43f2-8f50-396b9b16f94d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 48,
            "second": 44
        },
        {
            "id": "85324649-7fc2-4afb-869a-4b432f174586",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 48,
            "second": 46
        },
        {
            "id": "949de886-0db6-4b7e-88d8-a4051ddaa0c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 64
        },
        {
            "id": "02d91db7-62e4-4e74-8c26-cea062f59163",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 48,
            "second": 65
        },
        {
            "id": "286bf6e7-5bb3-4956-9e7f-e11cdd0243c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 48,
            "second": 66
        },
        {
            "id": "368fec6a-8862-4103-975f-2fa0bf2cfcd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 48,
            "second": 68
        },
        {
            "id": "7f844066-3420-4e86-88e1-4222568ea835",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 48,
            "second": 69
        },
        {
            "id": "7c038d42-8a68-4422-9a98-dd6bca7a868e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 48,
            "second": 70
        },
        {
            "id": "2eb0dc4f-7c6c-4119-b829-1b938071274f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 48,
            "second": 72
        },
        {
            "id": "d18b633b-6d92-4872-bfdb-6fe054a7a9f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 48,
            "second": 73
        },
        {
            "id": "eabfe7c9-80cb-46e1-ab9a-ba66018ec284",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 48,
            "second": 74
        },
        {
            "id": "9d7d0fdc-99b6-41d0-8924-f2afb8418b25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 48,
            "second": 75
        },
        {
            "id": "e8e1290f-be52-420d-ada7-1bac1c02a2f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 48,
            "second": 76
        },
        {
            "id": "c58a38df-1242-4701-b54a-09399e92883d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 48,
            "second": 77
        },
        {
            "id": "6a98866d-e04e-4c68-af1d-deb996355ce3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 48,
            "second": 78
        },
        {
            "id": "a6403ac4-f1e3-4c38-9817-30d6a195f7cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 48,
            "second": 79
        },
        {
            "id": "0fda4ca6-eabe-4c35-99b5-6878e72ff233",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 48,
            "second": 80
        },
        {
            "id": "caeb6682-5cfe-4cdc-b4d3-9d8b04f73650",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 48,
            "second": 82
        },
        {
            "id": "0bc730bf-4a7c-4683-afa4-a140f3041e1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 48,
            "second": 84
        },
        {
            "id": "9aaeaf43-39e4-4cc3-9026-460cd95d6e52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 48,
            "second": 85
        },
        {
            "id": "f03db127-b296-4d5c-a351-3bfd68de112d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 48,
            "second": 86
        },
        {
            "id": "0d5e092d-3287-46e5-a7a8-1fbbb3f3ca07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 48,
            "second": 87
        },
        {
            "id": "8ad1e0e6-2b1d-4852-81db-287f2afd085b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 48,
            "second": 88
        },
        {
            "id": "18a500d7-5c80-4048-a62b-43dacfd79b74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 48,
            "second": 89
        },
        {
            "id": "343f3021-129f-45d4-8383-966aaf5e6900",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 48,
            "second": 90
        },
        {
            "id": "23f0945c-80b0-4796-aeb0-1c504c389588",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 49,
            "second": 39
        },
        {
            "id": "5d8eaab6-2b7e-4dd0-952f-6e1b22a4a525",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 49,
            "second": 44
        },
        {
            "id": "6a710d2b-39a7-48ce-a21e-a4fee56e4a15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 49,
            "second": 45
        },
        {
            "id": "be68ca00-4692-4a5e-a6fd-5a4d90668975",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 49,
            "second": 46
        },
        {
            "id": "fb83f8e2-5be8-4ea6-8230-7f79aeb9ed7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 49,
            "second": 64
        },
        {
            "id": "2fefe2ec-2193-40ee-ab18-ecaf50f68d75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 49,
            "second": 66
        },
        {
            "id": "0a61f028-e6fb-4d61-a922-0b18269211dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 49,
            "second": 67
        },
        {
            "id": "468a8d80-0429-46d3-a4a2-461091ce7537",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 49,
            "second": 68
        },
        {
            "id": "645f32ed-e2a4-454b-84db-14ac920123c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 49,
            "second": 69
        },
        {
            "id": "db8bcf70-0d77-4205-b6f9-3943c9190e18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 49,
            "second": 70
        },
        {
            "id": "e8959d6f-6c5f-40ef-a68e-ae89b3b4cb30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 49,
            "second": 71
        },
        {
            "id": "68bcca99-bbe0-4f3b-96bd-bb30c96a8906",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 49,
            "second": 72
        },
        {
            "id": "b1f454e7-7675-4e63-82f2-3230c85d07b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 49,
            "second": 73
        },
        {
            "id": "b8c00418-7411-421f-904a-033e2d8fc69c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 49,
            "second": 74
        },
        {
            "id": "bababfbe-7319-4dee-a926-d2585fdb5cf8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 49,
            "second": 75
        },
        {
            "id": "299da4eb-b380-4ae4-b50c-4f0b86d2ebec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 49,
            "second": 76
        },
        {
            "id": "caa36c84-042d-402d-a25b-8bfc46dd05c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 49,
            "second": 77
        },
        {
            "id": "8a438882-133c-42c0-b245-d3559014a99c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 49,
            "second": 78
        },
        {
            "id": "d6148ea8-9d97-4458-a030-6042e14baa74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 49,
            "second": 79
        },
        {
            "id": "69255a47-615e-44e6-9163-64ac3dd5ed54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 49,
            "second": 80
        },
        {
            "id": "4f65689a-cc77-4cb8-a16c-ce0fb3bd8343",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 49,
            "second": 81
        },
        {
            "id": "3291227e-e55b-44a4-aeb9-ba9c719cdf4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 49,
            "second": 82
        },
        {
            "id": "c32fdda5-c091-4b9b-a55d-5cfc78e3f27a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 49,
            "second": 83
        },
        {
            "id": "bfd96245-c944-4800-8eb6-2231668534cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 49,
            "second": 84
        },
        {
            "id": "3b256ee5-53b0-4cc6-9363-08c14a5d7968",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 49,
            "second": 85
        },
        {
            "id": "3ad6deb4-8c0f-41c0-9450-ac0eb7fae7ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 49,
            "second": 86
        },
        {
            "id": "211e1f0e-213d-4124-8966-1ead52b47314",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 49,
            "second": 87
        },
        {
            "id": "7ba747cb-bcc1-48b7-b6ff-cdf679efed95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 49,
            "second": 88
        },
        {
            "id": "1edc9c76-36ca-41ad-b204-c0d80300cbb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 49,
            "second": 89
        },
        {
            "id": "5132f81e-4e18-4856-9316-dd933e918a3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 49,
            "second": 90
        },
        {
            "id": "03feefd1-aa4d-4c62-836a-ffcfd4038067",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 49,
            "second": 8217
        },
        {
            "id": "775a0e38-cd91-4bf5-93a0-50276761b309",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 50,
            "second": 39
        },
        {
            "id": "ee9af13e-f438-40e0-a36e-4f7cdfbddd2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 44
        },
        {
            "id": "7f3d7d33-3877-4d64-8d29-5c046f905c7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 50,
            "second": 45
        },
        {
            "id": "58f17a5d-91fd-4b3d-922a-219d1f4cd2ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 46
        },
        {
            "id": "7038ec0e-14a3-47cd-9956-0e3728b1d851",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 50,
            "second": 64
        },
        {
            "id": "4a6c846b-f190-4ace-805d-c52938dfa5f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 50,
            "second": 66
        },
        {
            "id": "81440061-bcc6-45cf-ae92-7569c746f80d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 50,
            "second": 67
        },
        {
            "id": "a11528e0-ed94-4da6-bca8-e65480517dfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 50,
            "second": 68
        },
        {
            "id": "1e8a85f3-4510-4c2f-ae77-3462ef50194e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 50,
            "second": 69
        },
        {
            "id": "316c35dd-6f25-4ebd-ba46-3f7d9e7fe607",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 50,
            "second": 70
        },
        {
            "id": "559d33ee-fe30-4401-b308-d8d8d372438e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 50,
            "second": 71
        },
        {
            "id": "377f0942-7dda-476e-aaed-186ad3ff94a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 50,
            "second": 72
        },
        {
            "id": "d909033e-a56e-4545-8a7c-a9d462b5129e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 50,
            "second": 73
        },
        {
            "id": "73b6d2ed-512b-4e26-995f-c1394801bb39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 50,
            "second": 74
        },
        {
            "id": "0996e793-0ff4-4e9f-826c-73da2832ea7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 50,
            "second": 75
        },
        {
            "id": "bf24765e-93b9-4fad-9a3e-83fd5ff7f8ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 50,
            "second": 76
        },
        {
            "id": "20b1caa8-f288-4980-814e-5284dad12c90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 50,
            "second": 77
        },
        {
            "id": "8e678b0c-3e95-4629-ac1a-658dd6748f51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 50,
            "second": 78
        },
        {
            "id": "1e72c478-a5fb-48ba-a36d-afbdd22ef65a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 50,
            "second": 79
        },
        {
            "id": "11771d49-5efc-4a1d-8ea4-5ad37a080cb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 50,
            "second": 80
        },
        {
            "id": "c7ade501-e1cc-4324-a8e5-ca368f21dfa2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 50,
            "second": 81
        },
        {
            "id": "f1a00be5-0d8e-4310-9323-b266b4dd4f1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 82
        },
        {
            "id": "b8609998-b7f4-4bf9-aef4-efa9376aa10d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 50,
            "second": 83
        },
        {
            "id": "92c36afe-e846-4075-85d9-372e7ca6db4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 50,
            "second": 84
        },
        {
            "id": "9a661c9f-f605-44b0-9e73-e5307e66c869",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 50,
            "second": 85
        },
        {
            "id": "e2004ff1-264d-4e93-94bc-2d804098e1ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 50,
            "second": 86
        },
        {
            "id": "5a702475-0bc1-46b5-96af-cfc294242959",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 50,
            "second": 87
        },
        {
            "id": "0804e461-0017-4a25-bb40-bbd33abc5201",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 50,
            "second": 88
        },
        {
            "id": "fb116eb0-9434-4f0c-a9ac-461a5cde6351",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 50,
            "second": 89
        },
        {
            "id": "0c581215-9195-4a96-b1a4-d89b54414f58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 50,
            "second": 90
        },
        {
            "id": "cbd0573a-4101-456a-b99d-bd7335778d03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 50,
            "second": 8217
        },
        {
            "id": "32a369be-f504-45eb-9b89-4bd515f69925",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 51,
            "second": 39
        },
        {
            "id": "ec6bc025-7dd4-4e5d-9c3e-703e75b89154",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 51,
            "second": 44
        },
        {
            "id": "1ca26b51-c7c0-40aa-abe3-6f8d0bf4dba9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 51,
            "second": 46
        },
        {
            "id": "d05036d1-19c1-473d-b923-690d709e6819",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 51,
            "second": 64
        },
        {
            "id": "6cb4afe1-86c5-48af-a41b-cec05df62677",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 51,
            "second": 65
        },
        {
            "id": "96648af7-595e-4b18-bfd1-2d0fe6b60e74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 51,
            "second": 66
        },
        {
            "id": "c8f55cbb-e1fc-4aa3-a425-8eec56a546f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 51,
            "second": 67
        },
        {
            "id": "bcd63213-3247-4ca8-b32e-d3ff570f59f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 51,
            "second": 68
        },
        {
            "id": "2693de46-753c-48da-8c3d-db17667a295d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 51,
            "second": 69
        },
        {
            "id": "62a75a3a-bbfa-4db2-8fbc-e2821c49bbc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 51,
            "second": 70
        },
        {
            "id": "397f39b5-7388-4bd1-990a-199eecf2fa1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 71
        },
        {
            "id": "13ae8733-d2de-46d0-a96f-ca150cff83b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 51,
            "second": 72
        },
        {
            "id": "cfc81904-669b-4222-b9b1-7645255dcbb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 51,
            "second": 73
        },
        {
            "id": "0ccc849a-ced3-4564-a1cb-c0005db5f587",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 51,
            "second": 74
        },
        {
            "id": "400448dc-19b5-4886-903b-76bcd8f47559",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 51,
            "second": 75
        },
        {
            "id": "913bbc67-d805-4e21-9049-60217eb9abfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 51,
            "second": 76
        },
        {
            "id": "79daa31e-be35-4fe6-acfa-af6311135606",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 51,
            "second": 77
        },
        {
            "id": "2bc9d500-66b6-4622-bd99-6a9b6842afac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 51,
            "second": 78
        },
        {
            "id": "fda733d3-64a2-444d-bdd3-8d4cdb930652",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 51,
            "second": 79
        },
        {
            "id": "ba4de231-40f8-4fd1-aff9-e5032e1da6a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 51,
            "second": 80
        },
        {
            "id": "fca77193-7c9c-46b6-a1c1-e5f6a5e22877",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 51,
            "second": 81
        },
        {
            "id": "4f8691fc-797d-4712-ae4b-139c81d268e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 51,
            "second": 82
        },
        {
            "id": "69996c71-a108-4c9d-be7e-8978dbe31726",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 51,
            "second": 83
        },
        {
            "id": "4e63896c-f655-45cf-9695-737d15d8c40d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 51,
            "second": 84
        },
        {
            "id": "e3641756-2628-4599-b04a-776413cb02b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 51,
            "second": 85
        },
        {
            "id": "22dfd7d8-58cd-4c47-9a39-46dc356d3032",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 51,
            "second": 86
        },
        {
            "id": "dda33a2f-7343-42aa-aa03-f2f4b06c9499",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 51,
            "second": 87
        },
        {
            "id": "7db3b4bb-daad-422e-807b-894b8649df99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 51,
            "second": 88
        },
        {
            "id": "ad1ba46d-a511-4a75-97f2-bfd90fc4726b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 51,
            "second": 89
        },
        {
            "id": "b397d46f-1f10-40b9-adf6-c5c455720811",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 51,
            "second": 90
        },
        {
            "id": "a65a2d12-8277-46f8-968c-c63201ec16a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 51,
            "second": 8217
        },
        {
            "id": "7e6f8668-c95f-46f6-9c9d-9fc4ebfd0419",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 52,
            "second": 39
        },
        {
            "id": "40f72a15-ea53-4050-b76f-56ef296abdbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 52,
            "second": 64
        },
        {
            "id": "1435e06d-beee-48ae-a4f7-918cb560d219",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 52,
            "second": 65
        },
        {
            "id": "3afa5391-7e4b-4f25-8bd2-2bf7c431a7ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 67
        },
        {
            "id": "f845a981-358b-463e-9872-74a9b5df27aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 69
        },
        {
            "id": "702ca187-e062-4267-bfe4-3a41a0fbac75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 71
        },
        {
            "id": "b6f02c4a-1f8e-4c29-9410-afa342792ccf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 52,
            "second": 74
        },
        {
            "id": "bd8643e6-47f0-4bcf-b847-d2b664f42fb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 79
        },
        {
            "id": "5d16c5e8-af84-4263-bb3b-3c037f92038f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 81
        },
        {
            "id": "449923a4-78e7-4d66-b9dc-068882d752ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 52,
            "second": 84
        },
        {
            "id": "b4820348-2c88-4260-9acc-89d2300c9e49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 52,
            "second": 85
        },
        {
            "id": "0b439e64-9dd3-401c-8fce-088516019b5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 52,
            "second": 86
        },
        {
            "id": "4cb90eb0-d380-4aee-9bf7-c7bc387726d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 52,
            "second": 87
        },
        {
            "id": "f3b75b57-8438-4d1f-906f-8f3ae22c83b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 52,
            "second": 88
        },
        {
            "id": "c9c65637-1f23-4e5e-9453-68a89f63e6aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 52,
            "second": 89
        },
        {
            "id": "60e7b155-09d5-4a9d-8a5f-019fa6a4f2a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 52,
            "second": 8217
        },
        {
            "id": "4d2dd17a-3f9c-4446-8719-33f7905e6d33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 53,
            "second": 39
        },
        {
            "id": "c09da5e1-0266-4da8-85c8-b152b8c41b61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 53,
            "second": 44
        },
        {
            "id": "2ea0fe31-79a2-4781-a063-55f524e38a97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 53,
            "second": 45
        },
        {
            "id": "87b78064-7ff6-4283-9836-21152c180bd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 53,
            "second": 46
        },
        {
            "id": "40a36233-ec11-4c4f-ae0d-bdff6613bc27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 53,
            "second": 64
        },
        {
            "id": "a5164180-a5a1-4eae-abeb-b0bb2ee903b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 53,
            "second": 65
        },
        {
            "id": "820905d7-5db7-4a2b-9f9d-4e4148a6b9ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 53,
            "second": 66
        },
        {
            "id": "b5651115-9749-462f-a4b3-564f90a25d90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 53,
            "second": 67
        },
        {
            "id": "e85fa5b9-0104-4c7f-91ad-c3e2ec006873",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 53,
            "second": 68
        },
        {
            "id": "18017f1d-a566-4fa4-a3cc-756885d53d93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 53,
            "second": 69
        },
        {
            "id": "bdfaea95-671a-4bc1-83b2-e5cddba980e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 53,
            "second": 70
        },
        {
            "id": "d81b0c12-c84d-459f-92ce-4ba88c7dfc66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 53,
            "second": 71
        },
        {
            "id": "8eddefa4-ae1f-451f-86fa-a7d8840e7bab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 53,
            "second": 72
        },
        {
            "id": "318f4440-9825-455c-9684-b76765ade873",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 53,
            "second": 73
        },
        {
            "id": "38b5fa3e-5776-4993-bc9c-977bd859ed68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 53,
            "second": 74
        },
        {
            "id": "59e5480b-cffa-4507-9207-2aa99c1a7be8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 53,
            "second": 75
        },
        {
            "id": "329b5e78-3ffd-4c00-81e3-fadf845ef2ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 53,
            "second": 76
        },
        {
            "id": "c35d354c-7058-48e8-948b-321569014d79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 53,
            "second": 77
        },
        {
            "id": "82d4cf56-262d-4bb5-a71f-4dfd8dbc0331",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 53,
            "second": 78
        },
        {
            "id": "a8e25135-03bd-4955-8e74-3b8a50c3d0ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 53,
            "second": 79
        },
        {
            "id": "bbf0efc7-0bb3-443e-b29f-ba65984c7eff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 53,
            "second": 80
        },
        {
            "id": "67f3fe01-b08e-4152-99fb-e48488b95944",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 53,
            "second": 81
        },
        {
            "id": "f7806f59-5ade-4a22-a523-93674e4d82de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 53,
            "second": 82
        },
        {
            "id": "84690e77-cc3d-4d8a-a780-7de357aac153",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 53,
            "second": 83
        },
        {
            "id": "44a184e2-fa76-4906-b6d8-7750a9a00e17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 53,
            "second": 84
        },
        {
            "id": "1cc13a5f-a324-4bc5-85ff-5c677f1d2b8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 53,
            "second": 85
        },
        {
            "id": "57e04dd4-0e50-4d2b-89a5-8693588d18f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 53,
            "second": 86
        },
        {
            "id": "b9788d36-fe0f-4ab9-b5e0-7e2119c996e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 53,
            "second": 87
        },
        {
            "id": "ac47ce54-66b5-495b-8506-0d3c17865a06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 53,
            "second": 88
        },
        {
            "id": "b7d0c991-0e2f-446a-bcb8-23dbff207881",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 53,
            "second": 89
        },
        {
            "id": "1b55b9ea-25b1-4813-9154-f100a414664a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 53,
            "second": 90
        },
        {
            "id": "0144a143-0ab9-49e1-9d3b-7d1c6bb37162",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 53,
            "second": 8217
        },
        {
            "id": "19db7b0a-9e31-4f31-9433-49d49772d6fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 54,
            "second": 39
        },
        {
            "id": "4d4db595-b1a8-44f6-961f-3b2a9269faea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 54,
            "second": 44
        },
        {
            "id": "bdb24912-6b7f-47a8-a465-6a8c3ef1f897",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 46
        },
        {
            "id": "acb6ce9a-3b9a-410b-a748-10ca7f87bf93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 54,
            "second": 64
        },
        {
            "id": "2a9ec51f-7869-4f1a-9938-3bc464764269",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 54,
            "second": 65
        },
        {
            "id": "3b1a5e4b-ce39-418a-8ea5-b2c92df46de6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 54,
            "second": 66
        },
        {
            "id": "2693030c-2aef-443e-80a1-203f97524468",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 67
        },
        {
            "id": "72805343-7877-4b35-b249-0ad10e7cc5fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 54,
            "second": 68
        },
        {
            "id": "be664724-93f3-4e0c-bc3d-9f0224744294",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 54,
            "second": 69
        },
        {
            "id": "57d9a176-bd0d-4355-8d4d-aba7abbb282c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 54,
            "second": 70
        },
        {
            "id": "91538201-2cb9-4c9c-8e89-7030b59b764b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 71
        },
        {
            "id": "dc1d7826-10cf-4671-bfcb-dfbc4345c5d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 54,
            "second": 72
        },
        {
            "id": "8bf375ec-8778-42d2-8fa8-a502ec91647f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 54,
            "second": 73
        },
        {
            "id": "a7441cf0-a63c-42f5-8d8f-32f74409b23e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 54,
            "second": 74
        },
        {
            "id": "be90414f-8a40-4932-a253-061e2b0658e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 54,
            "second": 75
        },
        {
            "id": "855e1f79-5362-4f8c-a7e8-40202bddac1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 54,
            "second": 76
        },
        {
            "id": "2f425938-ac24-46d3-8c5f-c3c187318297",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 54,
            "second": 77
        },
        {
            "id": "3f8bc7d2-579d-40d7-a877-9f68ab6da176",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 54,
            "second": 78
        },
        {
            "id": "d6a05dd7-c55e-4b21-b4c4-afd2ef341744",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 54,
            "second": 79
        },
        {
            "id": "cd9c7632-3ec3-47ac-aca1-3536d6220bae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 54,
            "second": 80
        },
        {
            "id": "76f066f6-ec48-46e6-b0fe-4552d5679f9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 54,
            "second": 81
        },
        {
            "id": "f9402d6f-95a2-4068-92b5-80780dfc7c06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 54,
            "second": 82
        },
        {
            "id": "150f727f-945e-4423-b9f5-b97716a1c382",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 54,
            "second": 83
        },
        {
            "id": "8583681c-eb22-4f84-a793-371cf9c46c71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 54,
            "second": 84
        },
        {
            "id": "f9be0804-53fd-4faa-896c-57008ef3188e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 54,
            "second": 85
        },
        {
            "id": "c2e69642-340e-4d3e-9ae0-655a8283bfcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 54,
            "second": 86
        },
        {
            "id": "50b8a649-448e-4116-ad0f-0e4e7de5d035",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 54,
            "second": 87
        },
        {
            "id": "04d2fc90-6f62-4bd5-944b-e97fe27861f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 54,
            "second": 88
        },
        {
            "id": "0573ab81-3262-4d1e-a5a2-683a169e6330",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 54,
            "second": 89
        },
        {
            "id": "4b02ed58-83de-4660-bdb4-625624925de9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 54,
            "second": 90
        },
        {
            "id": "85ee8dd3-acf8-4757-ac85-130535adffff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 54,
            "second": 8217
        },
        {
            "id": "aa89c8ca-9534-4029-ac11-18a128651432",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 39
        },
        {
            "id": "c1d0a408-9f57-43dc-9b1a-8a125139de9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -11,
            "first": 55,
            "second": 44
        },
        {
            "id": "b0a0cdd5-274e-47d4-9ae8-0c26425e94eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 55,
            "second": 45
        },
        {
            "id": "9b0a3112-2b46-4510-9c4c-6911e6e402d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 55,
            "second": 46
        },
        {
            "id": "ce7f4fc8-38cc-4780-b626-5eb388ef443d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 55,
            "second": 64
        },
        {
            "id": "f0ac701f-0c7e-46c5-9dee-f0f29e72ece0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 55,
            "second": 65
        },
        {
            "id": "58c8788a-f390-45e1-aedb-f9512451a54f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 66
        },
        {
            "id": "b82b8163-19dd-4713-9aff-2cc6cab0045c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 55,
            "second": 67
        },
        {
            "id": "c2f48417-0957-41b7-ad12-727abe2e9b6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 55,
            "second": 68
        },
        {
            "id": "4c942249-4d55-4c8e-9927-9e8e9be036ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 55,
            "second": 69
        },
        {
            "id": "0356b6a7-ff2f-4b74-bf57-e697a99470d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 55,
            "second": 70
        },
        {
            "id": "784a049b-634a-4da9-9fa5-18bbd469acd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 55,
            "second": 71
        },
        {
            "id": "9811a179-78a1-4050-bfd2-c5d68de1f3c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 72
        },
        {
            "id": "87a0bd42-79c5-428c-a4ed-85fa19dfae09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 73
        },
        {
            "id": "4603e6c7-c6a4-4d6b-9c16-20b76afc9205",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 55,
            "second": 74
        },
        {
            "id": "0525ef90-8926-4969-83f5-21aee0be3a8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 75
        },
        {
            "id": "d25e3f3e-7b75-406b-a5ee-e6bb01eca118",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 76
        },
        {
            "id": "9c830ad7-ffc7-424e-bbf0-64608074dcdc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 77
        },
        {
            "id": "33988e3f-6a4d-451e-bfdb-2e583aa843d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 78
        },
        {
            "id": "193c0386-92cd-4b5e-8f10-a6b986a5c804",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 55,
            "second": 79
        },
        {
            "id": "1bbeee20-95ec-4f19-9d57-6772c815a60a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 80
        },
        {
            "id": "4f0eaef1-beb7-48ea-9652-f7f02bebabaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 55,
            "second": 81
        },
        {
            "id": "c4aa916f-15bb-4e52-a96e-581138314cbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 82
        },
        {
            "id": "5cd28ae9-bbba-4b91-8861-3212377be49c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 55,
            "second": 83
        },
        {
            "id": "af93e75d-d9a4-434a-97a1-53a7214a6900",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 55,
            "second": 84
        },
        {
            "id": "dc16265c-31ed-4589-a396-6d9237fe97f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 85
        },
        {
            "id": "1c334267-7ec0-4868-bd46-7964812083de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 55,
            "second": 86
        },
        {
            "id": "9cbe6ae1-94c4-4a3e-b06f-c580ba95d15e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 55,
            "second": 87
        },
        {
            "id": "19a4e4ea-1463-4a6a-8e79-b83b2bd7e9a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 55,
            "second": 88
        },
        {
            "id": "6baccc88-f9ca-46f4-8778-4a3c6284b7db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 55,
            "second": 89
        },
        {
            "id": "48321aa7-9fc5-411c-a42f-3adf75a8647a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 55,
            "second": 90
        },
        {
            "id": "159f38d2-bc36-43cc-84bf-88a7645e1bec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 8217
        },
        {
            "id": "d219ed80-0e5c-4695-bee1-37d4363a37f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 56,
            "second": 39
        },
        {
            "id": "0807975d-e25f-4c07-ad36-f6b73cbd835d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 56,
            "second": 44
        },
        {
            "id": "f54a2709-5eff-40af-b9c6-4c3d6d01112e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 46
        },
        {
            "id": "a8908132-3969-469b-b6f1-f06692f474e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 56,
            "second": 64
        },
        {
            "id": "1dcdb56a-475c-43f0-a8b8-2517b228a5be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 56,
            "second": 65
        },
        {
            "id": "9db3c707-6f49-48bc-a934-1f2f16c4cd5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 56,
            "second": 66
        },
        {
            "id": "9750f29d-ac3b-479e-8623-415a9c60fd4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 56,
            "second": 67
        },
        {
            "id": "84829a24-668a-4181-8a17-f0905ee5503f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 56,
            "second": 68
        },
        {
            "id": "ff72e4f3-66a9-441c-8d03-60a539406203",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 56,
            "second": 69
        },
        {
            "id": "1c96b508-586b-4eef-b6be-3bdb9f975509",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 56,
            "second": 70
        },
        {
            "id": "287b3794-46a4-49bf-91a6-bc3db2f01651",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 71
        },
        {
            "id": "2215336e-6b77-4124-afe3-aba919757dab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 56,
            "second": 72
        },
        {
            "id": "eec716d7-d4bb-44ba-b876-d80b3f74da28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 56,
            "second": 73
        },
        {
            "id": "cd99fc4d-c9bc-4b7e-82a1-0b71d9e5da22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 56,
            "second": 74
        },
        {
            "id": "d6941f69-e7e4-49f1-94c2-c1021f9d7dcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 56,
            "second": 75
        },
        {
            "id": "a9ccee6a-561f-421a-8e0a-eb2c0527eada",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 56,
            "second": 76
        },
        {
            "id": "54abf717-c555-4705-af9c-52b00d510cd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 56,
            "second": 77
        },
        {
            "id": "d6ca667b-8420-4353-af77-d72dc1f30f15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 56,
            "second": 78
        },
        {
            "id": "0ea8ad8e-5a86-4b4f-adc3-023dae271ee6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 56,
            "second": 79
        },
        {
            "id": "cd9e67d6-572f-446b-8880-359b52458bd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 56,
            "second": 80
        },
        {
            "id": "05423d7c-dbeb-4c7f-b088-0186eb0a2257",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 56,
            "second": 81
        },
        {
            "id": "90682108-1de9-437e-a597-5accce3ca6db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 56,
            "second": 82
        },
        {
            "id": "a0fcf035-4d41-4937-b80b-9d48e851173a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 56,
            "second": 83
        },
        {
            "id": "3e2e371c-e2e9-4072-bf07-ff508e7fb9a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 56,
            "second": 84
        },
        {
            "id": "75cb3a99-8178-43d6-9621-7b8beeb7c61e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 56,
            "second": 85
        },
        {
            "id": "62a7fb15-489c-42ec-9e6e-f385e0215cf5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 56,
            "second": 86
        },
        {
            "id": "9eccb814-0a53-4c5a-a4b1-bd808f1adcee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 56,
            "second": 87
        },
        {
            "id": "c76581df-c089-4b46-b0d3-3e0d3ac5b2d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 56,
            "second": 88
        },
        {
            "id": "3f2a4f0e-37b8-4be8-b4cc-4fe4cc54c92f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 56,
            "second": 89
        },
        {
            "id": "01ca4163-20b9-4d4b-9b1f-e5d9cd3236b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 56,
            "second": 90
        },
        {
            "id": "bde6f377-8243-4407-bd59-bb3fdb5c2c14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 56,
            "second": 8217
        },
        {
            "id": "b93aa5dd-eecb-4925-8c60-8d60be825a6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 57,
            "second": 44
        },
        {
            "id": "039be30f-1e71-47fe-9f84-e3057dadbd4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 45
        },
        {
            "id": "1f18fa0d-ee20-421e-87c0-7e1a43b0c225",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 57,
            "second": 46
        },
        {
            "id": "31a829e7-0d8a-46cc-999a-418c743fbe87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 57,
            "second": 64
        },
        {
            "id": "5a5aff0b-370a-4ee4-808d-f59df4376f13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 57,
            "second": 65
        },
        {
            "id": "9fd5d0c5-f7f9-41db-8633-762632a65eca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 57,
            "second": 66
        },
        {
            "id": "023640cd-88e2-4590-86b9-ef0fba82908b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 67
        },
        {
            "id": "8a527114-35e0-4817-a857-02a247d0dc0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 57,
            "second": 68
        },
        {
            "id": "945873fa-db0a-476b-93f7-3163813b8a40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 57,
            "second": 69
        },
        {
            "id": "313a57fe-d156-45e9-8a2a-258437b9caea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 57,
            "second": 70
        },
        {
            "id": "71aca450-48bd-4f05-b8ca-2bc30398f664",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 57,
            "second": 72
        },
        {
            "id": "7edbcc72-0606-4510-a639-ab80f5ab5882",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 57,
            "second": 73
        },
        {
            "id": "01ecb576-25fa-4f00-96d1-b1646f403c8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 57,
            "second": 74
        },
        {
            "id": "84598dbe-18d9-4f1a-adab-a6c57d6f52b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 57,
            "second": 75
        },
        {
            "id": "434da89d-8a19-44ad-a4b0-33f056f3471b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 57,
            "second": 76
        },
        {
            "id": "939f92e4-a1cf-4759-92a5-860a6e2f8977",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 57,
            "second": 77
        },
        {
            "id": "6f70645e-29a4-4fed-b611-dfd50421ba36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 57,
            "second": 78
        },
        {
            "id": "86b55afe-7795-42a6-95ed-4ba8b4fb528e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 79
        },
        {
            "id": "fc211f70-6553-486d-b586-09cc30ea7aaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 57,
            "second": 80
        },
        {
            "id": "b087cf96-2969-44db-926b-5f77c2c84800",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 81
        },
        {
            "id": "d3a4db1f-931b-4a1d-8da4-426f339f47d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 57,
            "second": 82
        },
        {
            "id": "2bd30f3e-2a3f-4de2-b756-c855a0717e7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 57,
            "second": 83
        },
        {
            "id": "9e1b78f5-8e9b-490a-8366-8db350979be8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 57,
            "second": 84
        },
        {
            "id": "b380ef9f-031f-4e0d-8f39-4e817bcca101",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 57,
            "second": 85
        },
        {
            "id": "3f1d5b0c-235f-475b-9a8f-569fa3149ac3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 57,
            "second": 86
        },
        {
            "id": "97fd3151-5fb1-48b4-9752-e4598be4ad1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 57,
            "second": 87
        },
        {
            "id": "45b6f818-c9e9-4722-a6d2-cf62c98a515c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 57,
            "second": 88
        },
        {
            "id": "af159a42-d67d-48f2-a19c-c2e44e689bec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 57,
            "second": 89
        },
        {
            "id": "c861efed-a8b1-4bdd-ac5f-ad1d4795e4a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 57,
            "second": 90
        },
        {
            "id": "6fd9f019-24a4-4684-b9c4-1f7557eea758",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 64,
            "second": 39
        },
        {
            "id": "5e705fe6-0d18-4125-a21e-c89da8043e2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 64,
            "second": 44
        },
        {
            "id": "5947b092-1d1f-4fab-be9c-1029c7e826be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 64,
            "second": 45
        },
        {
            "id": "71db7b9b-efbe-44c0-8a00-bf6f10bc1047",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 64,
            "second": 46
        },
        {
            "id": "d1246756-77d1-4043-9b25-b4a60a1ecb27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 64,
            "second": 48
        },
        {
            "id": "22b69448-40a9-4156-aace-98cd8441a989",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 49
        },
        {
            "id": "5a8e9377-d914-49fc-af6d-781c90202f02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 64,
            "second": 51
        },
        {
            "id": "7b52297b-0eaa-49a3-9ca5-125f890b0a45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 64,
            "second": 52
        },
        {
            "id": "53cd4a35-1ebd-41e9-bc30-3081563d00c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 64,
            "second": 54
        },
        {
            "id": "474efa71-514c-420b-9154-86fea7d99e53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 64,
            "second": 56
        },
        {
            "id": "e07e2d0e-c45b-49bd-be81-7808e9dbfc47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 64,
            "second": 57
        },
        {
            "id": "22435466-a47b-4c51-acf5-18bdcd9f0ca1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 65
        },
        {
            "id": "62a06eb6-da0c-498f-9f66-220097681b1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 64,
            "second": 67
        },
        {
            "id": "b2494bf2-c719-4036-abe4-bb0e8c42ea5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 68
        },
        {
            "id": "0a36cbd5-b348-47f3-a2aa-031f1c7dd52f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 70
        },
        {
            "id": "cf332484-268e-4fb6-a7fb-7b73fbe27ad6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 64,
            "second": 71
        },
        {
            "id": "db4e2a1d-3aca-49d2-b5f8-50a8aca249f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 74
        },
        {
            "id": "f8de60f3-58f5-44c8-9997-a02c9d910204",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 64,
            "second": 79
        },
        {
            "id": "d3d4bb73-2f49-4ff7-b53a-18c701b88fb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 64,
            "second": 81
        },
        {
            "id": "593abfae-78e7-4c2a-b89d-7dbb85537180",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 64,
            "second": 83
        },
        {
            "id": "22d31b61-937e-4d66-8874-10f1d81f44a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 64,
            "second": 84
        },
        {
            "id": "be5a298e-c606-4aca-99e3-064f4f73302d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 64,
            "second": 86
        },
        {
            "id": "f35a133e-cd92-485a-b650-cc62373595b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 64,
            "second": 87
        },
        {
            "id": "9e534c8a-b1d2-41d3-87bc-8f756eb885dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 64,
            "second": 88
        },
        {
            "id": "f1edf0f6-757f-45a4-95eb-c06395e66cac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 64,
            "second": 89
        },
        {
            "id": "bee9c07a-9592-4ae0-8d31-c5f993c28b59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 64,
            "second": 90
        },
        {
            "id": "3b5fbe1c-3b80-443a-a46b-281f3e467c5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 64,
            "second": 8217
        },
        {
            "id": "ba230a65-cff9-41ed-826c-f69feef16d83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 65,
            "second": 39
        },
        {
            "id": "e58b32ab-475f-41c9-b338-a7d6d723d8d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 44
        },
        {
            "id": "274d8f80-1ef4-4245-8782-1acdd5b31471",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 45
        },
        {
            "id": "8e017d7a-de35-4035-993b-cd0a773fa9f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 48
        },
        {
            "id": "91f1ad5f-a817-4f2d-9559-c51f51a0aae2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 49
        },
        {
            "id": "37c46dff-c426-42d1-882d-d384382dea8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 51
        },
        {
            "id": "87913002-317c-4988-b9f7-df31aa26c321",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 52
        },
        {
            "id": "b3ab6cb3-a1b7-4ade-b206-11690cb27028",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 53
        },
        {
            "id": "16cdaab4-cdb1-4460-9700-ab036a8236d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 54
        },
        {
            "id": "963c274d-d830-41af-b0ce-ee7442ef6d59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 55
        },
        {
            "id": "5241104c-9442-4d8c-96d5-7cdcc6720369",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 57
        },
        {
            "id": "c45b0866-9013-4727-990d-518cd3c65bd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 65,
            "second": 64
        },
        {
            "id": "bf0578e8-2f2d-4fda-a9f7-5e48a89fc810",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 65,
            "second": 65
        },
        {
            "id": "18a5a35c-e398-458d-854f-8cbdf20f8e85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 65,
            "second": 67
        },
        {
            "id": "5daabac8-887d-46c0-b246-aaf69c0e4be8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 71
        },
        {
            "id": "d5ab4e3d-c4bb-4e62-a0ef-494690fc2689",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 74
        },
        {
            "id": "cdd7f0b1-c8c6-48d3-81ad-2eeaa46702fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 79
        },
        {
            "id": "2da925f1-7351-44a9-911c-978a4cd5885f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 81
        },
        {
            "id": "799f4b36-3e3c-4ab5-9dc7-2699d777bc32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 65,
            "second": 84
        },
        {
            "id": "cce13f16-339d-4ad1-b536-1bf4e9a90127",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 85
        },
        {
            "id": "8990524a-406d-4d1c-99d8-458d432500d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -10,
            "first": 65,
            "second": 86
        },
        {
            "id": "32c41baf-d833-475c-85d0-aef3c27cb9f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 65,
            "second": 87
        },
        {
            "id": "06600f4d-cb25-4d72-8d66-a3450416f248",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 65,
            "second": 89
        },
        {
            "id": "a02ab7a4-2589-43d6-a5a9-95f229f7d29f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 65,
            "second": 8217
        },
        {
            "id": "fec25f6e-7748-46bf-9219-e7358100c1e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 66,
            "second": 45
        },
        {
            "id": "5b4fc9ef-eb76-4d0c-8f79-fd4e7b209af1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 46
        },
        {
            "id": "c76a3cd5-1f3e-4e0b-b5ae-d73592a8107a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 66,
            "second": 48
        },
        {
            "id": "de465fb1-cf6d-46f6-aca4-668510148903",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 51
        },
        {
            "id": "d221ce30-6ad5-4a10-84e6-c2b70aba8b09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 66,
            "second": 52
        },
        {
            "id": "cc6b2750-8a9f-4aa2-a85a-a127f48e1264",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 53
        },
        {
            "id": "048c394d-accd-49e2-acb7-cdacfcf17e12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 66,
            "second": 54
        },
        {
            "id": "b937650b-4cd1-4eed-9e14-31f12c308e11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 66,
            "second": 56
        },
        {
            "id": "e83738b7-e2ae-4f0d-b19c-20a5ec3ba362",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 67
        },
        {
            "id": "1ddf2fc0-b710-4b93-848f-26847c5a4989",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 66,
            "second": 71
        },
        {
            "id": "9f7a5eae-372e-4f7c-b67e-d114848b127b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 74
        },
        {
            "id": "c1003f2d-d1fd-453c-8d00-26280820a5b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 79
        },
        {
            "id": "d61ba261-f630-44c9-a7df-72d48124849a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 81
        },
        {
            "id": "d9b0dd05-b7f8-4951-bc10-6de44c98235b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -13,
            "first": 67,
            "second": 45
        },
        {
            "id": "0121b52e-d3aa-4a7a-aa96-0d337e9f4dd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 67,
            "second": 49
        },
        {
            "id": "fea63186-c75e-48c5-8516-ad24dd659dab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 51
        },
        {
            "id": "9d54fb40-7c65-4819-9adf-6bb0c3824cce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 67,
            "second": 52
        },
        {
            "id": "5bbccbb9-fe8e-496b-9c90-049e9ab5dbf0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 53
        },
        {
            "id": "372b3738-8f21-4c9a-a8bb-8a2b9c87a4b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 67,
            "second": 64
        },
        {
            "id": "1aba2176-ab5b-4ca0-8544-b1fe4e5509ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 67
        },
        {
            "id": "e636b286-cc27-41fa-b139-52f50dc421f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 71
        },
        {
            "id": "1fe5cec0-31be-4edc-9867-5e423e19d3b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 79
        },
        {
            "id": "887a7728-6ae8-4fd1-a0d3-f93f0ed75334",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 68,
            "second": 39
        },
        {
            "id": "5e853658-a444-4e28-95e9-f2753234264a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 68,
            "second": 45
        },
        {
            "id": "3f7f2fb1-9a8b-4c7b-a766-2be106953c0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 68,
            "second": 48
        },
        {
            "id": "08d4c28f-fadc-4b43-81ce-e84bacb92574",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 50
        },
        {
            "id": "68fdf54b-f8c7-4e69-88c8-7ab96223807f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 52
        },
        {
            "id": "f76051e3-d852-4ec2-94d6-72819342a390",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 68,
            "second": 54
        },
        {
            "id": "7139975d-0cf7-4868-9ca8-b33d981b2b36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 55
        },
        {
            "id": "a796c82f-e776-4db0-b2c2-9093ad135550",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 56
        },
        {
            "id": "713a6489-f4a9-4ebd-b26e-b334a3ac6242",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 57
        },
        {
            "id": "e4f7b2d9-1a5a-484e-a9cb-93be9de11bca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 64
        },
        {
            "id": "f56e3990-60dc-4971-94f8-acdd482aac18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 67
        },
        {
            "id": "d40c832b-886b-4677-ac5d-3bcd25099323",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 68,
            "second": 71
        },
        {
            "id": "6e16d6a1-128b-49af-a2d2-71d57717e912",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 79
        },
        {
            "id": "387615cb-2561-4bac-a953-d721e226728a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 68,
            "second": 81
        },
        {
            "id": "2236a4a9-eb68-4453-b810-f4e40d1284e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 68,
            "second": 83
        },
        {
            "id": "f7922d1e-b980-4b1c-bc53-52ead7016311",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 68,
            "second": 84
        },
        {
            "id": "4b9807cb-5a6b-4c5d-838b-7dd3811d54bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 86
        },
        {
            "id": "67c7c17a-f7ec-4f4c-813f-9a4240029ec4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 89
        },
        {
            "id": "b85e9384-e493-4c7a-9872-d0b815d7945a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 68,
            "second": 8217
        },
        {
            "id": "3d6e50f0-b2be-40df-8861-15678010e49b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 69,
            "second": 44
        },
        {
            "id": "9a76e282-3ab5-4c99-b687-391cfb07dd5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 45
        },
        {
            "id": "781dd22a-7221-4735-8e7f-ddb8e4f8d79c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 69,
            "second": 46
        },
        {
            "id": "0607228b-adbe-4786-9699-5926640809d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 69,
            "second": 49
        },
        {
            "id": "d81a141a-8350-43ae-8c26-ef5cd5ae77a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 57
        },
        {
            "id": "44555e3c-fccc-4787-ab91-5597261af5f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 69,
            "second": 64
        },
        {
            "id": "f42c3736-c21e-48ef-8616-519ffb6f115b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 69,
            "second": 65
        },
        {
            "id": "1573b839-edb3-4321-b297-5601b1ef6980",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 67
        },
        {
            "id": "160364f7-6398-413d-af6b-97b4cbbff49e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 69,
            "second": 74
        },
        {
            "id": "d5bbe672-1bf7-4724-a987-e36aff9a3a34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 69,
            "second": 90
        },
        {
            "id": "6fae8ad3-5a73-44c6-b1bd-547ca7303c06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 70,
            "second": 44
        },
        {
            "id": "75498fef-0953-4033-a189-895f87dec42d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 45
        },
        {
            "id": "df6f6b9a-5961-4b17-8c44-c439d11e15bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 70,
            "second": 46
        },
        {
            "id": "603b3368-9b3e-4f51-8337-7f2ea6227074",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 49
        },
        {
            "id": "5895e62d-2aac-44d1-aae0-84a2f43e0255",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 50
        },
        {
            "id": "5c79926c-669a-4c64-bdad-dbaf3faea16c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 51
        },
        {
            "id": "8bddef86-ad6a-42a0-bdc5-b5781740346c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 52
        },
        {
            "id": "ad23f2a7-7786-44d1-a6b7-4cb6e41e43c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 53
        },
        {
            "id": "db6de20d-bb5b-4357-be8b-72e1e6a3f2d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 54
        },
        {
            "id": "c46def1c-655f-41ef-b03c-312a306ae5d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 64
        },
        {
            "id": "58eb2935-9c2e-4ea7-b06f-d810e47cec79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 65
        },
        {
            "id": "cd4fa4ee-5006-4b99-a285-419c1215c86d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 67
        },
        {
            "id": "69c2aa99-69cc-4233-b072-06e87fd47fea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 71
        },
        {
            "id": "6ad4ad9f-1458-4040-b592-2e9b6b87e5f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 79
        },
        {
            "id": "3d4a66bf-505b-47a5-9bca-a6eabeebd639",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 81
        },
        {
            "id": "e6fb3f14-013a-401d-8503-11b64d0804fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 82
        },
        {
            "id": "8f592bf3-5423-4741-97b7-99315a4e48a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 44
        },
        {
            "id": "796ab320-cc6a-457d-af08-b5bd155cb257",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 71,
            "second": 45
        },
        {
            "id": "27dd2496-8c49-4976-a508-3928704e982a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 46
        },
        {
            "id": "7a02ecec-681a-4196-9fb0-373cbd031ca4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 71,
            "second": 48
        },
        {
            "id": "935759c5-8068-4510-9fdf-8b06b8644f77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 71,
            "second": 49
        },
        {
            "id": "b5aef68c-bedc-4431-a2ba-43358a22695b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 71,
            "second": 52
        },
        {
            "id": "428a3969-6f5d-47c3-aff2-77b7107724e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 71,
            "second": 54
        },
        {
            "id": "b6b1e3e3-c82d-4bd0-a362-89345854474a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 71,
            "second": 56
        },
        {
            "id": "32a3cd6d-1442-417a-81f2-f8f79bddfbe3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 65
        },
        {
            "id": "f2078d16-5dca-4092-bf5a-edcd68b9969e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 67
        },
        {
            "id": "01838c63-0387-4db0-a9fe-3081b877c348",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 71,
            "second": 71
        },
        {
            "id": "b234c72c-9c68-4103-80ad-e5a906d4d675",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 74
        },
        {
            "id": "33889721-b7b8-4915-867e-20870838c1bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 71,
            "second": 79
        },
        {
            "id": "6278dfdf-b8c1-484f-a61f-21db747c91b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 71,
            "second": 81
        },
        {
            "id": "6c4f77a3-c6f8-4264-aab6-7604e454de81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 45
        },
        {
            "id": "f5ccaf7f-1fd8-4354-97b5-2ace707bc832",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 48
        },
        {
            "id": "c0163ce8-090c-4e58-a3f8-1b8084e3210c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 49
        },
        {
            "id": "73dd20f7-ccd5-4c2b-9d82-1eb8e0e4cbe5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 51
        },
        {
            "id": "22893500-225f-40de-9975-96e730de5e87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 52
        },
        {
            "id": "75c5e347-74f7-4eb7-bac5-95b477372bb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 72,
            "second": 53
        },
        {
            "id": "dc8427a7-4837-422f-890b-4e4b51c7ed1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 54
        },
        {
            "id": "ff478eb6-20fa-485e-9aec-9ac8c1dbeca3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 57
        },
        {
            "id": "e8f2298b-28f4-4dad-ae22-460991312d77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 72,
            "second": 64
        },
        {
            "id": "57aa44c9-5ab0-4924-80d9-09df65cda716",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 72,
            "second": 65
        },
        {
            "id": "25b7ca35-16bd-41f6-a1fc-2ef353c85e35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 72,
            "second": 67
        },
        {
            "id": "156e2851-7748-4b66-b560-fc90d1dd00a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 71
        },
        {
            "id": "328c670f-d3bc-4f6f-bd1a-a3bd4243d1ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 72,
            "second": 79
        },
        {
            "id": "3be33172-1b1d-430c-a90c-aca8c14edafe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 72,
            "second": 81
        },
        {
            "id": "34dbf841-21ca-4993-98e3-f7d9f2c80ede",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 73,
            "second": 45
        },
        {
            "id": "c02b1626-9bf1-439d-815f-cde3d4ee890d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 48
        },
        {
            "id": "a154c90d-41fe-4ff7-8e27-f9f3afdd5331",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 73,
            "second": 49
        },
        {
            "id": "3570d245-9f27-4e61-b94d-37a7bfac34ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 51
        },
        {
            "id": "5fd4c936-3e71-4ec6-bdb8-39a75644f97c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 73,
            "second": 52
        },
        {
            "id": "f75cb64a-dc4d-4948-be42-ba997b19af78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 53
        },
        {
            "id": "2952a40b-c307-4d9c-b216-db51f7d14f9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 73,
            "second": 54
        },
        {
            "id": "45ce6410-4ecd-4af8-ae6b-d25d3187e2b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 73,
            "second": 57
        },
        {
            "id": "22312f97-547f-4e9e-ab8f-1752df220910",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 73,
            "second": 64
        },
        {
            "id": "e847d7db-6881-4ad7-9bbf-508a285c4957",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 73,
            "second": 65
        },
        {
            "id": "e2fb2e1f-b631-4b85-836e-8b264e8cd016",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 73,
            "second": 67
        },
        {
            "id": "67d87fae-85d9-422e-8bb7-ffdd6ebcd41b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 73,
            "second": 71
        },
        {
            "id": "ea05213a-cf10-42bd-8acd-377277b55029",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 73,
            "second": 79
        },
        {
            "id": "74a063bf-bbf3-4100-96c6-244fde3f3bc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 73,
            "second": 81
        },
        {
            "id": "2285b7f8-f47e-42d4-a882-c7c8b162ff9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 73,
            "second": 90
        },
        {
            "id": "7b7c2db6-f1b2-49e0-8680-f3cc7416bbfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 44
        },
        {
            "id": "92ad67bc-7c5c-493d-89e3-77681f3341a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 45
        },
        {
            "id": "a5d82778-4587-4701-955e-5f26171f8794",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 46
        },
        {
            "id": "8f30dfae-8ee8-4544-b811-491456fb7af1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 48
        },
        {
            "id": "fca56704-ded5-4094-ab67-57d230b97086",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 74,
            "second": 49
        },
        {
            "id": "31230dd4-23c4-4132-ab44-992fad67eee4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 74,
            "second": 50
        },
        {
            "id": "21869410-2b05-4f5f-9a21-b07d90825d0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 51
        },
        {
            "id": "883ba465-4bab-410e-8e49-30361ec4cade",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 52
        },
        {
            "id": "b6f4c829-472a-4bab-8a69-d927e86cc300",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 53
        },
        {
            "id": "3c42c444-6f63-41eb-892f-f5ec9f892956",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 54
        },
        {
            "id": "751a847e-505f-452f-ab9d-c5dcf1c02880",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 56
        },
        {
            "id": "2dfe73fb-adcd-43a6-b2f5-37d6924fcc1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 57
        },
        {
            "id": "3e562435-8ff2-4be2-bb3b-d390a5061cbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 74,
            "second": 64
        },
        {
            "id": "08295600-2106-4285-81c8-9130b0864d19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 67
        },
        {
            "id": "1de4a79b-bf37-4397-946f-fbdd77aa3c0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 71
        },
        {
            "id": "36dce9d1-ee9c-452e-a672-862284ca1f18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 79
        },
        {
            "id": "4f7bc4f4-d4cb-4d9e-adda-b447f7f29e41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 81
        },
        {
            "id": "bc19d4f0-ca3e-43eb-9e32-0ee2939e2302",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 83
        },
        {
            "id": "20ed2c74-f256-4c69-96cc-091c1768f5d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 90
        },
        {
            "id": "30951d2c-73e0-441c-b3a9-86106cc8d885",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 39
        },
        {
            "id": "56466af8-f077-45b4-a139-f910c723d713",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 44
        },
        {
            "id": "16437a51-b75e-468a-bbc2-41afbed79eed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 75,
            "second": 45
        },
        {
            "id": "e03aa672-812e-4f19-b7ff-71c824bbd671",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 48
        },
        {
            "id": "2f8cb096-b3b6-40c2-bc18-c1f430ded5b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 49
        },
        {
            "id": "f7a86355-b7b5-435e-b2ea-f0f52252e28b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 51
        },
        {
            "id": "e0cd1d1c-32ba-4663-a4fd-340dd12d5241",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 52
        },
        {
            "id": "c116dc27-4c91-4d2f-bae8-a081c8d68188",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 53
        },
        {
            "id": "9261dc3f-3338-451b-858e-3d1c808c53e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 54
        },
        {
            "id": "181cab31-db05-47c0-b7c0-f4adee796501",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 55
        },
        {
            "id": "b8b8e834-b59b-4ce3-976b-fa6a4f98084d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 57
        },
        {
            "id": "46341d40-771e-44c5-856b-617d7b17a531",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 75,
            "second": 64
        },
        {
            "id": "bda9a411-7bc6-4fc1-a33f-fc457f7ba94a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 75,
            "second": 65
        },
        {
            "id": "87a44961-8616-4a89-9178-fffc3129ecfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 67
        },
        {
            "id": "87f74d5e-248c-47c9-b4af-50981807e926",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 71
        },
        {
            "id": "1921cb46-056f-4d46-989e-9013a4961d60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 74
        },
        {
            "id": "79c94ffd-316d-4529-aadc-419a451c679d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 79
        },
        {
            "id": "7072a99b-73b6-46ec-a0c0-95516ab968df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 75,
            "second": 81
        },
        {
            "id": "b6091c66-4e8d-4f88-bd15-452a2c967a02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 84
        },
        {
            "id": "a1402403-31a7-4ae2-9962-e09faf2db85d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 85
        },
        {
            "id": "dc513176-d807-4b45-a5ae-a9f2a0e22172",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 86
        },
        {
            "id": "85b4141a-60a2-4389-a5be-5c4309ade8a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 87
        },
        {
            "id": "c8f50d55-ebcd-4a91-915a-7040b714f3f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 89
        },
        {
            "id": "5dd0646b-3270-438a-929c-bf98a723f985",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 8217
        },
        {
            "id": "9154f8ad-d0be-440c-8cb3-97e8ce4ccf4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 76,
            "second": 39
        },
        {
            "id": "53b25abb-09ef-4c1d-8949-6538ce609ca4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 76,
            "second": 44
        },
        {
            "id": "35705259-64af-4761-925a-9b62b134be5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 76,
            "second": 46
        },
        {
            "id": "efc4a35a-21cc-4b1a-8be7-3deca5b1a035",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 49
        },
        {
            "id": "9c0c824e-410f-4ebf-8c28-d1129a027331",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 55
        },
        {
            "id": "7a7a05b0-5a76-41d3-8f7c-8ae71b2529de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 56
        },
        {
            "id": "31e06419-6e10-46b2-bd33-151835d10e29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 57
        },
        {
            "id": "7bee946b-fea7-4822-b16f-3ccfdecdd0b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 76,
            "second": 64
        },
        {
            "id": "ddcc1e0f-c21b-4827-8c53-2df492d1f1c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 76,
            "second": 65
        },
        {
            "id": "57bf2562-d293-4d24-bea0-e736803340b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 69
        },
        {
            "id": "9080b7d5-f4c3-42e3-baa6-084d2ce9d94b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 74
        },
        {
            "id": "3ac459de-dbd3-4ad7-a0bb-b02d6a6853a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 76,
            "second": 84
        },
        {
            "id": "acc5f319-c5e1-494d-a321-5eec3e15a246",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 85
        },
        {
            "id": "bd779ca3-f470-4db7-a8b0-02ffeaad0750",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 76,
            "second": 86
        },
        {
            "id": "a5ba74c5-9606-4aa9-b5e7-3a3ae7117226",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 87
        },
        {
            "id": "c1adbb76-9e30-412d-93b0-f9ce1f09e3c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 76,
            "second": 89
        },
        {
            "id": "f09163cc-07ee-4d23-8e23-3e724ffc486a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 90
        },
        {
            "id": "bc7f1de9-8f96-4301-982f-e426cae4dd46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 76,
            "second": 8217
        },
        {
            "id": "9f8a81e1-56ad-4cd7-a969-cee932df47c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 77,
            "second": 45
        },
        {
            "id": "d4a03242-a5e7-4148-8848-87be657d4e9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 77,
            "second": 48
        },
        {
            "id": "e5457112-d80e-44d0-8cc8-56e45ed97bda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 77,
            "second": 49
        },
        {
            "id": "ee60138b-8696-4c83-98bf-94893327fe56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 51
        },
        {
            "id": "60ec6fea-2551-40db-86cb-18da5f168948",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 77,
            "second": 52
        },
        {
            "id": "76cf8a21-14c4-475c-a81c-caf830eb9495",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 53
        },
        {
            "id": "b336e706-442f-4a95-ad37-fb3100f8c98a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 77,
            "second": 54
        },
        {
            "id": "83a8c8c2-de6c-4a67-88bb-1f1017945c08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 77,
            "second": 56
        },
        {
            "id": "8f9db5c1-1f5e-4369-ab6d-68a96aec1904",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 77,
            "second": 57
        },
        {
            "id": "ac035ed0-62d0-40ac-b7a8-59a61254f101",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 77,
            "second": 64
        },
        {
            "id": "963b9a02-7efa-4bda-803c-625d07419d75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 77,
            "second": 65
        },
        {
            "id": "b7f1904e-1af8-49ce-af79-fc753ea1066c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 77,
            "second": 67
        },
        {
            "id": "702dde48-cb94-4caf-8d18-d132fd680ff1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 77,
            "second": 71
        },
        {
            "id": "38ac141a-1e7b-4bce-9067-8a52d9f44177",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 77,
            "second": 79
        },
        {
            "id": "bcf838a1-bcc5-4a32-a6b3-7d591de0bdda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 77,
            "second": 81
        },
        {
            "id": "0dfcd5b3-f54a-4bf7-80e1-839f733055a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 78,
            "second": 44
        },
        {
            "id": "9ea47125-f0f9-41ba-a1ae-f37f003bcdec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 78,
            "second": 45
        },
        {
            "id": "6442ce45-2358-40e1-853d-3690889b78c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 78,
            "second": 46
        },
        {
            "id": "bc94bf72-dc8c-4dcd-8980-76929702a8ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 48
        },
        {
            "id": "a1fde7f6-cd47-4301-a01d-787b16d985ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 78,
            "second": 49
        },
        {
            "id": "74949221-354c-4c90-8d32-d8fffa13a87e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 78,
            "second": 50
        },
        {
            "id": "34749458-5817-46a8-9c04-4b9964b4b7b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 78,
            "second": 51
        },
        {
            "id": "85b5d6dd-844c-422d-9bf8-53c6e7167dae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 78,
            "second": 52
        },
        {
            "id": "d730e371-a61f-4a0d-b472-5d13484c353c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 78,
            "second": 53
        },
        {
            "id": "f5c53dd7-3a8f-4f9e-9940-dcd12c259859",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 54
        },
        {
            "id": "45d3fdab-6f47-427c-9816-97c5538dedfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 56
        },
        {
            "id": "9db01cb7-bcc8-44ad-bfb9-0917134a2d8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 78,
            "second": 57
        },
        {
            "id": "2f6e6cb6-ee19-4d3f-a44f-e820c3f7016f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 78,
            "second": 64
        },
        {
            "id": "f7b5e7cb-55ae-4e2f-a077-fb431d7c4f42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 78,
            "second": 67
        },
        {
            "id": "a6bf106f-653d-4d9b-a938-a9d452273b69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 78,
            "second": 71
        },
        {
            "id": "b9c6c175-6499-462e-8ba2-a197b09fdbde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 78,
            "second": 79
        },
        {
            "id": "271b53b4-006f-469a-bde4-8e4fba84325f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 78,
            "second": 81
        },
        {
            "id": "8b214b69-881a-4cba-9d73-6ff67848ab78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 78,
            "second": 83
        },
        {
            "id": "ed54d331-1eb9-4c2f-88ce-c7e97f53bf86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 90
        },
        {
            "id": "aaf8c403-1471-4410-b5be-dca9285d24e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 79,
            "second": 39
        },
        {
            "id": "b6773176-b433-441c-9ca0-9d81848134df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 45
        },
        {
            "id": "bcbc609f-384e-43d1-899a-89d200e2768e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 79,
            "second": 48
        },
        {
            "id": "417ca32c-b63a-4886-8830-5fc0bcfd12d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 50
        },
        {
            "id": "7249cbe8-213b-434e-8cdd-2096424778bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 79,
            "second": 51
        },
        {
            "id": "f699a49d-6167-40af-a095-a72b77597698",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 52
        },
        {
            "id": "197b1e35-fb54-4045-9df5-f7a074368169",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 79,
            "second": 54
        },
        {
            "id": "5e2cd73b-7f07-4f2f-bf98-6192287dbbb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 55
        },
        {
            "id": "1fdad382-e945-4d87-817e-c36ba103a257",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 56
        },
        {
            "id": "66a6de96-38a7-450a-a853-38b4e32b41a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 57
        },
        {
            "id": "d2681fce-59f0-44e5-8861-303e9f5f9906",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 64
        },
        {
            "id": "e89f6f3e-0210-4415-80a2-780a5fbdccd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 67
        },
        {
            "id": "a37dd2ed-0644-4933-9bd8-d00d33ed83a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 79,
            "second": 71
        },
        {
            "id": "960d6be4-2516-4315-913a-9906e7df9b13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 79
        },
        {
            "id": "995f0362-cb07-4dc3-9edc-081b7fcff5c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 79,
            "second": 81
        },
        {
            "id": "28457b21-aac1-4496-9183-321761a8c27b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 79,
            "second": 83
        },
        {
            "id": "61b7f3c9-22be-4a5f-87d3-b0e4b5c3d89c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 86
        },
        {
            "id": "1d882007-62fc-4dee-927c-73633912aac4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 89
        },
        {
            "id": "5024cd19-1c62-4e9b-b854-cdde723839e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 79,
            "second": 8217
        },
        {
            "id": "e6b53f74-daac-4466-85ac-9abdcf075248",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 80,
            "second": 39
        },
        {
            "id": "5c9a1b4b-47ff-470a-8376-58a3fea6ad6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 80,
            "second": 44
        },
        {
            "id": "eab79887-9441-448f-981a-db69fd86f055",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 45
        },
        {
            "id": "97532a14-26fb-4f4b-9c02-52c214fcc90d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -8,
            "first": 80,
            "second": 46
        },
        {
            "id": "6e6a0dd0-e3cc-4fac-aef7-3ad22d26374f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 48
        },
        {
            "id": "cfa8871c-72ae-4237-8ecf-f2f5b52f5872",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 52
        },
        {
            "id": "09a82d39-d0f3-4471-96fd-c07f5e9b0a18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 53
        },
        {
            "id": "52879402-f38b-4298-aee8-e1948b8173dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 56
        },
        {
            "id": "ad3c6865-44f5-4476-8299-346cbaa9cc4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 57
        },
        {
            "id": "5cd453d9-9285-4c43-811d-5410d4f75b77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 65
        },
        {
            "id": "ca0e1f2b-d325-40cb-8383-4ec7308ff51e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 69
        },
        {
            "id": "e6cc934c-7303-442f-bec7-6cd2eab3577b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 70
        },
        {
            "id": "f3fc287f-afb6-4934-94d0-dcdfa250df7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 73
        },
        {
            "id": "f4c92b0b-a4ee-4d93-a3ac-29d14cba68a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 74
        },
        {
            "id": "5b10ec12-fc16-4e48-95c8-7a4133c84068",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 77
        },
        {
            "id": "b46a5da1-45ce-4092-92c0-029f014b2215",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 78
        },
        {
            "id": "be970970-1fd2-425c-a4ce-15720f1b2955",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 83
        },
        {
            "id": "4fb11907-794e-42d5-9669-1931bcadd55a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 86
        },
        {
            "id": "3b7809c2-4cba-4f4a-a819-d293f92abc86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 87
        },
        {
            "id": "2fdd74f8-b2eb-4397-9a8d-8336b6d55238",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 89
        },
        {
            "id": "e981c471-8ce0-4b49-b380-7619500dd165",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 80,
            "second": 8217
        },
        {
            "id": "9acba8f9-86d6-4ca2-b24d-bee48a4fdedd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 81,
            "second": 39
        },
        {
            "id": "c14b5b17-4389-49c7-93b4-31e0c98e77bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 81,
            "second": 44
        },
        {
            "id": "1da36e9a-fd22-4be2-b8a6-a155ec40f29d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 81,
            "second": 45
        },
        {
            "id": "6ce3969c-05d2-4cc6-9e9e-b10f43dcec9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 81,
            "second": 48
        },
        {
            "id": "66bb9b2f-b3e1-4a93-a082-9031861294b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 81,
            "second": 50
        },
        {
            "id": "33edfa57-1b4a-4a1c-bb0c-f8e29ddfcfb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 81,
            "second": 52
        },
        {
            "id": "9a6b1725-9ae9-46fa-87c8-e39b0bbcbd61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 81,
            "second": 54
        },
        {
            "id": "55ba061c-63c8-45aa-8644-2de8f289d2c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 81,
            "second": 55
        },
        {
            "id": "e1eb5eca-6ffa-4fc0-aeac-a2ba0376fc8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 81,
            "second": 56
        },
        {
            "id": "642802ec-cd59-4da0-aab6-825712528ff9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 81,
            "second": 57
        },
        {
            "id": "a6a25210-419a-4a7a-8d48-2dbdb3cc13eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 81,
            "second": 64
        },
        {
            "id": "6dc33681-d66a-4f4b-8753-15c80d2452e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 65
        },
        {
            "id": "c62940b3-ea41-4a2a-88af-64821c62e52b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 81,
            "second": 67
        },
        {
            "id": "6813ccf0-9074-410d-9a7f-0bca0716c95d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 70
        },
        {
            "id": "751501be-ba18-46c2-93e3-2aa3eee1be2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 81,
            "second": 71
        },
        {
            "id": "7fbe3665-1626-4099-a256-5285e562736f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 6,
            "first": 81,
            "second": 74
        },
        {
            "id": "eb03f68b-f8c7-4ad4-a74c-3260d7b09e87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 81,
            "second": 79
        },
        {
            "id": "287bae03-f804-4595-b4e0-66c50b6aa855",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 81,
            "second": 81
        },
        {
            "id": "5f2399d6-b89d-4f99-a418-c782f02f209d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 81,
            "second": 83
        },
        {
            "id": "9e217281-2d2c-4151-94d2-12b245e062b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 86
        },
        {
            "id": "88952298-a9e2-4c12-8345-21e9d7254ff8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 87
        },
        {
            "id": "f9ceecd3-32f2-4898-8296-491954dfbded",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 88
        },
        {
            "id": "5a973f13-bcc9-4fa1-9801-14088fa0ec13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 89
        },
        {
            "id": "fbadbd50-9966-40ea-9cbd-3f7fcca72529",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 81,
            "second": 8217
        },
        {
            "id": "765c12bf-b560-460f-895d-23cdc537b8eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 39
        },
        {
            "id": "d83caf9f-0c20-4598-9e91-d8dc5cd3e0f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 82,
            "second": 45
        },
        {
            "id": "21098292-abea-4618-a4e9-b94fd830293b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 48
        },
        {
            "id": "e07a5b44-c1f2-4305-9523-f4ef822012eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 49
        },
        {
            "id": "d91f66ea-a10e-4395-9353-463907fcfe3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 51
        },
        {
            "id": "b7afc3f1-abdf-4393-8fbf-7604dd0fc545",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 82,
            "second": 52
        },
        {
            "id": "6b452a2a-f5e3-4ac5-b75b-b877758d1931",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 53
        },
        {
            "id": "2ab80243-d88d-456d-8df1-bc7c46b3bf06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 54
        },
        {
            "id": "e199ddc1-e98f-4e5a-aec5-e2ab258861a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 55
        },
        {
            "id": "4e923766-8ef1-4e85-a868-de974150b9ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 56
        },
        {
            "id": "be316fa7-e7fc-4793-b293-b4b06e302e59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 57
        },
        {
            "id": "45d06711-1770-4d2a-8c7b-e14fa17f1765",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 82,
            "second": 64
        },
        {
            "id": "c9d04ac1-a9d5-4750-a53a-9911b3f8fd9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 82,
            "second": 65
        },
        {
            "id": "41f1d848-6a06-4715-b0c1-e7c653d87d85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 82,
            "second": 67
        },
        {
            "id": "f00157c8-834d-40ec-a85e-c85912e7c6a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 69
        },
        {
            "id": "1220adf1-3643-42cf-8bc2-853160845266",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 82,
            "second": 71
        },
        {
            "id": "e0dd62ee-f601-41e7-89ad-0258bde17384",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 82,
            "second": 74
        },
        {
            "id": "57cf17db-9eab-4a76-8074-c96de74e2137",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 82,
            "second": 79
        },
        {
            "id": "a76511d1-6aa7-4fa1-9cde-3d31c98a27cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 81
        },
        {
            "id": "ba86a785-1a5c-4385-b18e-578041c386b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 82,
            "second": 84
        },
        {
            "id": "44e31c3c-4316-4a41-bb0b-e9470d958709",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 82,
            "second": 85
        },
        {
            "id": "8ba633e9-65f1-47d7-a0e9-5cf52e6ad4ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 82,
            "second": 86
        },
        {
            "id": "16fbba4d-23c8-4fef-a5ae-d47dae74a313",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 82,
            "second": 87
        },
        {
            "id": "36218e44-2744-4505-8cf3-e27ce634c52e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 82,
            "second": 89
        },
        {
            "id": "ee8aa5d2-daf9-4243-a9ec-6fef07b98a6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 8217
        },
        {
            "id": "6d7cbc28-6688-49ab-8c5e-925dc57a8ad3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 83,
            "second": 45
        },
        {
            "id": "93551594-0baa-4d12-aeea-a062fc376509",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 46
        },
        {
            "id": "44924ff7-c9cb-4049-a436-eff49be1d5af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 83,
            "second": 48
        },
        {
            "id": "5d2394a2-6811-47c9-a185-5b86606ea356",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 49
        },
        {
            "id": "1c745b3b-9358-4bc7-9438-55fb31a9b3b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 83,
            "second": 52
        },
        {
            "id": "69cb46c1-80db-44b1-b6f7-3d56c7634756",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 83,
            "second": 54
        },
        {
            "id": "4db6a906-21d4-4ab4-91fd-312e0639f18a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 83,
            "second": 56
        },
        {
            "id": "62c0525f-078e-40d1-9d29-6545f304107a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 67
        },
        {
            "id": "33691074-da7e-42db-89a5-9ac91a02a97a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 83,
            "second": 71
        },
        {
            "id": "bda51163-ed78-4623-b028-c6da3457e578",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 83,
            "second": 79
        },
        {
            "id": "871ecd7f-4ed2-468f-98ea-6cb8e2e4cbff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 83,
            "second": 81
        },
        {
            "id": "8319e0f3-a39a-40ea-86ad-091e28e8a879",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 44
        },
        {
            "id": "75c7adb0-da7f-4820-944e-1737f29092b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 84,
            "second": 45
        },
        {
            "id": "dd5025da-7b6e-4da6-a2de-7ef2e245c5b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 46
        },
        {
            "id": "5b623779-b44a-4d18-95b2-22860faa7a43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 49
        },
        {
            "id": "bbb499a9-ecd9-4943-b391-0eca79324efd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 50
        },
        {
            "id": "c89eb9f0-588b-4d42-84d5-dca5aa314169",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 51
        },
        {
            "id": "847319ae-42ad-44a1-a2c6-ffa414fa8db4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 52
        },
        {
            "id": "a386de6b-d99f-4fba-bc47-dbf779d2f83e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 53
        },
        {
            "id": "4d83f417-709c-44c8-b097-9facc9531334",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 54
        },
        {
            "id": "1e901c5c-ef73-4ee5-a118-f96151209099",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 64
        },
        {
            "id": "8285e9ee-63f5-4f82-9fc3-0a2b2ebdcd68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 65
        },
        {
            "id": "074371ee-3b6a-43d1-87f9-44ddccaa941e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 67
        },
        {
            "id": "3c746a32-1ca2-41b3-95c0-68cb571b9644",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 71
        },
        {
            "id": "5dace98f-051b-43ab-a548-d0d51559a1de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 85,
            "second": 44
        },
        {
            "id": "57ea7538-1b3a-4c53-b1c6-c5eecd562634",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 45
        },
        {
            "id": "1453a70c-27f7-40a9-a135-ecda0418b1ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 46
        },
        {
            "id": "7d8783a1-1879-4e34-84b4-b9563ee5c07a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 48
        },
        {
            "id": "caca0f6a-269e-40b1-82ca-1d41516e5925",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 85,
            "second": 49
        },
        {
            "id": "21fc8bba-0748-4e16-bf18-39f089aaebe1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 50
        },
        {
            "id": "1a009526-9a4c-43d7-8b46-73e72bd5ae8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 51
        },
        {
            "id": "5b5b0965-6316-4dc2-9593-2b717d1e03f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 52
        },
        {
            "id": "4ad8abf6-6012-4236-aeb0-f7a2ddec3fc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 53
        },
        {
            "id": "fb1347d0-7a13-42b5-86c6-6c867ae70d37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 54
        },
        {
            "id": "c1ae4fa4-82c1-4b45-a718-094690f144b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 56
        },
        {
            "id": "c26a6940-7540-4442-a1de-61939e290ffd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 57
        },
        {
            "id": "b270e2e3-d722-40de-8453-e7292df8b886",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 85,
            "second": 64
        },
        {
            "id": "4958b77a-4312-4f07-b775-a3e98054d352",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 85,
            "second": 65
        },
        {
            "id": "d82ed068-8e3b-4cce-bb47-62c91456378a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 67
        },
        {
            "id": "3efd4ee6-559c-4704-9903-8b200c1f4ec1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 71
        },
        {
            "id": "ee310f35-5c89-4a19-83bf-86a5bb05d78f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 79
        },
        {
            "id": "8e53a3c1-bad9-471a-97bb-c4392daa5014",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 81
        },
        {
            "id": "3173da66-f798-4223-a52c-c89e6e974474",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 83
        },
        {
            "id": "c3ec9df7-e239-4405-adf2-06e21e4cf483",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 90
        },
        {
            "id": "2c1c3a72-3c07-4f30-9494-5183aecb1664",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 86,
            "second": 44
        },
        {
            "id": "3b002fb1-490d-4c1d-9fc7-be8f6fc5e0fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 45
        },
        {
            "id": "ae89d4b8-1640-43d6-a3fe-fb63ddd8bc5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -9,
            "first": 86,
            "second": 46
        },
        {
            "id": "4b259942-640e-46e1-aa95-f502ef195bd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 48
        },
        {
            "id": "3f20f308-6d13-42d2-a992-549e99fae6b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 49
        },
        {
            "id": "357b80e3-5883-45dd-9e03-854b3435e81a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 50
        },
        {
            "id": "58f35b44-7f36-4ffc-b4fb-3bc9e7b1fce2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 51
        },
        {
            "id": "d4cb8448-327e-4e88-9056-25c4e5d4faae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 86,
            "second": 52
        },
        {
            "id": "24e523b2-59ee-42a6-af09-edff02ad0816",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 53
        },
        {
            "id": "2d7ae51e-b570-4433-b94c-5b17c0bb1ac8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 54
        },
        {
            "id": "07ec3fc1-5b8c-43b9-bc93-4329c405e2d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 56
        },
        {
            "id": "8ca47373-d102-4ff7-b95e-232620907382",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 57
        },
        {
            "id": "5602de4d-3440-479b-b18e-471d582ca51e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 86,
            "second": 64
        },
        {
            "id": "dacdaea0-1134-4ef4-8562-d774cabffb2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 86,
            "second": 65
        },
        {
            "id": "2284ceb7-c783-42a5-97df-2b46af9c97b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 67
        },
        {
            "id": "ef7c34f9-043c-458e-856d-9ed7f02d3853",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 71
        },
        {
            "id": "b5b19365-09db-4ccf-b8f4-b3ba9eb50f81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 79
        },
        {
            "id": "6e06c6e4-6112-4302-ad5e-5603886fabc1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 81
        },
        {
            "id": "03ef180a-06f3-411f-a29b-427f1af8d193",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 83
        },
        {
            "id": "ab7c4810-0055-4efa-9ff0-e67bf4505ccc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 90
        },
        {
            "id": "0a90598f-d284-4925-8430-b96816e2db08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 87,
            "second": 44
        },
        {
            "id": "f90e0a32-93c9-41af-95e4-1acdd93f423c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 87,
            "second": 45
        },
        {
            "id": "8976874d-d501-4097-83d9-fac1dd6525cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 87,
            "second": 46
        },
        {
            "id": "1ab8a4e6-0857-4441-baa0-101946a78818",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 48
        },
        {
            "id": "ddfe8cb5-1c18-4968-8c09-af50fba5b8dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 49
        },
        {
            "id": "4f7e8b7b-6537-4fc7-87c6-88b37ced5e00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 50
        },
        {
            "id": "001f633e-8f34-4269-808a-9379931a0639",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 51
        },
        {
            "id": "6901defc-34a0-4a61-adfb-e4dc3964e161",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 52
        },
        {
            "id": "805f0e9e-13bd-4c82-b102-9138146c789b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 53
        },
        {
            "id": "a666d48e-bcb8-4884-9e08-5ff9d8daa047",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 54
        },
        {
            "id": "0408f8e5-7a60-4377-b520-5ee31943a132",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 56
        },
        {
            "id": "b2b073a2-080f-48e4-9efe-104a367ba12d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 57
        },
        {
            "id": "f15edfe4-e415-4fcb-a93f-9311cb6868e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 87,
            "second": 64
        },
        {
            "id": "d45b7750-41c5-4446-afb9-b82bbcee1b5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 87,
            "second": 65
        },
        {
            "id": "a2fbb527-cb72-4b13-8217-2f74f16446f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 67
        },
        {
            "id": "a11dd047-d426-4179-b971-fbde2665ef32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 71
        },
        {
            "id": "2ecae396-0409-4adc-81fe-0d222f25b8a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 79
        },
        {
            "id": "55dcdcfe-65d3-46fb-b8ab-a99d39a4a61a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 81
        },
        {
            "id": "e3d0ad16-3242-413a-b54c-32b32058eb1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 83
        },
        {
            "id": "fbbeac47-ca42-4bf3-bbdf-4670d82e88c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 90
        },
        {
            "id": "0d965ed7-89a5-41de-8de1-0078464c6541",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 44
        },
        {
            "id": "95f49d12-d356-4971-92a5-c4cc500f17ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 88,
            "second": 45
        },
        {
            "id": "8d227f88-6680-4a26-82d0-8c5ba7958b41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 48
        },
        {
            "id": "625afacc-73b4-47c7-b2ce-764d07993891",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 49
        },
        {
            "id": "a04d2b0a-6364-41e4-baf1-d7fbeb5836c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 51
        },
        {
            "id": "6bb9c148-8807-4bb3-9ac0-3262455345e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 88,
            "second": 52
        },
        {
            "id": "30c246be-5994-4c2b-a351-60030ed4c814",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 53
        },
        {
            "id": "e5418212-a0bb-4776-91b1-6a007b6e9900",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 54
        },
        {
            "id": "e7af19d6-e51c-4bc2-910f-21af0363878b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 88,
            "second": 57
        },
        {
            "id": "91dc1877-cd26-4018-89aa-3698f1437c2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 88,
            "second": 64
        },
        {
            "id": "2b6abfc5-0940-4ceb-98bb-847476101db5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 88,
            "second": 65
        },
        {
            "id": "fae72f94-16b6-4d34-90c4-4be836f510c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 88,
            "second": 67
        },
        {
            "id": "1f6adfb7-ac47-4a82-b404-f37f17cb6550",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 88,
            "second": 71
        },
        {
            "id": "bf37b5f6-2ad8-49dd-a946-c359a114f27b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 88,
            "second": 79
        },
        {
            "id": "d0f7f058-79a5-44b0-ab92-696cfad7861d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 88,
            "second": 81
        },
        {
            "id": "1b224aa6-41a9-4ee1-ba39-68dee8f859a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 44
        },
        {
            "id": "78d25283-1cda-4128-86dd-1ecea65ee3d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 89,
            "second": 45
        },
        {
            "id": "426aaab6-c28c-4ce2-88fa-342b0190cb1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 46
        },
        {
            "id": "5e875d75-48eb-4d1a-b122-235520c77b03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 48
        },
        {
            "id": "9cbc98e1-d597-4f6a-ab40-e9b083e3529d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 49
        },
        {
            "id": "f8fc1418-ca8d-44a0-8f56-dd22ec8cdcb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 50
        },
        {
            "id": "711ae0a8-7514-49cc-98d4-c03611856749",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 51
        },
        {
            "id": "bf54ce8c-f951-4cf4-a918-8f56bfd9f54a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 52
        },
        {
            "id": "b9cebdf6-f5d6-4821-a6c6-7c65703dacae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 53
        },
        {
            "id": "bcb4f3a9-b7cf-4ca2-9b3f-ba62dfa15c45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 54
        },
        {
            "id": "a5681f5f-ddfd-4e0f-a51a-d2de6afa546b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 56
        },
        {
            "id": "839d55a4-af70-4ffb-978e-977611cd217c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 57
        },
        {
            "id": "e12eee47-acae-45c7-8881-f98614ac7045",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 64
        },
        {
            "id": "9d078338-f53e-45e5-bc33-275b411388aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 65
        },
        {
            "id": "21f8ea3d-e0dd-445f-bc20-e595fd517f59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 67
        },
        {
            "id": "f61f83d6-6dfb-46db-8777-83bbc5595ca8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 71
        },
        {
            "id": "ecce74d1-1f3a-4095-bbdb-5a66a2f109c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 79
        },
        {
            "id": "c5dc7d2d-7684-41f2-9198-e946ff7c83fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 81
        },
        {
            "id": "07b1c8a0-a0bd-44ab-a804-b1df562f0e06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 83
        },
        {
            "id": "315c1a06-9f98-4d66-93a3-06d4d0bf1bb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 90
        },
        {
            "id": "ca2cadbe-a164-45e3-8103-a2a209001a84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 45
        },
        {
            "id": "b46ead85-ce24-4e2d-bdd5-548cd20221bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 90,
            "second": 49
        },
        {
            "id": "5cc61ac2-d5af-428f-b553-7c4bb1283633",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 90,
            "second": 57
        },
        {
            "id": "835cca4f-2988-4503-a242-ceedf81756f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 90,
            "second": 64
        },
        {
            "id": "7490c78b-6e80-4cb0-92f0-356ceb902cc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 65
        },
        {
            "id": "2836b293-961a-4ea9-b259-c43f95ed32c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 67
        },
        {
            "id": "9c443090-d112-4ec2-a8e1-81ba4fd3891e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 71
        },
        {
            "id": "5cdd24fc-192b-4121-84d7-dcb89b8bbd30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 79
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 30,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}