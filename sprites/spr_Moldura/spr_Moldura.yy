{
    "id": "ac201e46-463b-49b5-ba28-88f782909515",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Moldura",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 153,
    "bbox_left": 2,
    "bbox_right": 176,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "20caba14-8e35-498b-8fc9-e465542efe16",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac201e46-463b-49b5-ba28-88f782909515",
            "compositeImage": {
                "id": "9c5a40fc-167e-4e6f-9778-01752b44b637",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20caba14-8e35-498b-8fc9-e465542efe16",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d35827f1-80d1-4378-92e7-e6e40151617b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20caba14-8e35-498b-8fc9-e465542efe16",
                    "LayerId": "529d40c3-c1ba-48da-ab0a-151612e0c3d2"
                }
            ]
        },
        {
            "id": "e67321ca-cb21-4d51-82ec-a4be19a163d3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac201e46-463b-49b5-ba28-88f782909515",
            "compositeImage": {
                "id": "b77fd300-0da1-4f94-a274-1460d1e70240",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e67321ca-cb21-4d51-82ec-a4be19a163d3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c38e2a52-747d-4c5c-ae9e-099d2421d614",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e67321ca-cb21-4d51-82ec-a4be19a163d3",
                    "LayerId": "529d40c3-c1ba-48da-ab0a-151612e0c3d2"
                }
            ]
        },
        {
            "id": "8b327673-9acf-4e6a-80b3-060fe338fc78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac201e46-463b-49b5-ba28-88f782909515",
            "compositeImage": {
                "id": "cf8a11f3-8efa-4a70-88a9-4912425d4f6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b327673-9acf-4e6a-80b3-060fe338fc78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2146f075-aa33-461b-ab60-0851428e3561",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b327673-9acf-4e6a-80b3-060fe338fc78",
                    "LayerId": "529d40c3-c1ba-48da-ab0a-151612e0c3d2"
                }
            ]
        },
        {
            "id": "8ff9fd14-e2a7-4263-92cc-5e6a5b8f0d39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ac201e46-463b-49b5-ba28-88f782909515",
            "compositeImage": {
                "id": "d3bb82bf-2607-4a14-bd22-9ffa2fa02547",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ff9fd14-e2a7-4263-92cc-5e6a5b8f0d39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a0fa108-2bf2-4552-9100-919b56ef056f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ff9fd14-e2a7-4263-92cc-5e6a5b8f0d39",
                    "LayerId": "529d40c3-c1ba-48da-ab0a-151612e0c3d2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 155,
    "layers": [
        {
            "id": "529d40c3-c1ba-48da-ab0a-151612e0c3d2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ac201e46-463b-49b5-ba28-88f782909515",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 178,
    "xorig": 0,
    "yorig": 0
}