{
    "id": "18f7c455-b710-4e54-9423-4bfb8974d3cc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_GeloSmoke",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 249,
    "bbox_left": 0,
    "bbox_right": 249,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3fc57c19-385e-4526-a483-83b8d958a092",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "18f7c455-b710-4e54-9423-4bfb8974d3cc",
            "compositeImage": {
                "id": "9a55ff89-f558-420a-aabb-45393d99f098",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fc57c19-385e-4526-a483-83b8d958a092",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "152a174b-6991-47f6-882d-26910f810b1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fc57c19-385e-4526-a483-83b8d958a092",
                    "LayerId": "b881fdd9-0619-47af-a6ce-b532da76da43"
                }
            ]
        },
        {
            "id": "71c3e9bc-ca43-4c46-8c50-4806da0f549d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "18f7c455-b710-4e54-9423-4bfb8974d3cc",
            "compositeImage": {
                "id": "d36f33dc-59a2-4a9b-ba60-615df6645dae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71c3e9bc-ca43-4c46-8c50-4806da0f549d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d14d8cd3-6aac-4601-9ce4-467c5f12e13d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71c3e9bc-ca43-4c46-8c50-4806da0f549d",
                    "LayerId": "b881fdd9-0619-47af-a6ce-b532da76da43"
                }
            ]
        },
        {
            "id": "0d2b9ace-1dbb-4ecd-8d76-5fe3975bfaf2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "18f7c455-b710-4e54-9423-4bfb8974d3cc",
            "compositeImage": {
                "id": "fae8c1a9-9059-4c99-a036-e9bf241ca459",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d2b9ace-1dbb-4ecd-8d76-5fe3975bfaf2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b916d3c1-3537-4e4c-85a8-6110f2755eb8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d2b9ace-1dbb-4ecd-8d76-5fe3975bfaf2",
                    "LayerId": "b881fdd9-0619-47af-a6ce-b532da76da43"
                }
            ]
        },
        {
            "id": "606afbcd-3cea-42e4-bb7c-e78b62d61b45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "18f7c455-b710-4e54-9423-4bfb8974d3cc",
            "compositeImage": {
                "id": "6405807b-57d0-40c6-a3be-cf1edd8f0b68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "606afbcd-3cea-42e4-bb7c-e78b62d61b45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1be04e84-dba3-4636-aec7-bb53f7e6f1c2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "606afbcd-3cea-42e4-bb7c-e78b62d61b45",
                    "LayerId": "b881fdd9-0619-47af-a6ce-b532da76da43"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 250,
    "layers": [
        {
            "id": "b881fdd9-0619-47af-a6ce-b532da76da43",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "18f7c455-b710-4e54-9423-4bfb8974d3cc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 79,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 250,
    "xorig": 125,
    "yorig": 125
}