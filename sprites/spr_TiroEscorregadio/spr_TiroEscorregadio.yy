{
    "id": "793d479e-881f-436b-92ca-53888a98e921",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_TiroEscorregadio",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 18,
    "bbox_left": 13,
    "bbox_right": 18,
    "bbox_top": 13,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a5938efe-1ee2-4ad8-942d-3d650811e4cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "793d479e-881f-436b-92ca-53888a98e921",
            "compositeImage": {
                "id": "857d1f7a-220b-48ea-a12e-2d2dd13abb53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5938efe-1ee2-4ad8-942d-3d650811e4cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2b5d03f9-d04e-4df7-a560-a07d4c80e073",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5938efe-1ee2-4ad8-942d-3d650811e4cc",
                    "LayerId": "25f89e1b-e371-4b6e-bc30-9c52076af092"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "25f89e1b-e371-4b6e-bc30-9c52076af092",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "793d479e-881f-436b-92ca-53888a98e921",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}