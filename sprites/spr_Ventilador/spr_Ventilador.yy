{
    "id": "847b85cd-a79c-4f7a-87b3-3ac7a409dae0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Ventilador",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 191,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "43f5a866-04d9-4b9a-b0de-74c2be19ba61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "847b85cd-a79c-4f7a-87b3-3ac7a409dae0",
            "compositeImage": {
                "id": "0da2cdc0-02b2-48ca-972b-2b9bab7f231d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43f5a866-04d9-4b9a-b0de-74c2be19ba61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb29ac64-7436-40b6-96ef-ba6e7db39cea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43f5a866-04d9-4b9a-b0de-74c2be19ba61",
                    "LayerId": "c3a5f876-5c31-4f88-8722-90415f5d5ae3"
                }
            ]
        },
        {
            "id": "ffef06d8-602b-469c-9830-71f5df7f0a5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "847b85cd-a79c-4f7a-87b3-3ac7a409dae0",
            "compositeImage": {
                "id": "54f543c1-6869-4a5a-a23d-19684120093a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffef06d8-602b-469c-9830-71f5df7f0a5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1abe878-e7d3-49a8-818e-dd6b1a3bc872",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffef06d8-602b-469c-9830-71f5df7f0a5d",
                    "LayerId": "c3a5f876-5c31-4f88-8722-90415f5d5ae3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c3a5f876-5c31-4f88-8722-90415f5d5ae3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "847b85cd-a79c-4f7a-87b3-3ac7a409dae0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 0,
    "yorig": 0
}