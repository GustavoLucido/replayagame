{
    "id": "c4be161d-0a82-4966-bae0-26c9174d71c5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Start",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a11dd0d2-f7a9-43da-a640-0f13f297b59a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4be161d-0a82-4966-bae0-26c9174d71c5",
            "compositeImage": {
                "id": "e50ba05d-1914-48ce-a2ca-52a5bcfbdd19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a11dd0d2-f7a9-43da-a640-0f13f297b59a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8dadb0e0-3f9b-4f13-b0a7-4071a7438a4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a11dd0d2-f7a9-43da-a640-0f13f297b59a",
                    "LayerId": "ddc7d5a0-08a7-467c-8dca-1869e61aeadd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "ddc7d5a0-08a7-467c-8dca-1869e61aeadd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c4be161d-0a82-4966-bae0-26c9174d71c5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}