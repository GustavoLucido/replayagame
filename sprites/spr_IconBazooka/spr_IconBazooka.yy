{
    "id": "5e95237c-bcf2-4bb4-9156-5afcc3bbd9b3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_IconBazooka",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 43,
    "bbox_left": 11,
    "bbox_right": 116,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "86adcfb8-5e99-4b19-8b06-16d8748f4bb6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5e95237c-bcf2-4bb4-9156-5afcc3bbd9b3",
            "compositeImage": {
                "id": "a2c8009a-a2b1-4b90-b2f1-c239944b0da3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86adcfb8-5e99-4b19-8b06-16d8748f4bb6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b42f49e-e176-40f1-b7ba-44b1a2e244f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86adcfb8-5e99-4b19-8b06-16d8748f4bb6",
                    "LayerId": "56fc9239-0eb0-472c-85ae-10087911e1c4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "56fc9239-0eb0-472c-85ae-10087911e1c4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5e95237c-bcf2-4bb4-9156-5afcc3bbd9b3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 63,
    "yorig": 61
}