{
    "id": "99c785ae-8adb-480d-81f0-7f15953a5a41",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Esorregarma",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 80,
    "bbox_left": 15,
    "bbox_right": 127,
    "bbox_top": 52,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f577fe36-8d86-45d8-9925-4b8d80ffd543",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "99c785ae-8adb-480d-81f0-7f15953a5a41",
            "compositeImage": {
                "id": "43179abb-a4ed-43a9-b54e-da5f5916d7c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f577fe36-8d86-45d8-9925-4b8d80ffd543",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "decb02a1-a32a-4797-a2bf-5203aceeaf77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f577fe36-8d86-45d8-9925-4b8d80ffd543",
                    "LayerId": "ed37e290-9bf4-4475-8f80-e3344695a0c5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "ed37e290-9bf4-4475-8f80-e3344695a0c5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "99c785ae-8adb-480d-81f0-7f15953a5a41",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 40,
    "yorig": 73
}