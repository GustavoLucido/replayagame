{
    "id": "c1dafd24-1333-4afb-a529-d45528368fe2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Granada",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 18,
    "bbox_right": 46,
    "bbox_top": 18,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f960b1a8-470a-4a3a-9a13-e5246dc099df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c1dafd24-1333-4afb-a529-d45528368fe2",
            "compositeImage": {
                "id": "8aef4b22-9a72-4f67-9967-05ab2c12ad62",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f960b1a8-470a-4a3a-9a13-e5246dc099df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eaedd936-bcc5-4997-97ed-1e3e40320b8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f960b1a8-470a-4a3a-9a13-e5246dc099df",
                    "LayerId": "a298593f-c5e7-4971-8f26-40f140226a48"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a298593f-c5e7-4971-8f26-40f140226a48",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c1dafd24-1333-4afb-a529-d45528368fe2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 37
}