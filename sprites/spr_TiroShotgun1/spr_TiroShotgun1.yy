{
    "id": "58318fc5-3f31-44cf-ad3e-215270dae337",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_TiroShotgun1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 5,
    "bbox_right": 9,
    "bbox_top": 6,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a37f297d-db6b-49c8-be52-15232ddf487e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58318fc5-3f31-44cf-ad3e-215270dae337",
            "compositeImage": {
                "id": "630289a6-f5b7-435c-b29d-464113edd8fd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a37f297d-db6b-49c8-be52-15232ddf487e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a49be3a-a53a-4006-9d0c-26fecee67a82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a37f297d-db6b-49c8-be52-15232ddf487e",
                    "LayerId": "fc5f8d91-daaf-489a-a191-d12066fd5d09"
                }
            ]
        },
        {
            "id": "d92a3b83-e550-4db7-867e-0157417f528b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58318fc5-3f31-44cf-ad3e-215270dae337",
            "compositeImage": {
                "id": "b480edd4-8886-4f2b-a5fa-5154017c207f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d92a3b83-e550-4db7-867e-0157417f528b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "375ef0a4-ef86-429e-b7bd-a9362fe9d493",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d92a3b83-e550-4db7-867e-0157417f528b",
                    "LayerId": "fc5f8d91-daaf-489a-a191-d12066fd5d09"
                }
            ]
        },
        {
            "id": "d6d1f776-2583-441b-bafb-d35ec8bb990e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58318fc5-3f31-44cf-ad3e-215270dae337",
            "compositeImage": {
                "id": "d68a8f36-53e9-461b-a00b-2a375a337508",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6d1f776-2583-441b-bafb-d35ec8bb990e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e78cba2-f8f0-426a-be1f-5ec45ff9d042",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6d1f776-2583-441b-bafb-d35ec8bb990e",
                    "LayerId": "fc5f8d91-daaf-489a-a191-d12066fd5d09"
                }
            ]
        },
        {
            "id": "0211d006-35dc-40a7-a00b-13666291ae67",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58318fc5-3f31-44cf-ad3e-215270dae337",
            "compositeImage": {
                "id": "fe81326c-c3ad-40af-83fc-cb9ae90e0314",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0211d006-35dc-40a7-a00b-13666291ae67",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06fbe71f-8457-4601-b695-ce3c20af82a9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0211d006-35dc-40a7-a00b-13666291ae67",
                    "LayerId": "fc5f8d91-daaf-489a-a191-d12066fd5d09"
                }
            ]
        },
        {
            "id": "6f78d792-7201-48a8-ac23-6e4461e4c385",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58318fc5-3f31-44cf-ad3e-215270dae337",
            "compositeImage": {
                "id": "0a0ddf05-666a-41d1-9d7d-f9b9067f3824",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f78d792-7201-48a8-ac23-6e4461e4c385",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1042312-1587-4b39-b680-0dc38f97c4bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f78d792-7201-48a8-ac23-6e4461e4c385",
                    "LayerId": "fc5f8d91-daaf-489a-a191-d12066fd5d09"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "fc5f8d91-daaf-489a-a191-d12066fd5d09",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "58318fc5-3f31-44cf-ad3e-215270dae337",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}