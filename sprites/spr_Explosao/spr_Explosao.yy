{
    "id": "a9ed42ef-4976-4a57-836d-133269ad8d38",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Explosao",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 320,
    "bbox_left": 18,
    "bbox_right": 311,
    "bbox_top": 45,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "29e97d0e-5adf-49cd-86f2-55a4c1747903",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9ed42ef-4976-4a57-836d-133269ad8d38",
            "compositeImage": {
                "id": "350d11f6-1b83-4976-b736-da5085ff7260",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "29e97d0e-5adf-49cd-86f2-55a4c1747903",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb8d079d-9eac-42df-a4fe-d88c956da744",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "29e97d0e-5adf-49cd-86f2-55a4c1747903",
                    "LayerId": "7d97d16e-66bf-4d58-9149-e0f4aaf1ab2a"
                }
            ]
        },
        {
            "id": "0bd7ae26-159b-4d11-ae6e-76809159cab0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9ed42ef-4976-4a57-836d-133269ad8d38",
            "compositeImage": {
                "id": "92e9d90d-e66a-41fc-af16-5461165383a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bd7ae26-159b-4d11-ae6e-76809159cab0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da043e4e-ba15-4eb8-bc7d-db625cd957c1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bd7ae26-159b-4d11-ae6e-76809159cab0",
                    "LayerId": "7d97d16e-66bf-4d58-9149-e0f4aaf1ab2a"
                }
            ]
        },
        {
            "id": "be1dfb4e-0657-4e4e-8ce8-e84843b76d75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9ed42ef-4976-4a57-836d-133269ad8d38",
            "compositeImage": {
                "id": "f028e280-32ca-49a2-99df-fe9b63d7ce51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "be1dfb4e-0657-4e4e-8ce8-e84843b76d75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0baff7b-5b58-4672-8b14-e25d8f192044",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "be1dfb4e-0657-4e4e-8ce8-e84843b76d75",
                    "LayerId": "7d97d16e-66bf-4d58-9149-e0f4aaf1ab2a"
                }
            ]
        },
        {
            "id": "c5cc9627-00cc-4280-a01e-2adff406bbcc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9ed42ef-4976-4a57-836d-133269ad8d38",
            "compositeImage": {
                "id": "c2391ef3-9d72-4863-99ad-3dd89fa4dcbc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c5cc9627-00cc-4280-a01e-2adff406bbcc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00a4a8be-380a-4425-8566-80ea682a34e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c5cc9627-00cc-4280-a01e-2adff406bbcc",
                    "LayerId": "7d97d16e-66bf-4d58-9149-e0f4aaf1ab2a"
                }
            ]
        },
        {
            "id": "fd8f61f2-9619-4bfa-a77a-0bebc6aafb23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9ed42ef-4976-4a57-836d-133269ad8d38",
            "compositeImage": {
                "id": "0f580d25-81a1-4153-ad34-ac041cc9ce49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd8f61f2-9619-4bfa-a77a-0bebc6aafb23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0772eea6-3edc-48ec-9a0e-46a9d4c6f8f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd8f61f2-9619-4bfa-a77a-0bebc6aafb23",
                    "LayerId": "7d97d16e-66bf-4d58-9149-e0f4aaf1ab2a"
                }
            ]
        },
        {
            "id": "cfde2e67-cca2-4710-884d-75446a06fb5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9ed42ef-4976-4a57-836d-133269ad8d38",
            "compositeImage": {
                "id": "eaab9bbe-2f68-4bcc-84b7-c976702c8a50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cfde2e67-cca2-4710-884d-75446a06fb5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7633eaf1-ba4c-4900-a6ed-f476139a614e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfde2e67-cca2-4710-884d-75446a06fb5b",
                    "LayerId": "7d97d16e-66bf-4d58-9149-e0f4aaf1ab2a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 350,
    "layers": [
        {
            "id": "7d97d16e-66bf-4d58-9149-e0f4aaf1ab2a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a9ed42ef-4976-4a57-836d-133269ad8d38",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 350,
    "xorig": 169,
    "yorig": 177
}