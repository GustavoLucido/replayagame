{
    "id": "1387bdcc-32ab-4975-a641-0e2db095e4e1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_TiroShotgun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 5,
    "bbox_right": 9,
    "bbox_top": 6,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "66b0af8a-3b79-4f7c-b834-1877be1d0906",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1387bdcc-32ab-4975-a641-0e2db095e4e1",
            "compositeImage": {
                "id": "d8abe104-cc3a-4db1-ab40-e52f4369a91a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "66b0af8a-3b79-4f7c-b834-1877be1d0906",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6298bc0a-2653-4cfc-9a2e-f3e46ccad823",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "66b0af8a-3b79-4f7c-b834-1877be1d0906",
                    "LayerId": "46ac7cb0-6d78-4a74-b03f-7a74d34ac893"
                }
            ]
        },
        {
            "id": "9ba6361b-254b-4ee7-abc7-0a22258b70e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1387bdcc-32ab-4975-a641-0e2db095e4e1",
            "compositeImage": {
                "id": "64f2b224-207a-4bd7-9d9b-9058ada8bc7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ba6361b-254b-4ee7-abc7-0a22258b70e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "115cf488-adda-45d2-a1e4-3f77aed1ecf7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ba6361b-254b-4ee7-abc7-0a22258b70e8",
                    "LayerId": "46ac7cb0-6d78-4a74-b03f-7a74d34ac893"
                }
            ]
        },
        {
            "id": "1865facc-9ff0-4e4f-8e73-aeba97165304",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1387bdcc-32ab-4975-a641-0e2db095e4e1",
            "compositeImage": {
                "id": "a7caa913-0f49-4df9-b75a-bda6a0a219de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1865facc-9ff0-4e4f-8e73-aeba97165304",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9da42eba-7c44-4155-a58c-8f0a20b0b0fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1865facc-9ff0-4e4f-8e73-aeba97165304",
                    "LayerId": "46ac7cb0-6d78-4a74-b03f-7a74d34ac893"
                }
            ]
        },
        {
            "id": "895df009-3973-4731-bfe4-e3b3788b25b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1387bdcc-32ab-4975-a641-0e2db095e4e1",
            "compositeImage": {
                "id": "d33df766-f8ff-4dbe-931f-0027adbbdd53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "895df009-3973-4731-bfe4-e3b3788b25b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2df165d9-36a7-4d36-bf20-f0a21164fd98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "895df009-3973-4731-bfe4-e3b3788b25b5",
                    "LayerId": "46ac7cb0-6d78-4a74-b03f-7a74d34ac893"
                }
            ]
        },
        {
            "id": "9a095b86-513c-4e9a-b6f4-ab56570bd04e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1387bdcc-32ab-4975-a641-0e2db095e4e1",
            "compositeImage": {
                "id": "3e7236fa-cf51-4726-a822-dd64678a5d5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a095b86-513c-4e9a-b6f4-ab56570bd04e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff6ccd31-1211-4a68-b27b-a7ee7dff0126",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a095b86-513c-4e9a-b6f4-ab56570bd04e",
                    "LayerId": "46ac7cb0-6d78-4a74-b03f-7a74d34ac893"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "46ac7cb0-6d78-4a74-b03f-7a74d34ac893",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1387bdcc-32ab-4975-a641-0e2db095e4e1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}