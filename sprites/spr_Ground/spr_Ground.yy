{
    "id": "e4afbf6c-6e85-4ccd-a8d4-3678da9f9352",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Ground",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "616b240f-ba93-45e2-9eec-480c230b2b6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4afbf6c-6e85-4ccd-a8d4-3678da9f9352",
            "compositeImage": {
                "id": "b7e42711-a6c9-4432-a1de-068f0189d784",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "616b240f-ba93-45e2-9eec-480c230b2b6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a46145d0-a517-45ed-bff5-088b776bc155",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "616b240f-ba93-45e2-9eec-480c230b2b6c",
                    "LayerId": "8e1d7bfb-d64e-4e54-b6e5-94e2b508708e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8e1d7bfb-d64e-4e54-b6e5-94e2b508708e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e4afbf6c-6e85-4ccd-a8d4-3678da9f9352",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}