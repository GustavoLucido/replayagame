{
    "id": "3fc53b8e-7401-472d-af25-80f949034ded",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_BurningGround",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b215260a-8473-48d5-9fae-675a1ad04156",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3fc53b8e-7401-472d-af25-80f949034ded",
            "compositeImage": {
                "id": "6cdb6614-7dce-4c96-bd34-1b95bca70596",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b215260a-8473-48d5-9fae-675a1ad04156",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa2b0caf-f577-4714-a005-81b701ac92f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b215260a-8473-48d5-9fae-675a1ad04156",
                    "LayerId": "596889a0-87be-4929-812f-b6faae53a2c6"
                }
            ]
        },
        {
            "id": "ed9729b3-31b9-4bff-8225-367b970f4e4c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3fc53b8e-7401-472d-af25-80f949034ded",
            "compositeImage": {
                "id": "f89c0559-3421-4d84-bb99-1672f3c22d1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed9729b3-31b9-4bff-8225-367b970f4e4c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1aabdf74-83d8-4432-ad40-9cde4e10f757",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed9729b3-31b9-4bff-8225-367b970f4e4c",
                    "LayerId": "596889a0-87be-4929-812f-b6faae53a2c6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "596889a0-87be-4929-812f-b6faae53a2c6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3fc53b8e-7401-472d-af25-80f949034ded",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}