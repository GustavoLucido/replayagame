{
    "id": "8cda43af-9993-4f24-a3a5-c288d2c7c4f4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Tiro",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 11,
    "bbox_right": 20,
    "bbox_top": 11,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b7c0d1d9-2fcd-423c-b877-7f67f4f9780b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8cda43af-9993-4f24-a3a5-c288d2c7c4f4",
            "compositeImage": {
                "id": "3994f10e-31eb-40c4-b4e8-df4de6a07cc2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7c0d1d9-2fcd-423c-b877-7f67f4f9780b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6716f2ac-11d9-480d-905a-4eff8bdd9393",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7c0d1d9-2fcd-423c-b877-7f67f4f9780b",
                    "LayerId": "85c96129-04ad-4217-bef1-7565d7052170"
                }
            ]
        },
        {
            "id": "41eff7a3-b0f3-44c2-a45a-9eab79f6e9d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8cda43af-9993-4f24-a3a5-c288d2c7c4f4",
            "compositeImage": {
                "id": "bbc0cdb7-b3ca-463b-a6ca-afb57725ae34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41eff7a3-b0f3-44c2-a45a-9eab79f6e9d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "299f8e1c-2fc9-4f14-ad4f-b60639ce95b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41eff7a3-b0f3-44c2-a45a-9eab79f6e9d0",
                    "LayerId": "85c96129-04ad-4217-bef1-7565d7052170"
                }
            ]
        },
        {
            "id": "974ec2ee-ce5a-474a-bdf2-142c14e39a32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8cda43af-9993-4f24-a3a5-c288d2c7c4f4",
            "compositeImage": {
                "id": "f3c2b92f-0d8f-4829-bb13-a414eb938356",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "974ec2ee-ce5a-474a-bdf2-142c14e39a32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05cce82f-d762-431c-bb8d-36fddf8e7bc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "974ec2ee-ce5a-474a-bdf2-142c14e39a32",
                    "LayerId": "85c96129-04ad-4217-bef1-7565d7052170"
                }
            ]
        },
        {
            "id": "8b5e8d62-c9cc-482b-95fa-57f8df171731",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8cda43af-9993-4f24-a3a5-c288d2c7c4f4",
            "compositeImage": {
                "id": "e2d411f5-c42f-49f9-9d83-bfe2eba3a650",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b5e8d62-c9cc-482b-95fa-57f8df171731",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a154a07e-7bbc-4068-8ba6-e79354f82afe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b5e8d62-c9cc-482b-95fa-57f8df171731",
                    "LayerId": "85c96129-04ad-4217-bef1-7565d7052170"
                }
            ]
        },
        {
            "id": "822c073e-4a19-4f34-8d3d-1addab96827f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8cda43af-9993-4f24-a3a5-c288d2c7c4f4",
            "compositeImage": {
                "id": "c0c20548-f5b2-4e70-9b7c-5090c5b2c896",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "822c073e-4a19-4f34-8d3d-1addab96827f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91087cf3-b6c9-42a6-a862-2b5cbce2eaa6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "822c073e-4a19-4f34-8d3d-1addab96827f",
                    "LayerId": "85c96129-04ad-4217-bef1-7565d7052170"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "85c96129-04ad-4217-bef1-7565d7052170",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8cda43af-9993-4f24-a3a5-c288d2c7c4f4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}