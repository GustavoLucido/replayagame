{
    "id": "244edf5d-6e64-4966-b7f1-46abd74bc25e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_InimigoMetralha",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "03c2e799-cbb9-42cb-b6db-287cd3091373",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "244edf5d-6e64-4966-b7f1-46abd74bc25e",
            "compositeImage": {
                "id": "dbe6f784-5baf-448c-b2d3-d90668a5847a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03c2e799-cbb9-42cb-b6db-287cd3091373",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb392e5e-9c2d-479b-b786-89610e6977eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03c2e799-cbb9-42cb-b6db-287cd3091373",
                    "LayerId": "4e1aecae-f52b-4723-b06c-eb73c014e8e3"
                }
            ]
        },
        {
            "id": "b4a654dd-caf6-45a4-9f5e-9a6338cbfa32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "244edf5d-6e64-4966-b7f1-46abd74bc25e",
            "compositeImage": {
                "id": "dca2279d-624e-431f-8a81-42db9d56c788",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4a654dd-caf6-45a4-9f5e-9a6338cbfa32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd7f6aa3-c9bc-4439-8379-b601abdd0fe8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4a654dd-caf6-45a4-9f5e-9a6338cbfa32",
                    "LayerId": "4e1aecae-f52b-4723-b06c-eb73c014e8e3"
                }
            ]
        },
        {
            "id": "a988b043-7e5b-4c66-ad73-77d1ef0e5fbf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "244edf5d-6e64-4966-b7f1-46abd74bc25e",
            "compositeImage": {
                "id": "cecde772-ea9c-4f09-8a30-774a0dd46900",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a988b043-7e5b-4c66-ad73-77d1ef0e5fbf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2212e183-fc27-4d04-b5cc-bfb04cfa7113",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a988b043-7e5b-4c66-ad73-77d1ef0e5fbf",
                    "LayerId": "4e1aecae-f52b-4723-b06c-eb73c014e8e3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "4e1aecae-f52b-4723-b06c-eb73c014e8e3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "244edf5d-6e64-4966-b7f1-46abd74bc25e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 64
}