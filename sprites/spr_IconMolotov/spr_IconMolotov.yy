{
    "id": "c9426289-c294-44ef-b209-4e721325d43b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_IconMolotov",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 43,
    "bbox_left": 16,
    "bbox_right": 31,
    "bbox_top": 13,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "018169ef-f89d-4275-ba59-1d3b0a1c638a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9426289-c294-44ef-b209-4e721325d43b",
            "compositeImage": {
                "id": "87668b2a-23ee-42cd-b29e-1df1c5efab9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "018169ef-f89d-4275-ba59-1d3b0a1c638a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81f80973-cf5c-4a99-97b8-9f1fdd55e94e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "018169ef-f89d-4275-ba59-1d3b0a1c638a",
                    "LayerId": "b9b13b6e-9950-4bb6-b5ea-c839288b3a60"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "b9b13b6e-9950-4bb6-b5ea-c839288b3a60",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c9426289-c294-44ef-b209-4e721325d43b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 28
}