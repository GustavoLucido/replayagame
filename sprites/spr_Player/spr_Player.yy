{
    "id": "cb9a8b49-01f5-4cc2-ab82-efb5fa9719e2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Player",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 159,
    "bbox_left": 16,
    "bbox_right": 79,
    "bbox_top": 16,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "40a461a1-4c2e-40b7-9f84-4d1957f8c0c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb9a8b49-01f5-4cc2-ab82-efb5fa9719e2",
            "compositeImage": {
                "id": "2a50f10d-137a-4783-8aa0-13a3831b1523",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40a461a1-4c2e-40b7-9f84-4d1957f8c0c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a237e56e-a13c-437f-9904-c163d76a3fa7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40a461a1-4c2e-40b7-9f84-4d1957f8c0c9",
                    "LayerId": "76a61422-ccdd-478d-a0fb-0a77f919ee31"
                }
            ]
        },
        {
            "id": "bfc47682-2968-4193-85a2-f0da14cdefab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb9a8b49-01f5-4cc2-ab82-efb5fa9719e2",
            "compositeImage": {
                "id": "104bc693-8f79-4de5-8555-b8854ed88074",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bfc47682-2968-4193-85a2-f0da14cdefab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87b0be16-f759-4a6e-914c-77c6222cecb2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfc47682-2968-4193-85a2-f0da14cdefab",
                    "LayerId": "76a61422-ccdd-478d-a0fb-0a77f919ee31"
                }
            ]
        },
        {
            "id": "41797c64-90af-40ea-a0e4-21133302b9c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb9a8b49-01f5-4cc2-ab82-efb5fa9719e2",
            "compositeImage": {
                "id": "ab08efdb-3c39-44ae-a431-7cda16af9839",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41797c64-90af-40ea-a0e4-21133302b9c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d82fc38-e012-496e-98e1-5e690de92aaf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41797c64-90af-40ea-a0e4-21133302b9c9",
                    "LayerId": "76a61422-ccdd-478d-a0fb-0a77f919ee31"
                }
            ]
        },
        {
            "id": "7c358147-40b9-450a-8c19-0e2814f4840d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cb9a8b49-01f5-4cc2-ab82-efb5fa9719e2",
            "compositeImage": {
                "id": "866de0b1-cfd7-4601-b172-8a2db4d88905",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c358147-40b9-450a-8c19-0e2814f4840d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09fbd720-90d0-400d-95bf-e48a9ae5dd65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c358147-40b9-450a-8c19-0e2814f4840d",
                    "LayerId": "76a61422-ccdd-478d-a0fb-0a77f919ee31"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 165,
    "layers": [
        {
            "id": "76a61422-ccdd-478d-a0fb-0a77f919ee31",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cb9a8b49-01f5-4cc2-ab82-efb5fa9719e2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 11,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 80
}