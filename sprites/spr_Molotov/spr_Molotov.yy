{
    "id": "50a85db4-19c3-4d27-a287-bfd12669aac2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Molotov",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 43,
    "bbox_left": 16,
    "bbox_right": 31,
    "bbox_top": 13,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5c32eabb-5876-4d03-98e7-c142ce2b3c6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50a85db4-19c3-4d27-a287-bfd12669aac2",
            "compositeImage": {
                "id": "59fd73ad-3edc-4eb6-9ea8-613ee78f8482",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c32eabb-5876-4d03-98e7-c142ce2b3c6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca2ffd6d-10e1-4c22-ad26-d174dc12dee6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c32eabb-5876-4d03-98e7-c142ce2b3c6e",
                    "LayerId": "7e1d7c17-44f5-42e9-861a-f674dc48c31a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "7e1d7c17-44f5-42e9-861a-f674dc48c31a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "50a85db4-19c3-4d27-a287-bfd12669aac2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 28
}