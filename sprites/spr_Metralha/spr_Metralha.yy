{
    "id": "4204cfd6-da31-46e1-9cb1-eed117579a69",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Metralha",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 43,
    "bbox_left": 4,
    "bbox_right": 151,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d6e191d8-c4d2-4032-8f0d-61dd8044563f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4204cfd6-da31-46e1-9cb1-eed117579a69",
            "compositeImage": {
                "id": "9e02320b-e0bc-420e-984b-b9bb4b122fa4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6e191d8-c4d2-4032-8f0d-61dd8044563f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef216923-09c6-4581-8aa0-712c2ab6e43e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6e191d8-c4d2-4032-8f0d-61dd8044563f",
                    "LayerId": "fd257edf-b3ae-41bf-be4b-70a079400229"
                }
            ]
        },
        {
            "id": "97e780a2-2d9c-4a6e-a90e-2ebeef54a238",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4204cfd6-da31-46e1-9cb1-eed117579a69",
            "compositeImage": {
                "id": "04b7271b-8b3b-4050-9b39-0abf9b693119",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97e780a2-2d9c-4a6e-a90e-2ebeef54a238",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a1a6b11d-4840-4109-a56f-13726e347001",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97e780a2-2d9c-4a6e-a90e-2ebeef54a238",
                    "LayerId": "fd257edf-b3ae-41bf-be4b-70a079400229"
                }
            ]
        },
        {
            "id": "1b1e5e5c-f1b6-400f-88a8-3b71a3d83ee6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4204cfd6-da31-46e1-9cb1-eed117579a69",
            "compositeImage": {
                "id": "db7c3443-be4b-453a-8bdc-425b906a4a74",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b1e5e5c-f1b6-400f-88a8-3b71a3d83ee6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0441904d-64d5-488d-bdc2-5dbd349c85f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b1e5e5c-f1b6-400f-88a8-3b71a3d83ee6",
                    "LayerId": "fd257edf-b3ae-41bf-be4b-70a079400229"
                }
            ]
        },
        {
            "id": "07bc8f16-0edd-405c-8c62-0ff26be32b61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4204cfd6-da31-46e1-9cb1-eed117579a69",
            "compositeImage": {
                "id": "09b37eb2-6456-4216-ab49-e810febeb0cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07bc8f16-0edd-405c-8c62-0ff26be32b61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e116a343-09e4-4548-8c3c-24139bc0c8be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07bc8f16-0edd-405c-8c62-0ff26be32b61",
                    "LayerId": "fd257edf-b3ae-41bf-be4b-70a079400229"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 45,
    "layers": [
        {
            "id": "fd257edf-b3ae-41bf-be4b-70a079400229",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4204cfd6-da31-46e1-9cb1-eed117579a69",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 40,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 180,
    "xorig": 62,
    "yorig": 18
}