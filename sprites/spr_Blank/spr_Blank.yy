{
    "id": "dde5661c-1583-4a56-8e7b-0560a8457260",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Blank",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 40,
    "bbox_left": 24,
    "bbox_right": 43,
    "bbox_top": 24,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0fd45ebf-7b82-4e37-98c1-ae9710031875",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dde5661c-1583-4a56-8e7b-0560a8457260",
            "compositeImage": {
                "id": "bc26579a-1e1b-487e-aa6d-001c1ef54cbb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0fd45ebf-7b82-4e37-98c1-ae9710031875",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0e739b5-2415-424b-ae41-c430cd3b4f50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0fd45ebf-7b82-4e37-98c1-ae9710031875",
                    "LayerId": "c31940f8-f9a6-43a4-93d5-6b0cede82260"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c31940f8-f9a6-43a4-93d5-6b0cede82260",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dde5661c-1583-4a56-8e7b-0560a8457260",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}