{
    "id": "e53ac07b-aa03-4a84-b896-c631661dd880",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_MascaraVida",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 285,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "90f43b61-405c-44da-8abd-7f2ff85a3c81",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e53ac07b-aa03-4a84-b896-c631661dd880",
            "compositeImage": {
                "id": "3b432d24-6955-4c8d-bdb2-8c856c9c6efd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90f43b61-405c-44da-8abd-7f2ff85a3c81",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ee1197a-d3a5-4df9-b7a1-d8f08e2744db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90f43b61-405c-44da-8abd-7f2ff85a3c81",
                    "LayerId": "9b0e0a2d-eed5-49e1-99b6-88c32f3df363"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "9b0e0a2d-eed5-49e1-99b6-88c32f3df363",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e53ac07b-aa03-4a84-b896-c631661dd880",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 286,
    "xorig": 0,
    "yorig": 0
}