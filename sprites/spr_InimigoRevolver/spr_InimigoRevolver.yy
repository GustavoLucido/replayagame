{
    "id": "c9e25ec3-6bd3-427a-92e4-2707350c1697",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_InimigoRevolver",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 167,
    "bbox_left": 10,
    "bbox_right": 68,
    "bbox_top": 31,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ba87e565-2041-460a-8654-860bafe54d24",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9e25ec3-6bd3-427a-92e4-2707350c1697",
            "compositeImage": {
                "id": "a2794117-39b4-42b2-9931-d6aa00dd6fb7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba87e565-2041-460a-8654-860bafe54d24",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "020b0b67-9ff9-408e-b6da-8c6037c8e77c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba87e565-2041-460a-8654-860bafe54d24",
                    "LayerId": "2337ba09-45fc-4a03-b2b1-57198a9fc259"
                }
            ]
        },
        {
            "id": "db32b492-85ed-4a3c-82fa-dc11c317788e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9e25ec3-6bd3-427a-92e4-2707350c1697",
            "compositeImage": {
                "id": "9cebb233-e8c4-4c14-b638-7bfb4c6a7439",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db32b492-85ed-4a3c-82fa-dc11c317788e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acdc25d1-b101-44da-a8db-414a8ef41548",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db32b492-85ed-4a3c-82fa-dc11c317788e",
                    "LayerId": "2337ba09-45fc-4a03-b2b1-57198a9fc259"
                }
            ]
        },
        {
            "id": "3cba736c-5081-4afc-9508-e487b319965a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9e25ec3-6bd3-427a-92e4-2707350c1697",
            "compositeImage": {
                "id": "97d83887-c2a7-45d3-993e-22e8dafcd125",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cba736c-5081-4afc-9508-e487b319965a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb23b534-9996-4afb-bcfa-2ee24f0580f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cba736c-5081-4afc-9508-e487b319965a",
                    "LayerId": "2337ba09-45fc-4a03-b2b1-57198a9fc259"
                }
            ]
        },
        {
            "id": "87b43066-0e02-4f30-8a9a-b8a04ea2b8d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9e25ec3-6bd3-427a-92e4-2707350c1697",
            "compositeImage": {
                "id": "8fb453ec-478d-4c88-9f65-f9cca70aad11",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87b43066-0e02-4f30-8a9a-b8a04ea2b8d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f165b27a-2eed-4638-bc36-9a50907e155e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87b43066-0e02-4f30-8a9a-b8a04ea2b8d9",
                    "LayerId": "2337ba09-45fc-4a03-b2b1-57198a9fc259"
                }
            ]
        },
        {
            "id": "e7e34d1b-c9fa-4160-ac1e-bbac24562188",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9e25ec3-6bd3-427a-92e4-2707350c1697",
            "compositeImage": {
                "id": "7005e949-ff76-4426-855d-7c37f03ff169",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7e34d1b-c9fa-4160-ac1e-bbac24562188",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd74a876-2821-4488-b0c2-ec191f413469",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7e34d1b-c9fa-4160-ac1e-bbac24562188",
                    "LayerId": "2337ba09-45fc-4a03-b2b1-57198a9fc259"
                }
            ]
        },
        {
            "id": "f24d5533-aa3a-4bae-a6d8-d91fe4d04e96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9e25ec3-6bd3-427a-92e4-2707350c1697",
            "compositeImage": {
                "id": "77cfe62a-1b13-4d41-9efc-9229fc1fa0f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f24d5533-aa3a-4bae-a6d8-d91fe4d04e96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b30dd165-57cd-4fd5-aebb-726eb20a056f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f24d5533-aa3a-4bae-a6d8-d91fe4d04e96",
                    "LayerId": "2337ba09-45fc-4a03-b2b1-57198a9fc259"
                }
            ]
        },
        {
            "id": "a3f4c42f-6019-4926-bfce-8198497d7cf2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9e25ec3-6bd3-427a-92e4-2707350c1697",
            "compositeImage": {
                "id": "b5e3886e-b4df-447e-a079-32afe7652e3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3f4c42f-6019-4926-bfce-8198497d7cf2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d07d0c76-afc1-4abb-857d-7e577c5319e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3f4c42f-6019-4926-bfce-8198497d7cf2",
                    "LayerId": "2337ba09-45fc-4a03-b2b1-57198a9fc259"
                }
            ]
        },
        {
            "id": "719f7b65-e03f-433c-867e-4bd9187482c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9e25ec3-6bd3-427a-92e4-2707350c1697",
            "compositeImage": {
                "id": "6d85e882-2a44-41f4-949f-b190e5327804",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "719f7b65-e03f-433c-867e-4bd9187482c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60bcda2b-8834-4863-b9d5-e96c6dea25dd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "719f7b65-e03f-433c-867e-4bd9187482c7",
                    "LayerId": "2337ba09-45fc-4a03-b2b1-57198a9fc259"
                }
            ]
        },
        {
            "id": "24ecd999-83ce-495a-b2f8-53ac2ae40f36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9e25ec3-6bd3-427a-92e4-2707350c1697",
            "compositeImage": {
                "id": "430b3f2e-4b2c-4b0f-899b-839c83c1b200",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "24ecd999-83ce-495a-b2f8-53ac2ae40f36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84326ab7-5f80-47ec-8bed-c3df2dcb730c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "24ecd999-83ce-495a-b2f8-53ac2ae40f36",
                    "LayerId": "2337ba09-45fc-4a03-b2b1-57198a9fc259"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 173,
    "layers": [
        {
            "id": "2337ba09-45fc-4a03-b2b1-57198a9fc259",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c9e25ec3-6bd3-427a-92e4-2707350c1697",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 78,
    "xorig": 39,
    "yorig": 100
}