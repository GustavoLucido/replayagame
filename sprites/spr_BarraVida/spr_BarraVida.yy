{
    "id": "5d262c8e-7433-439c-8f06-0cfbdcfa3396",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_BarraVida",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 285,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e407ec04-efa3-4e0a-a092-ee83a5fba3c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d262c8e-7433-439c-8f06-0cfbdcfa3396",
            "compositeImage": {
                "id": "cb0f2333-ab0c-4c72-bd27-6a81b0f4f38c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e407ec04-efa3-4e0a-a092-ee83a5fba3c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8baf38eb-9427-4961-bf71-2b7ba4242da7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e407ec04-efa3-4e0a-a092-ee83a5fba3c1",
                    "LayerId": "ad3898c5-73f9-4dff-bf4e-672a4a10afb5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ad3898c5-73f9-4dff-bf4e-672a4a10afb5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5d262c8e-7433-439c-8f06-0cfbdcfa3396",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 286,
    "xorig": 0,
    "yorig": 0
}