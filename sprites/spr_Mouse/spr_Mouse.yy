{
    "id": "00ef9940-a076-4551-846e-91dbe73b5f5d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Mouse",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 14,
    "bbox_right": 49,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "37e0f3c0-514d-4a00-aa03-baec378a6027",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "00ef9940-a076-4551-846e-91dbe73b5f5d",
            "compositeImage": {
                "id": "af77e48a-2ba6-4eb3-bd67-92c45e8dfa9c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37e0f3c0-514d-4a00-aa03-baec378a6027",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c63502e0-7bfa-4d0c-b337-a95e0a391c22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37e0f3c0-514d-4a00-aa03-baec378a6027",
                    "LayerId": "833b7da4-dd9b-46ee-a2d8-637a38e2b538"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "833b7da4-dd9b-46ee-a2d8-637a38e2b538",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "00ef9940-a076-4551-846e-91dbe73b5f5d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}