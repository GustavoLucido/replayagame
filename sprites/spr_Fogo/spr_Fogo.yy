{
    "id": "7ee4c568-54b3-43b3-b545-69cdf8896db4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Fogo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 96,
    "bbox_left": 4,
    "bbox_right": 93,
    "bbox_top": 22,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "990bee53-6acc-4f74-8bb8-86c18b19ad0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7ee4c568-54b3-43b3-b545-69cdf8896db4",
            "compositeImage": {
                "id": "8c98947e-565e-46ae-93c4-983ee951fcf7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "990bee53-6acc-4f74-8bb8-86c18b19ad0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a57a5134-4422-422b-a02b-ce9887d8816c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "990bee53-6acc-4f74-8bb8-86c18b19ad0f",
                    "LayerId": "b4616495-d29b-45c1-9e16-564879c7c5e9"
                }
            ]
        },
        {
            "id": "9d96a118-3557-4de7-92d5-3a445ec5f0f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7ee4c568-54b3-43b3-b545-69cdf8896db4",
            "compositeImage": {
                "id": "dc739132-d21f-4fc5-9bae-8147b819c442",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d96a118-3557-4de7-92d5-3a445ec5f0f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c6661a4-415a-4560-a900-3a3e50d8be92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d96a118-3557-4de7-92d5-3a445ec5f0f9",
                    "LayerId": "b4616495-d29b-45c1-9e16-564879c7c5e9"
                }
            ]
        },
        {
            "id": "646997ff-8784-46db-a2c7-306aafa5aba5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7ee4c568-54b3-43b3-b545-69cdf8896db4",
            "compositeImage": {
                "id": "da019fa4-8ca7-4071-86d5-c04ab8056b0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "646997ff-8784-46db-a2c7-306aafa5aba5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c17b9ab6-8691-46d0-a973-6fe14ba2f571",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "646997ff-8784-46db-a2c7-306aafa5aba5",
                    "LayerId": "b4616495-d29b-45c1-9e16-564879c7c5e9"
                }
            ]
        },
        {
            "id": "d4fd8f73-3436-4d9f-9833-f26420adbc86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7ee4c568-54b3-43b3-b545-69cdf8896db4",
            "compositeImage": {
                "id": "aa79cc34-507e-4c93-950d-9834c7788af8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4fd8f73-3436-4d9f-9833-f26420adbc86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c5ea5e2-d2bc-4ba4-ba62-a4f3e171e400",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4fd8f73-3436-4d9f-9833-f26420adbc86",
                    "LayerId": "b4616495-d29b-45c1-9e16-564879c7c5e9"
                }
            ]
        },
        {
            "id": "ec41d2da-70e5-44cb-9ac9-ecd8731bb985",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7ee4c568-54b3-43b3-b545-69cdf8896db4",
            "compositeImage": {
                "id": "67acdc13-1878-421d-891a-8a388b9fb8d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ec41d2da-70e5-44cb-9ac9-ecd8731bb985",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "677489b9-50d5-49ff-b193-32175b96e44c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ec41d2da-70e5-44cb-9ac9-ecd8731bb985",
                    "LayerId": "b4616495-d29b-45c1-9e16-564879c7c5e9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "b4616495-d29b-45c1-9e16-564879c7c5e9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7ee4c568-54b3-43b3-b545-69cdf8896db4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 51,
    "yorig": 83
}