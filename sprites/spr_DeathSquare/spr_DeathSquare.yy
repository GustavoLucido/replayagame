{
    "id": "c00e467d-ceac-4803-80e4-98f5f5be212d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_DeathSquare",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c067096e-efb7-4348-a9a0-6fe2d0200462",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c00e467d-ceac-4803-80e4-98f5f5be212d",
            "compositeImage": {
                "id": "f3025dbb-51b4-4eca-9d65-93ca156faea3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c067096e-efb7-4348-a9a0-6fe2d0200462",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b020bc95-ced6-473b-852b-85b86c8e4c39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c067096e-efb7-4348-a9a0-6fe2d0200462",
                    "LayerId": "6000dc66-4bef-4863-8cbb-a99ae60c518e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "6000dc66-4bef-4863-8cbb-a99ae60c518e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c00e467d-ceac-4803-80e4-98f5f5be212d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}