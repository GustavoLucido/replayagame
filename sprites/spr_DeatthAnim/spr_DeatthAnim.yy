{
    "id": "6f5a7fb6-1a95-4d2c-a975-a79e4dcb66e8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_DeatthAnim",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 6,
    "bbox_right": 293,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "711cfe54-b272-4ca7-9947-ae4e50843229",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f5a7fb6-1a95-4d2c-a975-a79e4dcb66e8",
            "compositeImage": {
                "id": "661a558e-19d6-42ca-b289-1fba8dd13910",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "711cfe54-b272-4ca7-9947-ae4e50843229",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "211c698f-d35f-458f-a035-a59000e1047a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "711cfe54-b272-4ca7-9947-ae4e50843229",
                    "LayerId": "14bf9c23-4133-41c1-96d1-eb3899c726c6"
                }
            ]
        },
        {
            "id": "449c2ee1-500b-46fc-9fef-c5ce2fef97c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f5a7fb6-1a95-4d2c-a975-a79e4dcb66e8",
            "compositeImage": {
                "id": "ad265ae8-43d5-4171-8472-29a1b3e96b01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "449c2ee1-500b-46fc-9fef-c5ce2fef97c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6255ca3f-5a09-4bad-bef6-458cca6630b6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "449c2ee1-500b-46fc-9fef-c5ce2fef97c9",
                    "LayerId": "14bf9c23-4133-41c1-96d1-eb3899c726c6"
                }
            ]
        },
        {
            "id": "0beccc4d-77df-4b14-a18e-ec126e38250c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f5a7fb6-1a95-4d2c-a975-a79e4dcb66e8",
            "compositeImage": {
                "id": "e5658501-587a-4597-afd7-f93af52875cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0beccc4d-77df-4b14-a18e-ec126e38250c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0beef54a-42c9-44b9-92a5-6c4a82683e58",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0beccc4d-77df-4b14-a18e-ec126e38250c",
                    "LayerId": "14bf9c23-4133-41c1-96d1-eb3899c726c6"
                }
            ]
        },
        {
            "id": "e21c0a78-5998-468d-a02a-b9200ea9011e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f5a7fb6-1a95-4d2c-a975-a79e4dcb66e8",
            "compositeImage": {
                "id": "8a758297-3e37-4240-ba27-cff319dd1053",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e21c0a78-5998-468d-a02a-b9200ea9011e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "81c447b7-1e7b-496c-99b5-692329b26b55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e21c0a78-5998-468d-a02a-b9200ea9011e",
                    "LayerId": "14bf9c23-4133-41c1-96d1-eb3899c726c6"
                }
            ]
        },
        {
            "id": "e817ef29-8acf-4e75-9767-9a64c21fda36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6f5a7fb6-1a95-4d2c-a975-a79e4dcb66e8",
            "compositeImage": {
                "id": "a6facc61-538e-4544-bd3f-18be43fe7558",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e817ef29-8acf-4e75-9767-9a64c21fda36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6db305bd-51e6-4b32-9c09-9d5bbffe6689",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e817ef29-8acf-4e75-9767-9a64c21fda36",
                    "LayerId": "14bf9c23-4133-41c1-96d1-eb3899c726c6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "14bf9c23-4133-41c1-96d1-eb3899c726c6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6f5a7fb6-1a95-4d2c-a975-a79e4dcb66e8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 300,
    "xorig": 150,
    "yorig": 150
}