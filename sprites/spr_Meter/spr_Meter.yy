{
    "id": "fdb8bb65-ed78-4fd6-8cb6-d0c5bb435994",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Meter",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 1,
    "bbox_right": 45,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d94ca6ed-38b3-4421-b49c-9958efbc6528",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb8bb65-ed78-4fd6-8cb6-d0c5bb435994",
            "compositeImage": {
                "id": "90a80f01-3e3f-424e-8b37-1515c5db657c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d94ca6ed-38b3-4421-b49c-9958efbc6528",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e25a11b4-cb0d-4cdc-8ef4-2a66fb96a7fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d94ca6ed-38b3-4421-b49c-9958efbc6528",
                    "LayerId": "f522eb41-1eff-43bc-8ddc-1e3882b10014"
                }
            ]
        },
        {
            "id": "6b06e75a-3925-4659-98e4-1b1434c217b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb8bb65-ed78-4fd6-8cb6-d0c5bb435994",
            "compositeImage": {
                "id": "93d82e97-596d-4bf9-9c0e-c65782996676",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b06e75a-3925-4659-98e4-1b1434c217b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f8f2dc0-228d-4f88-a79e-87e5ec701e30",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b06e75a-3925-4659-98e4-1b1434c217b7",
                    "LayerId": "f522eb41-1eff-43bc-8ddc-1e3882b10014"
                }
            ]
        },
        {
            "id": "ed3cfce1-b5cf-4403-aaff-b8fe25f05d09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb8bb65-ed78-4fd6-8cb6-d0c5bb435994",
            "compositeImage": {
                "id": "52c74cd7-b98b-414f-bcd8-dbcec7ec3151",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed3cfce1-b5cf-4403-aaff-b8fe25f05d09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "566397ba-6392-4dca-8eb6-723d74e42e8b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed3cfce1-b5cf-4403-aaff-b8fe25f05d09",
                    "LayerId": "f522eb41-1eff-43bc-8ddc-1e3882b10014"
                }
            ]
        },
        {
            "id": "b92b1ee0-1666-4cf8-a04a-56f194b7a619",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb8bb65-ed78-4fd6-8cb6-d0c5bb435994",
            "compositeImage": {
                "id": "552df5d7-9c13-4a8f-928f-13a5d643918c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b92b1ee0-1666-4cf8-a04a-56f194b7a619",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b33fd1e3-08d4-423d-b4a8-41c8f150b544",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b92b1ee0-1666-4cf8-a04a-56f194b7a619",
                    "LayerId": "f522eb41-1eff-43bc-8ddc-1e3882b10014"
                }
            ]
        },
        {
            "id": "ea73c981-34c8-4fbd-bd98-55894a2f4a06",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb8bb65-ed78-4fd6-8cb6-d0c5bb435994",
            "compositeImage": {
                "id": "19df099c-22b8-4a71-86c0-41a25c3f8b1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea73c981-34c8-4fbd-bd98-55894a2f4a06",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c4d504b-0a16-455c-8668-7fcb81b70ca3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea73c981-34c8-4fbd-bd98-55894a2f4a06",
                    "LayerId": "f522eb41-1eff-43bc-8ddc-1e3882b10014"
                }
            ]
        },
        {
            "id": "6407259d-532d-4bf5-ba05-e74b2a9b1654",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb8bb65-ed78-4fd6-8cb6-d0c5bb435994",
            "compositeImage": {
                "id": "77d9decc-2a3c-4767-9d3f-d020328b799f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6407259d-532d-4bf5-ba05-e74b2a9b1654",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09520b2e-30ef-4130-b29a-e5a7a6ed2a6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6407259d-532d-4bf5-ba05-e74b2a9b1654",
                    "LayerId": "f522eb41-1eff-43bc-8ddc-1e3882b10014"
                }
            ]
        },
        {
            "id": "59be850f-4ee5-44b6-8e78-384d9026e8e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb8bb65-ed78-4fd6-8cb6-d0c5bb435994",
            "compositeImage": {
                "id": "899b1d38-404e-42a3-9f00-9a0bfde9c43f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59be850f-4ee5-44b6-8e78-384d9026e8e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f5962e4-2e17-4d3a-ab2b-f7c46ef37e77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59be850f-4ee5-44b6-8e78-384d9026e8e1",
                    "LayerId": "f522eb41-1eff-43bc-8ddc-1e3882b10014"
                }
            ]
        },
        {
            "id": "c6903ec8-69c2-42d3-ad9b-b4d97d161bbd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb8bb65-ed78-4fd6-8cb6-d0c5bb435994",
            "compositeImage": {
                "id": "c5a5ac98-105d-48bc-992f-cb5ce7e91a84",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6903ec8-69c2-42d3-ad9b-b4d97d161bbd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df807815-52a2-425e-9cf5-c17340b3b449",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6903ec8-69c2-42d3-ad9b-b4d97d161bbd",
                    "LayerId": "f522eb41-1eff-43bc-8ddc-1e3882b10014"
                }
            ]
        },
        {
            "id": "3e40211c-e869-468c-8c9d-7236923c4690",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb8bb65-ed78-4fd6-8cb6-d0c5bb435994",
            "compositeImage": {
                "id": "8481f3ed-94ba-43ff-9deb-ecc1b47b11e4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e40211c-e869-468c-8c9d-7236923c4690",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "623e9a22-0fa3-468b-8102-53c11748fc9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e40211c-e869-468c-8c9d-7236923c4690",
                    "LayerId": "f522eb41-1eff-43bc-8ddc-1e3882b10014"
                }
            ]
        },
        {
            "id": "4f214e6e-8e0d-42ae-bcd1-ae478845b583",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb8bb65-ed78-4fd6-8cb6-d0c5bb435994",
            "compositeImage": {
                "id": "69804267-28ef-43f4-9e9b-677ebd22ac22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f214e6e-8e0d-42ae-bcd1-ae478845b583",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "430068d4-4ba4-4545-975b-1d7a15734240",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f214e6e-8e0d-42ae-bcd1-ae478845b583",
                    "LayerId": "f522eb41-1eff-43bc-8ddc-1e3882b10014"
                }
            ]
        },
        {
            "id": "25db02e0-a24a-4ffb-8cf2-ffe893f4c6b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb8bb65-ed78-4fd6-8cb6-d0c5bb435994",
            "compositeImage": {
                "id": "7184bc7e-409a-42a5-a1ac-4d0c6abe9e31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25db02e0-a24a-4ffb-8cf2-ffe893f4c6b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c42b4c3a-6a33-48a6-9c77-d1a1ce848e1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25db02e0-a24a-4ffb-8cf2-ffe893f4c6b5",
                    "LayerId": "f522eb41-1eff-43bc-8ddc-1e3882b10014"
                }
            ]
        },
        {
            "id": "b5bc97bc-c328-46a4-a952-9fa266175042",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb8bb65-ed78-4fd6-8cb6-d0c5bb435994",
            "compositeImage": {
                "id": "8ca73799-406c-4396-a84d-0b32824790d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5bc97bc-c328-46a4-a952-9fa266175042",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6dd6a69a-305a-47d3-9196-8a23f213253b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5bc97bc-c328-46a4-a952-9fa266175042",
                    "LayerId": "f522eb41-1eff-43bc-8ddc-1e3882b10014"
                }
            ]
        },
        {
            "id": "a2f715b5-0227-4016-aa2a-6ba30391bfa4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb8bb65-ed78-4fd6-8cb6-d0c5bb435994",
            "compositeImage": {
                "id": "08af93a6-c1ba-4629-be5a-837fd7fe842a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2f715b5-0227-4016-aa2a-6ba30391bfa4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a961f87b-c2fb-4eb3-a46b-acea5286bbba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2f715b5-0227-4016-aa2a-6ba30391bfa4",
                    "LayerId": "f522eb41-1eff-43bc-8ddc-1e3882b10014"
                }
            ]
        },
        {
            "id": "8e2c29da-d786-4dd2-94c8-912a289f212e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb8bb65-ed78-4fd6-8cb6-d0c5bb435994",
            "compositeImage": {
                "id": "3cbb3027-4132-4380-8635-c1d17cc85af4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8e2c29da-d786-4dd2-94c8-912a289f212e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf9edd5e-3ab1-42c5-b6e1-1766662573e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8e2c29da-d786-4dd2-94c8-912a289f212e",
                    "LayerId": "f522eb41-1eff-43bc-8ddc-1e3882b10014"
                }
            ]
        },
        {
            "id": "07a9c3aa-8d57-4da0-aafa-19a220126c3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb8bb65-ed78-4fd6-8cb6-d0c5bb435994",
            "compositeImage": {
                "id": "c672b1c5-cc35-45cc-a8bb-922a06645499",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07a9c3aa-8d57-4da0-aafa-19a220126c3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3feff9b-51e1-48a9-9d2f-89d4ff94cb7f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07a9c3aa-8d57-4da0-aafa-19a220126c3b",
                    "LayerId": "f522eb41-1eff-43bc-8ddc-1e3882b10014"
                }
            ]
        },
        {
            "id": "e3b65a10-8a12-4fbe-bed4-d2bba5df6971",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb8bb65-ed78-4fd6-8cb6-d0c5bb435994",
            "compositeImage": {
                "id": "628d9b85-0e0a-4ea4-881a-81324430ad5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3b65a10-8a12-4fbe-bed4-d2bba5df6971",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad762cef-dd1b-40b1-be05-a9ae8c3a21f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3b65a10-8a12-4fbe-bed4-d2bba5df6971",
                    "LayerId": "f522eb41-1eff-43bc-8ddc-1e3882b10014"
                }
            ]
        },
        {
            "id": "39f0316e-862e-4ad8-bd39-1bf2a9b614e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb8bb65-ed78-4fd6-8cb6-d0c5bb435994",
            "compositeImage": {
                "id": "7e28fb85-48c6-4b49-93b2-3b7cb3c62f3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39f0316e-862e-4ad8-bd39-1bf2a9b614e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b1abc82a-9c48-484b-9c46-61532c2ef803",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39f0316e-862e-4ad8-bd39-1bf2a9b614e1",
                    "LayerId": "f522eb41-1eff-43bc-8ddc-1e3882b10014"
                }
            ]
        },
        {
            "id": "7a09d4cb-d741-4d70-97a9-03cc0099492a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb8bb65-ed78-4fd6-8cb6-d0c5bb435994",
            "compositeImage": {
                "id": "1c3f9603-2f4b-400e-91d8-2dd175a51156",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a09d4cb-d741-4d70-97a9-03cc0099492a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9374686-2507-4476-bb6d-a5251382eddc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a09d4cb-d741-4d70-97a9-03cc0099492a",
                    "LayerId": "f522eb41-1eff-43bc-8ddc-1e3882b10014"
                }
            ]
        },
        {
            "id": "0858c39c-5025-4589-a308-ad7976b40300",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb8bb65-ed78-4fd6-8cb6-d0c5bb435994",
            "compositeImage": {
                "id": "b0c886d9-b7a9-4ba8-bd8d-64486214b85b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0858c39c-5025-4589-a308-ad7976b40300",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ab7893b-9ec6-43b8-8dfd-a26c05315e40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0858c39c-5025-4589-a308-ad7976b40300",
                    "LayerId": "f522eb41-1eff-43bc-8ddc-1e3882b10014"
                }
            ]
        },
        {
            "id": "c975b633-764e-438f-97a1-fec0ba26145c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb8bb65-ed78-4fd6-8cb6-d0c5bb435994",
            "compositeImage": {
                "id": "f984e131-3107-4957-935d-d8fbf8956d46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c975b633-764e-438f-97a1-fec0ba26145c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f8a21da-1324-4574-ae26-fecb9c56a127",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c975b633-764e-438f-97a1-fec0ba26145c",
                    "LayerId": "f522eb41-1eff-43bc-8ddc-1e3882b10014"
                }
            ]
        },
        {
            "id": "feafbe2a-c6df-4b8d-9285-e980b8f75e3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb8bb65-ed78-4fd6-8cb6-d0c5bb435994",
            "compositeImage": {
                "id": "bf14867b-417b-4aa8-94fb-5ad59d3653d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "feafbe2a-c6df-4b8d-9285-e980b8f75e3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23b95f9a-d864-4000-8063-f6041810e2ec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "feafbe2a-c6df-4b8d-9285-e980b8f75e3e",
                    "LayerId": "f522eb41-1eff-43bc-8ddc-1e3882b10014"
                }
            ]
        },
        {
            "id": "bde4349d-f050-4c06-affb-63b4d60c5e28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb8bb65-ed78-4fd6-8cb6-d0c5bb435994",
            "compositeImage": {
                "id": "9e16a23d-9b3a-4638-be15-783d3e8612a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bde4349d-f050-4c06-affb-63b4d60c5e28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d348325c-9f7d-4044-a0cf-0fec139389bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bde4349d-f050-4c06-affb-63b4d60c5e28",
                    "LayerId": "f522eb41-1eff-43bc-8ddc-1e3882b10014"
                }
            ]
        },
        {
            "id": "cec68935-de20-4cff-931f-0908c82cef49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb8bb65-ed78-4fd6-8cb6-d0c5bb435994",
            "compositeImage": {
                "id": "ca505282-74c3-44bc-90de-33c117bf456c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cec68935-de20-4cff-931f-0908c82cef49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "994ba867-7428-47e7-96f4-ce28b86d2e6b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cec68935-de20-4cff-931f-0908c82cef49",
                    "LayerId": "f522eb41-1eff-43bc-8ddc-1e3882b10014"
                }
            ]
        },
        {
            "id": "970af5c0-6a2b-427b-a23e-c70c65a495ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb8bb65-ed78-4fd6-8cb6-d0c5bb435994",
            "compositeImage": {
                "id": "617abe4e-7925-4ffa-b4f5-05b4f952f442",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "970af5c0-6a2b-427b-a23e-c70c65a495ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af33dea5-3b25-4d27-9382-95a0da3a1eb6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "970af5c0-6a2b-427b-a23e-c70c65a495ba",
                    "LayerId": "f522eb41-1eff-43bc-8ddc-1e3882b10014"
                }
            ]
        },
        {
            "id": "a5767e18-c386-4ae8-941f-0d6b4b35bc21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb8bb65-ed78-4fd6-8cb6-d0c5bb435994",
            "compositeImage": {
                "id": "d5607a44-27e3-45da-81ff-4f340f25e16b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a5767e18-c386-4ae8-941f-0d6b4b35bc21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c56250c-80a5-47fb-96ac-5142b40c7929",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a5767e18-c386-4ae8-941f-0d6b4b35bc21",
                    "LayerId": "f522eb41-1eff-43bc-8ddc-1e3882b10014"
                }
            ]
        },
        {
            "id": "a3713ce8-93f2-4168-8270-e1fba303a4cc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdb8bb65-ed78-4fd6-8cb6-d0c5bb435994",
            "compositeImage": {
                "id": "8df6c8ca-7a2f-447a-9dcd-297995342506",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3713ce8-93f2-4168-8270-e1fba303a4cc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa197865-aa2f-4370-97c6-2ed5ad493e39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3713ce8-93f2-4168-8270-e1fba303a4cc",
                    "LayerId": "f522eb41-1eff-43bc-8ddc-1e3882b10014"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "f522eb41-1eff-43bc-8ddc-1e3882b10014",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fdb8bb65-ed78-4fd6-8cb6-d0c5bb435994",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 8
}