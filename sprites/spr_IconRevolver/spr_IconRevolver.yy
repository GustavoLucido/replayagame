{
    "id": "54607efc-c30a-4b2c-bbdb-8ecfae00d91e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_IconRevolver",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 5,
    "bbox_right": 66,
    "bbox_top": 35,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "609ede82-3f0d-46fb-9bc4-92f6749b551b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "54607efc-c30a-4b2c-bbdb-8ecfae00d91e",
            "compositeImage": {
                "id": "f6672ba9-a9c5-4b64-bbc7-f634e9ad9e33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "609ede82-3f0d-46fb-9bc4-92f6749b551b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa756da4-c895-42be-8998-e29f7082c0f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "609ede82-3f0d-46fb-9bc4-92f6749b551b",
                    "LayerId": "c5b3505f-7753-4fad-a181-3da2312384bb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 90,
    "layers": [
        {
            "id": "c5b3505f-7753-4fad-a181-3da2312384bb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "54607efc-c30a-4b2c-bbdb-8ecfae00d91e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 90,
    "xorig": 0,
    "yorig": 50
}