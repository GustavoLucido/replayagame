{
    "id": "1cb3104a-01ff-4d6b-96c5-4d66b9550401",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Tile2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 32,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "129443b5-0900-4f42-8f60-98fc9dd2d148",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1cb3104a-01ff-4d6b-96c5-4d66b9550401",
            "compositeImage": {
                "id": "5bfa1d68-2df2-444a-bc22-b22e28fadac0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "129443b5-0900-4f42-8f60-98fc9dd2d148",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fab21352-5f20-49a6-8893-22bd092ed1ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "129443b5-0900-4f42-8f60-98fc9dd2d148",
                    "LayerId": "5e9ab9b8-ba9b-4b95-bcdc-f6092858e192"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "5e9ab9b8-ba9b-4b95-bcdc-f6092858e192",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1cb3104a-01ff-4d6b-96c5-4d66b9550401",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}