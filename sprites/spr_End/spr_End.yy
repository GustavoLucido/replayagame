{
    "id": "07a33c2f-bd4f-4800-bd5c-21eab6927e2f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_End",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4bd85c36-0db5-4fc4-9b81-43a003636c5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07a33c2f-bd4f-4800-bd5c-21eab6927e2f",
            "compositeImage": {
                "id": "3627d910-ef2c-4477-a502-2998ffb0721e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4bd85c36-0db5-4fc4-9b81-43a003636c5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ff5b0c2-36de-40b5-96e4-56132da8e698",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bd85c36-0db5-4fc4-9b81-43a003636c5b",
                    "LayerId": "fd682589-5516-4c99-aecc-88cd746bbf12"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "fd682589-5516-4c99-aecc-88cd746bbf12",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "07a33c2f-bd4f-4800-bd5c-21eab6927e2f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}