{
    "id": "5d09ed45-7032-473d-8dc0-e7f207db4c9e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_BackTrees",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1426,
    "bbox_left": 0,
    "bbox_right": 4443,
    "bbox_top": 57,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "387676e3-8249-42df-b5c1-3339eb626ac1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d09ed45-7032-473d-8dc0-e7f207db4c9e",
            "compositeImage": {
                "id": "38175980-7cc4-4696-89ff-4062ab8ed937",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "387676e3-8249-42df-b5c1-3339eb626ac1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7d92e0d0-8795-4a5e-8ced-7b96958536fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "387676e3-8249-42df-b5c1-3339eb626ac1",
                    "LayerId": "bb8fb891-e647-4500-9bc1-8592185733a8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1427,
    "layers": [
        {
            "id": "bb8fb891-e647-4500-9bc1-8592185733a8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5d09ed45-7032-473d-8dc0-e7f207db4c9e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 4444,
    "xorig": 1233,
    "yorig": 967
}