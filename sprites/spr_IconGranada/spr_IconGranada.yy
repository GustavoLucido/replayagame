{
    "id": "3923b092-f4f0-4db0-96a8-9686531b4739",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_IconGranada",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 18,
    "bbox_right": 46,
    "bbox_top": 18,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ae4410b3-1a96-4868-b6b3-1fb1b383264a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3923b092-f4f0-4db0-96a8-9686531b4739",
            "compositeImage": {
                "id": "dcdcfcbe-5d61-42e5-9f26-77dafc7fbbfe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae4410b3-1a96-4868-b6b3-1fb1b383264a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9bda8bdd-2300-4aaa-8609-ec5007d97280",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae4410b3-1a96-4868-b6b3-1fb1b383264a",
                    "LayerId": "bf44b338-1daf-4751-93a6-95550951c314"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "bf44b338-1daf-4751-93a6-95550951c314",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3923b092-f4f0-4db0-96a8-9686531b4739",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 37
}