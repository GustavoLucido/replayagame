{
    "id": "03f53fa2-63c0-4099-be57-f478027fe595",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_PlayerAgachado",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 15,
    "bbox_right": 78,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bdd59a67-d414-4977-8153-f7fc1db256d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03f53fa2-63c0-4099-be57-f478027fe595",
            "compositeImage": {
                "id": "aaca39ce-927c-4d13-8fee-0f50e29531bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bdd59a67-d414-4977-8153-f7fc1db256d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b67d0e6a-125a-416a-8548-cd3e7fd252be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bdd59a67-d414-4977-8153-f7fc1db256d1",
                    "LayerId": "4c6d9cc3-74fb-49c8-8923-615d4cadfed4"
                }
            ]
        },
        {
            "id": "fde60278-6dca-4d0c-891e-c6d5c5dafe28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03f53fa2-63c0-4099-be57-f478027fe595",
            "compositeImage": {
                "id": "135101a3-ac6c-4c75-9683-bff647283513",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fde60278-6dca-4d0c-891e-c6d5c5dafe28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "523dfc7e-9271-406a-be42-5f416271e038",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fde60278-6dca-4d0c-891e-c6d5c5dafe28",
                    "LayerId": "4c6d9cc3-74fb-49c8-8923-615d4cadfed4"
                }
            ]
        },
        {
            "id": "cb0d9841-c200-4da1-9a07-8a72b3530811",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03f53fa2-63c0-4099-be57-f478027fe595",
            "compositeImage": {
                "id": "0b9de708-b05a-4b50-a290-7c5ccb3b34c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb0d9841-c200-4da1-9a07-8a72b3530811",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41a8dddd-5b38-425d-b519-323e6cc34f7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb0d9841-c200-4da1-9a07-8a72b3530811",
                    "LayerId": "4c6d9cc3-74fb-49c8-8923-615d4cadfed4"
                }
            ]
        },
        {
            "id": "76e34ed4-c6b0-4993-ba03-86dfc1509094",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03f53fa2-63c0-4099-be57-f478027fe595",
            "compositeImage": {
                "id": "6ff70ab9-fac3-404a-a430-7295e8ce039e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "76e34ed4-c6b0-4993-ba03-86dfc1509094",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b8fb6e2-25a8-4f3f-ac4b-20779336ac59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "76e34ed4-c6b0-4993-ba03-86dfc1509094",
                    "LayerId": "4c6d9cc3-74fb-49c8-8923-615d4cadfed4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "4c6d9cc3-74fb-49c8-8923-615d4cadfed4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "03f53fa2-63c0-4099-be57-f478027fe595",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 47,
    "yorig": 16
}