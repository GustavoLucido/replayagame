{
    "id": "743d96d2-467b-4e22-9117-b83e267d3c19",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Avistado",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 112,
    "bbox_left": 21,
    "bbox_right": 44,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "99051bea-08ba-4860-a4d7-3155fca0d4d6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "743d96d2-467b-4e22-9117-b83e267d3c19",
            "compositeImage": {
                "id": "9bb5a561-8ead-4cb1-83d9-2d987a6b2f88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "99051bea-08ba-4860-a4d7-3155fca0d4d6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "41daf407-c092-4d26-9175-530f1e8e97ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "99051bea-08ba-4860-a4d7-3155fca0d4d6",
                    "LayerId": "172fa43a-86d7-4c6f-9e55-61d6c07a7946"
                }
            ]
        },
        {
            "id": "9cedbaee-0185-46f8-afc1-ac1de48a3023",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "743d96d2-467b-4e22-9117-b83e267d3c19",
            "compositeImage": {
                "id": "1aaacbb4-6063-42cd-868e-3bff654679b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9cedbaee-0185-46f8-afc1-ac1de48a3023",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02f6f607-e345-4236-bd34-bc889330285d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9cedbaee-0185-46f8-afc1-ac1de48a3023",
                    "LayerId": "172fa43a-86d7-4c6f-9e55-61d6c07a7946"
                }
            ]
        },
        {
            "id": "f81d6997-1121-4eba-ab14-9e5ff18359a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "743d96d2-467b-4e22-9117-b83e267d3c19",
            "compositeImage": {
                "id": "766deae1-b689-406f-84bf-90c1f50875f3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f81d6997-1121-4eba-ab14-9e5ff18359a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a40ed61-a7ea-4da1-8a4a-79e96068a0b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f81d6997-1121-4eba-ab14-9e5ff18359a4",
                    "LayerId": "172fa43a-86d7-4c6f-9e55-61d6c07a7946"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 90,
    "layers": [
        {
            "id": "172fa43a-86d7-4c6f-9e55-61d6c07a7946",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "743d96d2-467b-4e22-9117-b83e267d3c19",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 33,
    "yorig": 104
}