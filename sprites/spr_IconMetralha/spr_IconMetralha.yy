{
    "id": "b5f37710-e2e0-49a0-bedb-65cff8cde15a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_IconMetralha",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 43,
    "bbox_left": 4,
    "bbox_right": 151,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0282c962-7e9e-4935-a236-66c562170e2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b5f37710-e2e0-49a0-bedb-65cff8cde15a",
            "compositeImage": {
                "id": "6c50b031-6661-48d6-a434-53db53bd290b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0282c962-7e9e-4935-a236-66c562170e2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "51de20e7-1ad0-4777-a29a-1b8d3017932c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0282c962-7e9e-4935-a236-66c562170e2b",
                    "LayerId": "cf7a6456-395c-461a-a1a5-a900c7359a30"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 45,
    "layers": [
        {
            "id": "cf7a6456-395c-461a-a1a5-a900c7359a30",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b5f37710-e2e0-49a0-bedb-65cff8cde15a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 40,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 180,
    "xorig": 62,
    "yorig": 18
}