{
    "id": "4cf4fe17-65ba-4c3c-b2eb-cf59705ea23f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_LOS",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 199,
    "bbox_left": 0,
    "bbox_right": 549,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ac22763a-ed46-4cfe-9a1b-5e4d3a32a29f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4cf4fe17-65ba-4c3c-b2eb-cf59705ea23f",
            "compositeImage": {
                "id": "acaf509b-4510-4964-8126-9dce34d6089f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac22763a-ed46-4cfe-9a1b-5e4d3a32a29f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3cab0229-9b39-4250-a045-2c620401ce61",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac22763a-ed46-4cfe-9a1b-5e4d3a32a29f",
                    "LayerId": "c1f0d1f7-3b27-4879-843c-7fe089757f8a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 200,
    "layers": [
        {
            "id": "c1f0d1f7-3b27-4879-843c-7fe089757f8a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4cf4fe17-65ba-4c3c-b2eb-cf59705ea23f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 33,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 550,
    "xorig": 0,
    "yorig": 110
}