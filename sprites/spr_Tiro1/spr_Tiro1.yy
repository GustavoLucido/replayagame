{
    "id": "498e43ce-de8b-45bb-a515-46aff4a5a01a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Tiro1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": 11,
    "bbox_right": 20,
    "bbox_top": 11,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bf4dd1af-b5df-48f9-a6af-5691d7e2b484",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "498e43ce-de8b-45bb-a515-46aff4a5a01a",
            "compositeImage": {
                "id": "14ad0dea-5b5c-4c6a-9073-f0f365f7eff6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf4dd1af-b5df-48f9-a6af-5691d7e2b484",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a490ae89-a900-4e0c-a96a-98385b00921b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf4dd1af-b5df-48f9-a6af-5691d7e2b484",
                    "LayerId": "4bd98d60-c0b6-476f-87d6-0de3339ef07a"
                }
            ]
        },
        {
            "id": "3d1c1db2-635e-423e-862f-7d66a6d3401a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "498e43ce-de8b-45bb-a515-46aff4a5a01a",
            "compositeImage": {
                "id": "51407d38-9763-4c35-a01e-54726d16c3f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d1c1db2-635e-423e-862f-7d66a6d3401a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61e61080-66bd-4a93-bca2-c9307ef389d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d1c1db2-635e-423e-862f-7d66a6d3401a",
                    "LayerId": "4bd98d60-c0b6-476f-87d6-0de3339ef07a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4bd98d60-c0b6-476f-87d6-0de3339ef07a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "498e43ce-de8b-45bb-a515-46aff4a5a01a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}