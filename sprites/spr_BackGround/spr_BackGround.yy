{
    "id": "60604b55-bafe-4788-a703-56064455104b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_BackGround",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3299,
    "bbox_left": 0,
    "bbox_right": 5331,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "acfed361-8143-43f6-b473-6f3a06ccaf57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "60604b55-bafe-4788-a703-56064455104b",
            "compositeImage": {
                "id": "eeeac905-f765-47a6-b79d-4c632bb43623",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "acfed361-8143-43f6-b473-6f3a06ccaf57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a418953b-1f9b-4098-b9af-d281a2d7621f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "acfed361-8143-43f6-b473-6f3a06ccaf57",
                    "LayerId": "661bf6e6-965f-458e-b90a-4a21e5f69086"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 3300,
    "layers": [
        {
            "id": "661bf6e6-965f-458e-b90a-4a21e5f69086",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "60604b55-bafe-4788-a703-56064455104b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 5332,
    "xorig": 0,
    "yorig": 0
}