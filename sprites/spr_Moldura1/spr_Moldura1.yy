{
    "id": "344419a4-3fbb-49db-8fa8-525a36eca24d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Moldura1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 153,
    "bbox_left": 2,
    "bbox_right": 176,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b95899d5-5f21-4373-819b-69ec0d1d844c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "344419a4-3fbb-49db-8fa8-525a36eca24d",
            "compositeImage": {
                "id": "b05ab2e4-e012-423c-97aa-49ef4446abdb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b95899d5-5f21-4373-819b-69ec0d1d844c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f95ae239-ed36-4bce-bff8-82d78b9a9b70",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b95899d5-5f21-4373-819b-69ec0d1d844c",
                    "LayerId": "e3c9b237-2143-49e6-9bda-a956ed12d298"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 155,
    "layers": [
        {
            "id": "e3c9b237-2143-49e6-9bda-a956ed12d298",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "344419a4-3fbb-49db-8fa8-525a36eca24d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 178,
    "xorig": 0,
    "yorig": 0
}