{
    "id": "41299cd3-a3e3-4bfd-a898-5cce2d5bb8ad",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_VentoInfinito",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 745,
    "bbox_left": 0,
    "bbox_right": 193,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2a222233-3851-45c2-b036-95109e8bc806",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "41299cd3-a3e3-4bfd-a898-5cce2d5bb8ad",
            "compositeImage": {
                "id": "620e361d-01f8-48b2-ad56-42a63fdd4472",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a222233-3851-45c2-b036-95109e8bc806",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "519852c6-1cc6-42ca-bca0-f0524b3f9bd0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a222233-3851-45c2-b036-95109e8bc806",
                    "LayerId": "4abe90bf-18bd-49b0-8f8d-3d0574d52ee0"
                }
            ]
        },
        {
            "id": "dd275892-0055-4421-8ef5-8dfc5cd64cbd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "41299cd3-a3e3-4bfd-a898-5cce2d5bb8ad",
            "compositeImage": {
                "id": "4b740697-3dca-41df-9c94-4130736fe060",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd275892-0055-4421-8ef5-8dfc5cd64cbd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e1b7b72-0f08-4d43-8699-4a3a9762a619",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd275892-0055-4421-8ef5-8dfc5cd64cbd",
                    "LayerId": "4abe90bf-18bd-49b0-8f8d-3d0574d52ee0"
                }
            ]
        },
        {
            "id": "fbf950cc-e936-4189-a4b3-54a6c8d0088f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "41299cd3-a3e3-4bfd-a898-5cce2d5bb8ad",
            "compositeImage": {
                "id": "1ad07893-cd2c-4be4-8b48-0df082b46b7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbf950cc-e936-4189-a4b3-54a6c8d0088f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "148d9024-b4f0-43e4-8a3c-c367f8d281cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbf950cc-e936-4189-a4b3-54a6c8d0088f",
                    "LayerId": "4abe90bf-18bd-49b0-8f8d-3d0574d52ee0"
                }
            ]
        },
        {
            "id": "6eac9784-0dff-490f-8dbc-49b767347e68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "41299cd3-a3e3-4bfd-a898-5cce2d5bb8ad",
            "compositeImage": {
                "id": "dbb9df02-3a58-46a2-a33b-560f3d5b8155",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6eac9784-0dff-490f-8dbc-49b767347e68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "133e7cbd-adbd-49f4-ac9c-fc47d8dfdc86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6eac9784-0dff-490f-8dbc-49b767347e68",
                    "LayerId": "4abe90bf-18bd-49b0-8f8d-3d0574d52ee0"
                }
            ]
        },
        {
            "id": "59d6b9bc-3206-4028-a943-b65ebe24aad4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "41299cd3-a3e3-4bfd-a898-5cce2d5bb8ad",
            "compositeImage": {
                "id": "580c90c9-778e-4e0e-b6ba-0e1eb6087740",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59d6b9bc-3206-4028-a943-b65ebe24aad4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a9625a8-8889-4c39-ab75-efc98cdda45b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59d6b9bc-3206-4028-a943-b65ebe24aad4",
                    "LayerId": "4abe90bf-18bd-49b0-8f8d-3d0574d52ee0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 746,
    "layers": [
        {
            "id": "4abe90bf-18bd-49b0-8f8d-3d0574d52ee0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "41299cd3-a3e3-4bfd-a898-5cce2d5bb8ad",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 35,
            "visible": true
        }
    ],
    "origin": 6,
    "originLocked": false,
    "playbackSpeed": 11,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 194,
    "xorig": 0,
    "yorig": 745
}