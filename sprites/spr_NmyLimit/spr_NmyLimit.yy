{
    "id": "2d19a267-65c5-4edd-b54e-7745f0e9a26a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_NmyLimit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ed37dab4-f203-4d66-b86c-5ab9f1b9bcf9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d19a267-65c5-4edd-b54e-7745f0e9a26a",
            "compositeImage": {
                "id": "6a8b5140-f91b-4691-8183-92725f961af1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed37dab4-f203-4d66-b86c-5ab9f1b9bcf9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b6d21513-66a1-4c5e-a899-1f861495d6f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed37dab4-f203-4d66-b86c-5ab9f1b9bcf9",
                    "LayerId": "84e3aa3a-fa2c-4b79-bf23-53388a2ef7c9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "84e3aa3a-fa2c-4b79-bf23-53388a2ef7c9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2d19a267-65c5-4edd-b54e-7745f0e9a26a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}