{
    "id": "c58e19a8-dd9c-476a-b908-1ca1e750d1b4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_TLS_Neve",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 5,
    "bbox_right": 159,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fddf3dfa-0d89-4364-abd6-cc2ec4ab5802",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c58e19a8-dd9c-476a-b908-1ca1e750d1b4",
            "compositeImage": {
                "id": "64715f10-59e9-43d8-9e5c-9e60282de4dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fddf3dfa-0d89-4364-abd6-cc2ec4ab5802",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf8ab355-4c3e-461a-9df9-39be4ace8314",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fddf3dfa-0d89-4364-abd6-cc2ec4ab5802",
                    "LayerId": "fe73e679-00d8-4f46-b266-0c81fe1a2484"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "fe73e679-00d8-4f46-b266-0c81fe1a2484",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c58e19a8-dd9c-476a-b908-1ca1e750d1b4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 0,
    "yorig": 0
}