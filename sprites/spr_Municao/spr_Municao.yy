{
    "id": "a9eb0a38-1697-48af-8980-fcd4b4932ee2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Municao",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 95,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aba1a18d-8dca-4dca-bd30-8dec3f041ad5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9eb0a38-1697-48af-8980-fcd4b4932ee2",
            "compositeImage": {
                "id": "0d33e9b0-bedf-46ca-85f6-c0aeafcb16e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aba1a18d-8dca-4dca-bd30-8dec3f041ad5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "525549b3-8e24-4fc4-900f-051a631365f9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aba1a18d-8dca-4dca-bd30-8dec3f041ad5",
                    "LayerId": "1d47cd53-6083-4f44-a922-a2e08cb017de"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "1d47cd53-6083-4f44-a922-a2e08cb017de",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a9eb0a38-1697-48af-8980-fcd4b4932ee2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 0,
    "yorig": 0
}