{
    "id": "256c2b42-f592-4379-8c68-f92b796b28f6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Missil",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 30,
    "bbox_right": 80,
    "bbox_top": 41,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b52ccd42-d7ad-44c9-a950-02bd70aa72f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "256c2b42-f592-4379-8c68-f92b796b28f6",
            "compositeImage": {
                "id": "d57b978f-e810-4528-afd4-7c914982be5b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b52ccd42-d7ad-44c9-a950-02bd70aa72f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e144b24-1962-4743-8034-2d320e942a4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b52ccd42-d7ad-44c9-a950-02bd70aa72f6",
                    "LayerId": "2d491d97-466f-45b6-8eaa-505e280281a4"
                }
            ]
        },
        {
            "id": "2128a3a5-eb52-4f14-a8e4-649fdf7340a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "256c2b42-f592-4379-8c68-f92b796b28f6",
            "compositeImage": {
                "id": "450c9191-8804-4f19-872e-03744d6eed7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2128a3a5-eb52-4f14-a8e4-649fdf7340a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9bbb275-8331-4e7e-b9dd-baa618b6ba04",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2128a3a5-eb52-4f14-a8e4-649fdf7340a6",
                    "LayerId": "2d491d97-466f-45b6-8eaa-505e280281a4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "2d491d97-466f-45b6-8eaa-505e280281a4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "256c2b42-f592-4379-8c68-f92b796b28f6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 9,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 55,
    "yorig": 52
}