{
    "id": "07a99093-4696-4211-8cbc-c573be15b94c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Shotgun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 6,
    "bbox_right": 124,
    "bbox_top": 38,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "63464588-9a19-4a44-8fbe-b31681bf91f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07a99093-4696-4211-8cbc-c573be15b94c",
            "compositeImage": {
                "id": "fbf7d5c6-c203-47f6-a5b6-0496d4347ebc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63464588-9a19-4a44-8fbe-b31681bf91f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1564437e-ee14-4a52-90db-be6ff9e38f0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63464588-9a19-4a44-8fbe-b31681bf91f7",
                    "LayerId": "960433c9-7678-477f-b5f5-8311b8021641"
                }
            ]
        },
        {
            "id": "328f399f-f7f3-4d43-a0a1-a9a7f12fff3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07a99093-4696-4211-8cbc-c573be15b94c",
            "compositeImage": {
                "id": "704fc552-054c-405e-8a1e-892a5d6c0a87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "328f399f-f7f3-4d43-a0a1-a9a7f12fff3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f00c412-ab2f-4b18-8147-fe72eee3e712",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "328f399f-f7f3-4d43-a0a1-a9a7f12fff3d",
                    "LayerId": "960433c9-7678-477f-b5f5-8311b8021641"
                }
            ]
        },
        {
            "id": "7747348d-8e0b-444b-b463-6d726950c18a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07a99093-4696-4211-8cbc-c573be15b94c",
            "compositeImage": {
                "id": "f8680c5f-9aeb-4c0a-89b5-c99d9edacfdf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7747348d-8e0b-444b-b463-6d726950c18a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21827fd1-eee4-4e9b-9bf3-83f9d6ca9893",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7747348d-8e0b-444b-b463-6d726950c18a",
                    "LayerId": "960433c9-7678-477f-b5f5-8311b8021641"
                }
            ]
        },
        {
            "id": "55fdb5c2-16c3-4ead-9ee8-fde5d30367b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07a99093-4696-4211-8cbc-c573be15b94c",
            "compositeImage": {
                "id": "5c911c39-d34d-470d-a2e1-1fee953bb1f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55fdb5c2-16c3-4ead-9ee8-fde5d30367b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e4e8c3b7-c4f6-4a08-aec2-b7667878c35d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55fdb5c2-16c3-4ead-9ee8-fde5d30367b6",
                    "LayerId": "960433c9-7678-477f-b5f5-8311b8021641"
                }
            ]
        },
        {
            "id": "a1fa67d3-7734-4524-8412-8cfea405a782",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07a99093-4696-4211-8cbc-c573be15b94c",
            "compositeImage": {
                "id": "cda5c865-0f83-4654-b444-867c9ca640d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1fa67d3-7734-4524-8412-8cfea405a782",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92f4462a-1138-47a1-a577-138c5a4234a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1fa67d3-7734-4524-8412-8cfea405a782",
                    "LayerId": "960433c9-7678-477f-b5f5-8311b8021641"
                }
            ]
        },
        {
            "id": "e0a94c1b-2b60-4a22-b332-60cdaed1af2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07a99093-4696-4211-8cbc-c573be15b94c",
            "compositeImage": {
                "id": "7f12c480-ba48-49d8-80b7-552860ea6d4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0a94c1b-2b60-4a22-b332-60cdaed1af2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ca0a953-9cf3-4cf0-8055-e612e72e55e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0a94c1b-2b60-4a22-b332-60cdaed1af2b",
                    "LayerId": "960433c9-7678-477f-b5f5-8311b8021641"
                }
            ]
        },
        {
            "id": "c7027e13-e180-4df8-89ad-1a667035a012",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07a99093-4696-4211-8cbc-c573be15b94c",
            "compositeImage": {
                "id": "05523722-e868-4137-88c7-088ccf0c2150",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7027e13-e180-4df8-89ad-1a667035a012",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87690f6b-425c-44c5-be93-250f452d07bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7027e13-e180-4df8-89ad-1a667035a012",
                    "LayerId": "960433c9-7678-477f-b5f5-8311b8021641"
                }
            ]
        },
        {
            "id": "b907f774-6e26-4b8e-bf95-3d395dd5b222",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07a99093-4696-4211-8cbc-c573be15b94c",
            "compositeImage": {
                "id": "215d81dd-1603-4d69-8ed1-ec0ce8c41207",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b907f774-6e26-4b8e-bf95-3d395dd5b222",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62dd8e03-1d74-4d69-ac53-aabaacb45263",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b907f774-6e26-4b8e-bf95-3d395dd5b222",
                    "LayerId": "960433c9-7678-477f-b5f5-8311b8021641"
                }
            ]
        },
        {
            "id": "a9cb5756-a1fb-4e45-bded-e7a92b2a4528",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07a99093-4696-4211-8cbc-c573be15b94c",
            "compositeImage": {
                "id": "912f6a0e-f269-4e23-ac51-063e374efed6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9cb5756-a1fb-4e45-bded-e7a92b2a4528",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45368e66-2756-4075-8031-665ce9b0d7d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9cb5756-a1fb-4e45-bded-e7a92b2a4528",
                    "LayerId": "960433c9-7678-477f-b5f5-8311b8021641"
                }
            ]
        },
        {
            "id": "28242b0e-f99c-46a7-ba8e-91d4f163788a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07a99093-4696-4211-8cbc-c573be15b94c",
            "compositeImage": {
                "id": "32b1f66f-ef86-4398-9c32-4029504957c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "28242b0e-f99c-46a7-ba8e-91d4f163788a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "295cf304-e83b-4c69-abbe-ccea8ce7928a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "28242b0e-f99c-46a7-ba8e-91d4f163788a",
                    "LayerId": "960433c9-7678-477f-b5f5-8311b8021641"
                }
            ]
        },
        {
            "id": "68e2ed07-27ee-41e0-ac5a-04362af9a155",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "07a99093-4696-4211-8cbc-c573be15b94c",
            "compositeImage": {
                "id": "384c93d9-2c0a-441b-b078-1dac869d0ff0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68e2ed07-27ee-41e0-ac5a-04362af9a155",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a7e93f4-303c-41c0-86e0-7e66d72907e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68e2ed07-27ee-41e0-ac5a-04362af9a155",
                    "LayerId": "960433c9-7678-477f-b5f5-8311b8021641"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "960433c9-7678-477f-b5f5-8311b8021641",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "07a99093-4696-4211-8cbc-c573be15b94c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 46,
    "yorig": 49
}