{
    "id": "92ff59d9-f05f-4912-bf4e-7f7a41cec98d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Glow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 84,
    "bbox_left": 16,
    "bbox_right": 150,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f95105a3-3c9e-4b2d-b5d2-ec5716340218",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92ff59d9-f05f-4912-bf4e-7f7a41cec98d",
            "compositeImage": {
                "id": "edf7866c-7904-459c-b33f-f9e18e4ef247",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f95105a3-3c9e-4b2d-b5d2-ec5716340218",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60906447-058f-4d1b-91c9-3aba2ef0236e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f95105a3-3c9e-4b2d-b5d2-ec5716340218",
                    "LayerId": "2dd63e65-ceda-48e3-b35e-41fc824dcb43"
                }
            ]
        },
        {
            "id": "d5caef23-ca7e-4fb8-8d9d-0dd8ea4a604a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92ff59d9-f05f-4912-bf4e-7f7a41cec98d",
            "compositeImage": {
                "id": "a8c8f9c9-3f48-4954-b3ab-907c4a18991b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5caef23-ca7e-4fb8-8d9d-0dd8ea4a604a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fdc6355b-7449-4cce-9673-9d817b7ccafa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5caef23-ca7e-4fb8-8d9d-0dd8ea4a604a",
                    "LayerId": "2dd63e65-ceda-48e3-b35e-41fc824dcb43"
                }
            ]
        },
        {
            "id": "e957f968-d8b2-4e8f-b489-2c7dd3e14eaf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92ff59d9-f05f-4912-bf4e-7f7a41cec98d",
            "compositeImage": {
                "id": "1b55a175-4564-483f-b29d-bdc1e2ff88be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e957f968-d8b2-4e8f-b489-2c7dd3e14eaf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "141787ef-3763-41b7-b1b2-e25d72ba0a97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e957f968-d8b2-4e8f-b489-2c7dd3e14eaf",
                    "LayerId": "2dd63e65-ceda-48e3-b35e-41fc824dcb43"
                }
            ]
        },
        {
            "id": "52c58264-3226-4694-b69a-013174c2077f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92ff59d9-f05f-4912-bf4e-7f7a41cec98d",
            "compositeImage": {
                "id": "f04becea-ecbb-41cf-83d1-273b8dffdef8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52c58264-3226-4694-b69a-013174c2077f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "32179e91-97f2-467e-8640-2b5976f584e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52c58264-3226-4694-b69a-013174c2077f",
                    "LayerId": "2dd63e65-ceda-48e3-b35e-41fc824dcb43"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 90,
    "layers": [
        {
            "id": "2dd63e65-ceda-48e3-b35e-41fc824dcb43",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "92ff59d9-f05f-4912-bf4e-7f7a41cec98d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 63,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 80,
    "yorig": 45
}