{
    "id": "98c73f11-5cdf-4d1f-bc84-7a72aa893d2d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Space",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 1,
    "bbox_right": 158,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c56ccb35-993b-451b-90b3-6b88290b768a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98c73f11-5cdf-4d1f-bc84-7a72aa893d2d",
            "compositeImage": {
                "id": "88fa2122-1aef-4a27-a083-8616e877a100",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c56ccb35-993b-451b-90b3-6b88290b768a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b08c1276-2724-48bc-bbf6-36b65f8b658e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c56ccb35-993b-451b-90b3-6b88290b768a",
                    "LayerId": "0f029cd2-a5c7-4853-804a-ebfeab00f6c0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0f029cd2-a5c7-4853-804a-ebfeab00f6c0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "98c73f11-5cdf-4d1f-bc84-7a72aa893d2d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 0,
    "yorig": 0
}