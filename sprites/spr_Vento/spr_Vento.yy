{
    "id": "ba24a1e7-cca8-45fd-ad49-411265a394c1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Vento",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 449,
    "bbox_left": 7,
    "bbox_right": 114,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2108629f-f9d8-44a8-a9bc-de4f6ebd0b3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba24a1e7-cca8-45fd-ad49-411265a394c1",
            "compositeImage": {
                "id": "330c73d2-1415-43d7-ba05-417ad091b53b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2108629f-f9d8-44a8-a9bc-de4f6ebd0b3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d349650-2243-4662-bca9-e3dee69aa651",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2108629f-f9d8-44a8-a9bc-de4f6ebd0b3e",
                    "LayerId": "7b94be9f-8ada-40ac-a189-448ed9289cde"
                }
            ]
        },
        {
            "id": "35558448-48a6-4a1c-840d-1617704a8ed1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba24a1e7-cca8-45fd-ad49-411265a394c1",
            "compositeImage": {
                "id": "87dfc71f-cda0-4988-bf5d-efa891855ab8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35558448-48a6-4a1c-840d-1617704a8ed1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "daa70a5f-cf0e-4710-afbc-cfc398ac4557",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35558448-48a6-4a1c-840d-1617704a8ed1",
                    "LayerId": "7b94be9f-8ada-40ac-a189-448ed9289cde"
                }
            ]
        },
        {
            "id": "180724dd-0fd5-4ff1-8a04-d8fbb4315cf6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba24a1e7-cca8-45fd-ad49-411265a394c1",
            "compositeImage": {
                "id": "d9bfc386-bd04-4a72-965b-1d0c281695cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "180724dd-0fd5-4ff1-8a04-d8fbb4315cf6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ea81b4a6-510e-48e6-8e71-ea4364638175",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "180724dd-0fd5-4ff1-8a04-d8fbb4315cf6",
                    "LayerId": "7b94be9f-8ada-40ac-a189-448ed9289cde"
                }
            ]
        },
        {
            "id": "4d1e77be-63d7-4520-a14f-3f29caa6c81a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba24a1e7-cca8-45fd-ad49-411265a394c1",
            "compositeImage": {
                "id": "0963cdd6-447a-46c2-b549-d51c4f2476d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d1e77be-63d7-4520-a14f-3f29caa6c81a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cfd031e2-765d-4361-b5ae-85d58f50ab97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d1e77be-63d7-4520-a14f-3f29caa6c81a",
                    "LayerId": "7b94be9f-8ada-40ac-a189-448ed9289cde"
                }
            ]
        },
        {
            "id": "de08280d-9b56-4a35-908d-1108947047b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba24a1e7-cca8-45fd-ad49-411265a394c1",
            "compositeImage": {
                "id": "6be724b1-80b2-4695-b5d2-55b9a731ab02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de08280d-9b56-4a35-908d-1108947047b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbce3ac1-7d2b-4574-a5c4-8c1ca9eaf356",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de08280d-9b56-4a35-908d-1108947047b3",
                    "LayerId": "7b94be9f-8ada-40ac-a189-448ed9289cde"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 450,
    "layers": [
        {
            "id": "7b94be9f-8ada-40ac-a189-448ed9289cde",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ba24a1e7-cca8-45fd-ad49-411265a394c1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 50,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 11,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 117,
    "xorig": 58,
    "yorig": 470
}