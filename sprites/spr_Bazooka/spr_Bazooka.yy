{
    "id": "5bcbe07b-a32f-468e-9e1e-84d92c5eeff0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Bazooka",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 43,
    "bbox_left": 11,
    "bbox_right": 116,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "caf3de5d-6bb4-44f6-b647-52c2fbb68582",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5bcbe07b-a32f-468e-9e1e-84d92c5eeff0",
            "compositeImage": {
                "id": "d7f992b9-d746-4e5e-8939-c6b413511bf2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "caf3de5d-6bb4-44f6-b647-52c2fbb68582",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "672e6889-6732-440c-adf8-bfe09fde92ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "caf3de5d-6bb4-44f6-b647-52c2fbb68582",
                    "LayerId": "7efca0a5-0fd8-4a27-a893-38fffda3ca1f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "7efca0a5-0fd8-4a27-a893-38fffda3ca1f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5bcbe07b-a32f-468e-9e1e-84d92c5eeff0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 63,
    "yorig": 61
}