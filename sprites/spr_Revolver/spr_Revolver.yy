{
    "id": "418ece65-4433-4495-8bfb-d77096529a5b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Revolver",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 5,
    "bbox_right": 66,
    "bbox_top": 35,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "57f3313f-af5e-4ea0-9808-d5a8ea86db5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "418ece65-4433-4495-8bfb-d77096529a5b",
            "compositeImage": {
                "id": "7414c8ed-f353-4798-a99a-485ef84e1f03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57f3313f-af5e-4ea0-9808-d5a8ea86db5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ed5500f-8f06-4cf6-8194-b986481467e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57f3313f-af5e-4ea0-9808-d5a8ea86db5d",
                    "LayerId": "aeae1c11-171e-4264-a234-02386b836e4e"
                }
            ]
        },
        {
            "id": "bf7c620d-fd58-44c2-8c5d-5ebfd06ff83a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "418ece65-4433-4495-8bfb-d77096529a5b",
            "compositeImage": {
                "id": "e528d04e-0fea-4546-ba98-4a7bd7c782d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf7c620d-fd58-44c2-8c5d-5ebfd06ff83a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "508897a2-4bfb-41a8-a3a4-f1edb49d665a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf7c620d-fd58-44c2-8c5d-5ebfd06ff83a",
                    "LayerId": "aeae1c11-171e-4264-a234-02386b836e4e"
                }
            ]
        },
        {
            "id": "47e5152a-c4de-4b07-8c07-8dfbec3bbca5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "418ece65-4433-4495-8bfb-d77096529a5b",
            "compositeImage": {
                "id": "6603fb52-77a6-417a-8de9-24f6e289ad35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47e5152a-c4de-4b07-8c07-8dfbec3bbca5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b32da373-c601-4301-9d84-7b638d8504ac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47e5152a-c4de-4b07-8c07-8dfbec3bbca5",
                    "LayerId": "aeae1c11-171e-4264-a234-02386b836e4e"
                }
            ]
        },
        {
            "id": "8f8c5edb-fef6-4f8e-888e-bcf846f539a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "418ece65-4433-4495-8bfb-d77096529a5b",
            "compositeImage": {
                "id": "360b743a-96e6-4952-b826-7f39ba2a735c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f8c5edb-fef6-4f8e-888e-bcf846f539a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ec74863a-29c1-4495-9029-c6a143295ae2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f8c5edb-fef6-4f8e-888e-bcf846f539a6",
                    "LayerId": "aeae1c11-171e-4264-a234-02386b836e4e"
                }
            ]
        },
        {
            "id": "fd4569fc-bf73-4a6c-b20d-1174fefb862d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "418ece65-4433-4495-8bfb-d77096529a5b",
            "compositeImage": {
                "id": "67391dfe-a1ff-4ec5-9e82-ab7894e9f17b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd4569fc-bf73-4a6c-b20d-1174fefb862d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6312301d-512b-4e6d-879a-4fd1932429f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd4569fc-bf73-4a6c-b20d-1174fefb862d",
                    "LayerId": "aeae1c11-171e-4264-a234-02386b836e4e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 90,
    "layers": [
        {
            "id": "aeae1c11-171e-4264-a234-02386b836e4e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "418ece65-4433-4495-8bfb-d77096529a5b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 90,
    "xorig": 0,
    "yorig": 50
}