{
    "id": "65ae8006-7b95-4435-b2c4-a2c21ee56daf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_MolduraArma",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 136,
    "bbox_left": 4,
    "bbox_right": 385,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1a9c642d-b4e3-414c-805c-39b3e93a07db",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65ae8006-7b95-4435-b2c4-a2c21ee56daf",
            "compositeImage": {
                "id": "c2f5dd5a-bf43-4aa9-842b-34af65809c44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a9c642d-b4e3-414c-805c-39b3e93a07db",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f79e8553-d508-4493-8030-1451b9fb63a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a9c642d-b4e3-414c-805c-39b3e93a07db",
                    "LayerId": "62770553-210f-4317-82d3-75df77f2e8e2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 155,
    "layers": [
        {
            "id": "62770553-210f-4317-82d3-75df77f2e8e2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "65ae8006-7b95-4435-b2c4-a2c21ee56daf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 397,
    "xorig": 0,
    "yorig": 0
}