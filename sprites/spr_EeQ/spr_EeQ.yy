{
    "id": "32af7933-2d73-4281-ab10-72b4eacd43bb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_EeQ",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 1,
    "bbox_right": 94,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a7988baf-07a6-4b0a-80f4-0ddf9fbd993c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "32af7933-2d73-4281-ab10-72b4eacd43bb",
            "compositeImage": {
                "id": "7abc9f14-265f-4788-9952-196c98cea6b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7988baf-07a6-4b0a-80f4-0ddf9fbd993c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39f185b8-5f66-452d-b239-8d67ac530acd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7988baf-07a6-4b0a-80f4-0ddf9fbd993c",
                    "LayerId": "9909df88-d4ca-4808-b29f-bbc97caf3c4b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9909df88-d4ca-4808-b29f-bbc97caf3c4b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "32af7933-2d73-4281-ab10-72b4eacd43bb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 0,
    "yorig": 0
}