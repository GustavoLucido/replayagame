{
    "id": "f779505d-840b-4be7-bffd-bca74a7a9004",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_CacoGelo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 5,
    "bbox_right": 26,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5c17d54e-d7fa-4a84-be3f-a284b59f862c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f779505d-840b-4be7-bffd-bca74a7a9004",
            "compositeImage": {
                "id": "aa269b50-db71-4a0b-8feb-772d41b5c5ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c17d54e-d7fa-4a84-be3f-a284b59f862c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39e5ee67-3651-4c19-b002-a057aa804abc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c17d54e-d7fa-4a84-be3f-a284b59f862c",
                    "LayerId": "0591a14d-869b-45dd-b4bf-d3a3c1e3e2e7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0591a14d-869b-45dd-b4bf-d3a3c1e3e2e7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f779505d-840b-4be7-bffd-bca74a7a9004",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}