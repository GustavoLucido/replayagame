{
    "id": "3c191e47-f96c-4f18-935e-7ede02356b23",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Missil1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 61,
    "bbox_left": 30,
    "bbox_right": 80,
    "bbox_top": 41,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "569e8872-e622-41ca-a362-6480e1145535",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c191e47-f96c-4f18-935e-7ede02356b23",
            "compositeImage": {
                "id": "64197973-d40e-4499-b6f7-92bed0e12910",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "569e8872-e622-41ca-a362-6480e1145535",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5915cd31-1e53-434e-b843-fc833bc9a312",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "569e8872-e622-41ca-a362-6480e1145535",
                    "LayerId": "54b6708a-15c0-48c1-8d26-503244b942c6"
                }
            ]
        },
        {
            "id": "005dc89a-5ebb-47d0-a885-67c8c7f11b6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c191e47-f96c-4f18-935e-7ede02356b23",
            "compositeImage": {
                "id": "b780d7bf-9cc3-4b79-a627-539c3d425fb2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "005dc89a-5ebb-47d0-a885-67c8c7f11b6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fb10334-7d75-41f2-9e57-a43b3042eb8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "005dc89a-5ebb-47d0-a885-67c8c7f11b6e",
                    "LayerId": "54b6708a-15c0-48c1-8d26-503244b942c6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "54b6708a-15c0-48c1-8d26-503244b942c6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3c191e47-f96c-4f18-935e-7ede02356b23",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 9,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 55,
    "yorig": 52
}