{
    "id": "041c5cce-8f99-4f22-b253-fadf1a12107e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_IconShotgun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 6,
    "bbox_right": 124,
    "bbox_top": 38,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9d3a8864-795b-40a4-92bc-afa52f129373",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "041c5cce-8f99-4f22-b253-fadf1a12107e",
            "compositeImage": {
                "id": "dc8b789d-e5f4-43e6-a33a-ed8edbcd7c55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9d3a8864-795b-40a4-92bc-afa52f129373",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b12c11e-c8de-4454-b0d2-b8561cd05e87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9d3a8864-795b-40a4-92bc-afa52f129373",
                    "LayerId": "2b812808-d1be-40e6-b845-e5ad8b5bb401"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "2b812808-d1be-40e6-b845-e5ad8b5bb401",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "041c5cce-8f99-4f22-b253-fadf1a12107e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 160,
    "xorig": 46,
    "yorig": 49
}