{
    "id": "343ee4e5-535f-4d96-8791-b2b19cf6c1b7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_MaxAmmo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "21b6bc9f-39bb-40fe-8d7c-d32c2dc834aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "343ee4e5-535f-4d96-8791-b2b19cf6c1b7",
            "compositeImage": {
                "id": "e5d7b188-088c-4192-b371-c6cf51774268",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "21b6bc9f-39bb-40fe-8d7c-d32c2dc834aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f278957-92c2-42a6-9224-363048bc1ff7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "21b6bc9f-39bb-40fe-8d7c-d32c2dc834aa",
                    "LayerId": "004c6f33-e150-40a8-a690-2dca188a2061"
                }
            ]
        },
        {
            "id": "ef3fe654-7684-4a51-b1a6-6b81f6a33cff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "343ee4e5-535f-4d96-8791-b2b19cf6c1b7",
            "compositeImage": {
                "id": "eb97fe44-a376-430b-9ef0-981be759eb6f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ef3fe654-7684-4a51-b1a6-6b81f6a33cff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8262f1b6-0f37-47cc-9061-2b5e60b61aa0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ef3fe654-7684-4a51-b1a6-6b81f6a33cff",
                    "LayerId": "004c6f33-e150-40a8-a690-2dca188a2061"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "004c6f33-e150-40a8-a690-2dca188a2061",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "343ee4e5-535f-4d96-8791-b2b19cf6c1b7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}