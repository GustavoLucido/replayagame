{
    "id": "30497175-0867-4552-8fd5-ee5144a13d80",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Planador",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 12,
    "bbox_right": 116,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bb2e398a-205d-4d36-85ff-36210cff08d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30497175-0867-4552-8fd5-ee5144a13d80",
            "compositeImage": {
                "id": "9a2c7c43-a98f-4984-9afc-8b753f53f2e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb2e398a-205d-4d36-85ff-36210cff08d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91c93952-7c1c-4974-8b7b-abef7b702e34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb2e398a-205d-4d36-85ff-36210cff08d4",
                    "LayerId": "4e69d6f5-3328-4ef9-9d15-cda1dba46285"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "4e69d6f5-3328-4ef9-9d15-cda1dba46285",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "30497175-0867-4552-8fd5-ee5144a13d80",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}