{
    "id": "6151cd7f-1e64-4b80-9e71-9ab5e89cad0a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_DestructableGround",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "569fbac3-c592-4a26-b090-46126deaaf9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6151cd7f-1e64-4b80-9e71-9ab5e89cad0a",
            "compositeImage": {
                "id": "27c51fc7-8194-4195-9c4c-b581c32b7e90",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "569fbac3-c592-4a26-b090-46126deaaf9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c63f9ca9-551f-4cd2-a876-272febba4efe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "569fbac3-c592-4a26-b090-46126deaaf9d",
                    "LayerId": "85fddbde-9427-4b81-822d-1f27332a4ccb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "85fddbde-9427-4b81-822d-1f27332a4ccb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6151cd7f-1e64-4b80-9e71-9ab5e89cad0a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}