{
    "id": "7f34b4b4-66be-44ed-9f16-6c73ce2df565",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_WASD",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 1,
    "bbox_right": 94,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5916c441-3d63-4b06-98c3-c480e9e5357d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7f34b4b4-66be-44ed-9f16-6c73ce2df565",
            "compositeImage": {
                "id": "a7082511-c767-4b96-96c9-039b6539947e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5916c441-3d63-4b06-98c3-c480e9e5357d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6891b2fb-37c5-4cf5-9fa0-4bec7d3715a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5916c441-3d63-4b06-98c3-c480e9e5357d",
                    "LayerId": "2d88cbb6-09a7-4215-855d-ca9419ccf23b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "2d88cbb6-09a7-4215-855d-ca9419ccf23b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7f34b4b4-66be-44ed-9f16-6c73ce2df565",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 0,
    "yorig": 0
}