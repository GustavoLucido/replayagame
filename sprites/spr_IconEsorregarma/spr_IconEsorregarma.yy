{
    "id": "fc333dc7-46a8-45e6-9c3c-29dab0e3a227",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_IconEsorregarma",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 80,
    "bbox_left": 15,
    "bbox_right": 127,
    "bbox_top": 52,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eb457d6e-511d-4737-994f-74de0ba30d7d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fc333dc7-46a8-45e6-9c3c-29dab0e3a227",
            "compositeImage": {
                "id": "a34ac796-c948-465e-95c2-7154cb8fa45a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb457d6e-511d-4737-994f-74de0ba30d7d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49a01d7a-d4a9-4e4b-b032-df37913d37e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb457d6e-511d-4737-994f-74de0ba30d7d",
                    "LayerId": "2cccd711-c5d1-4cbf-aae6-5640368cd3df"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "2cccd711-c5d1-4cbf-aae6-5640368cd3df",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fc333dc7-46a8-45e6-9c3c-29dab0e3a227",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 40,
    "yorig": 73
}