{
    "id": "1e6fc5dd-4d2d-44b2-9d0c-9b70b455fa8c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_Explosao1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 183,
    "bbox_left": 2,
    "bbox_right": 180,
    "bbox_top": 14,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a7d29914-4233-4296-a326-cb4f3ad02142",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e6fc5dd-4d2d-44b2-9d0c-9b70b455fa8c",
            "compositeImage": {
                "id": "f0e1b33b-f73a-4576-b088-23be1dc9dba3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7d29914-4233-4296-a326-cb4f3ad02142",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2568afc5-e5a6-45fe-aadd-004ee02113fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7d29914-4233-4296-a326-cb4f3ad02142",
                    "LayerId": "26f5cac7-2989-4e8e-a7e8-4103700f0001"
                }
            ]
        },
        {
            "id": "2f49bcf0-822b-445c-b9da-634a6d0f6930",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e6fc5dd-4d2d-44b2-9d0c-9b70b455fa8c",
            "compositeImage": {
                "id": "87b1dab8-5094-4716-8f0e-4e202e778dd4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f49bcf0-822b-445c-b9da-634a6d0f6930",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b7b5e956-b199-4dd9-8de8-5ce2dd4eccfc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f49bcf0-822b-445c-b9da-634a6d0f6930",
                    "LayerId": "26f5cac7-2989-4e8e-a7e8-4103700f0001"
                }
            ]
        },
        {
            "id": "8191d4b6-6630-4420-8475-a4196b322c0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e6fc5dd-4d2d-44b2-9d0c-9b70b455fa8c",
            "compositeImage": {
                "id": "d30bfc25-3ed9-4d66-b255-7d4b8ed36d8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8191d4b6-6630-4420-8475-a4196b322c0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "313f872d-2c63-4260-9f5d-47b436e9c145",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8191d4b6-6630-4420-8475-a4196b322c0e",
                    "LayerId": "26f5cac7-2989-4e8e-a7e8-4103700f0001"
                }
            ]
        },
        {
            "id": "f803c3bd-0779-4455-ae7e-970083faae08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e6fc5dd-4d2d-44b2-9d0c-9b70b455fa8c",
            "compositeImage": {
                "id": "9febfa3d-2bf4-467f-94aa-5248f7d9866e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f803c3bd-0779-4455-ae7e-970083faae08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "482373c9-30e3-4d50-b6af-4a1fdadc760f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f803c3bd-0779-4455-ae7e-970083faae08",
                    "LayerId": "26f5cac7-2989-4e8e-a7e8-4103700f0001"
                }
            ]
        },
        {
            "id": "5c60f6c8-1935-418b-a1d2-a645826baf2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e6fc5dd-4d2d-44b2-9d0c-9b70b455fa8c",
            "compositeImage": {
                "id": "f5a6cd53-558c-4b65-bcc8-0bca1a5ef269",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c60f6c8-1935-418b-a1d2-a645826baf2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4cc02ac7-932f-4a78-ac29-a7eb053bb2de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c60f6c8-1935-418b-a1d2-a645826baf2d",
                    "LayerId": "26f5cac7-2989-4e8e-a7e8-4103700f0001"
                }
            ]
        },
        {
            "id": "c83e82e2-1767-44f6-a2fe-c64846dadf95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e6fc5dd-4d2d-44b2-9d0c-9b70b455fa8c",
            "compositeImage": {
                "id": "aebfd97b-63d1-4b6c-ad19-8cf326c8146d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c83e82e2-1767-44f6-a2fe-c64846dadf95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02ae0606-f23e-4c51-b9dc-d502a2b31985",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c83e82e2-1767-44f6-a2fe-c64846dadf95",
                    "LayerId": "26f5cac7-2989-4e8e-a7e8-4103700f0001"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 200,
    "layers": [
        {
            "id": "26f5cac7-2989-4e8e-a7e8-4103700f0001",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1e6fc5dd-4d2d-44b2-9d0c-9b70b455fa8c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 200,
    "xorig": 96,
    "yorig": 102
}