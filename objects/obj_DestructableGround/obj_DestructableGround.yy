{
    "id": "64c98adf-55d8-4d4f-beeb-9ebaf80bc06f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_DestructableGround",
    "eventList": [
        {
            "id": "b45e4e23-1d56-4304-ac49-5a2880e1610d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d8126fe5-7775-4b58-8dbf-6827567d280f",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "64c98adf-55d8-4d4f-beeb-9ebaf80bc06f"
        },
        {
            "id": "6f2cfa4e-7ae7-40e4-96b8-f160786e8b06",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "64c98adf-55d8-4d4f-beeb-9ebaf80bc06f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "7f53f958-f7cb-4149-9bfc-8f99f3e40553",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6151cd7f-1e64-4b80-9e71-9ab5e89cad0a",
    "visible": true
}