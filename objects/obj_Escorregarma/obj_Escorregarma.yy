{
    "id": "8a1bee6c-4157-463a-acc9-2302f257eca5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Escorregarma",
    "eventList": [
        {
            "id": "2f393e21-57cc-4626-a14e-a91ceaab91e6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "aaf51c09-089f-4547-8857-c68db1e7acd4",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "8a1bee6c-4157-463a-acc9-2302f257eca5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "99c785ae-8adb-480d-81f0-7f15953a5a41",
    "visible": true
}