{
    "id": "9b7e4239-7b33-4390-8c0d-9a9179b07002",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_End",
    "eventList": [
        {
            "id": "803dd52e-2fa8-430c-94ed-35e34dc80e97",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "aaf51c09-089f-4547-8857-c68db1e7acd4",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "9b7e4239-7b33-4390-8c0d-9a9179b07002"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "07a33c2f-bd4f-4800-bd5c-21eab6927e2f",
    "visible": true
}