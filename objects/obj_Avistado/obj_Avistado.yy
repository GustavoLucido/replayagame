{
    "id": "4a0021bb-426a-4211-a5b2-54e7d0238fe2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Avistado",
    "eventList": [
        {
            "id": "d5e26d6a-acc0-435b-b1ee-7feffc0f3233",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4a0021bb-426a-4211-a5b2-54e7d0238fe2"
        },
        {
            "id": "00873aa6-84bd-4a9f-b90a-be467dd6a0ee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "19e303c1-ae6d-4099-a53e-37df3b31cd16",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "4a0021bb-426a-4211-a5b2-54e7d0238fe2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "3b79da7d-78a7-44c2-b2f2-f633a7975567",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "avisado_index",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "743d96d2-467b-4e22-9117-b83e267d3c19",
    "visible": true
}