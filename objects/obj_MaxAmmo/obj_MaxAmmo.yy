{
    "id": "48ca3843-deca-4a43-bbd1-ae0aca81082e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_MaxAmmo",
    "eventList": [
        {
            "id": "49ff3392-e178-4cdb-be33-be560689498f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "48ca3843-deca-4a43-bbd1-ae0aca81082e"
        },
        {
            "id": "a1efb9b2-ef1a-4e0c-aaf7-fa7ff291f3bf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "48ca3843-deca-4a43-bbd1-ae0aca81082e"
        },
        {
            "id": "6f65c18c-9d49-4724-b047-5adaab3dfdb1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "48ca3843-deca-4a43-bbd1-ae0aca81082e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "343ee4e5-535f-4d96-8791-b2b19cf6c1b7",
    "visible": true
}