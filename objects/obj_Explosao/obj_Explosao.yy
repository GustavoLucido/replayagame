{
    "id": "d8126fe5-7775-4b58-8dbf-6827567d280f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Explosao",
    "eventList": [
        {
            "id": "6cae0f8a-5123-45e6-93a7-0d334689cc53",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "d8126fe5-7775-4b58-8dbf-6827567d280f"
        },
        {
            "id": "3009b980-2c26-4164-9e54-40a95c1291e6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "df5b699d-dfa5-4b37-9e54-666ed9a27c98",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d8126fe5-7775-4b58-8dbf-6827567d280f"
        },
        {
            "id": "8171f931-8379-4811-a09a-5922c13ca611",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d8126fe5-7775-4b58-8dbf-6827567d280f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a9ed42ef-4976-4a57-836d-133269ad8d38",
    "visible": true
}