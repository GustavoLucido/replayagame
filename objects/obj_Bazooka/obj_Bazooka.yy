{
    "id": "ade67ff7-762c-470f-b83b-2ccce2d2eb18",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Bazooka",
    "eventList": [
        {
            "id": "e2279ef8-5cf3-4457-ab39-91a551e38b4b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "aaf51c09-089f-4547-8857-c68db1e7acd4",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "ade67ff7-762c-470f-b83b-2ccce2d2eb18"
        },
        {
            "id": "bae2d4f8-4b7c-4cd9-a578-b6fdb4cf09f1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ade67ff7-762c-470f-b83b-2ccce2d2eb18"
        },
        {
            "id": "2699f746-4e6b-4a4f-ba6a-7a884a020c00",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ade67ff7-762c-470f-b83b-2ccce2d2eb18"
        },
        {
            "id": "0bb98ed8-7908-407c-a9ab-451495d850c6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "ade67ff7-762c-470f-b83b-2ccce2d2eb18"
        },
        {
            "id": "249b316c-e970-438e-b984-8e6a4c58c6dc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "ade67ff7-762c-470f-b83b-2ccce2d2eb18"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5bcbe07b-a32f-468e-9e1e-84d92c5eeff0",
    "visible": true
}