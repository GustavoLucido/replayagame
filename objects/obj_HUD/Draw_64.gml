draw_sprite(spr_MolduraArma, 0,  1180, 740);

draw_set_font(fon_8bit)

if (instance_exists(obj_Player))
{
	if (global.armaAtual = 1)
	{
		draw_text_transformed(1375, 800, 8, 2, 3, -90)
		draw_sprite_ext(spr_IconRevolver,0,1460, 820, 1.3, 1.3, 0, c_white, 1)
	}

	if (global.armaAtual = 2)
	{
		draw_text_transformed(1245, 805, string(obj_Player.municaoMolotov) + "/16", 1.3, 1, 0)
		draw_sprite_ext(spr_IconMolotov , 0, 1510, 820, 1.3, 1.3, 0, c_white, 1)	
	}

	if (global.armaAtual = 3)
	{
		draw_text_transformed(1245, 805, string(obj_Player.municaoShotgun) + "/60", 1.3, 1, 0)
		draw_sprite_ext(spr_IconShotgun,0,1495, 820, 0.7, 0.7, 20, c_white, 1)
	}

	if (global.armaAtual = 4)
	{
		draw_text_transformed(1245, 805, string(obj_Player.municaoMetralha) + "/90", 1.3, 1, 0)
		draw_sprite_ext(spr_IconMetralha,0,1495, 820, 0.6, 0.6, 20, c_white, 1)
	}

	if (global.armaAtual = 5)
	{
		draw_text_transformed(1245, 805, string(obj_Player.municaoGranada) + "/16", 1.3, 1, 0)
		draw_sprite_ext(spr_IconGranada, 0, 1510, 825, 1.3, 1.3, 0, c_white, 1)
	}

	if (global.armaAtual = 6)
	{
		draw_text_transformed(1245, 805, string(obj_Player.municaoBazooka) + "/10", 1.3, 1, 0)
		draw_sprite_ext(spr_IconBazooka, 0, 1515, 840, 0.8, 0.8, 20, c_white, 1)	
	}
}


//--------------------------- Barra vida ----------------------------

if (dano = false)
{
	if (instance_exists(obj_Player))
		draw_sprite_ext(spr_BarraVida, 0,  175, 800, max(0, obj_Player.hp/obj_Player.maxHp), 1, 0, c_red,1);
	
	draw_sprite_ext(spr_MascaraVida, 0,  175, 800, 1, 1, 0, c_white,1);
	draw_sprite(spr_Moldura, 0, 20, 721)

}
	
else
{
	if (instance_exists(obj_Player))
		draw_sprite_ext(spr_BarraVida, 0,  173, 798, max(0, obj_Player.hp/obj_Player.maxHp), 1, 0, c_white ,1);
	
	draw_sprite_ext(spr_MascaraVida, 0,  173, 798, 1, 1, 0, c_white,1);
	draw_sprite(spr_Moldura1, 0, 18, 719)
}



