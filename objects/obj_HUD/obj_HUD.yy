{
    "id": "aa7bf09d-d81d-4798-8f36-2daf69baba58",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_HUD",
    "eventList": [
        {
            "id": "e207184f-8555-4860-bd9b-ff0f40b60352",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "aa7bf09d-d81d-4798-8f36-2daf69baba58"
        },
        {
            "id": "9880bc6b-e5c9-475d-9b0b-b7ae218e862b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "aa7bf09d-d81d-4798-8f36-2daf69baba58"
        },
        {
            "id": "07f8d826-4872-407c-9a9f-e5bf1044cd0e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "aa7bf09d-d81d-4798-8f36-2daf69baba58"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}