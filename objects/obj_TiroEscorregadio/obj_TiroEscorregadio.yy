{
    "id": "c026e67d-61bb-40f6-854b-b7522f12c9a5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_TiroEscorregadio",
    "eventList": [
        {
            "id": "d4f909d1-c650-4455-a756-b1d2d373e2cb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c026e67d-61bb-40f6-854b-b7522f12c9a5"
        },
        {
            "id": "aae414c0-0bf6-49af-978f-3f3156ed3a8f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c026e67d-61bb-40f6-854b-b7522f12c9a5"
        },
        {
            "id": "f7b86c04-4404-4364-b43f-93365521e6cb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "7f53f958-f7cb-4149-9bfc-8f99f3e40553",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c026e67d-61bb-40f6-854b-b7522f12c9a5"
        },
        {
            "id": "e5745f75-647b-4520-b522-9e7a26dfffb6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "9d5f4f50-4a32-474f-9629-05fd64609303",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c026e67d-61bb-40f6-854b-b7522f12c9a5"
        },
        {
            "id": "d83e8791-cecb-4729-aac7-50e1f70a6ef6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "df5b699d-dfa5-4b37-9e54-666ed9a27c98",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c026e67d-61bb-40f6-854b-b7522f12c9a5"
        },
        {
            "id": "eb90881c-4500-4df2-b255-db545b8abf06",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "64c98adf-55d8-4d4f-beeb-9ebaf80bc06f",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c026e67d-61bb-40f6-854b-b7522f12c9a5"
        },
        {
            "id": "9079bcd0-346a-4fe4-8f0d-09d5934f2472",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "19e303c1-ae6d-4099-a53e-37df3b31cd16",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c026e67d-61bb-40f6-854b-b7522f12c9a5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "793d479e-881f-436b-92ca-53888a98e921",
    "visible": true
}