{
    "id": "230e0182-c8cb-42b1-a1c0-ba929a38d936",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_TiroShotgun",
    "eventList": [
        {
            "id": "cb5577e7-7df2-410f-b0f9-fda6f7c5405c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "230e0182-c8cb-42b1-a1c0-ba929a38d936"
        },
        {
            "id": "41659d6d-fbe7-417f-85c7-8da7dee5fba5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "230e0182-c8cb-42b1-a1c0-ba929a38d936"
        },
        {
            "id": "2b76431b-16a4-4cfa-a85a-3a89b480d12f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "7f53f958-f7cb-4149-9bfc-8f99f3e40553",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "230e0182-c8cb-42b1-a1c0-ba929a38d936"
        },
        {
            "id": "40b40d1a-63fa-4ca1-bfdb-b9d8c8cbf09e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "df5b699d-dfa5-4b37-9e54-666ed9a27c98",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "230e0182-c8cb-42b1-a1c0-ba929a38d936"
        },
        {
            "id": "e2a457fa-606a-4dcd-a2f4-3e75a26ac7b0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "64c98adf-55d8-4d4f-beeb-9ebaf80bc06f",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "230e0182-c8cb-42b1-a1c0-ba929a38d936"
        },
        {
            "id": "9c70977f-5dba-4729-955e-6005b38cbf10",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "19e303c1-ae6d-4099-a53e-37df3b31cd16",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "230e0182-c8cb-42b1-a1c0-ba929a38d936"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1387bdcc-32ab-4975-a641-0e2db095e4e1",
    "visible": true
}