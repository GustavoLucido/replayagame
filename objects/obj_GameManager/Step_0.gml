if (keyboard_check_pressed(ord("1")) && obj_Player.meter == false && obj_Player.revolverOn == true)
	global.armaAtual = 1;
	
if (keyboard_check_pressed(ord("2")) && obj_Player.meter == false && obj_Player.molotovOn == true)
	global.armaAtual = 2;
	
if (keyboard_check_pressed(ord("3")) && obj_Player.meter == false && obj_Player.granadaOn == true)
	global.armaAtual = 5;

if (keyboard_check_pressed(ord("4")) && obj_Player.meter == false && obj_Player.metralhaOn == true)
	global.armaAtual = 4;
	
if (keyboard_check_pressed(ord("5")) && obj_Player.meter == false && obj_Player.bazookaOn == true)
	global.armaAtual = 6;	

if (keyboard_check_pressed(ord("6")) && obj_Player.meter == false && obj_Player.shotgunOn == true)
	global.armaAtual = 3;	

if (keyboard_check_pressed(ord("7")) && obj_Player.meter == false && obj_Player.escorregarmaOn == true)
	global.armaAtual = 7;	


if (keyboard_check_pressed(ord("E")))
{
	global.armaAtual ++;
	
	if (global.armaAtual == 1 && obj_Player.revolverOn == false)
		global.armaAtual = 0

	
	if (global.armaAtual == 2 && obj_Player.molotovOn == false)
		global.armaAtual ++
	
	if (global.armaAtual == 3 && obj_Player.shotgunOn == false)
		global.armaAtual ++
	
	if (global.armaAtual == 4 && obj_Player.metralhaOn == false) 
		global.armaAtual ++
	
	if (global.armaAtual == 5 && obj_Player.granadaOn == false)
		global.armaAtual ++
	
	if (global.armaAtual == 6 && obj_Player.bazookaOn == false)
		global.armaAtual ++
	
	if (global.armaAtual == 7)
		global.armaAtual = 1
}

if (keyboard_check_pressed(ord("Q")))
{
	global.armaAtual --;
	
	if (global.armaAtual == 0)
		global.armaAtual = 6
	
	if (global.armaAtual == 6 && obj_Player.bazookaOn == false)
		global.armaAtual --
	
	if (global.armaAtual == 5 && obj_Player.granadaOn == false)
		global.armaAtual --
	
	if (global.armaAtual == 4 && obj_Player.metralhaOn == false) 
		global.armaAtual --
		
	if (global.armaAtual == 3 && obj_Player.shotgunOn == false)
		global.armaAtual --
		
	if (global.armaAtual == 2 && obj_Player.molotovOn == false)
		global.armaAtual --
	
	if (global.armaAtual == 1 && obj_Player.revolverOn == false)
		global.armaAtual = 0
}
	
cursor_sprite = spr_Cursor