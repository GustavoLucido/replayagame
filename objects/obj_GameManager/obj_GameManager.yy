{
    "id": "5d2e15a2-5387-4d1b-9920-f6c9209b1fa4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_GameManager",
    "eventList": [
        {
            "id": "19f721c5-e5e5-41b3-b3d8-0e09a980cbd5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5d2e15a2-5387-4d1b-9920-f6c9209b1fa4"
        },
        {
            "id": "126acba3-aa63-400f-9340-ee8b3db6e7c7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5d2e15a2-5387-4d1b-9920-f6c9209b1fa4"
        },
        {
            "id": "b35593d7-6607-4758-afa8-b99dab55ec7c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "5d2e15a2-5387-4d1b-9920-f6c9209b1fa4"
        },
        {
            "id": "76887b07-abb3-4073-ac51-61b6541ccf93",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 2,
            "m_owner": "5d2e15a2-5387-4d1b-9920-f6c9209b1fa4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}