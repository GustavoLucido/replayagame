{
    "id": "19e303c1-ae6d-4099-a53e-37df3b31cd16",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Inimigo",
    "eventList": [
        {
            "id": "56191ec1-2e95-400f-a992-5419c96f0c08",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "19e303c1-ae6d-4099-a53e-37df3b31cd16"
        },
        {
            "id": "b2f31ca7-bd30-40b2-8681-0fe8f4b17f4e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "19e303c1-ae6d-4099-a53e-37df3b31cd16"
        },
        {
            "id": "4f09ebc3-788b-4e35-853e-5d3c3b48a0b7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "19e303c1-ae6d-4099-a53e-37df3b31cd16"
        },
        {
            "id": "149655bc-a5b3-46ad-8fe2-bae87b1eaf26",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "19e303c1-ae6d-4099-a53e-37df3b31cd16"
        },
        {
            "id": "43613fbe-0ff1-4621-ad43-412e72f5c954",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "19e303c1-ae6d-4099-a53e-37df3b31cd16"
        },
        {
            "id": "756a46e0-bf8f-470b-ad34-d39a6f939c0e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "22259775-2a37-4b05-bbe6-43d1e3baf2b7",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "19e303c1-ae6d-4099-a53e-37df3b31cd16"
        },
        {
            "id": "076bc604-5ae5-4e5b-8baf-a148beea90a8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "230e0182-c8cb-42b1-a1c0-ba929a38d936",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "19e303c1-ae6d-4099-a53e-37df3b31cd16"
        },
        {
            "id": "fcc1c1da-cacc-48fc-a67e-2fffd5382c5b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "c026e67d-61bb-40f6-854b-b7522f12c9a5",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "19e303c1-ae6d-4099-a53e-37df3b31cd16"
        },
        {
            "id": "9572bbf0-7f93-497c-bd36-4a0db3d1305e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d8126fe5-7775-4b58-8dbf-6827567d280f",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "19e303c1-ae6d-4099-a53e-37df3b31cd16"
        },
        {
            "id": "e8aa1ebb-0f64-4797-ba3e-5bcafccd416a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "5941b725-7cf5-4512-a9c5-85a8e65b47df",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "19e303c1-ae6d-4099-a53e-37df3b31cd16"
        },
        {
            "id": "6ca0ac81-e33a-4e62-944f-cdeb3ca6dc54",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "19e303c1-ae6d-4099-a53e-37df3b31cd16"
        },
        {
            "id": "a4fcc71b-5e8c-4754-9750-9e311adf4e45",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 6,
            "eventtype": 2,
            "m_owner": "19e303c1-ae6d-4099-a53e-37df3b31cd16"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "a3ff7779-317d-4efc-9443-6d93fb16a743",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "83cbae08-2257-4b1c-ae45-c7eca7a64067",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 0
        },
        {
            "id": "627239f7-54d2-43ce-8d3b-f7de7350e5a8",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 64
        },
        {
            "id": "9a70dca7-7897-4cd9-a775-7b5a6e462b4f",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 64
        }
    ],
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "139fc105-7d07-4702-a7f2-43801d9d65c8",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "nmy_index",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}