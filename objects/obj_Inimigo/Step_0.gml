//----------------------------------------- Check Colisao com o chao ------------------------------------------------------
if (place_meeting(x, y + 1, obj_Ground) || place_meeting(x, y + 1, obj_DestructableGround))
	{
		velocidade_v = 0;
		noChao = true;
		assistChao = 0;
	}
	
else
{
	if (noChao == true && escalando == false)
		assistChao++;

//------------------------------------------------- Gravidade ----------------------------------------------------

	if (velocidade_v < 30 && escalando == false)
		velocidade_v += gravidade;
}

if (assistChao > 7)
	noChao = false;

//---------- Pulo ------------//
if (cima && noChao == true && escalando = false)
		{
			cima = false
			velocidade_v -= pulo;
			noChao = false;
		}

//--------- Escada ---------//
if (place_meeting(x, y, obj_Escada))
	{
		escalando = true;
		
		if (cima)
		{
			velocidade_v = -10;
			velocidade_h = 0;
		}
		
		if (baixo)
		{
			velocidade_v = 10;
			velocidade_h = 0;
		}
		
		
		if (cima && baixo || !cima && !baixo)		//Ficar parado
		{
			velocidade_v = 0;
		}
	}
	
else
{
	escalando = false;
}
		
//------------------------------------------------ Movimentacao -------------------------------------------------------

//------------------------------------- Patrulhando -----------------------------------//

if (patrulhando == true)
{

	if (direita == true && noChao == true && velocidade_h < 5)		
	{
		varDireita = true;
		velocidade_h += spd/3;
		olhandoDireita = true;
	}

	if (esquerda == true && noChao == true && velocidade_h > -5)
	{
		varDireita = false;
		velocidade_h -= spd/3;
		olhandoDireita = false;
	}

	if (direita == true && noChao == false && velocidade_h < 5)		//Movimentacao no ar
	{
		varDireita = true;
		velocidade_h += spd/6;
		olhandoDireita = true;
	}

	if (esquerda && noChao == false && velocidade_h > -5)			//Movimentacao no ar
	{
		varDireita = false;
		velocidade_h -= spd/6;
		olhandoDireita = false;
	}
}

//------------------------------------- Perseguindo -------------------------------//

if (perseguindo == true)
{
	
	
//----------- Decidir o lado pra andar -------------//

	if (instance_exists(obj_Player))
	{
		distPlayer = x - obj_Player.x
	
		if (distPlayer > 0)
		{
			image_xscale = -1
			direita = false
			esquerda = true
		}
	
		else
		{
			image_xscale = 1
			direita = true
			esquerda = false
		}
	
	//--------------- Andar ate o player ---------------//	
	
		if (direita == true && velocidade_h < 8 && distPlayer < -600)		
		{
			varDireita = true;
			velocidade_h += spd/3;
			olhandoDireita = true;
		}

		if (esquerda == true && velocidade_h > -8 && distPlayer > 600)
		{
			varDireita = false;
			velocidade_h -= spd/3;
			olhandoDireita = false;
		}
	
		if (distPlayer < 680 && distPlayer > - 680)
		{
			atirar = true;
			velocidade_h = 0
		}
	}
}

//------------------------------------- Procurando -------------------------------//

if (procurando == true)		//Ficar parado
{
	procurando = false;
	velocidade_h = 0;
	alarm[0] = 1 * room_speed;
}
//-------------------------------------------------- Atirar -----------------------------------------------------------------------------------

//----------- Revolver -------------//

if (atirar && cooldownTiro <= 0 && revolverOn == true && municaoRevolver > 0 && avistou == true)
{
	instance_create_layer (x, y - 10, "Bullet_layer", obj_TiroInimigo);
	cooldownTiro = 2.5;
	municaoRevolver --;
	atirar = false
}


//----------- Molotov -------------//

if (atirar && cooldownTiro <= 0  && meter == false && molotovOn == true)
{
	instance_create_layer (x, y - 80, "Bullet_layer", obj_Meter);
	meter = true;
	atirar = false
}


//----------- Shotgun -------------//

if (atirar && cooldownTiro <= 0 && shotgunOn == true)
{
	instance_create_layer (x, y - 10, "Bullet_layer", obj_TiroShotgun);
	instance_create_layer (x, y - 10, "Bullet_layer", obj_TiroShotgun);
	instance_create_layer (x, y - 10, "Bullet_layer", obj_TiroShotgun);
	instance_create_layer (x, y - 10, "Bullet_layer", obj_TiroShotgun);
	instance_create_layer (x, y - 10, "Bullet_layer", obj_TiroShotgun);
	cooldownTiro = 2;
	atirar = false
}


//----------- Metrtalhadora -------------//

if (atirar && cooldownTiro <= 0 && metralhaOn == true)
{
	instance_create_layer (x, y - 10, "Bullet_layer", obj_TiroShotgun);
	cooldownTiro = 0.3;
	atirar = false
}


//----------- Granada -------------//

if (atirar && cooldownTiro <= 0 && meter == false && granadaOn == true)
{
	instance_create_layer (x, y - 80, "Bullet_layer", obj_Meter);
	meter = true;
}


//----------- Bazooka -------------//

if (atirar && cooldownTiro <= 0 && bazookaOn == true)
{
	instance_create_layer (x, y - 30, "Bullet_layer", obj_Missil);
	cooldownTiro = 3;
}




//---------- Cooldown do Tiro -----------//
if (cooldownTiro >= 0)
	cooldownTiro -= 1/15;
	
//---------- Reload das armas -----------//
if (municaoRevolver == 0)
	{
		alarm [6] = room_speed * 3.5;
		municaoRevolver --;
		
	}
	
	
	
//--------------------------- Parar de se mover com colisao no eixo "X" (Horizontal) ------------------------------------------------------

//----------------------- Ground -----------------------//

if (place_meeting(x + velocidade_h, y, obj_Ground))	
{	
	if (patrulhando == true || perseguindo == true)
	{
		if (direita == true)
		{
			direita = false
			esquerda = true;
		}
	
		else
		{
			direita = true;
			esquerda = false;
		}
	
	
		while (!place_meeting(x+sign(velocidade_h), y, obj_Ground))
		{
			x += sign(velocidade_h);
		}
	
		velocidade_h = 0;	
	}
}


//----------------------- Destructable Ground -----------------------//

if (place_meeting(x + velocidade_h, y, obj_DestructableGround))	
{	
	if (patrulhando == true || perseguindo == true)
	{
		if (direita == true)
		{
			direita = false
			esquerda = true;
		}
	
		else
		{
			direita = true;
			esquerda = false;
		}
	
	
		while (!place_meeting(x+sign(velocidade_h), y, obj_DestructableGround))
		{
			x += sign(velocidade_h);
		}
	
		velocidade_h = 0;	
	}
}


//----------------------- Enemy Limit -----------------------//

if (place_meeting(x + velocidade_h, y, obj_NmyLimit) && perseguindo == false)	
{	
	if (patrulhando == true || perseguindo == true)
	{
		if (direita == true)
		{
			procurando = true;
			patrulhando = false;
			direita = false
			esquerda = true;
		}
	
		else
		{
			procurando = true;
			patrulhando = false;
			direita = true;
			esquerda = false;
		}
	
	
		while (!place_meeting(x+sign(velocidade_h), y, obj_NmyLimit))
		{
			x += sign(velocidade_h);
		}
	
		velocidade_h = 0;	
	}
}


//---------------
x += velocidade_h;



//--------------------------- Parar de se mover com colisao no eixo "Y" (Vertical) ------------------------------------------------------

//----------------------- Ground -----------------------//

if (place_meeting(x, y + velocidade_v , obj_Ground))
{	
	while (!place_meeting(x, y+sign(velocidade_v), obj_Ground))
	{		
		y += sign(velocidade_v);
	}
	
	velocidade_v = 0;
}

if (!place_meeting(x + 70, y + 50 , obj_Ground) && direita == true)
{
	procurando = true;
	patrulhando = false;
	direita = false
	esquerda = true;
	velocidade_h = 0;
}

if (!place_meeting(x - 70, y + 50 , obj_Ground) && direita == false)
{
	procurando = true;
	patrulhando = false;
	direita = true;
	esquerda = false;
	velocidade_h = 0;
}


//----------------------- Destructable Ground -----------------------//

if (place_meeting(x, y + velocidade_v , obj_DestructableGround))
{	
	while (!place_meeting(x, y+sign(velocidade_v), obj_DestructableGround))
	{		
		y += sign(velocidade_v);
	}
	

	if (!place_meeting(x + 40, y + 70 , obj_DestructableGround) && direita == true)
	{
		procurando = true;
		patrulhando = false;
		direita = false
		esquerda = true;
		velocidade_h = 0;
	}

	if (!place_meeting(x - 40, y + 70 , obj_DestructableGround) && direita == false)
	{
		procurando = true;
		patrulhando = false;
		direita = true;
		esquerda = false;
		velocidade_h = 0;
	}
	

	velocidade_v = 0;
}


//---------------
y += velocidade_v;



//--------------------------------------------------- Sprite -----------------------------------------------------------------

if (velocidade_h > 0)
{
	image_xscale = 1;
	image_speed = 2;
}


if (velocidade_h < 0)
{
	image_xscale = -1;
	image_speed = 2;
}

if (velocidade_h == 0)
{
	image_speed = 0;
	image_index = 0;
}

//--------------------------------------------------- Morte --------------------------------------------------------

if (vida <= 0)
	instance_destroy()