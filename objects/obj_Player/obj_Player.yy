{
    "id": "aaf51c09-089f-4547-8857-c68db1e7acd4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Player",
    "eventList": [
        {
            "id": "07e5f455-c7af-4029-a019-89701b8fe65e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "aaf51c09-089f-4547-8857-c68db1e7acd4"
        },
        {
            "id": "d87d85da-19b6-4dfc-a1b5-2f6bf9765ee4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "aaf51c09-089f-4547-8857-c68db1e7acd4"
        },
        {
            "id": "e7fa2973-a8b2-4ffa-86a4-a3060ef0ea41",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "aaf51c09-089f-4547-8857-c68db1e7acd4"
        },
        {
            "id": "b546ab6c-9c59-4f1d-a46b-74befed714ed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "aaf51c09-089f-4547-8857-c68db1e7acd4"
        },
        {
            "id": "2e4c9652-4540-4ea2-b303-0a5355d3419f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d28a3dac-4d44-4680-9d87-01291639bfc8",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "aaf51c09-089f-4547-8857-c68db1e7acd4"
        },
        {
            "id": "64ade036-67a5-4aed-ad2d-8a6b70578583",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "aaf51c09-089f-4547-8857-c68db1e7acd4"
        },
        {
            "id": "64b7a7cd-ecce-4b11-9648-e66595aa4065",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "5941b725-7cf5-4512-a9c5-85a8e65b47df",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "aaf51c09-089f-4547-8857-c68db1e7acd4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "991c5f9c-3cc2-4648-a10e-56e03a37126e",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "aebb31e4-297c-47fe-ac60-a7e7d25ba09f",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 0
        },
        {
            "id": "c2c8979b-bb89-49e6-a5ef-ce9740c5da4b",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 64,
            "y": 64
        },
        {
            "id": "fb4c5055-98da-48d6-87ef-c9733da787af",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 64
        }
    ],
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "a459f7f3-9570-4960-b27e-afbc14a6aec0",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "False",
            "varName": "variable_name",
            "varType": 3
        }
    ],
    "solid": false,
    "spriteId": "cb9a8b49-01f5-4cc2-ab82-efb5fa9719e2",
    "visible": true
}