instance_destroy(other)

if (flinch <= 0)
{
	velocidade_h = -velocidade_h * 0.5;

	hp --;

	flinch = 15;
}

instance_create_layer(x,y, "Instances", obj_ShakeNmy)

if (!audio_is_playing(snd_Hurt_1) && !audio_is_playing(snd_Hurt_2))
{
	if (hurt1 == true)
	{
		audio_play_sound(snd_Hurt_1, 1, false);
		hurt1 = false;
	}
	
	else
	{
		audio_play_sound(snd_Hurt_2, 1, false);
		hurt1 = true;
	}
}