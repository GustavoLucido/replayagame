//----------------------------------------- Movimentaçao Personagem -----------------------------------------------

var direita = keyboard_check(ord("D"))
var esquerda = keyboard_check(ord("A"));
var cima = keyboard_check_pressed(ord("W")) || keyboard_check_pressed(vk_space);
var baixo = keyboard_check(ord("S"));

//----------------------------------------- Inicio e Fim das Fases ---------------------------------------------
if (spawn == true)
{
	if (global.indo == true)
	{
		x = obj_Spawn.x;
		y = obj_Spawn.y;
		spawn = false;
	}
	
	else
	{
		x = obj_Despawn.x;
		y = obj_Despawn.y;
		spawn = false;
	}
}

//----------------------------------------- Check Colisao com o chao ------------------------------------------------------

if (place_meeting(x, y + 1, obj_Ground) || place_meeting(x, y + 1, obj_DestructableGround))
	{
		velocidade_v = 0;
		noChao = true;
		wallJump = false;
		assistChao = 0;
		
		//-------------------- Agachar ---------------------//
		if (baixo)
		{		
			sprite_index = spr_PlayerAgachado;
			agachado = true;
			spd = 3;
			if (velocidade_h >= 7 || velocidade_h <= -7)
				velocidade_h = 0;
			
		}
			
		if (!keyboard_check(ord("S")) && !place_meeting(x, y - 30 , obj_Ground) && !place_meeting(x, y - 30 , obj_DestructableGround) && agachado == true)
		{
			sprite_index = spr_Player;
			agachado = false;	
			spd = 9;
		}
	}
	
else
{
	if (noChao == true && escalando == false)
		assistChao++;

//------------------------------------------------- Gravidade ----------------------------------------------------

	if (velocidade_v < 30 && escalando == false)
		velocidade_v += gravidade;
}

if (assistChao > 7)
	noChao = false;

//---------- Pulo ------------//
if (cima && noChao == true && escalando = false)
		{
			velocidade_v -= pulo;
			noChao = false;
		}

//-------- Planador --------//
if (cima && noChao == false && velocidade_v > -20 && escalando = false)
	instance_create_layer(obj_Player.x, obj_Player.y - 80, "Instances", obj_Planador);
	
	
//--------- Escada ---------//
if (place_meeting(x, y, obj_Escada))
	{
		escalando = true;
		
		if (keyboard_check(ord("W")))
		{
			velocidade_v = -10;
			velocidade_h = 0;
		}
		
		if (keyboard_check(ord("S")))
		{
			velocidade_v = 10;
			velocidade_h = 0;
		}
		
		
		if (keyboard_check(ord("W")) && keyboard_check(ord("S")) || !keyboard_check(ord("W")) && !keyboard_check(ord("S")))		//Ficar parado
		{
			velocidade_v = 0;
		}
	}
	
else
{
	escalando = false;
}
		
//------------------------------------------------ Movimentacao -------------------------------------------------------

if (agachado == false)
{
	if (direita == true && noChao == true && velocidade_h < 9)		
	{
		varDireita = true;
		velocidade_h += spd/3;
		olhandoDireita = true;
	}

	if (esquerda == true && noChao == true && velocidade_h > -9)
	{
		varDireita = false;
		velocidade_h -= spd/3;
		olhandoDireita = false;
	}

	if (direita == true && noChao == false && velocidade_h < 9)		//Movimentacao no ar
	{
		varDireita = true;
		velocidade_h += spd/6;
		olhandoDireita = true;
	}

	if (esquerda && noChao == false && velocidade_h > -9)			//Movimentacao no ar
	{
		varDireita = false;
		velocidade_h -= spd/6;
		olhandoDireita = false;
	}
}

else
{
	if (direita == true && noChao == true && velocidade_h < 5)		
	{
		varDireita = true;
		velocidade_h += spd/3;
		olhandoDireita = true;
	}

	if (esquerda == true && noChao == true && velocidade_h > -5)
	{
		varDireita = false;
		velocidade_h -= spd/3;
		olhandoDireita = false;
	}

	if (direita == true && noChao == false && velocidade_h < 5)		//Movimentacao no ar
	{
		varDireita = true;
		velocidade_h += spd/6;
		olhandoDireita = true;
	}

	if (esquerda && noChao == false && velocidade_h > -5)			//Movimentacao no ar
	{
		varDireita = false;
		velocidade_h -= spd/6;
		olhandoDireita = false;
	}
}

if (esquerda && direita && noChao == true || !esquerda && !direita && wallJump == false)		//Ficar parado
{
	velocidade_h = 0;
}


//-------------------------------------------------- Atirar -----------------------------------------------------------------------------------

//----------- Revolver -------------//

if (mouse_check_button(mb_left) && cooldownTiro <= 0 && global.armaAtual == 1 && revolverOn == true && municaoRevolver > 0)
{
	instance_create_layer (obj_Arma.x, obj_Arma.y - 10, "Bullet_layer", obj_Tiro);
	obj_Arma.image_speed = 1;
	cooldownTiro = 0.5 * room_speed;
	
	instance_create_layer (x, y, "Instances", obj_Shake);
	
	audio_play_sound(snd_Revolver, 1, false)
}


//----------- Molotov -------------//

if (mouse_check_button(mb_left) && cooldownTiro <= 0 && global.armaAtual == 2 && meter == false && molotovOn == true && municaoMolotov > 0)
{
	instance_create_layer (obj_Player.x, obj_Player.y - 80, "Bullet_layer", obj_Meter);
	meter = true;
	municaoMolotov --;
	
	
}


//----------- Shotgun -------------//

if (mouse_check_button(mb_left) && cooldownTiro <= 0 && global.armaAtual == 3 && shotgunOn == true && municaoShotgun > 0)
{
	if (varDireita == true)
	{
		instance_create_layer (obj_Arma.x, obj_Arma.y - 10, "Bullet_layer", obj_TiroShotgun);
		instance_create_layer (obj_Arma.x, obj_Arma.y - 10, "Bullet_layer", obj_TiroShotgun);
		instance_create_layer (obj_Arma.x, obj_Arma.y - 10, "Bullet_layer", obj_TiroShotgun);
		instance_create_layer (obj_Arma.x, obj_Arma.y - 10, "Bullet_layer", obj_TiroShotgun);
		instance_create_layer (obj_Arma.x, obj_Arma.y - 10, "Bullet_layer", obj_TiroShotgun);
	}
	
	else
	{
		instance_create_layer (obj_Arma.x, obj_Arma.y - 10, "Bullet_layer", obj_TiroShotgun);
		instance_create_layer (obj_Arma.x, obj_Arma.y - 10, "Bullet_layer", obj_TiroShotgun);
		instance_create_layer (obj_Arma.x, obj_Arma.y - 10, "Bullet_layer", obj_TiroShotgun);
		instance_create_layer (obj_Arma.x, obj_Arma.y - 10, "Bullet_layer", obj_TiroShotgun);
		instance_create_layer (obj_Arma.x, obj_Arma.y - 10, "Bullet_layer", obj_TiroShotgun);
	}
		
		
	obj_Arma.image_speed = 1;
	cooldownTiro = 0.8 * room_speed;
	municaoShotgun --;
	
	instance_create_layer (x, y, "Instances", obj_Shake);
	
	audio_play_sound(snd_Shotgun, 1, false)
}


//----------- Metrtalhadora -------------//

if (mouse_check_button(mb_left) && cooldownTiro <= 0 && global.armaAtual == 4 && metralhaOn == true && municaoMetralha > 0)
{
	if (varDireita == true)
	instance_create_layer (obj_Arma.x , obj_Arma.y - 10, "Bullet_layer", obj_TiroShotgun);
	
	else
	instance_create_layer (obj_Arma.x , obj_Arma.y - 10, "Bullet_layer", obj_TiroShotgun);
	
	obj_Arma.image_speed = 1;
	cooldownTiro = 0.1 * room_speed;
	municaoMetralha --;
	
	instance_create_layer (x, y, "Instances", obj_Shake);
	audio_stop_sound(snd_AK47)
	audio_play_sound(snd_AK47, 1, false)
}


//----------- Granada -------------//

if (mouse_check_button(mb_left) && cooldownTiro <= 0 && global.armaAtual == 5 && meter == false && granadaOn == true && municaoGranada > 0)
{
	instance_create_layer (obj_Player.x, obj_Player.y - 80, "Bullet_layer", obj_Meter);
	meter = true;
	municaoGranada --;
}


//----------- Bazooka -------------//

if (mouse_check_button(mb_left) && cooldownTiro <= 0 && global.armaAtual == 6 && bazookaOn == true && municaoBazooka > 0)
{
	instance_create_layer (obj_Arma.x, obj_Arma.y - 30, "Bullet_layer", obj_Missil);
	cooldownTiro = 2 * room_speed;
	municaoBazooka --;
	
	instance_create_layer (x, y, "Instances", obj_Shake);
	
	audio_play_sound(snd_Launch, 1, false)
}


//----------- Escorregarma -------------//

if (mouse_check_button(mb_left) && cooldownTiro <= 0 && global.armaAtual == 7 && escorregarmaOn == true && municaoEscorregarma > 0)
{
	instance_create_layer (obj_Arma.x, obj_Arma.y - 10, "Bullet_layer", obj_TiroEscorregadio);
	cooldownTiro = 1.5;
	municaoEscorregarma --;
}


//---------- Cooldown do Tiro -----------//
if (cooldownTiro >= 0)
	cooldownTiro --;
	
	
//----------------------------------- Parar de se mover com colisao no eixo "X" ------------------------------------------------------

if (place_meeting(x + velocidade_h, y, obj_Ground))	
{	
	while (!place_meeting(x+sign(velocidade_h), y, obj_Ground))
	{
		x += sign(velocidade_h);
	}
	
	velocidade_h = 0;	
}


if (place_meeting(x + velocidade_h, y, obj_DestructableGround))	
{	
	while (!place_meeting(x+sign(velocidade_h), y, obj_DestructableGround))
	{
		x += sign(velocidade_h);
	}
	
	velocidade_h = 0;	
}


x += velocidade_h;


//----------------------------------- Parar de se mover com colisao no eixo "Y" ------------------------------------------------------

if (place_meeting(x, y + velocidade_v , obj_Ground))
{	
	while (!place_meeting(x, y+sign(velocidade_v), obj_Ground))
	{
		y += sign(velocidade_v);
	}
	
	velocidade_v = 0;
}


if (place_meeting(x, y + velocidade_v , obj_DestructableGround))
{
	while (!place_meeting(x, y+sign(velocidade_v), obj_DestructableGround))
	{
		y += sign(velocidade_v);
	}
	
	velocidade_v = 0;
}


y += velocidade_v;

//-----------------------------------------------------Sprite-----------------------------------------------------------------

if (x <= mouse_x)
{
	image_xscale = 1;
}

else
{
	image_xscale = -1;
}


//--------------------------------------------------- Morte -----------------------------------------------------------------

if (hp <= 0)
{
	instance_create_layer(x, y, "Instances", obj_DeathAnim)	
	obj_GameManager.alarm[10] = 90
	instance_destroy(obj_Arma)
	instance_destroy()
}

//-------------------------------------------------- Cheat Code -------------------------------------------------

if (keyboard_check_pressed(ord("P")))
{
	if (cheat_code == false)
	{
		hp = 10;
	
		revolverOn = true;
		metralhaOn = true;
		shotgunOn = true;
		molotovOn = true;
		granadaOn = true;
		bazookaOn = true;
		escorregarmaOn = true;
	
		municaoRevolver = 99;
		municaoMetralha = 90;
		municaoShotgun = 60;
		municaoMolotov = 16;
		municaoGranada = 16;
		municaoBazooka = 10;
		municaoEscorregarma = 99;
		
		cheat_code = true
	}
	
	else
	cheat_code = false
}