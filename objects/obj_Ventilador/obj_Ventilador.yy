{
    "id": "8f539406-bd96-4298-9b89-9e5ae3de6070",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Ventilador",
    "eventList": [
        {
            "id": "ba3f0150-a7bc-4e41-8d59-af9dbdf84736",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8f539406-bd96-4298-9b89-9e5ae3de6070"
        },
        {
            "id": "9f96f9be-d4e7-4e23-8a02-140f4b9394a2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8f539406-bd96-4298-9b89-9e5ae3de6070"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "7f53f958-f7cb-4149-9bfc-8f99f3e40553",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "201c01b6-a8c6-4c2f-be4d-f1ca3c960db5",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "infinito",
            "varType": 3
        }
    ],
    "solid": false,
    "spriteId": "847b85cd-a79c-4f7a-87b3-3ac7a409dae0",
    "visible": true
}