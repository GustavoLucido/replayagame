//----------------------- Morte --------------------------

if (place_meeting(instAvistado.x, instAvistado.y, instInimigo))
{
	instAvistado.x = instInimigo.x;
	instAvistado.y = instInimigo.y - 30;
}

else 
{
	instance_destroy(instAvistado)
	instance_destroy()
}


//----------------------------------------------- Line of Sight ------------------------------------------------


//--------------------- PLayer em pé ----------------------//

if (instance_exists(obj_Player))
{
	if (place_meeting(x,y, instInimigo))
	{
		if (place_meeting(x, y, obj_Player) && obj_Player.sprite_index == spr_Player)
			inimigoAvistado = true
	
		if (inimigoAvistado == true || instInimigo.perseguindo == true)
		{
			if (collision_line(instInimigo.x, instInimigo.y - 60, obj_Player.x, obj_Player.y , obj_Ground, false, false) || (collision_line(instInimigo.x, instInimigo.y - 60, obj_Player.x, obj_Player.y, obj_Caixa, false, false)))
			{
				if (collision_line(instInimigo.x, instInimigo.y - 60, obj_Player.x, obj_Player.y - 50, obj_Ground, false, false) || (collision_line(instInimigo.x, instInimigo.y - 60, obj_Player.x, obj_Player.y - 50, obj_Caixa, false, false)))
				{
					inimigoAvistado = false;
					instInimigo.avistou = false
				}
		
				else
				{


					instInimigo.perseguindo = true;
					instInimigo.patrulhando = false;
					instInimigo.procurando = false;
			
					alarm[0] = room_speed * 4
					
					if (instInimigo.perseguindo == true)
						alarm[1] = 5
			
					instAvistado.image_index = 0
				}
			}
	
			else
			{
					instInimigo.perseguindo = true;
					instInimigo.patrulhando = false;
					instInimigo.procurando = false;
					
					alarm[0] = room_speed * 4
					
					if (instInimigo.perseguindo == true)
						alarm[1] = 5
			
					instAvistado.image_index = 0			
			}
		}
		//--------------------- PLayer em agachado ----------------------//	
	
		if (place_meeting(x, y, obj_Player) && obj_Player.sprite_index == spr_PlayerAgachado)
			inimigoAvistado = true;
	
		if (inimigoAvistado == true || instInimigo.perseguindo == true)
		{
			if (collision_line(instInimigo.x, instInimigo.y - 60, obj_Player.x, obj_Player.y + 40 , obj_Ground, false, false) || (collision_line(instInimigo.x, instInimigo.y - 60, obj_Player.x, obj_Player.y + 40, obj_Caixa, false, false)))
			{
				if (collision_line(instInimigo.x, instInimigo.y - 60, obj_Player.x, obj_Player.y + 10, obj_Ground, false, false) || (collision_line(instInimigo.x, instInimigo.y - 60, obj_Player.x, obj_Player.y + 10, obj_Caixa, false, false)))
				{
					inimigoAvistado = false;
					instInimigo.avistou = false
				}
				
				else
				{
					instInimigo.avistou = true;
					instInimigo.perseguindo = true;
					instInimigo.patrulhando = false;
					instInimigo.procurando = false;
					
					alarm[0] = room_speed * 4
					
					if (instInimigo.perseguindo == true)
						alarm[1] = 5
			
					instAvistado.confuso = false
				}
			}
	
			else
			{		
					instInimigo.avistou = true;
					instInimigo.perseguindo = true;
					instInimigo.patrulhando = false;
					instInimigo.procurando = false;
					
					alarm[0] = room_speed * 4
					
					if (instInimigo.perseguindo == true)
						alarm[1] = 5					
			
					instAvistado.confuso = false
			}
		}
	}
}

//----------------------------- Fazer o LoS e o HUD de exclamção seguirem o bot ---------------------------------//
	
if (place_meeting(x,y, instInimigo))
{
	
	x = instInimigo.x
	y = instInimigo.y
	
	

	image_xscale = instInimigo.image_xscale;		
}

