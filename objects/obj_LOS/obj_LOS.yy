{
    "id": "d17f4e96-c5fd-42ca-9e54-67551af0388e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_LOS",
    "eventList": [
        {
            "id": "386637d7-2b17-4e19-8f9d-e581400622fb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d17f4e96-c5fd-42ca-9e54-67551af0388e"
        },
        {
            "id": "b9cb1729-a5e0-498b-a935-87f24c991c75",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d17f4e96-c5fd-42ca-9e54-67551af0388e"
        },
        {
            "id": "b6801022-f8ee-4edb-9d25-5c2cb56c8124",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "d17f4e96-c5fd-42ca-9e54-67551af0388e"
        },
        {
            "id": "d9d1123e-3ea5-497b-a306-d84031ef4593",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "d17f4e96-c5fd-42ca-9e54-67551af0388e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "c69da7c8-d318-4e23-a67f-39b129a5faf9",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "los_index",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "4cf4fe17-65ba-4c3c-b2eb-cf59705ea23f",
    "visible": false
}