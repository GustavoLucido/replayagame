{
    "id": "d906d783-fcae-4614-88f5-3d5f8bbdd435",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_DeathAnim",
    "eventList": [
        {
            "id": "a109a826-4798-480a-bb84-56a194e5feb7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "d906d783-fcae-4614-88f5-3d5f8bbdd435"
        },
        {
            "id": "f8e73418-d9c1-4a14-a555-5b9c52c2d99e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d906d783-fcae-4614-88f5-3d5f8bbdd435"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6f5a7fb6-1a95-4d2c-a975-a79e4dcb66e8",
    "visible": true
}