{
    "id": "d612c70e-7daf-4b44-abc5-d6dbcdc6b34e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Granada",
    "eventList": [
        {
            "id": "e9e4e2d4-2e55-499e-88ae-ed4a3ca984f7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d612c70e-7daf-4b44-abc5-d6dbcdc6b34e"
        },
        {
            "id": "6e247923-d6fb-40f8-ad7d-a7a5b24edea3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d612c70e-7daf-4b44-abc5-d6dbcdc6b34e"
        },
        {
            "id": "58ef439f-2f19-4b56-9b6d-daf2ecb16c93",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "7f53f958-f7cb-4149-9bfc-8f99f3e40553",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d612c70e-7daf-4b44-abc5-d6dbcdc6b34e"
        },
        {
            "id": "a353ebdd-70a9-445a-9107-e1f7495af568",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "64c98adf-55d8-4d4f-beeb-9ebaf80bc06f",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d612c70e-7daf-4b44-abc5-d6dbcdc6b34e"
        },
        {
            "id": "43a0bc71-ff19-4f53-acd6-31430232db63",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "19e303c1-ae6d-4099-a53e-37df3b31cd16",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d612c70e-7daf-4b44-abc5-d6dbcdc6b34e"
        },
        {
            "id": "d2336087-0c84-4e16-b28b-e0fb905fa916",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "d612c70e-7daf-4b44-abc5-d6dbcdc6b34e"
        },
        {
            "id": "3a304a1b-56aa-4f46-92f5-1ca46fab07c7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "d612c70e-7daf-4b44-abc5-d6dbcdc6b34e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "47b57e53-a8f9-4234-beda-f0cca9f13000",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "8cad2f9f-8841-469c-87e4-36aa62f216fc",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 48,
            "y": 0
        },
        {
            "id": "bcac6567-1c5a-4974-ab6b-752e6d9c1f84",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 48,
            "y": 48
        },
        {
            "id": "9420e9d3-5cf2-4f76-b730-c0ac707b23f0",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 48
        }
    ],
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "a816a2f3-5097-48ba-9f9d-7fefb344aa8d",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "granadaIcon",
            "varType": 3
        }
    ],
    "solid": true,
    "spriteId": "c1dafd24-1333-4afb-a529-d45528368fe2",
    "visible": true
}