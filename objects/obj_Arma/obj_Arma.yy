{
    "id": "d4b72905-20c8-4627-b847-372c49b54b9c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Arma",
    "eventList": [
        {
            "id": "25096fb6-2120-4a72-8dc4-1eaf74a58dff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d4b72905-20c8-4627-b847-372c49b54b9c"
        },
        {
            "id": "916105b6-bd06-40cf-89f3-078759bfd815",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d4b72905-20c8-4627-b847-372c49b54b9c"
        },
        {
            "id": "a718a19f-08ba-4300-8594-9ad694e3e3f2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "aaf51c09-089f-4547-8857-c68db1e7acd4",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d4b72905-20c8-4627-b847-372c49b54b9c"
        },
        {
            "id": "fb71c508-3e26-4b81-b858-758b3663dd3d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "d4b72905-20c8-4627-b847-372c49b54b9c"
        },
        {
            "id": "1297a395-6f68-43fe-96b7-5d5af00c5cc7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "d4b72905-20c8-4627-b847-372c49b54b9c"
        },
        {
            "id": "92a30b1b-1492-472a-97e9-ecc7a88a78ad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "d4b72905-20c8-4627-b847-372c49b54b9c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "418ece65-4433-4495-8bfb-d77096529a5b",
    "visible": true
}