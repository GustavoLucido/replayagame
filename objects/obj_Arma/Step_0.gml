//----------------------------------- Mostrar a arma equipada --------------------------------------//

if (global.armaAtual == 1)
	sprite_index = spr_Revolver;
	
if (global.armaAtual == 2)
	sprite_index = spr_Molotov;

if (global.armaAtual == 3)
	sprite_index = spr_Shotgun;
	
if (global.armaAtual == 4)
	sprite_index = spr_Metralha;
	
if (global.armaAtual == 5)
	sprite_index = spr_Granada;
	
if (global.armaAtual == 6)
	sprite_index = spr_Bazooka;
	
if (global.armaAtual == 7)
	sprite_index = spr_Esorregarma;

//------------------------------------- Angulação e Offset da arma equipada ---------------------------------//

if (instance_exists(obj_Player))
{
	if (obj_Player.revolverOn == true)
	{
	
		image_angle = point_direction(x, y, mouse_x, mouse_y)

		if (obj_Player.x <= mouse_x)
		{
			direita = true;
			image_yscale = 1;
		}

		if (obj_Player.x > mouse_x)
		{
			direita = false;
			image_yscale = -1
		}
	
	

		//----------------- Armas que ficam a frente do player -----------------------//
		if (direita == true && global.armaAtual != 2 || direita == true && global.armaAtual != 5 )
			x = obj_Player.x - 16;


		if (direita == false && global.armaAtual != 2 || direita == false && global.armaAtual != 5 )
			x = obj_Player.x + 16;


		//----------------- Armas que ficam atras do player -----------------------//

		if (direita == true && global.armaAtual == 2 || direita == true && global.armaAtual == 5)	
			x = obj_Player.x - 20;	

		if (direita == false && global.armaAtual == 2 || direita == false && global.armaAtual == 5)
			x = obj_Player.x + 20;


	//------------------------------------------- Animaçoes -----------------------------------------------

		if (obj_Player.agachado == true)
			y = obj_Player.y + 40;
		
		else
			y = obj_Player.y + 7;
	}

	else
	{
		sprite_index = spr_IconRevolver
	
		if (descendo == true && float < 0.7)
		{
			float += 0.03
	
			if (float >= 0.7)
				resbalo = true
		}
		if (descendo == true && resbalo == true)
			float -= 0.05



		if (descendo == false && float > -0.7)
		{
			float -= 0.03
	
			if (float <= -0.7)
				resbalo = true
		}
		if (descendo == false && resbalo == true)
			float += 0.05

		y = y + float
		glow.y = y
	}
}
