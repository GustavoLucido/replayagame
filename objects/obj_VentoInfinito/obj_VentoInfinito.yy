{
    "id": "50b04109-ad13-49bc-b142-44146034e60b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_VentoInfinito",
    "eventList": [
        {
            "id": "a68e49f0-0884-4e89-bf9a-57b9aca0dab9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "2c2f3525-8fbb-4454-8023-78acb9e28432",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "50b04109-ad13-49bc-b142-44146034e60b"
        },
        {
            "id": "e7ed33a3-0e4c-40de-8fdd-5065d2f234bc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "50b04109-ad13-49bc-b142-44146034e60b"
        },
        {
            "id": "e903edd7-6486-47df-a7ab-a751492214e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "50b04109-ad13-49bc-b142-44146034e60b"
        },
        {
            "id": "efaa6894-c2d1-43ca-932f-485169971a09",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "50b04109-ad13-49bc-b142-44146034e60b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "41299cd3-a3e3-4bfd-a898-5cce2d5bb8ad",
    "visible": true
}