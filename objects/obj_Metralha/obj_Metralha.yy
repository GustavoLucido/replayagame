{
    "id": "4834878f-e1d6-4d0a-b875-a19ccabd9dec",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Metralha",
    "eventList": [
        {
            "id": "6b3b00fd-1ecc-4056-8176-643a01378590",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "aaf51c09-089f-4547-8857-c68db1e7acd4",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "4834878f-e1d6-4d0a-b875-a19ccabd9dec"
        },
        {
            "id": "416ffcc8-8f86-446e-9422-3381a0e0936a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4834878f-e1d6-4d0a-b875-a19ccabd9dec"
        },
        {
            "id": "268b36b9-6584-415b-8688-ae2e65f5e025",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4834878f-e1d6-4d0a-b875-a19ccabd9dec"
        },
        {
            "id": "5042f0a3-44e6-436d-af5b-600147a10103",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "4834878f-e1d6-4d0a-b875-a19ccabd9dec"
        },
        {
            "id": "fe32d9a5-a655-4193-82fe-02a7b3f6d896",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "4834878f-e1d6-4d0a-b875-a19ccabd9dec"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b5f37710-e2e0-49a0-bedb-65cff8cde15a",
    "visible": true
}