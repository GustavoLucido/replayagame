{
    "id": "6600bc6f-4bb2-4766-a551-cad4e46e2526",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Missil1",
    "eventList": [
        {
            "id": "46086a18-271f-4c58-b042-e1484349b441",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6600bc6f-4bb2-4766-a551-cad4e46e2526"
        },
        {
            "id": "d6bf682b-51d4-4be5-a594-ac1ddd54c7e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6600bc6f-4bb2-4766-a551-cad4e46e2526"
        },
        {
            "id": "c7575c97-d1f1-4183-82e2-cb0a9713fd0b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "7f53f958-f7cb-4149-9bfc-8f99f3e40553",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "6600bc6f-4bb2-4766-a551-cad4e46e2526"
        },
        {
            "id": "39a8c53f-3d9e-4c7a-873e-ea3ea8031a59",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "64c98adf-55d8-4d4f-beeb-9ebaf80bc06f",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "6600bc6f-4bb2-4766-a551-cad4e46e2526"
        },
        {
            "id": "cc1e0d12-2690-4a60-82bd-77fc9cc35c3c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "df5b699d-dfa5-4b37-9e54-666ed9a27c98",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "6600bc6f-4bb2-4766-a551-cad4e46e2526"
        },
        {
            "id": "020828cd-75c5-418b-bc60-6b859d9c7c92",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "19e303c1-ae6d-4099-a53e-37df3b31cd16",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "6600bc6f-4bb2-4766-a551-cad4e46e2526"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "256c2b42-f592-4379-8c68-f92b796b28f6",
    "visible": true
}