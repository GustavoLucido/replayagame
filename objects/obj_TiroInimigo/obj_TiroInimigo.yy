{
    "id": "d28a3dac-4d44-4680-9d87-01291639bfc8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_TiroInimigo",
    "eventList": [
        {
            "id": "05bdb740-142e-4fff-adc3-2be98b0f42d1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d28a3dac-4d44-4680-9d87-01291639bfc8"
        },
        {
            "id": "06b7d1f4-c6f2-4b61-bf03-3a27d8fa22cd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d28a3dac-4d44-4680-9d87-01291639bfc8"
        },
        {
            "id": "65af30d8-e554-436f-baf8-01b19de5d847",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "d28a3dac-4d44-4680-9d87-01291639bfc8"
        },
        {
            "id": "b9669405-9892-4935-84fa-d8f38cd835cc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "7f53f958-f7cb-4149-9bfc-8f99f3e40553",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d28a3dac-4d44-4680-9d87-01291639bfc8"
        },
        {
            "id": "449db584-fa63-457a-8cec-46e7a7e26ddd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "64c98adf-55d8-4d4f-beeb-9ebaf80bc06f",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d28a3dac-4d44-4680-9d87-01291639bfc8"
        },
        {
            "id": "a8568f76-26fa-4847-8a58-a55de5d5dc21",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "df5b699d-dfa5-4b37-9e54-666ed9a27c98",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d28a3dac-4d44-4680-9d87-01291639bfc8"
        },
        {
            "id": "cd82ec5d-eb0d-4995-b04c-56d09e40d2ad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "19e303c1-ae6d-4099-a53e-37df3b31cd16",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d28a3dac-4d44-4680-9d87-01291639bfc8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "498e43ce-de8b-45bb-a515-46aff4a5a01a",
    "visible": true
}