//------------------ Mostrar o tiro apenas após não estar mais em contato com a arma ------------------------//

sprite_index = spr_Blank;


//--------------------------------- Checar a direção do tiro ---------------------------------//
if (instance_exists(obj_Player))
	direction = point_direction (x, y, obj_Player.x + random_range(-10, 10), obj_Player.y + random_range(-10, 10)) ;

direction = direction + random_range(-3, 3);
speed = 10;
image_angle = direction;