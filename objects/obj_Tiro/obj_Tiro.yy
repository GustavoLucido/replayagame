{
    "id": "22259775-2a37-4b05-bbe6-43d1e3baf2b7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Tiro",
    "eventList": [
        {
            "id": "5d4ac098-6b08-48da-b96a-53b988928428",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "22259775-2a37-4b05-bbe6-43d1e3baf2b7"
        },
        {
            "id": "dd777e3e-4ba9-4757-bfee-c05ca83200c0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "22259775-2a37-4b05-bbe6-43d1e3baf2b7"
        },
        {
            "id": "3eb0776b-240b-43d4-88b2-49d62922be11",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "22259775-2a37-4b05-bbe6-43d1e3baf2b7"
        },
        {
            "id": "c39da11a-8009-4774-945c-9710b4aaf569",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "7f53f958-f7cb-4149-9bfc-8f99f3e40553",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "22259775-2a37-4b05-bbe6-43d1e3baf2b7"
        },
        {
            "id": "e43e2c6d-8819-44f4-b014-81198079a9bb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "64c98adf-55d8-4d4f-beeb-9ebaf80bc06f",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "22259775-2a37-4b05-bbe6-43d1e3baf2b7"
        },
        {
            "id": "da8d146d-133c-4e4c-9178-adf361ff191a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "df5b699d-dfa5-4b37-9e54-666ed9a27c98",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "22259775-2a37-4b05-bbe6-43d1e3baf2b7"
        },
        {
            "id": "38c53dae-0c4f-4a17-9adb-ed89a55fe8dc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "19e303c1-ae6d-4099-a53e-37df3b31cd16",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "22259775-2a37-4b05-bbe6-43d1e3baf2b7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8cda43af-9993-4f24-a3a5-c288d2c7c4f4",
    "visible": true
}