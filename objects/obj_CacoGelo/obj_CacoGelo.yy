{
    "id": "80d414ff-6c88-4489-aa8c-5bc92c0ccd6c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_CacoGelo",
    "eventList": [
        {
            "id": "5ac059a2-2e46-4a9b-a81f-2cba2368b568",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "80d414ff-6c88-4489-aa8c-5bc92c0ccd6c"
        },
        {
            "id": "f431f4c8-5a94-41a7-a56c-f97373439a4e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "80d414ff-6c88-4489-aa8c-5bc92c0ccd6c"
        },
        {
            "id": "9cba376a-d25a-4b23-99bf-92bae4002d61",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "7f53f958-f7cb-4149-9bfc-8f99f3e40553",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "80d414ff-6c88-4489-aa8c-5bc92c0ccd6c"
        },
        {
            "id": "208ac54d-85f6-4840-8a8c-64bd0b41c1d5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "80d414ff-6c88-4489-aa8c-5bc92c0ccd6c"
        },
        {
            "id": "fd28d4e3-43bb-46e7-899b-ad758a2f332c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "80d414ff-6c88-4489-aa8c-5bc92c0ccd6c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "ca92afe7-7eda-477b-a09b-44731de73f04",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "8a622cd5-249c-41bc-aa81-8df26875de95",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 0
        },
        {
            "id": "ff1dd951-e162-4a7c-b4c9-3d82a6cc9274",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 32
        },
        {
            "id": "ff5112fc-a272-4663-be54-072d6e8d0ca3",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 32
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f779505d-840b-4be7-bffd-bca74a7a9004",
    "visible": true
}