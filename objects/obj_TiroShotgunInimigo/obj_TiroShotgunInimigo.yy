{
    "id": "038f1ce2-2b2e-4dd4-b0dd-5379c29e0a0a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_TiroShotgunInimigo",
    "eventList": [
        {
            "id": "f1f0f83c-6275-489b-93fd-472e74e993aa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "038f1ce2-2b2e-4dd4-b0dd-5379c29e0a0a"
        },
        {
            "id": "50cb4bb2-aee8-4146-aada-c8a29ba4fa8c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "038f1ce2-2b2e-4dd4-b0dd-5379c29e0a0a"
        },
        {
            "id": "fd0ee6e3-d424-4917-8a48-4f2d5a4991b2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "7f53f958-f7cb-4149-9bfc-8f99f3e40553",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "038f1ce2-2b2e-4dd4-b0dd-5379c29e0a0a"
        },
        {
            "id": "f48c3a52-0ed1-4d23-b3d6-148e935418ba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "df5b699d-dfa5-4b37-9e54-666ed9a27c98",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "038f1ce2-2b2e-4dd4-b0dd-5379c29e0a0a"
        },
        {
            "id": "1daca569-d2cc-4138-a29f-ae5c73b3551e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "64c98adf-55d8-4d4f-beeb-9ebaf80bc06f",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "038f1ce2-2b2e-4dd4-b0dd-5379c29e0a0a"
        },
        {
            "id": "4415e1e6-2536-4527-8e14-aa19be5c2152",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "19e303c1-ae6d-4099-a53e-37df3b31cd16",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "038f1ce2-2b2e-4dd4-b0dd-5379c29e0a0a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1387bdcc-32ab-4975-a641-0e2db095e4e1",
    "visible": true
}