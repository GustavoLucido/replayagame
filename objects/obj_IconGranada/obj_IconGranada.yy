{
    "id": "5fb47802-ddcc-4b0f-a7fb-da314ae6ffed",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_IconGranada",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "415ebf87-e332-431e-9606-2625079181b1",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "045c75d9-63ac-4a25-b7b2-dc8e0fad6aca",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 48,
            "y": 0
        },
        {
            "id": "682796a1-139a-429f-91b3-5acace3fcc5b",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 48,
            "y": 48
        },
        {
            "id": "fda3a78d-a141-42d2-9ca1-80dc8bdb15e2",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 48
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "c1dafd24-1333-4afb-a529-d45528368fe2",
    "visible": true
}