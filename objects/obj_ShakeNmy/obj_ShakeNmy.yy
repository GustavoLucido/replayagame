{
    "id": "7cf0a2a6-d742-4306-9e69-8e7a3b991acb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ShakeNmy",
    "eventList": [
        {
            "id": "ccd0cfa3-d903-4974-b860-123ab2c834f5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7cf0a2a6-d742-4306-9e69-8e7a3b991acb"
        },
        {
            "id": "578803b7-356d-4208-850e-86af5d385d5d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7cf0a2a6-d742-4306-9e69-8e7a3b991acb"
        },
        {
            "id": "6c6bf44e-252f-46ba-918b-0d92099accaf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "7cf0a2a6-d742-4306-9e69-8e7a3b991acb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}