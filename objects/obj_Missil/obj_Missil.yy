{
    "id": "c673d17d-9a66-45c3-a172-5bd3fbda1119",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Missil",
    "eventList": [
        {
            "id": "65511ebd-c413-46ed-afa4-29fe22a7d405",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c673d17d-9a66-45c3-a172-5bd3fbda1119"
        },
        {
            "id": "1ae3f2b2-ed08-4e91-b746-bb906c4ea6a9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c673d17d-9a66-45c3-a172-5bd3fbda1119"
        },
        {
            "id": "dea543a4-8f8a-4c72-990d-b70a7bdfa0d8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "7f53f958-f7cb-4149-9bfc-8f99f3e40553",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c673d17d-9a66-45c3-a172-5bd3fbda1119"
        },
        {
            "id": "5b7af543-33cc-4c3d-977e-6883a0f7157e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "64c98adf-55d8-4d4f-beeb-9ebaf80bc06f",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c673d17d-9a66-45c3-a172-5bd3fbda1119"
        },
        {
            "id": "206e052b-a099-4c89-a2ef-905e7769c233",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "df5b699d-dfa5-4b37-9e54-666ed9a27c98",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c673d17d-9a66-45c3-a172-5bd3fbda1119"
        },
        {
            "id": "5357d78d-064c-47ad-9e7d-53aa240137e0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "19e303c1-ae6d-4099-a53e-37df3b31cd16",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c673d17d-9a66-45c3-a172-5bd3fbda1119"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "256c2b42-f592-4379-8c68-f92b796b28f6",
    "visible": true
}