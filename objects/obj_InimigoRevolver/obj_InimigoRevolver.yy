{
    "id": "ccb48375-97a9-4814-ae0c-8a6a197f6106",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_InimigoRevolver",
    "eventList": [
        {
            "id": "3eed27a9-9ca1-40b1-989d-e5ac531df956",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ccb48375-97a9-4814-ae0c-8a6a197f6106"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "19e303c1-ae6d-4099-a53e-37df3b31cd16",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "dc435083-2077-41e2-aca0-278ab49f4e7d",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "e14c3941-46d8-4398-83e3-38231fcad3fd",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 78,
            "y": 0
        },
        {
            "id": "93914d0a-5fc0-4721-aa8f-bc049136aeb3",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 78,
            "y": 173
        },
        {
            "id": "f54581bc-6dda-4f4a-b7b0-bae07826787e",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 173
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c9e25ec3-6bd3-427a-92e4-2707350c1697",
    "visible": true
}