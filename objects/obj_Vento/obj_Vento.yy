{
    "id": "9d5f4f50-4a32-474f-9629-05fd64609303",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Vento",
    "eventList": [
        {
            "id": "dc7543e3-ae13-42cb-b288-2133ec1e839d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "2c2f3525-8fbb-4454-8023-78acb9e28432",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "9d5f4f50-4a32-474f-9629-05fd64609303"
        },
        {
            "id": "d2e15da0-eb96-4d26-a165-a82bd92f4430",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9d5f4f50-4a32-474f-9629-05fd64609303"
        },
        {
            "id": "7142ac3f-b8a9-4074-813c-d3ce700cee49",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "9d5f4f50-4a32-474f-9629-05fd64609303"
        },
        {
            "id": "cc86e0f4-5cda-493c-88d1-89b31439d05c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9d5f4f50-4a32-474f-9629-05fd64609303"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ba24a1e7-cca8-45fd-ad49-411265a394c1",
    "visible": true
}