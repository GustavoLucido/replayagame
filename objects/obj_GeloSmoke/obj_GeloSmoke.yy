{
    "id": "9ed9afa5-fafc-4be2-a76a-374d15cf7714",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_GeloSmoke",
    "eventList": [
        {
            "id": "c0718276-bb0b-43ca-89a6-3444019e849e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "9ed9afa5-fafc-4be2-a76a-374d15cf7714"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "18f7c455-b710-4e54-9423-4bfb8974d3cc",
    "visible": true
}