{
    "id": "2c2f3525-8fbb-4454-8023-78acb9e28432",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Planador",
    "eventList": [
        {
            "id": "0b984f68-3531-4901-b092-3722dad1f4b9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2c2f3525-8fbb-4454-8023-78acb9e28432"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "30497175-0867-4552-8fd5-ee5144a13d80",
    "visible": true
}