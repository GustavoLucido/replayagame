{
    "id": "85230d15-e61b-4454-8688-d8bd15ff3d50",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_DeathSquare",
    "eventList": [
        {
            "id": "8f91f063-8ef1-406d-a973-eacf1d1eccf4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "aaf51c09-089f-4547-8857-c68db1e7acd4",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "85230d15-e61b-4454-8688-d8bd15ff3d50"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c00e467d-ceac-4803-80e4-98f5f5be212d",
    "visible": true
}