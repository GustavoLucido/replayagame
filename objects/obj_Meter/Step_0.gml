x = obj_Player.x;
y = obj_Player.y - 80;

if (mouse_check_button_released(mb_left) || image_index == 25)
{
	obj_Player.meter = false;
	obj_Player.forcaArremesso = image_index + 3;
	
	if (global.armaAtual == 2)
	{
		instance_create_layer (obj_Arma.x, obj_Arma.y - 10, "Bullet_layer", obj_Molotov);
		obj_Player.cooldownTiro = 3;
	}
	
	if (global.armaAtual == 5)
	{
		instance_create_layer (obj_Arma.x, obj_Arma.y - 10, "Bullet_layer", obj_Granada);
		obj_Player.cooldownTiro = 3;
	}
	
	instance_destroy();
}