obj_Player.meter = false;
obj_Player.forcaArremesso = 28;

if (global.armaAtual == 2)
{
	instance_create_layer (obj_Arma.x, obj_Arma.y - 10, "Bullet_layer", obj_Molotov);
	obj_Player.cooldownTiro = 3;
}

if (global.armaAtual == 5)
{
	instance_create_layer (obj_Arma.x, obj_Arma.y - 10, "Bullet_layer", obj_Granada);
	obj_Player.cooldownTiro = 3;
}

instance_destroy();