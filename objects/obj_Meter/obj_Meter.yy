{
    "id": "d3b5dbf8-cb6f-4af7-a968-8ddb8505e992",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Meter",
    "eventList": [
        {
            "id": "bfb9119e-d37c-4c2e-ac15-bacce53d1924",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d3b5dbf8-cb6f-4af7-a968-8ddb8505e992"
        },
        {
            "id": "bcacb63e-7725-42c6-853c-5ed167e16349",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "d3b5dbf8-cb6f-4af7-a968-8ddb8505e992"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fdb8bb65-ed78-4fd6-8cb6-d0c5bb435994",
    "visible": true
}