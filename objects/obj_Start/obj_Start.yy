{
    "id": "c6373aa5-1beb-42e4-a662-17e6b86dc4df",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Start",
    "eventList": [
        {
            "id": "ac0500b4-8303-4568-afa5-f273714a71e0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "aaf51c09-089f-4547-8857-c68db1e7acd4",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c6373aa5-1beb-42e4-a662-17e6b86dc4df"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c4be161d-0a82-4966-bae0-26c9174d71c5",
    "visible": true
}