{
    "id": "3cd0d828-d704-43f3-a25a-a1972cc04230",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Shotgun",
    "eventList": [
        {
            "id": "64a8ed97-dce0-4778-b648-5b6237a9b2eb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3cd0d828-d704-43f3-a25a-a1972cc04230"
        },
        {
            "id": "27186226-0c95-4243-a75d-aecdf5ba797f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "aaf51c09-089f-4547-8857-c68db1e7acd4",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "3cd0d828-d704-43f3-a25a-a1972cc04230"
        },
        {
            "id": "d05405a9-2c93-4a77-b5b7-30518ed4d29a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3cd0d828-d704-43f3-a25a-a1972cc04230"
        },
        {
            "id": "d29f242e-cec0-44b1-9235-5d6b9df5cc75",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "3cd0d828-d704-43f3-a25a-a1972cc04230"
        },
        {
            "id": "c8b45e85-1ac6-4226-ac7c-a827c6fa4fef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "3cd0d828-d704-43f3-a25a-a1972cc04230"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "041c5cce-8f99-4f22-b253-fadf1a12107e",
    "visible": true
}