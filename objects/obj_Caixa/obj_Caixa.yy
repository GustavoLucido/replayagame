{
    "id": "df5b699d-dfa5-4b37-9e54-666ed9a27c98",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Caixa",
    "eventList": [
        {
            "id": "98af1c81-9567-4a9d-a9a6-1b3f5b2fe31c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "df5b699d-dfa5-4b37-9e54-666ed9a27c98"
        },
        {
            "id": "970ffa93-1f42-4d16-b826-751c01db8ec8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "22259775-2a37-4b05-bbe6-43d1e3baf2b7",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "df5b699d-dfa5-4b37-9e54-666ed9a27c98"
        },
        {
            "id": "e6dc05a3-fb6f-4fa0-b4cb-ad8aa89b9adb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "230e0182-c8cb-42b1-a1c0-ba929a38d936",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "df5b699d-dfa5-4b37-9e54-666ed9a27c98"
        },
        {
            "id": "fddcb537-6816-4b1a-818c-e4f9c4fcf562",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "c026e67d-61bb-40f6-854b-b7522f12c9a5",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "df5b699d-dfa5-4b37-9e54-666ed9a27c98"
        },
        {
            "id": "58c03ae9-2b24-4fe4-840f-3440a215a761",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "df5b699d-dfa5-4b37-9e54-666ed9a27c98"
        },
        {
            "id": "1b1ace93-6529-4499-9365-144658182ec2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "5941b725-7cf5-4512-a9c5-85a8e65b47df",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "df5b699d-dfa5-4b37-9e54-666ed9a27c98"
        },
        {
            "id": "631740ef-0250-4a30-8c43-91d128a6dffe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d28a3dac-4d44-4680-9d87-01291639bfc8",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "df5b699d-dfa5-4b37-9e54-666ed9a27c98"
        },
        {
            "id": "cf0e4853-5a6f-4db2-a475-5054af17e397",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "df5b699d-dfa5-4b37-9e54-666ed9a27c98"
        },
        {
            "id": "c907f276-bcdd-44a7-a3a0-6ade813d0250",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "df5b699d-dfa5-4b37-9e54-666ed9a27c98"
        },
        {
            "id": "535c284f-4280-4af1-a1e2-9b0c89464551",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d8126fe5-7775-4b58-8dbf-6827567d280f",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "df5b699d-dfa5-4b37-9e54-666ed9a27c98"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "64c98adf-55d8-4d4f-beeb-9ebaf80bc06f",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d8c994b3-2eb0-4e32-b192-cb64bbb3036f",
    "visible": true
}