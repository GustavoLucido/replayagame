{
    "id": "cdff4d3f-d4e9-4f93-93ec-0a0034808cb6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Molotov",
    "eventList": [
        {
            "id": "5ef23717-bb81-40a4-88d9-199cb6ff0d3c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cdff4d3f-d4e9-4f93-93ec-0a0034808cb6"
        },
        {
            "id": "bda71a72-604d-455e-9ce5-77453e1832fe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "cdff4d3f-d4e9-4f93-93ec-0a0034808cb6"
        },
        {
            "id": "171e07d0-9cb0-4efc-b010-080835db4971",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "7f53f958-f7cb-4149-9bfc-8f99f3e40553",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "cdff4d3f-d4e9-4f93-93ec-0a0034808cb6"
        },
        {
            "id": "852e6e93-622e-4ddc-a9a8-6411ccc457ca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "64c98adf-55d8-4d4f-beeb-9ebaf80bc06f",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "cdff4d3f-d4e9-4f93-93ec-0a0034808cb6"
        },
        {
            "id": "dac1476d-cede-4701-8856-fac3721da3c3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "19e303c1-ae6d-4099-a53e-37df3b31cd16",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "cdff4d3f-d4e9-4f93-93ec-0a0034808cb6"
        },
        {
            "id": "e4405821-899a-4e53-a9d0-dbc38621b199",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "cdff4d3f-d4e9-4f93-93ec-0a0034808cb6"
        },
        {
            "id": "4349cd05-7e79-4ec5-9173-f63669eccbfb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "cdff4d3f-d4e9-4f93-93ec-0a0034808cb6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "3048feda-3f13-4ff7-8747-bf1d04add48b",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "9252a518-9181-4f1d-9ee0-2a5d59b88484",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 48,
            "y": 0
        },
        {
            "id": "ef06e617-861f-4745-af1f-e89d74fde650",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 48,
            "y": 48
        },
        {
            "id": "57c91e9e-2726-41a3-b804-82a02d7cf3f0",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 48
        }
    ],
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "4b0931c3-29be-47da-931c-a25ac2bf9fe7",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "molotovIcon",
            "varType": 3
        }
    ],
    "solid": true,
    "spriteId": "50a85db4-19c3-4d27-a287-bfd12669aac2",
    "visible": true
}