{
    "id": "cc05954d-9e96-4a77-84dc-ac977011ad9f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Shake",
    "eventList": [
        {
            "id": "9a8b2b18-d6f6-4e73-a125-d50deb304304",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cc05954d-9e96-4a77-84dc-ac977011ad9f"
        },
        {
            "id": "64e181ec-fd2f-4ad8-95f5-e6cbc7f440b9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "cc05954d-9e96-4a77-84dc-ac977011ad9f"
        },
        {
            "id": "a3f57070-2262-4a8f-b4d1-43dea4cbbcf0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "cc05954d-9e96-4a77-84dc-ac977011ad9f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}