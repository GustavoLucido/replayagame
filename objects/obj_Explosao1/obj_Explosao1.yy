{
    "id": "99f9a807-cdfa-42c0-83bd-b96560444817",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Explosao1",
    "eventList": [
        {
            "id": "a6b12a0e-b8f6-4940-84a9-242981fbb3b1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "99f9a807-cdfa-42c0-83bd-b96560444817"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a9ed42ef-4976-4a57-836d-133269ad8d38",
    "visible": true
}