{
    "id": "295e0bf6-2ee7-4be3-bdeb-053ed9a2d0cf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_IconMolotov",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "bd1dd9ab-19bb-400a-a367-d7ca5691a463",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "2149bc08-7043-4de2-93c9-ead7759285e1",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 48,
            "y": 0
        },
        {
            "id": "69ddac09-de46-442c-be9a-c1d355f2fb40",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 48,
            "y": 48
        },
        {
            "id": "f53e2c65-c76a-4865-b05f-81a8bee618af",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 48
        }
    ],
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "9a878463-6629-4b00-b2cf-dbceefc8a964",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "molotovIcon",
            "varType": 3
        }
    ],
    "solid": true,
    "spriteId": "50a85db4-19c3-4d27-a287-bfd12669aac2",
    "visible": true
}