carregado = false
carregandoAssist = false;

//----------------------------

if (revolverBox == true)
	icon = instance_create_layer(x + 10, y + 55, "Instances", obj_IconRevolver)
	
if (shotgunBox == true)
{
	icon = instance_create_layer(x + 35, y + 55, "Instances", obj_IconShotgun)
	icon.image_xscale = 0.7
	icon.image_yscale = 0.7
	icon.image_angle = 20
}
	
if (metralhaBox == true)
{
	icon = instance_create_layer(x + 40, y + 60, "Instances", obj_IconMetralha)
	icon.image_xscale = 0.5
	icon.image_yscale = 0.5
	icon.image_angle = 20
}
	
if (molotovBox == true)
{
	icon = instance_create_layer(x + 50, y + 60, "Instances", obj_IconMolotov)
	icon.image_xscale = 1
	icon.image_yscale = 1
}
	
if (granadaBox == true)
{
	icon = instance_create_layer(x + 50, y + 60, "Instances", obj_IconGranada)
	icon.image_xscale = 0.7	
	icon.image_yscale = 0.7
}
	
if (bazookaBox == true)
{
	icon = instance_create_layer(x + 55, y + 75, "Instances", obj_IconBazooka)
	icon.image_xscale = 0.7
	icon.image_yscale = 0.7
	icon.image_angle = 20
}