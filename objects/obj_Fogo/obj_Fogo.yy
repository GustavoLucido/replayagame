{
    "id": "5941b725-7cf5-4512-a9c5-85a8e65b47df",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Fogo",
    "eventList": [
        {
            "id": "5b2618e2-c78c-493c-8916-49d0f3ebc74d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5941b725-7cf5-4512-a9c5-85a8e65b47df"
        },
        {
            "id": "76449369-317d-4a15-ae75-17bf765a366b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "5941b725-7cf5-4512-a9c5-85a8e65b47df"
        },
        {
            "id": "0afc246c-52b0-4616-a922-66ae1eb194ef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5941b725-7cf5-4512-a9c5-85a8e65b47df"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "ca936e36-1279-426f-b08c-66769881bacd",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "infinito",
            "varType": 3
        }
    ],
    "solid": false,
    "spriteId": "7ee4c568-54b3-43b3-b545-69cdf8896db4",
    "visible": true
}